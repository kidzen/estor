<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "LAPORAN_PENYELENGARAAN_SISKEN".
 *
 * @property string $ID
 * @property string $TAHUN
 * @property string $BULAN
 * @property string $NO_KERJA
 * @property string $ARAHAN_KERJA
 * @property string $JK_JG
 * @property integer $BENGKEL_PANEL_ID
 * @property string $NAMA_PANEL
 * @property string $KATEGORI_PANEL
 * @property integer $SELENGGARA_ID
 * @property string $TEMPOH_SELENGGARA
 * @property integer $ALASAN_ID
 * @property string $ALASAN
 * @property string $TARIKH_ARAHAN
 * @property string $NO_SEBUTHARGA
 * @property string $TARIKH_KENDERAAN_TIBA
 * @property string $TARIKH_JANGKA_SIAP
 * @property string $TARIKH_SEBUTHARGA
 * @property string $STATUS_SEBUTHARGA
 * @property string $NO_DO
 * @property string $TARIKH_SIAP
 * @property string $TEMPOH_JAMINAN
 * @property string $TARIKH_DO
 * @property string $TARIKH_CETAK
 * @property string $STATUS
 * @property string $CATATAN_SEBUTHARGA
 * @property string $CATATAN
 * @property string $NAMA_PIC
 * @property string $TARIKH_PERMOHONAN
 * @property integer $ID_KENDERAAN
 * @property string $KATEGORI_KEROSAKAN_ID
 * @property string $KATEGORI_KEROSAKAN
 * @property string $KATEGORI
 * @property string $ISSERVICE
 * @property string $ISPANCIT
 * @property string $ODOMETER_TERKINI
 * @property string $NO_PLAT
 * @property string $MODEL
 * @property string $KOD_JABATAN
 * @property string $NAMA_JABATAN
 * @property string $JUMLAH_KOS
 */
class LaporanPenyelengaraanSisken extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\expression('current_timestamp()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'laporan_penyelengaraan_sisken';
    }

    public static function primaryKey() {
        return ["ID"];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id'], 'required'],
            [['id'], 'number'],
            [['bengkel_panel_id', 'selenggara_id', 'alasan_id', 'id_kenderaan'], 'integer'],
            [['tahun', 'bulan', 'no_kerja', 'arahan_kerja', 'jk_jg', 'nama_panel', 'kategori_panel', 'tempoh_selenggara', 'alasan', 'tarikh_arahan', 'no_sebutharga', 'tarikh_kenderaan_tiba', 'tarikh_jangka_siap', 'tarikh_sebutharga', 'status_sebutharga', 'no_do', 'tarikh_siap', 'tempoh_jaminan', 'tarikh_do', 'tarikh_cetak', 'status', 'catatan_sebutharga', 'catatan', 'nama_pic', 'tarikh_permohonan', 'kategori_kerosakan_id', 'kategori_kerosakan', 'kategori', 'isservice', 'ispancit', 'odometer_terkini', 'no_plat', 'model', 'kod_jabatan', 'nama_jabatan', 'jumlah_kos'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'id'),
            'tahun' => Yii::t('app', 'Tahun'),
            'bulan' => Yii::t('app', 'Bulan'),
            'no_kerja' => Yii::t('app', 'No  Kerja'),
            'arahan_kerja' => Yii::t('app', 'Arahan  Kerja'),
            'jk_jg' => Yii::t('app', 'Jk  Jg'),
            'bengkel_panel_id' => Yii::t('app', 'Bengkel  Panel  id'),
            'nama_panel' => Yii::t('app', 'Nama  Panel'),
            'kategori_panel' => Yii::t('app', 'Kategori  Panel'),
            'selenggara_id' => Yii::t('app', 'Selenggara  id'),
            'tempoh_selenggara' => Yii::t('app', 'Tempoh  Selenggara'),
            'alasan_id' => Yii::t('app', 'Alasan  id'),
            'alasan' => Yii::t('app', 'Alasan'),
            'tarikh_arahan' => Yii::t('app', 'Tarikh  Arahan'),
            'no_sebutharga' => Yii::t('app', 'No  Sebutharga'),
            'tarikh_kenderaan_tiba' => Yii::t('app', 'Tarikh  Kenderaan  Tiba'),
            'tarikh_jangka_siap' => Yii::t('app', 'Tarikh  Jangka  Siap'),
            'tarikh_sebutharga' => Yii::t('app', 'Tarikh  Sebutharga'),
            'status_sebutharga' => Yii::t('app', 'Status  Sebutharga'),
            'no_do' => Yii::t('app', 'No  Do'),
            'tarikh_siap' => Yii::t('app', 'Tarikh  Siap'),
            'tempoh_jaminan' => Yii::t('app', 'Tempoh  Jaminan'),
            'tarikh_do' => Yii::t('app', 'Tarikh  Do'),
            'tarikh_cetak' => Yii::t('app', 'Tarikh  Cetak'),
            'status' => Yii::t('app', 'Status'),
            'catatan_sebutharga' => Yii::t('app', 'Catatan  Sebutharga'),
            'catatan' => Yii::t('app', 'Catatan'),
            'nama_pic' => Yii::t('app', 'Nama  Pic'),
            'tarikh_permohonan' => Yii::t('app', 'Tarikh  Permohonan'),
            'id_kenderaan' => Yii::t('app', 'Id  Kenderaan'),
            'kategori_kerosakan_id' => Yii::t('app', 'Kategori  Kerosakan  id'),
            'kategori_kerosakan' => Yii::t('app', 'Kategori  Kerosakan'),
            'kategori' => Yii::t('app', 'Kategori'),
            'isservice' => Yii::t('app', 'Isservice'),
            'ispancit' => Yii::t('app', 'Ispancit'),
            'odometer_terkini' => Yii::t('app', 'Odometer  Terkini'),
            'no_plat' => Yii::t('app', 'No  Plat'),
            'model' => Yii::t('app', 'Model'),
            'kod_jabatan' => Yii::t('app', 'Kod  Jabatan'),
            'nama_jabatan' => Yii::t('app', 'Nama  Jabatan'),
            'jumlah_kos' => Yii::t('app', 'Jumlah  Kos'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getOrder() {
        return $this->hasOne(Orders::className(), ['arahan_kerja_id' => 'id']);
    }
//    public function getCreator() {
//        return $this->hasOne(People::className(), ['id' => 'created_by']);
//    }
//
//    public function getUpdator() {
//        return $this->hasOne(People::className(), ['id' => 'updated_by']);
//    }

}
