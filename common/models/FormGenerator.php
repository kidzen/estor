<?php

namespace common\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "ACTIVITY_LOGS".
 *
 * @property string $ID
 * @property integer $USER_ID
 * @property string $REMOTE_IP
 * @property string $ACTION
 * @property string $CONTROLLER
 * @property string $PARAMS
 * @property string $ROUTE
 * @property string $STATUS
 * @property string $MESSAGES
 * @property string $CREATED_AT
 */
class FormGenerator extends Model {

    const LOG_STATUS_SUCCESS = 'success';
    const LOG_STATUS_INFO = 'info';
    const LOG_STATUS_WARNING = 'warning';
    const LOG_STATUS_ERROR = 'error';
    const LOG_STATUS_FAIL = 'fail';

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public $ID;
    public $FORM_ID;
    public $FORM_NAME;
    public $PARAMS;
    public $INVENTORY_ID;
    public $INVENTORY_LIST;
    public $ORDER_ID;
    public $YEAR;
    public $MONTH;
    public $printAll;



    /**
     * @inheritdoc
     */
//    public static function tableName() {
//        return 'activity_logs';
//    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['form_id'], 'required'],
            [['form_id','id'], 'number'],
            [['inventory_id','inventory_list','order_id','year','month','printAll'], 'integer'],
            [[ 'form_name', 'params'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'id',
            'year' => 'Tahun',
            'month' => 'Bulan',
            'order_id' => 'No Pesanan',
            'inventory_id' => 'Nama Inventori',
            'inventory_list' => 'Senarai inventori',
            'form_id' => 'ID Borang',
            'form_name' => 'Nama Borang',
            'params' => 'Maklumat Borang',
            'printAll' => 'Cetak Semua Transaksi',
        ];
    }

    /**
     * Adds a message to ActionLog model
     *
     * @param string $status The log status information
     * @param mixed $message The log message
     * @param int $uID The user id
     */
//    public static function add($status = null, $message = null) {
//        $model = Yii::createObject(__CLASS__);
//        $model->user_id = (int)Yii::$app->user->id;
//        $model->remote_ip = $_SERVER['remote_addr'];
//        $model->action = Yii::$app->requestedAction->id;
//        $model->controller = Yii::$app->requestedAction->controller->id;
//        $model->params = serialize(Yii::$app->requestedAction->controller->actionParams);
//        $model->route = Yii::$app->requestedRoute;
//        $model->status = $status;
//        $model->messages = ($message !== null) ? serialize($message) : null;
//
//        return $model->save();
//    }
}
