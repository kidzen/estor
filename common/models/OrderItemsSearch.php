<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrderItems;

/**
 * OrderItemsSearch represents the model behind the search form about `common\models\OrderItems`.
 */
class OrderItemsSearch extends OrderItems {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'rq_quantity', 'app_quantity', 'current_balance', 'unit_price'], 'number'],
            [['inventory_id', 'order_id', 'created_by', 'updated_by', 'deleted'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = OrderItems::find();
        $query->joinWith('order');
        if (!Yii::$app->user->isAdmin) {
            $query->andWhere(['order_items.deleted' => 0]);
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'inventory_id' => $this->inventory_id,
            'order_id' => $this->order_id,
            'rq_quantity' => $this->rq_quantity,
            'app_quantity' => $this->app_quantity,
            'current_balance' => $this->current_balance,
            'unit_price' => $this->unit_price,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted' => $this->deleted,
            'deleted_at' => $this->deleted_at,
        ]);

        return $dataProvider;
    }

}
