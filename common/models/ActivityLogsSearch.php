<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ActivityLogs;

/**
 * ActivityLogsSearch represents the model behind the search form about `common\models\ActivityLogs`.
 */
class ActivityLogsSearch extends ActivityLogs {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id'], 'number'],
            [['user_id'], 'integer'],
            [['remote_ip', 'action', 'controller', 'params', 'route', 'status', 'messages', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ActivityLogs::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
        ]);

        $this->load($params);

//        if (!($this->load($params) && $this->validate())) {
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
//            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'remote_ip', $this->remote_ip])
                ->andFilterWhere(['like', 'action', $this->action])
                ->andFilterWhere(['like', 'controller', $this->controller])
                ->andFilterWhere(['like', 'params', $this->params])
                ->andFilterWhere(['like', 'route', $this->route])
                ->andFilterWhere(['like', 'status', $this->status])
                ->andFilterWhere(['like', 'created_at', $this->created_at])
                ->andFilterWhere(['like', 'messages', $this->messages]);

        return $dataProvider;
    }

}
