<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\InventoryItems;

/**
 * InventoryItemsSearch represents the model behind the search form about `common\models\InventoryItems`.
 */
class EntrySearch extends InventoryItems {

    /**
     * @inheritdoc
     */
    public $INVENTORY_DETAILS;

    public function attributes() {
        // add related fields to searchable attributes
//        return array_merge(parent::attributes(), ['category.name', 'inventory.quantity', 'inventory.description', 'inventory.card_no', 'inventory.code_no',
//            'checkInTransaction.check_date', 'checkOutTransaction.check_date', 'inventory_details']);
        return array_merge(parent::attributes(), ['category.name', 'inventory.quantity', 'inventory.description', 'inventory.card_no', 'inventory.code_no',
            'transactionIn.check_date', 'checkOutTransaction.order_date', 'inventory_details']);
    }

    public function rules() {
        return [
            [['id', 'unit_price'], 'number'],
//            [['deleted'], 'default', 'value'=>0],
            [['inventory_id', 'checkin_transaction_id', 'checkout_transaction_id', 'created_by', 'updated_by', 'deleted'], 'integer'],
            [['sku', 'created_at', 'updated_at', 'deleted_at','inventory_details'], 'safe'],
//            [['category.name', 'inventory.quantity', 'inventory.description', 'inventory.card_no', 'inventory.code_no', 'checkInTransaction.check_date',
//            'checkOutTransaction.check_date', 'inventory_details'], 'safe'],
//            [['category.name', 'inventory.quantity', 'inventory.description', 'inventory.card_no', 'inventory.code_no', 'checkInTransaction.check_date',
//            'checkOutTransaction.check_date', 'inventory_details'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = InventoryItems::find();
        $query->joinWith(['category']);
//        $query->joinWith(['checkInTransaction']);
//        $query->joinWith(['checkOutTransaction']);
        $query->joinWith(['transactionIn']);
        $query->joinWith(['transactionOut']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

//        $dataProvider->sort->defaultOrder = ['created_at' => SORT_DESC];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider->sort->attributes['id'] = [
            'asc' => ['inventory_items.id' => SORT_ASC],
            'desc' => ['inventory_items.id' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['created_at'] = [
            'asc' => ['inventory_items.created_at' => SORT_ASC],
            'desc' => ['inventory_items.created_at' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['inventory_details'] = [
            'asc' => ['inventories.code_no' => SORT_ASC],
            'desc' => ['inventories.code_no' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['category.name'] = [
            'asc' => ['categories.name' => SORT_ASC],
            'desc' => ['categories.name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['inventory.quantity'] = [
            'asc' => ['inventories.quantity' => SORT_ASC],
            'desc' => ['inventories.quantity' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['transactionIn.check_date'] = [
            'asc' => ['transactions.check_date' => SORT_ASC],
            'desc' => ['transactions.check_date' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['checkOutTransaction.order_date'] = [
            'asc' => ['orders.order_date' => SORT_ASC],
            'desc' => ['orders.order_date' => SORT_DESC],
        ];

        // grid filtering conditions
        $query->andFilterWhere([
            'inventory_items.id' => $this->id,
            'inventory_id' => $this->inventory_id,
            'checkin_transaction_id' => $this->checkin_transaction_id,
            'checkout_transaction_id' => $this->checkout_transaction_id,
            'unit_price' => $this->unit_price,
            'inventory_items.created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted' => $this->deleted,
            'deleted_at' => $this->deleted_at,
//            'inventory.description' => $this>getAttribute('inventory.description'),
        ]);
//        var_dump($this->getAttributes());die();
        $query->andFilterWhere(['like', 'sku', $this->sku]);
        $query->andFilterWhere(['like', 'categories.name', $this->getAttribute('category.name')]);
        $query->andFilterWhere(['like', 'inventories.quantity', $this->getAttribute('inventory.quantity')]);
//        $query->andFilterWhere(['like', 'inventory.description', $this->getAttribute('inventory.description')]);
//        $query->orFilterWhere(['like', 'inventory.card_no', $this->getAttribute('inventory_details')]);
//        $query->orFilterWhere(['like', 'inventory.code_no', $this->getAttribute('inventory_details')]);
        $query->orFilterWhere(['like', 'inventories.card_no', $this->inventory_details]);
        $query->orFilterWhere(['like', 'inventories.code_no', $this->inventory_details]);
        $query->orFilterWhere(['like', 'inventories.description', $this->inventory_details]);

        return $dataProvider;
    }

}
