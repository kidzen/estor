<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "KEWPS_10".
 *
 * @property string $ORDER_ID
 * @property string $ORDER_REQUIRED_DATE
 * @property string $ORDER_ITEM_ID
 * @property string $ORDER_NO
 * @property string $ORDERED_BY
 * @property string $ORDER_DATE
 * @property string $TRANSACTION_ID
 * @property string $CARD_NO
 * @property string $CODE_NO
 * @property string $DESCRIPTION
 * @property string $RQ_QUANTITY
 * @property string $APP_QUANTITY
 * @property string $CURRENT_BALANCE
 * @property string $UNIT_PRICE
 * @property string $AVERAGE_UNIT_PRICE
 * @property string $BATCH_TOTAL_PRICE
 * @property string $CREATED_DATE
 * @property integer $CREATED_BY
 * @property integer $APPROVED_BY
 * @property string $APPROVED_DATE
 */
class FormKewps10 extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\expression('current_timestamp()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'KEWPS_10';
    }

    public static function primaryKey() {
        return ['order_item_id'];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['order_id', 'order_item_id', 'transaction_id', 'rq_quantity', 'app_quantity', 'current_balance', 'unit_price', 'average_unit_price', 'batch_total_price'], 'number'],
            [['created_by', 'approved_by'], 'integer'],
            [['order_required_date', 'order_date'], 'string', 'max' => 7],
            [['order_no', 'ordered_by'], 'string', 'max' => 20],
            [['card_no', 'code_no', 'description'], 'string', 'max' => 255],
            [['created_date', 'approved_date'], 'string', 'max' => 8],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'order_id' => Yii::t('app', 'Order  id'),
            'order_required_date' => Yii::t('app', 'Order  Required  Date'),
            'order_item_id' => Yii::t('app', 'Order  Item  id'),
            'order_no' => Yii::t('app', 'Order  No'),
            'ordered_by' => Yii::t('app', 'Ordered  By'),
            'order_date' => Yii::t('app', 'Order  Date'),
            'transaction_id' => Yii::t('app', 'Transaction  id'),
            'card_no' => Yii::t('app', 'Card  No'),
            'code_no' => Yii::t('app', 'Code  No'),
            'description' => Yii::t('app', 'Description'),
            'rq_quantity' => Yii::t('app', 'Rq  Quantity'),
            'app_quantity' => Yii::t('app', 'App  Quantity'),
            'current_balance' => Yii::t('app', 'Current  Balance'),
            'unit_price' => Yii::t('app', 'Unit  Price'),
            'average_unit_price' => Yii::t('app', 'Average  Unit  Price'),
            'batch_total_price' => Yii::t('app', 'Batch  Total  Price'),
            'created_date' => Yii::t('app', 'Created  Date'),
            'created_by' => Yii::t('app', 'Created  By'),
            'approved_by' => Yii::t('app', 'Approved  By'),
            'approved_date' => Yii::t('app', 'Approved  Date'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getApprovedBy() {
        return $this->hasOne(MpspStaff::className(), ['no_pekerja' => 'staff_no'])
                        ->viaTable('{{%PEOPLE}}', ['id' => 'approved_by']);
    }

    public function getCreatedBy() {
        return $this->hasOne(MpspStaff::className(), ['no_pekerja' => 'staff_no'])
                        ->viaTable('{{%PEOPLE}}', ['id' => 'created_by']);
    }


    public function getOrder() {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    public function getOrderItems() {
        return $this->hasOne(OrderItems::className(), ['id' => 'order_item_id']);
    }

    public function getRecordBy() {
        return $this->hasOne(People::className(), ['id' => 'created_by']);
    }

    public function getUpdator() {
        return $this->hasOne(People::className(), ['id' => 'updated_by']);
    }

}
