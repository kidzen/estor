<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "SYS.USER_VIEWS".
 *
 * @property string $VIEW_NAME
 * @property string $TEXT_LENGTH
 * @property string $TEXT
 * @property string $TYPE_TEXT_LENGTH
 * @property string $TYPE_TEXT
 * @property string $OID_TEXT_LENGTH
 * @property string $OID_TEXT
 * @property string $VIEW_TYPE_OWNER
 * @property string $VIEW_TYPE
 * @property string $SUPERVIEW_NAME
 * @property string $EDITIONING_VIEW
 * @property string $READ_ONLY
 */
class EstorViews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sys.user_views';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['view_name'], 'required'],
            [['text_length', 'type_text_length', 'oid_text_length'], 'number'],
            [['text'], 'string'],
            [['view_name', 'view_type_owner', 'view_type', 'superview_name'], 'string', 'max' => 30],
            [['type_text', 'oid_text'], 'string', 'max' => 4000],
            [['editioning_view', 'read_only'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'view_name' => Yii::t('app', 'View  Name'),
            'text_length' => Yii::t('app', 'Text  Length'),
            'text' => Yii::t('app', 'Text'),
            'type_text_length' => Yii::t('app', 'Type  Text  Length'),
            'type_text' => Yii::t('app', 'Type  Text'),
            'oid_text_length' => Yii::t('app', 'Oid  Text  Length'),
            'oid_text' => Yii::t('app', 'Oid  Text'),
            'view_type_owner' => Yii::t('app', 'View  Type  Owner'),
            'view_type' => Yii::t('app', 'View  Type'),
            'superview_name' => Yii::t('app', 'Superview  Name'),
            'editioning_view' => Yii::t('app', 'Editioning  View'),
            'read_only' => Yii::t('app', 'Read  Only'),
        ];
    }
}
