<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[ReportOutQuarter]].
 *
 * @see ReportOutQuarter
 */
class ReportOutQuarterQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ReportOutQuarter[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ReportOutQuarter|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
