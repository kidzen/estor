<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\InventoryItems;

/**
 * InventoryItemsSearch represents the model behind the search form about `common\models\InventoryItems`.
 */
class ApprovalItemsSearch extends InventoryItems
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'unit_price'], 'number'],
            [['inventory_id', 'checkin_transaction_id', 'checkout_transaction_id', 'created_by', 'updated_by', 'deleted'], 'integer'],
            [['sku', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InventoryItems::find();

        // add conditions that should always apply here
        $query->joinWith('requestOrderItems');
//        $query->joinWith('category');
//        $query->where(['order_items.id'=>4]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'inventory_id' => $this->inventory_id,
            'checkin_transaction_id' => $this->checkin_transaction_id,
            'checkout_transaction_id' => $this->checkout_transaction_id,
            'unit_price' => $this->unit_price,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted' => $this->deleted,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'sku', $this->sku]);

        return $dataProvider;
    }
}
