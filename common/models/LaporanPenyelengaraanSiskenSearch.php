<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LaporanPenyelengaraanSisken;

/**
 * LaporanPenyelengaraanSiskenSearch represents the model behind the search form about `common\models\LaporanPenyelengaraanSisken`.
 */
class LaporanPenyelengaraanSiskenSearch extends LaporanPenyelengaraanSisken {

    /**
     * @inheritdoc
     */
    public function attributes() {
        return array_merge(parent::attributes(),[
            'order.id','order.order_no'
        ]);
    }

    public function rules() {
        return [
            [['id'], 'number'],
            [['tahun', 'bulan', 'no_kerja', 'arahan_kerja', 'jk_jg', 'nama_panel', 'kategori_panel', 'tempoh_selenggara', 'alasan', 'tarikh_arahan', 'no_sebutharga', 'tarikh_kenderaan_tiba', 'tarikh_jangka_siap', 'tarikh_sebutharga', 'status_sebutharga', 'no_do', 'tarikh_siap', 'tempoh_jaminan', 'tarikh_do', 'tarikh_cetak', 'status', 'catatan_sebutharga', 'catatan', 'nama_pic', 'tarikh_permohonan', 'kategori_kerosakan_id', 'kategori_kerosakan', 'kategori', 'isservice', 'ispancit', 'odometer_terkini', 'no_plat', 'model', 'kod_jabatan', 'nama_jabatan', 'jumlah_kos'
                ,'order.order_no'], 'safe'],
            [['bengkel_panel_id', 'selenggara_id', 'alasan_id', 'id_kenderaan'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = LaporanPenyelengaraanSisken::find();
        $query->joinWith('order');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->attributes['order.order_no'] = ['asc' => ['orders.order_no' => SORT_ASC], 'desc' => ['orders.order_no' => SORT_DESC],];
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'bengkel_panel_id' => $this->bengkel_panel_id,
            'selenggara_id' => $this->selenggara_id,
            'alasan_id' => $this->alasan_id,
            'id_kenderaan' => $this->id_kenderaan,
        ]);

        $query->andFilterWhere(['like', 'tahun', $this->tahun])
                ->andFilterWhere(['like', 'bulan', $this->bulan])
                ->andFilterWhere(['like', 'no_kerja', $this->no_kerja])
                ->andFilterWhere(['like', 'arahan_kerja', $this->arahan_kerja])
                ->andFilterWhere(['like', 'jk_jg', $this->jk_jg])
                ->andFilterWhere(['like', 'nama_panel', $this->nama_panel])
                ->andFilterWhere(['like', 'kategori_panel', $this->kategori_panel])
                ->andFilterWhere(['like', 'tempoh_selenggara', $this->tempoh_selenggara])
                ->andFilterWhere(['like', 'alasan', $this->alasan])
                ->andFilterWhere(['like', 'tarikh_arahan', $this->tarikh_arahan])
                ->andFilterWhere(['like', 'no_sebutharga', $this->no_sebutharga])
                ->andFilterWhere(['like', 'tarikh_kenderaan_tiba', $this->tarikh_kenderaan_tiba])
                ->andFilterWhere(['like', 'tarikh_jangka_siap', $this->tarikh_jangka_siap])
                ->andFilterWhere(['like', 'tarikh_sebutharga', $this->tarikh_sebutharga])
                ->andFilterWhere(['like', 'status_sebutharga', $this->status_sebutharga])
                ->andFilterWhere(['like', 'no_do', $this->no_do])
                ->andFilterWhere(['like', 'tarikh_siap', $this->tarikh_siap])
                ->andFilterWhere(['like', 'tempoh_jaminan', $this->tempoh_jaminan])
                ->andFilterWhere(['like', 'tarikh_do', $this->tarikh_do])
                ->andFilterWhere(['like', 'tarikh_cetak', $this->tarikh_cetak])
                ->andFilterWhere(['like', 'status', $this->status])
                ->andFilterWhere(['like', 'catatan_sebutharga', $this->catatan_sebutharga])
                ->andFilterWhere(['like', 'catatan', $this->catatan])
                ->andFilterWhere(['like', 'nama_pic', $this->nama_pic])
                ->andFilterWhere(['like', 'tarikh_permohonan', $this->tarikh_permohonan])
                ->andFilterWhere(['like', 'kategori_kerosakan_id', $this->kategori_kerosakan_id])
                ->andFilterWhere(['like', 'kategori_kerosakan', $this->kategori_kerosakan])
                ->andFilterWhere(['like', 'kategori', $this->kategori])
                ->andFilterWhere(['like', 'isservice', $this->isservice])
                ->andFilterWhere(['like', 'ispancit', $this->ispancit])
                ->andFilterWhere(['like', 'odometer_terkini', $this->odometer_terkini])
                ->andFilterWhere(['like', 'no_plat', $this->no_plat])
                ->andFilterWhere(['like', 'model', $this->model])
                ->andFilterWhere(['like', 'kod_jabatan', $this->kod_jabatan])
                ->andFilterWhere(['like', 'nama_jabatan', $this->nama_jabatan])
                ->andFilterWhere(['like', 'jumlah_kos', $this->jumlah_kos]);

        return $dataProvider;
    }

}
