<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "ORDER_ITEMS".
 *
 * @property string $ID
 * @property integer $INVENTORY_ID
 * @property integer $ORDER_ID
 * @property string $RQ_QUANTITY
 * @property string $APP_QUANTITY
 * @property string $CURRENT_BALANCE
 * @property string $UNIT_PRICE
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class OrderItems extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
        [
        'class' => TimestampBehavior::className(),
        'createdAtAttribute' => 'created_at',
        'updatedAtAttribute' => 'updated_at',
        'value' => new \yii\db\expression('current_timestamp()'),
        ],
        [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
        'value' => Yii::$app->user->id,
        ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order_items';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
        [['rq_quantity', 'inventory_id'], 'required', 'on' => 'checkout'],
        [['id', 'rq_quantity', 'app_quantity', 'current_balance', 'unit_price'], 'number'],
        [['inventory_id', 'order_id', 'created_by', 'updated_by', 'deleted'], 'integer'],
        [['created_at', 'updated_at', 'deleted_at'], 'safe'],
        [['id', 'id'], 'unique', 'targetAttribute' => ['id', 'id'], 'message' => 'The combination of  and ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
        'id' => Yii::t('app', 'id'),
        'inventory_id' => Yii::t('app', 'ID Inventori'),
        'order_id' => Yii::t('app', 'ID Pesanan'),
        'rq_quantity' => Yii::t('app', 'Kuantiti Dimohon'),
        'app_quantity' => Yii::t('app', 'Kuantiti Disahkan'),
        'current_balance' => Yii::t('app', 'Kuantiti Semasa Pengeluaran'),
        'unit_price' => Yii::t('app', 'Harga Seunit'),
        'created_at' => Yii::t('app', 'Tarikh Dijana'),
        'updated_at' => Yii::t('app', 'Tarikh Dikemaskini'),
        'created_by' => Yii::t('app', 'Dijana Oleh'),
        'updated_by' => Yii::t('app', 'Dikemaskini Oleh'),
        'deleted' => Yii::t('app', 'Status'),
        'deleted_at' => Yii::t('app', 'Tarikh Kemaskini Status'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getInventory() {
        return $this->hasOne(Inventories::className(), ['id' => 'inventory_id']);
    }

    public function getVehicle() {
        return $this->hasOne(VehicleList::className(), ['id' => 'vehicle_id'])
        ->via('order');
    }

    public function getOrder() {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    public function getOrderApprovedBy() {
        return $this->hasOne(People::className(), ['id' => 'approved_by'])
        ->via('order');
    }

    public function getOrderCreator() {
        return $this->hasOne(People::className(), ['id' => 'created_by'])
        ->via('order');
    }


    public function getArahanKerja() {
        return $this->hasOne(LaporanPenyelengaraanSisken::className(), ['id' => 'arahan_kerja_id'])
        ->via('order');
    }

    public function getTransaction() {
        return $this->hasOne(Transactions::className(), ['id' => 'transaction_id'])
        ->via('order');
    }

    public function getItems() {
        return $this->hasMany(InventoryItems::className(), ['checkout_transaction_id' => 'id']);
    }

    public function getCreator() {
        return $this->hasOne(People::className(), ['id' => 'created_by']);
    }

    public function getUpdator() {
        return $this->hasOne(People::className(), ['id' => 'updated_by']);
    }

    public static function findAllByOrderItemId($id) {
        return static::find(['orders.id' => static::findOne($id)]);
    }

    public static function updateAppQuantity($id) {
        $itemCount = \common\models\InventoryItems::find()->where(['checkout_transaction_id' => $id])->count();
        $modelOrderItem = static::findOne($id);
        $modelOrderItem->app_quantity = $itemCount;
        if ($modelOrderItem->save(false)) {
            return true;
        }
        return false;
    }

    public static function checkAppQuantity() {
        $unsync = \common\models\OrderItemsAppQuantityCheck::find()->count();
        if ($unsync === 0) {
            return true;
        }
        return false;
    }

    public static function getFaultyAppQuantity() {
        $ids = \common\models\OrderItemsAppQuantityCheck::find()->all();
        return $ids->id;
    }

}
