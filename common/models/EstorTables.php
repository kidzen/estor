<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "SYS.USER_TABLES".
 *
 * @property string $TABLE_NAME
 * @property string $TABLESPACE_NAME
 * @property string $CLUSTER_NAME
 * @property string $IOT_NAME
 * @property string $STATUS
 * @property string $PCT_FREE
 * @property string $PCT_USED
 * @property string $INI_TRANS
 * @property string $MAX_TRANS
 * @property string $INITIAL_EXTENT
 * @property string $NEXT_EXTENT
 * @property string $MIN_EXTENTS
 * @property string $MAX_EXTENTS
 * @property string $PCT_INCREASE
 * @property string $FREELISTS
 * @property string $FREELIST_GROUPS
 * @property string $LOGGING
 * @property string $BACKED_UP
 * @property string $NUM_ROWS
 * @property string $BLOCKS
 * @property string $EMPTY_BLOCKS
 * @property string $AVG_SPACE
 * @property string $CHAIN_CNT
 * @property string $AVG_ROW_LEN
 * @property string $AVG_SPACE_FREELIST_BLOCKS
 * @property string $NUM_FREELIST_BLOCKS
 * @property string $DEGREE
 * @property string $INSTANCES
 * @property string $CACHE
 * @property string $TABLE_LOCK
 * @property string $SAMPLE_SIZE
 * @property string $LAST_ANALYZED
 * @property string $PARTITIONED
 * @property string $IOT_TYPE
 * @property string $TEMPORARY
 * @property string $SECONDARY
 * @property string $NESTED
 * @property string $BUFFER_POOL
 * @property string $FLASH_CACHE
 * @property string $CELL_FLASH_CACHE
 * @property string $ROW_MOVEMENT
 * @property string $GLOBAL_STATS
 * @property string $USER_STATS
 * @property string $DURATION
 * @property string $SKIP_CORRUPT
 * @property string $MONITORING
 * @property string $CLUSTER_OWNER
 * @property string $DEPENDENCIES
 * @property string $COMPRESSION
 * @property string $COMPRESS_FOR
 * @property string $DROPPED
 * @property string $READ_ONLY
 * @property string $SEGMENT_CREATED
 * @property string $RESULT_CACHE
 */
class EstorTables extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sys.user_tables';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['table_name'], 'required'],
            [['pct_free', 'pct_used', 'ini_trans', 'max_trans', 'initial_extent', 'next_extent', 'min_extents', 'max_extents', 'pct_increase', 'freelists', 'freelist_groups', 'num_rows', 'blocks', 'empty_blocks', 'avg_space', 'chain_cnt', 'avg_row_len', 'avg_space_freelist_blocks', 'num_freelist_blocks', 'sample_size'], 'number'],
            [['table_name', 'tablespace_name', 'cluster_name', 'iot_name', 'cluster_owner'], 'string', 'max' => 30],
            [['status', 'table_lock', 'row_movement', 'skip_corrupt', 'dependencies', 'compression'], 'string', 'max' => 8],
            [['logging', 'partitioned', 'nested', 'global_stats', 'user_stats', 'monitoring', 'dropped', 'read_only', 'segment_created'], 'string', 'max' => 3],
            [['backed_up', 'temporary', 'secondary'], 'string', 'max' => 1],
            [['degree', 'instances'], 'string', 'max' => 10],
            [['cache'], 'string', 'max' => 5],
            [['last_analyzed', 'buffer_pool', 'flash_cache', 'cell_flash_cache', 'result_cache'], 'string', 'max' => 7],
            [['iot_type', 'compress_for'], 'string', 'max' => 12],
            [['duration'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'table_name' => Yii::t('app', 'Table  Name'),
            'tablespace_name' => Yii::t('app', 'Tablespace  Name'),
            'cluster_name' => Yii::t('app', 'Cluster  Name'),
            'iot_name' => Yii::t('app', 'Iot  Name'),
            'status' => Yii::t('app', 'Status'),
            'pct_free' => Yii::t('app', 'Pct  Free'),
            'pct_used' => Yii::t('app', 'Pct  Used'),
            'ini_trans' => Yii::t('app', 'Ini  Trans'),
            'max_trans' => Yii::t('app', 'Max  Trans'),
            'initial_extent' => Yii::t('app', 'Initial  Extent'),
            'next_extent' => Yii::t('app', 'Next  Extent'),
            'min_extents' => Yii::t('app', 'Min  Extents'),
            'max_extents' => Yii::t('app', 'Max  Extents'),
            'pct_increase' => Yii::t('app', 'Pct  Increase'),
            'freelists' => Yii::t('app', 'Freelists'),
            'freelist_groups' => Yii::t('app', 'Freelist  Groups'),
            'logging' => Yii::t('app', 'Logging'),
            'backed_up' => Yii::t('app', 'Backed  Up'),
            'num_rows' => Yii::t('app', 'Num  Rows'),
            'blocks' => Yii::t('app', 'Blocks'),
            'empty_blocks' => Yii::t('app', 'Empty  Blocks'),
            'avg_space' => Yii::t('app', 'Avg  Space'),
            'chain_cnt' => Yii::t('app', 'Chain  Cnt'),
            'avg_row_len' => Yii::t('app', 'Avg  Row  Len'),
            'avg_space_freelist_blocks' => Yii::t('app', 'Avg  Space  Freelist  Blocks'),
            'num_freelist_blocks' => Yii::t('app', 'Num  Freelist  Blocks'),
            'degree' => Yii::t('app', 'Degree'),
            'instances' => Yii::t('app', 'Instances'),
            'cache' => Yii::t('app', 'Cache'),
            'table_lock' => Yii::t('app', 'Table  Lock'),
            'sample_size' => Yii::t('app', 'Sample  Size'),
            'last_analyzed' => Yii::t('app', 'Last  Analyzed'),
            'partitioned' => Yii::t('app', 'Partitioned'),
            'iot_type' => Yii::t('app', 'Iot  Type'),
            'temporary' => Yii::t('app', 'Temporary'),
            'secondary' => Yii::t('app', 'Secondary'),
            'nested' => Yii::t('app', 'Nested'),
            'buffer_pool' => Yii::t('app', 'Buffer  Pool'),
            'flash_cache' => Yii::t('app', 'Flash  Cache'),
            'cell_flash_cache' => Yii::t('app', 'Cell  Flash  Cache'),
            'row_movement' => Yii::t('app', 'Row  Movement'),
            'global_stats' => Yii::t('app', 'Global  Stats'),
            'user_stats' => Yii::t('app', 'User  Stats'),
            'duration' => Yii::t('app', 'Duration'),
            'skip_corrupt' => Yii::t('app', 'Skip  Corrupt'),
            'monitoring' => Yii::t('app', 'Monitoring'),
            'cluster_owner' => Yii::t('app', 'Cluster  Owner'),
            'dependencies' => Yii::t('app', 'Dependencies'),
            'compression' => Yii::t('app', 'Compression'),
            'compress_for' => Yii::t('app', 'Compress  For'),
            'dropped' => Yii::t('app', 'Dropped'),
            'read_only' => Yii::t('app', 'Read  Only'),
            'segment_created' => Yii::t('app', 'Segment  Created'),
            'result_cache' => Yii::t('app', 'Result  Cache'),
        ];
    }
}
