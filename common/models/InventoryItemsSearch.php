<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\InventoryItems;

/**
 * InventoryItemsSearch represents the model behind the search form about `common\models\InventoryItems`.
 */
class InventoryItemsSearch extends InventoryItems {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'unit_price'], 'number'],
            [['inventory_id', 'checkin_transaction_id', 'checkout_transaction_id', 'created_by', 'updated_by', 'deleted'], 'integer'],
            [['sku', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = InventoryItems::find();
        $query->joinWith('category');
//        $query->joinWith('transactionOut');
        if (!Yii::$app->user->isAdmin) {
            $query->andWhere(['inventory_items.deleted' => 0]);
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->defaultOrder = ['created_at' => SORT_DESC];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider->sort->attributes['id'] = [
            'asc' => ['inventory_items.id' => SORT_ASC],
            'desc' => ['inventory_items.id' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['deleted'] = [
            'asc' => ['inventory_items.deleted' => SORT_ASC],
            'desc' => ['inventory_items.deleted' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['inventory.description'] = [
            'asc' => ['inventories.description' => SORT_ASC],
            'desc' => ['inventories.description' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['created_at'] = [
            'asc' => ['inventory_items.created_at' => SORT_ASC],
            'desc' => ['inventory_items.created_at' => SORT_DESC],
        ];

        // grid filtering conditions
        $query->andFilterWhere([
            'inventory_items.id' => $this->id,
            'inventory_items.inventory_id' => $this->inventory_id,
            'inventory_items.checkin_transaction_id' => $this->checkin_transaction_id,
            'inventory_items.checkout_transaction_id' => $this->checkout_transaction_id,
            'inventory_items.unit_price' => $this->unit_price,
            'inventory_items.created_at' => $this->created_at,
            'inventory_items.updated_at' => $this->updated_at,
            'inventory_items.created_by' => $this->created_by,
            'inventory_items.updated_by' => $this->updated_by,
            'inventory_items.deleted' => $this->deleted,
            'inventory_items.deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'inventory_items.sku', $this->sku]);

        return $dataProvider;
    }

}
