<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VehicleList;

/**
 * VehicleListSearch represents the model behind the search form about `common\models\VehicleList`.
 */
class VehicleListSearch extends VehicleList {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id'], 'number'],
            [['reg_no', 'model', 'type', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'updated_by', 'deleted'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = VehicleList::find();
        if (!Yii::$app->user->isAdmin) {
            $query->andWhere(['vehicle_list.deleted' => 0]);
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted' => $this->deleted,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'reg_no', $this->reg_no])
                ->andFilterWhere(['like', 'model', $this->model])
                ->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }

}
