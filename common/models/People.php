<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use common\models\Roles;
use common\models\MpspStaff;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\Security;
use common\models\People;
use yii\web\UploadedFile;

/**
 * This is the model class for table "PEOPLE".
 *
 * @property string $ID
 * @property string $USERNAME
 * @property string $EMAIL
 * @property string $PASSWORD
 * @property integer $ROLE_ID
 * @property integer $DELETED
 * @property string $DELETED_AT
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property string $PASSWORD_RESET_TOKEN
 * @property string $AUTH_KEY
 */
class People extends ActiveRecord implements IdentityInterface {

    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public $password_temp;
    // public $STAFF_NO;
    public $repassword_temp;
    public $profile_pic_file;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\expression('current_timestamp()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'people';
    }

    public static function attribues() {
        return array_merge(parent::attributes(),[
            'mpspProfile.nama', 'mpspProfile.jawatan','mpspProfile.keterangan',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id','staff_no'], 'number'],
            [['staff_no', 'email'], 'required'],
//            [['username', 'email'], 'required'],
            [['password_temp', 'repassword_temp'], 'required', 'on' => ['admin-create']],
            ['repassword_temp', 'compare', 'compareAttribute' => 'password_temp', 'message' => 'Both password are not the same.'],
            [['role_id', 'deleted'], 'integer'],
//            [['role_id', 'deleted'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
//            [['username', 'email', 'password','password_reset_token', 'auth_key'], 'string', 'max' => 255],
//            [['username', 'profile_pic', 'email', 'password', 'profile_pic', 'password_temp', 'repassword_temp', 'password_reset_token', 'auth_key'], 'string', 'max' => 255],
            [['username', 'staff_no', 'profile_pic', 'email', 'password', 'profile_pic', 'password_temp', 'repassword_temp', 'password_reset_token', 'auth_key'], 'string', 'max' => 255],
            [['mpspProfile.nama', 'mpspProfile.jawatan','mpspProfile.keterangan',], 'string', 'max' => 255],
            ['username', 'unique', 'message' => 'The combination of  and ID has already been taken.'],
//            [['staff_no'], 'unique', 'message' => 'The Staff No has already been registered.'],
//            [['id'], 'unique', 'targetAttribute' => ['id'], 'message' => 'The combination of  and ID has already been taken.'],
            [['id', 'id'], 'unique', 'targetAttribute' => ['id', 'id'], 'message' => 'The combination of  and ID has already been taken.'],
            [['staff_no', 'staff_no'], 'unique', 'targetAttribute' => ['staff_no', 'staff_no'], 'message' => 'The combination of  and STAFF NO has already been taken.'],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Roles::className(), 'targetAttribute' => ['role_id' => 'id']],
            ['deleted', 'default', 'value' => self::STATUS_ACTIVE],
            ['deleted', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['profile_pic_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    public function scenarios() {
        return [
            'default' => ['username', 'staff_no', 'email', 'password'],
//            'default' => ['staff_no', 'email', 'password'],
            'admin-create' => ['username', 'email', 'password_temp', 'repassword_temp', 'role_id', 'profile_pic', 'staff_no'],
            'admin-update' => ['username', 'email', 'password_temp', 'repassword_temp', 'role_id', 'profile_pic', 'staff_no']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'id'),
            'username' => Yii::t('app', 'Id Staff'),
            'staff_no' => Yii::t('app', 'No Pekerja'),
//            'name' => Yii::t('app', 'Nama Penuh'),
//            'position' => Yii::t('app', 'Jawatan'),
//            'department' => Yii::t('app', 'Jabatan'),
            'profile_pic' => Yii::t('app', 'Gambar'),
            'profile_pic_file' => Yii::t('app', 'Gambar Profil'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Kata Laluan'),
            'password_temp' => Yii::t('app', 'Kata Laluan'),
            'repassword_temp' => Yii::t('app', 'Ulang Kata Laluan'),
            'role_id' => Yii::t('app', 'ID Akses'),
            'deleted' => Yii::t('app', 'Status'),
            'deleted_at' => Yii::t('app', 'Tarikh Kemaskini Status'),
            'created_at' => Yii::t('app', 'Tarikh Dijana'),
            'updated_at' => Yii::t('app', 'Tarikh Dikemaskini'),
            'password_reset_token' => Yii::t('app', 'Token Kata Laluan'),
            'auth_key' => Yii::t('app', 'Kunci Identiti'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return static::findOne(['id' => $id, 'deleted' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($USERNAME) {
        return static::findOne(['username' => $USERNAME, 'deleted' => self::STATUS_ACTIVE]);
    }

    public static function findByStaffNo($STAFF_NO) {
        return static::findOne(['staff_no' => $STAFF_NO, 'deleted' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
                    'password_reset_token' => $token,
                    'deleted' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    public function getUsername() {
        return isset($this->username) ? $this->username : NULL;
    }

//    public function getStaffDetails() {
//        return isset($this->username) ? $this->username : NULL;
//    }

    public function getAvatar() {
        return isset($this->profile_pic) ? $this->profile_pic : NULL;
    }

    public function getName() {
//        return isset($this->mpspProfile->nama) ? $this->mpspProfile->nama : NULL;
        return isset($this->username) ? $this->username : NULL;
    }

    public function getRole() {
        return isset($this->roles->name) ? $this->roles->name : NULL;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($PASSWORD) {
        return Yii::$app->security->validatePassword($PASSWORD, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($PASSWORD) {
        $this->password = Yii::$app->security->generatePasswordHash($PASSWORD);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    public function getRoles() {
        return $this->hasOne(Roles::className(), ['id' => 'role_id']);
    }

    public function getMpspProfile() {
        return $this->hasOne(MpspStaff::className(), ['no_pekerja' => 'staff_no']);
    }

//    public function beforeSave($insert) {
//        if ($this->beforeSave($insert)) {
//            if ($this->isNewRecord || (!$this->isNewRecord && $this->password)) {
////                $this->setPassword($this->password);
////                $this->generateAuthKey();
////                $this->generatePasswordResetToken();
////                var_dump($insert);
////                die();
//            }
//            return true;
//        }
////        var_dump($insert);
////                die();
//        return false;
//    }
}
