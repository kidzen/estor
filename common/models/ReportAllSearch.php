<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ReportAll;

/**
 * ReportAllSearch represents the model behind the search form about `common\models\ReportAll`.
 */
class ReportAllSearch extends ReportAll {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'year', 'count_in', 'price_in', 'count_out', 'price_out', 'count_current', 'price_current'], 'number'],
            [['quarter'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
// bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ReportAll::find();

// add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
//             $query->where('0=1');
            return $dataProvider;
        }
// grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'year' => $this->year,
            'count_in' => $this->count_in,
            'price_in' => $this->price_in,
            'count_out' => $this->count_out,
            'price_out' => $this->price_out,
            'count_current' => $this->count_current,
            'price_current' => $this->price_current,
        ]);

        $query->andFilterWhere(['like', 'quarter', $this->quarter]);

        return $dataProvider;
    }

}
