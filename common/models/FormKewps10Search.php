<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FormKewps10;

/**
 * FormKewps10Search represents the model behind the search form about `common\models\FormKewps10`.
 */
class FormKewps10Search extends FormKewps10
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'order_item_id', 'transaction_id', 'rq_quantity', 'app_quantity', 'current_balance', 'unit_price', 'average_unit_price', 'batch_total_price'], 'number'],
            [['order_required_date', 'order_no', 'ordered_by', 'order_date', 'card_no', 'code_no', 'description', 'created_date', 'approved_date'], 'safe'],
            [['created_by', 'approved_by'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormKewps10::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'order_item_id' => $this->order_item_id,
            'transaction_id' => $this->transaction_id,
            'rq_quantity' => $this->rq_quantity,
            'app_quantity' => $this->app_quantity,
            'current_balance' => $this->current_balance,
            'unit_price' => $this->unit_price,
            'average_unit_price' => $this->average_unit_price,
            'batch_total_price' => $this->batch_total_price,
            'created_by' => $this->created_by,
            'approved_by' => $this->approved_by,
        ]);

        $query->andFilterWhere(['like', 'order_required_date', $this->order_required_date])
            ->andFilterWhere(['like', 'order_no', $this->order_no])
            ->andFilterWhere(['like', 'ordered_by', $this->ordered_by])
            ->andFilterWhere(['like', 'order_date', $this->order_date])
            ->andFilterWhere(['like', 'card_no', $this->card_no])
            ->andFilterWhere(['like', 'code_no', $this->code_no])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'created_date', $this->created_date])
            ->andFilterWhere(['like', 'approved_date', $this->approved_date]);

        return $dataProvider;
    }
}
