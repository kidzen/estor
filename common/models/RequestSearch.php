<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrderItems;

/**
 * OrderItemsSearch represents the model behind the search form about `common\models\OrderItems`.
 */
class RequestSearch extends OrderItems {

    /**
     * @inheritdoc
     */
    public $INVENTORY_DETAILS;

    public function attributes() {
        // add related fields to searchable attributes
//        return array_merge(parent::attributes(), ['category.name', 'inventory.quantity', 'inventory.description', 'inventory.card_no', 'inventory.code_no',
//            'checkInTransaction.check_date', 'checkOutTransaction.check_date', 'inventory_details']);
        return array_merge(parent::attributes(), [
            'category.name',
            'order.order_no', 'order.order_date', 'order.required_date', 'order.approved',
            'arahanKerja.no_kerja',
            'inventory.quantity', 'inventory.description', 'inventory.card_no', 'inventory.code_no',
            'inventory_details', 'usage_detail'
        ]);
    }

    public function rules() {
        return [
            [['id', 'rq_quantity', 'app_quantity', 'current_balance', 'unit_price'], 'number'],
            [['inventory_id', 'order_id', 'created_by', 'updated_by', 'deleted'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at', 'category.name', 'category.name', 'order.order_no', 'order.order_date', 'order.required_date', 'order.approved', 'arahanKerja.no_kerja',
            'inventory.quantity', 'inventory.description', 'inventory.card_no', 'inventory.code_no',
//                'inventory_details', 'usage_detail'
                ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = OrderItems::find();
        $query->joinWith('order');
        $query->joinWith('inventory');
        $query->joinWith('transaction');
        $query->joinWith('arahanKerja');
        $query->joinWith('vehicle');
//        $query->joinWith('items');
        if (!Yii::$app->user->isAdmin) {
            $query->andWhere(['order_items.deleted' => 0]);
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
//        order Items
        $dataProvider->sort->attributes['quantity'] = ['asc' => ['order_items.app_quantity' => SORT_ASC], 'desc' => ['order_items.app_quantity' => SORT_DESC],];
        $dataProvider->sort->attributes['created_at'] = ['asc' => ['order_items.created_at' => SORT_ASC], 'desc' => ['order_items.created_at' => SORT_DESC],];
        $dataProvider->sort->attributes['updated_at'] = ['asc' => ['order_items.updated_at' => SORT_ASC], 'desc' => ['order_items.updated_at' => SORT_DESC],];
        $dataProvider->sort->attributes['id'] = ['asc' => ['order_items.id' => SORT_ASC], 'desc' => ['order_items.id' => SORT_DESC],];
//        order
        $dataProvider->sort->attributes['order.order_no'] = ['asc' => ['orders.order_no' => SORT_ASC], 'desc' => ['orders.order_no' => SORT_DESC],];
        $dataProvider->sort->attributes['order.order_date'] = ['asc' => ['orders.order_date' => SORT_ASC], 'desc' => ['orders.order_date' => SORT_DESC],];
        $dataProvider->sort->attributes['order.required_date'] = ['asc' => ['orders.required_date' => SORT_ASC], 'desc' => ['orders.required_date' => SORT_DESC],];
        $dataProvider->sort->attributes['order.ordered_by'] = ['asc' => ['orders.ordered_by' => SORT_ASC], 'desc' => ['orders.ordered_by' => SORT_DESC],];
        $dataProvider->sort->attributes['order.approved'] = ['asc' => ['orders.approved' => SORT_ASC], 'desc' => ['orders.approved' => SORT_DESC],];
//        inventories
        $dataProvider->sort->attributes['inventory_details'] = ['asc' => ['inventories.code_no' => SORT_ASC], 'desc' => ['inventories.code_no' => SORT_DESC],];
        $dataProvider->sort->attributes['inventory.quantity'] = ['asc' => ['inventories.quantity' => SORT_ASC], 'desc' => ['inventories.quantity' => SORT_DESC],];
//        transactions
        $dataProvider->sort->attributes['transaction.check_date'] = ['asc' => ['transactions.check_date' => SORT_ASC], 'desc' => ['transactions.check_date' => SORT_DESC],];
//        arahan kerja
        $dataProvider->sort->attributes['arahanKerja.id'] = ['asc' => ['laporan_penyelengaraan_sisken.no_kerja' => SORT_ASC], 'desc' => ['laporan_penyelengaraan_sisken.no_kerja' => SORT_DESC],];
//        vehicle
        $dataProvider->sort->attributes['usage_detail'] = ['asc' => ['vehicle_list.reg_no' => SORT_ASC], 'desc' => ['vehicle_list.reg_no' => SORT_DESC],];

        // grid filtering conditions
        $query->andFilterWhere([
            'order_items.id' => $this->id,
            'orders.approved' => $this->getAttribute('order.approved'),
            'order_items.inventory_id' => $this->inventory_id,
            'order_items.order_id' => $this->order_id,
            'order_items.rq_quantity' => $this->rq_quantity,
            'order_items.app_quantity' => $this->app_quantity,
            'order_items.current_balance' => $this->current_balance,
            'order_items.unit_price' => $this->unit_price,
            'order_items.created_at' => $this->created_at,
            'order_items.updated_at' => $this->updated_at,
            'order_items.created_by' => $this->created_by,
            'order_items.updated_by' => $this->updated_by,
            'order_items.deleted' => $this->deleted,
            'order_items.deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'laporan_penyelengaraan_sisken.no_kerja', $this->getAttribute('arahanKerja.no_kerja')]);
        $query->andFilterWhere(['like', 'orders.order_no', $this->getAttribute('order.order_no')]);
        $query->andFilterWhere(['like', 'orders.order_date', $this->getAttribute('order.order_date')]);
        $query->andFilterWhere(['like', 'orders.required_date', $this->getAttribute('order.required_date')]);
        $query->andFilterWhere(['like', 'inventories.card_no', $this->getAttribute('inventory.card_no')]);
        $query->andFilterWhere(['like', 'inventories.code_no', $this->getAttribute('inventory.code_no')]);
        $query->andFilterWhere(['like', 'inventories.description', $this->getAttribute('inventory.description')]);
        $query->andFilterWhere(['like', 'inventories.quantity', $this->getAttribute('inventory.quantity')]);
//        $query->orFilterWhere(['like', 'inventories.card_no', $this->inventory_details]);
//        $query->orFilterWhere(['like', 'inventories.code_no', $this->inventory_details]);
//        $query->orFilterWhere(['like', 'inventories.description', $this->inventory_details]);
//        $query->orFilterWhere(['like', 'inventories.card_no', $this->getAttribute('inventory_details')]);
//        $query->orFilterWhere(['like', 'inventories.code_no', $this->getAttribute('inventory_details')]);
//        $query->orFilterWhere(['like', 'inventories.description', $this->getAttribute('inventory_details')]);
//        if($this->getAttribute('inventory_details')) {
//            $query->andFilterWhere(['like', 'inventories.card_no', $this->getAttribute('inventory_details')]);
//        }
//        if($this->getAttribute('inventory_details')) {
//            $query->andFilterWhere(['like', 'inventories.code_no', $this->getAttribute('inventory_details')]);
//        }
//        if($this->getAttribute('inventory_details')) {
//            $query->andFilterWhere(['like', 'inventories.description', $this->getAttribute('inventory_details')]);
//        }
//        $query->andFilterWhere(['like', 'inventories.code_no', $this->getAttribute('order.code_no')]);
//        $query->andFilterWhere(['like', 'inventories.card_no', $this->getAttribute('order.card_no')]);
//        $query->andFilterWhere(['like', 'inventories.description', $this->getAttribute('order.description')]);


        return $dataProvider;
    }

}
