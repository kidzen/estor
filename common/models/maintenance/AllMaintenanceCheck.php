<?php

namespace common\models\maintenance;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "ALL_MAINTENANCE_CHECK".
 *
 * @property string $ID
 * @property string $TYPE
 * @property string $TABLE_NAME
 * @property string $COL_NAME
 * @property string $DATA_ID
 * @property string $FAULT_VALUE
 * @property string $TRUE_VALUE
 */
class AllMaintenanceCheck extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\expression('current_timestamp()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'all_maintenance_check';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'data_id'], 'number'],
            [['type'], 'string', 'max' => 11],
            [['table_name'], 'string', 'max' => 19],
            [['col_name'], 'string', 'max' => 17],
            [['fault_value', 'true_value'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
            'type' => Yii::t('app', 'Type'),
            'table_name' => Yii::t('app', 'Table  Name'),
            'col_name' => Yii::t('app', 'Col  Name'),
            'data_id' => Yii::t('app', 'Data  id'),
            'fault_value' => Yii::t('app', 'Fault  Value'),
            'true_value' => Yii::t('app', 'True  Value'),
        ];
    }
    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(People::className(), ['id' => 'created_by']);
    }
    public function getUpdator() {
        return $this->hasOne(People::className(), ['id' => 'updated_by']);
    }
}
