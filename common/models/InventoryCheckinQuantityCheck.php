<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "IC_QUANTITY_CHECK".
 *
 * @property string $IV_ID
 * @property string $IC_ID
 * @property string $IV_QUANTITY
 * @property string $ACTUAL_QUANTITY
 */
class InventoryCheckinQuantityCheck extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\expression('current_timestamp()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ic_quantity_check';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iv_id', 'ic_id', 'iv_quantity', 'actual_quantity'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'iv_id' => Yii::t('app', 'Iv  id'),
            'ic_id' => Yii::t('app', 'Ic  id'),
            'iv_quantity' => Yii::t('app', 'Iv  Quantity'),
            'actual_quantity' => Yii::t('app', 'Actual  Quantity'),
        ];
    }
    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(People::className(), ['id' => 'created_by']);
    }
    public function getUpdator() {
        return $this->hasOne(People::className(), ['id' => 'updated_by']);
    }
}
