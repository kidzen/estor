<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\InventoriesCheckIn;

/**
 * InventoriesCheckInSearch represents the model behind the search form about `common\models\InventoriesCheckIn`.
 */
class InventoriesCheckInSearch extends InventoriesCheckIn {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'items_quantity', 'items_total_price'], 'number'],
            [['transaction_id', 'inventory_id', 'vendor_id', 'check_by', 'approved', 'approved_by', 'created_by', 'updated_by', 'deleted'], 'integer'],
            [['check_date', 'approved_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = InventoriesCheckIn::find();
        if (!Yii::$app->user->isAdmin) {
            $query->andWhere(['inventories_checkin.deleted' => 0]);
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'transaction_id' => $this->transaction_id,
            'inventory_id' => $this->inventory_id,
            'vendor_id' => $this->vendor_id,
            'items_quantity' => $this->items_quantity,
            'items_total_price' => $this->items_total_price,
            'check_by' => $this->check_by,
            'approved' => $this->approved,
            'approved_by' => $this->approved_by,
            'approved_at' => $this->approved_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted' => $this->deleted,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'check_date', $this->check_date]);

        return $dataProvider;
    }

}
