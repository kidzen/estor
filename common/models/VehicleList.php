<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "VEHICLE_LIST".
 *
 * @property string $ID
 * @property string $REG_NO
 * @property string $MODEL
 * @property string $TYPE
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class VehicleList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\expression('current_timestamp()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vehicle_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['id'], 'required'],
            [['id'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_by', 'updated_by', 'deleted'], 'integer'],
            [['reg_no', 'type'], 'string', 'max' => 20],
            [['model'], 'string', 'max' => 100],
            [['id', 'id'], 'unique', 'targetAttribute' => ['id', 'id'], 'message' => 'The combination of  and ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
            'reg_no' => Yii::t('app', 'No Plat'),
            'model' => Yii::t('app', 'Model'),
            'type' => Yii::t('app', 'Jenis'),
            'created_at' => Yii::t('app', 'Dijana Pada'),
            'updated_at' => Yii::t('app', 'Dikemaskini Pada'),
            'created_by' => Yii::t('app', 'Dijana Oleh'),
            'updated_by' => Yii::t('app', 'Dikemaskini Oleh'),
            'deleted' => Yii::t('app', 'Status'),
            'deleted_at' => Yii::t('app', 'Tarikh Kemaskini Status'),
        ];
    }
    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(People::className(), ['id' => 'created_by']);
    }
    public function getUpdator() {
        return $this->hasOne(People::className(), ['id' => 'updated_by']);
    }
}
