<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Approval;

/**
 * ApprovalSearch represents the model behind the search form about `common\models\Approval`.
 */
class ApprovalSearch extends Approval {

    public function attributes() {
        // add related fields to searchable attributes
//        return array_merge(parent::attributes(), ['category.name', 'inventory.quantity', 'inventory.description', 'inventory.card_no', 'inventory.code_no',
//            'checkInTransaction.check_date', 'checkOutTransaction.check_date', 'inventory_details']);
        return array_merge(parent::attributes(), [
            'category.name',
            'order.order_no', 'order.order_date', 'order.required_date', 'order.approved',
            'arahanKerja.no_kerja',
            'inventory.quantity', 'inventory.description', 'inventory.card_no', 'inventory.code_no',
            'inventory_details', 'usage_detail'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
//            [['id', 'id_inventory_items', 'id_inventories', 'id_categories', 'id_inventories_checkin', 'id_order_items', 'id_orders',
//                'rq_quantity', 'app_quantity', 'current_balance', 'total_price', 'unit_price'], 'number'],
            [['id', 'available_per_order', 'id_inventory_items', 'id_inventories', 'id_categories', 'id_inventories_checkin',
            'id_order_items', 'id_orders', 'unit_price', 'rq_quantity', 'app_quantity', 'current_balance', 'total_price'], 'number'],
            [['sku', 'order_no', 'items_category_name', 'items_inventory_name', 'order_date', 'required_date'], 'safe'],
            [['approved'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $ordersId) {
        $query = Approval::find();
        $query->where(['id_orders' => $ordersId]);
//        $query->andWhere(['inventory_items.checkout_transaction_id' => null]);
        $query->joinWith('inventoryItem');
//        $query->joinWith('category');
        $query->joinWith('inventory');
//        $query->joinWith('orderItem');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // $dataProvider->sort->attributes['unit_price'] = ['asc' => ['approval.unit_price' => SORT_ASC], 'desc' => ['approval.unit_price' => SORT_DESC],];
        // $dataProvider->sort->attributes['inventory.quantity'] = ['asc' => ['inventory.quantity' => SORT_ASC], 'desc' => ['inventory.quantity' => SORT_DESC],];
        // grid filtering conditions
        $query->andFilterWhere([
            'approval.id' => $this->id,
            'approval.id_inventory_items' => $this->id_inventory_items,
            'approval.id_inventories' => $this->id_inventories,
            'approval.id_categories' => $this->id_categories,
            'approval.id_inventories_checkin' => $this->id_inventories_checkin,
            'approval.id_order_items' => $this->id_order_items,
            'approval.id_orders' => $this->id_orders,
            'approval.rq_quantity' => $this->rq_quantity,
            'approval.app_quantity' => $this->app_quantity,
            'approval.approved' => $this->approved,
            'approval.current_balance' => $this->current_balance,
            'approval.unit_price' => $this->unit_price,
            'approval.total_price' => $this->total_price,
        ]);

        $query->andFilterWhere(['like', 'approval.sku', $this->sku]);
        $query->andFilterWhere(['like', 'approval.order_no', $this->order_no]);
        $query->andFilterWhere(['like', 'approval.items_category_name', $this->items_category_name]);
        $query->andFilterWhere(['like', 'items_inventory_name', $this->items_inventory_name]);
        $query->andFilterWhere(['like', 'order_date', $this->order_date]);
        $query->andFilterWhere(['like', 'required_date', $this->required_date]);
        $query->andFilterWhere(['like', 'required_date', $this->required_date]);
        ;

        return $dataProvider;
    }

}
