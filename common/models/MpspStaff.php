<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "MPSP_STAFF".
 *
 * @property string $NO_PEKERJA
 * @property string $NAMA
 * @property string $JAWATAN
 * @property string $KETERANGAN
 */
class MpspStaff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\expression('current_timestamp()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mpsp_staff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_pekerja'], 'string', 'max' => 20],
            [['nama', 'jawatan'], 'string', 'max' => 100],
            [['keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'no_pekerja' => Yii::t('app', 'No  Pekerja'),
            'nama' => Yii::t('app', 'Nama'),
            'jawatan' => Yii::t('app', 'Jawatan'),
            'keterangan' => Yii::t('app', 'Keterangan'),
        ];
    }
    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(People::className(), ['id' => 'created_by']);
    }
    public function getUpdator() {
        return $this->hasOne(People::className(), ['id' => 'updated_by']);
    }
}
