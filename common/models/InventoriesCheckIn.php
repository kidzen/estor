<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "INVENTORIES_CHECKIN".
 *
 * @property string $ID
 * @property integer $TRANSACTION_ID
 * @property integer $INVENTORY_ID
 * @property integer $VENDOR_ID
 * @property string $ITEMS_QUANTITY
 * @property string $ITEMS_TOTAL_PRICE
 * @property string $CHECK_DATE
 * @property integer $CHECK_BY
 * @property integer $APPROVED
 * @property integer $APPROVED_BY
 * @property string $APPROVED_AT
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class InventoriesCheckIn extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\expression('current_timestamp()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'inventories_checkin';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['inventory_id', 'items_quantity'], 'required', 'on' => 'entry-create'],
            [['items_quantity'], 'compare', 'compareValue' => 0, 'operator' => '>', 'type' => 'number', 'on' => 'entry-create'],
            [['id', 'items_quantity', 'items_total_price'], 'number'],
            [['transaction_id', 'inventory_id', 'vendor_id', 'check_by', 'approved', 'approved_by', 'created_by', 'updated_by', 'deleted'], 'integer'],
            [['approved_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['check_date'], 'string'],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'id'),
            'transaction_id' => Yii::t('app', 'ID Transaksi'),
            'inventory_id' => Yii::t('app', 'ID Inventori'),
            'vendor_id' => Yii::t('app', 'ID Vendor'),
            'items_quantity' => Yii::t('app', 'Kuantiti Barang'),
//            'items_unit_price' => Yii::t('app', 'Jumlah Harga Seunit (RM)'),
            'items_total_price' => Yii::t('app', 'Jumlah Harga Barang (RM)'),
            'check_date' => Yii::t('app', 'Tarikh Cek'),
            'check_by' => Yii::t('app', 'Dicek Oleh'),
            'approved' => Yii::t('app', 'Status Pengesahan'),
            'approved_by' => Yii::t('app', 'Disahkan Oleh'),
            'approved_at' => Yii::t('app', 'Disahkan Pada'),
            'created_at' => Yii::t('app', 'Tarikh Dijana'),
            'updated_at' => Yii::t('app', 'Tarikh Dikemaskini'),
            'created_by' => Yii::t('app', 'Dijana Oleh'),
            'updated_by' => Yii::t('app', 'Dikemaskini Oleh'),
            'deleted' => Yii::t('app', 'Status'),
            'deleted_at' => Yii::t('app', 'Tarikh Kemaskini Status'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getTransaction() {
        return $this->hasOne(Transactions::className(), ['id' => 'transaction_id']);
    }

    public function getCategory() {
        return $this->hasOne(Categories::className(), ['id' => 'category_id'])
                        ->via('inventory');
    }

    public function getInventory() {
        return $this->hasOne(Inventories::className(), ['id' => 'inventory_id']);
    }

    public function getItems() {
        return $this->hasMany(InventoryItems::className(), ['checkin_transaction_id' => 'id']);
    }

    public function getItemsDetails() {
        return $this->hasOne(InventoryItems::className(), ['checkin_transaction_id' => 'id']);
    }

    public function getVendor() {
        return $this->hasOne(Vendors::className(), ['id' => 'vendor_id']);
    }

    public function getCreator() {
        return $this->hasOne(People::className(), ['id' => 'created_by']);
    }

    public function getUpdator() {
        return $this->hasOne(People::className(), ['id' => 'updated_by']);
    }

    public static function getQuantityCheckIn($id) {
        $quantity = InventoryItems::find()
                ->where(['checkin_transaction_id' => $id])
                ->andWhere(['inventory_items.deleted' => 0])
                ->asArray()
                ->count();
        return $quantity;
    }

    public static function updateQuantityCheckIn($id) {
        $model = static::findOne($id);
        $model->items_quantity = static::getQuantityCheckIn($id);
        if ($model->save()) {
            return true;
        } else {
            return false;
        }
    }
    public static function getTotalPriceCheckIn($id) {
        $total_price = InventoryItems::find()
                ->where(['checkin_transaction_id' => $id])
                ->andWhere(['inventory_items.deleted' => 0])
                ->asArray()
                ->sum('unit_price');
        return $total_price;
    }

    public static function updateTotalPriceCheckIn($id) {
        $model = static::findOne($id);
        $model->items_total_price = static::getTotalPriceCheckIn($id);
        if ($model->save()) {
            return true;
        } else {
            return false;
        }
    }
    public static function updateQuantityAndTotalPriceCheckIn($id) {
        $model = static::findOne($id);
        $model->items_quantity = static::getQuantityCheckIn($id);
        $model->items_total_price = static::getTotalPriceCheckIn($id);
        if ($model->save()) {
            return true;
        } else {
            return false;
        }
    }

}
