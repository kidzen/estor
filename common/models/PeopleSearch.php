<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\People;

/**
 * PeopleSearch represents the model behind the search form about `common\models\People`.
 */
class PeopleSearch extends People {

    /**
     * @inheritdoc
     */
    public function attributes() {
        return array_merge(parent::attributes(), [
            'mpspProfile.nama',
            'mpspProfile.jawatan',
            'mpspProfile.keterangan',
            'roles.name',
                ]
        );
    }

    public function rules() {
        return [
            [['id'], 'number'],
            [['username', 'profile_pic', 'email', 'password', 'deleted_at', 'created_at', 'updated_at', 'password_reset_token', 'auth_key',
            'mpspProfile.nama', 'mpspProfile.jawatan', 'mpspProfile.keterangan', 'roles.name'], 'safe'],
            [['role_id', 'deleted'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = People::find();
        $query->joinWith('mpspProfile');
        $query->joinWith('roles');
        if (!Yii::$app->user->isAdmin) {
            $query->andWhere(['people.deleted' => 0]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->sort->attributes['id'] = ['asc' => ['people.id' => SORT_ASC], 'desc' => ['people.id' => SORT_DESC],];
        $dataProvider->sort->attributes['deleted_at'] = ['asc' => ['people.deleted_at' => SORT_ASC], 'desc' => ['people.deleted_at' => SORT_DESC],];
        $dataProvider->sort->attributes['deleted'] = ['asc' => ['people.deleted' => SORT_ASC], 'desc' => ['people.deleted' => SORT_DESC],];
        $dataProvider->sort->attributes['mpspProfile.nama'] = ['asc' => ['mpsp_staff.nama' => SORT_ASC], 'desc' => ['mpsp_staff.nama' => SORT_DESC],];
        $dataProvider->sort->attributes['mpspProfile.jawatan'] = ['asc' => ['mpsp_staff.jawatan' => SORT_ASC], 'desc' => ['mpsp_staff.jawatan' => SORT_DESC],];
        $dataProvider->sort->attributes['mpspProfile.keterangan'] = ['asc' => ['mpsp_staff.keterangan' => SORT_ASC], 'desc' => ['mpsp_staff.keterangan' => SORT_DESC],];
        $dataProvider->sort->attributes['roles.name'] = ['asc' => ['roles.name' => SORT_ASC], 'desc' => ['roles.name' => SORT_DESC],];

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'people.id' => $this->id,
            'people.role_id' => $this->role_id,
            'roles.name' => $this->getAttribute('roles.name'),
            'people.deleted' => $this->deleted,
//            'deleted_at' => $this->deleted_at,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
                ->andFilterWhere(['like', 'email', $this->email])
                ->andFilterWhere(['like', 'mpsp_staff.nama', $this->getAttribute('mpspProfile.nama')])
                ->andFilterWhere(['like', 'mpsp_staff.jawatan', $this->getAttribute('mpspProfile.jawatan')])
                ->andFilterWhere(['like', 'mpsp_staff.keterangan', $this->getAttribute('mpspProfile.keterangan')])
                ->andFilterWhere(['like', 'password', $this->password])
                ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
                ->andFilterWhere(['like', 'people.deleted_at', $this->deleted_at])
                ->andFilterWhere(['like', 'people.created_at', $this->created_at])
                ->andFilterWhere(['like', 'people.updated_at', $this->updated_at])
                ->andFilterWhere(['like', 'people.auth_key', $this->auth_key]);

        return $dataProvider;
    }

}
