<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Orders;

/**
 * OrdersSearch represents the model behind the search form about `common\models\Orders`.
 */
class OrdersSearch extends Orders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'number'],
            [['transaction_id', 'approved', 'approved_by', 'vehicle_id', 'checkout_by', 'created_by', 'updated_by', 'deleted'], 'integer'],
            [['order_date', 'ordered_by', 'order_no', 'approved_at', 'required_date', 'checkout_date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find();
        if (!Yii::$app->user->isAdmin) {
            $query->andWhere(['orders.deleted' => 0]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'transaction_id' => $this->transaction_id,
            'ordered_by' => $this->ordered_by,
            'approved' => $this->approved,
            'approved_by' => $this->approved_by,
            'approved_at' => $this->approved_at,
            'vehicle_id' => $this->vehicle_id,
            'checkout_by' => $this->checkout_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted' => $this->deleted,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'order_date', $this->order_date])
            ->andFilterWhere(['like', 'order_no', $this->order_no])
            ->andFilterWhere(['like', 'required_date', $this->required_date])
            ->andFilterWhere(['like', 'checkout_date', $this->checkout_date]);

        return $dataProvider;
    }
}
