<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VehicleReport;

/**
 * VehicleReportSearch represents the model behind the search form about `common\models\VehicleReport`.
 */
class VehicleReportSearch extends VehicleReport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reg_no'], 'safe'],
            [['sum_usage'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VehicleReport::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'sum_usage' => $this->sum_usage,
        ]);

        $query->andFilterWhere(['like', 'reg_no', $this->reg_no]);

        return $dataProvider;
    }
}
