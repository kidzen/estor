<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Inventories;

/**
 * InventoriesSearch represents the model behind the search form about `common\models\Inventories`.
 */
class InventoriesSearch extends Inventories {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'quantity'], 'number'],
            [['category_id', 'min_stock', 'approved', 'approved_by', 'created_by', 'updated_by', 'deleted'], 'integer'],
            [['card_no', 'code_no', 'description', 'location', 'approved_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Inventories::find();
        if (!Yii::$app->user->isAdmin) {
            $query->andWhere(['inventories.deleted' => 0]);
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'quantity' => $this->quantity,
            'min_stock' => $this->min_stock,
            'approved' => $this->approved,
            'approved_at' => $this->approved_at,
            'approved_by' => $this->approved_by,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted' => $this->deleted,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'card_no', $this->card_no])
                ->andFilterWhere(['like', 'code_no', $this->code_no])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', 'location', $this->location]);

        return $dataProvider;
    }

}
