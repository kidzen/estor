<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\InventoriesCheckIn;

/**
 * InventoryItemsSearch represents the model behind the search form about `common\models\InventoryItems`.
 */
class EntryTransactionSearch extends InventoriesCheckIn {

    /**
     * @inheritdoc
     */
    public $INVENTORY_DETAILS;

    public function attributes() {
        // add related fields to searchable attributes
//        return array_merge(parent::attributes(), ['category.name', 'inventory.quantity', 'inventory.description', 'inventory.card_no', 'inventory.code_no',
//            'checkInTransaction.check_date', 'checkOutTransaction.check_date', 'inventory_details']);
        return array_merge(parent::attributes(), ['category.name', 'inventory.quantity', 'inventory.description', 'inventory.card_no', 'inventory.code_no',
            'inventory_details', 'creator.username', 'updator.username']);
    }

    public function rules() {
        return [
//            [['id', 'unit_price'], 'number'],
////            [['deleted'], 'default', 'value'=>0],
//            [['inventory_id', 'checkin_transaction_id', 'checkout_transaction_id', 'created_by', 'updated_by', 'deleted'], 'integer'],
//            [['sku', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
//            [['category.name', 'inventory.quantity', 'inventory.description', 'inventory.card_no', 'inventory.code_no', 'checkInTransaction.check_date',
//            'checkOutTransaction.check_date', 'inventory_details'], 'safe'],
//            [['category.name', 'inventory.quantity', 'inventory.description', 'inventory.card_no', 'inventory.code_no', 'checkInTransaction.check_date',
//            'checkOutTransaction.check_date', 'inventory_details'], 'safe'],
            [['id', 'items_quantity', 'items_total_price'], 'number'],
            [['transaction_id', 'inventory_id', 'vendor_id', 'check_by', 'approved', 'approved_by', 'created_by', 'updated_by', 'deleted'], 'integer'],
            [['check_date', 'approved_at', 'created_at', 'updated_at', 'deleted_at', 'inventory_details', 'inventory.quantity','creator.username'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = InventoriesCheckIn::find();
        $query->joinWith(['category']);
        $query->joinWith(['transaction']);
        $query->joinWith(['creator']);
        if (!Yii::$app->user->isAdmin) {
            $query->andWhere(['inventories_checkin.deleted' => 0]);
        }

//        $query->orFilterWhere(['like', 'inventories_checkin.deleted', 0]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->defaultOrder = ['created_at' => SORT_DESC];
//        if (!\Yii::$app->user->isAdmin) {
//            $query->andFilterWhere(['like', 'inventories_checkin.deleted', 0]);
//        }
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider->sort->attributes['created_at'] = [
            'asc' => ['inventories_checkin.created_at' => SORT_ASC],
            'desc' => ['inventories_checkin.created_at' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['updated_at'] = [
            'asc' => ['inventories_checkin.updated_at' => SORT_ASC],
            'desc' => ['inventories_checkin.updated_at' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['creator.username'] = [
            'asc' => ['people.username' => SORT_ASC],
            'desc' => ['people.username' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['updator.username'] = [
            'asc' => ['people.username' => SORT_ASC],
            'desc' => ['people.username' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['id'] = [
            'asc' => ['inventories_checkin.id' => SORT_ASC],
            'desc' => ['inventories_checkin.id' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['inventory_details'] = [
            'asc' => ['inventories.code_no' => SORT_ASC],
            'desc' => ['inventories.code_no' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['category.name'] = [
            'asc' => ['categories.name' => SORT_ASC],
            'desc' => ['categories.name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['inventory.quantity'] = [
            'asc' => ['inventories.quantity' => SORT_ASC],
            'desc' => ['inventories.quantity' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['check_date'] = [
            'asc' => ['inventories_checkin.check_date' => SORT_ASC],
            'desc' => ['inventories_checkin.check_date' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['check_by'] = [
            'asc' => ['inventories_checkin.check_by' => SORT_ASC],
            'desc' => ['inventories_checkin.check_by' => SORT_DESC],
        ];
        // grid filtering conditions
        $query->andFilterWhere([
            'inventories_checkin.id' => $this->id,
            'inventories_checkin.transaction_id' => $this->transaction_id,
            'inventories_checkin.inventory_id' => $this->inventory_id,
            'inventories_checkin.vendor_id' => $this->vendor_id,
            'inventories_checkin.items_quantity' => $this->items_quantity,
            'inventories_checkin.items_total_price' => $this->items_total_price,
            'inventories_checkin.check_by' => $this->check_by,
            'inventories_checkin.approved' => $this->approved,
            'inventories_checkin.approved_by' => $this->approved_by,
            'inventories_checkin.approved_at' => $this->approved_at,
//            'inventories_checkin.created_at' => $this->created_at,
//            'inventories_checkin.updated_at' => $this->updated_at,
            'inventories_checkin.created_by' => $this->created_by,
            'inventories_checkin.updated_by' => $this->updated_by,
//            'inventories_checkin.deleted' => $this->deleted,
            'inventories_checkin.deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'inventories_checkin.check_date', $this->check_date]);
        $query->andFilterWhere(['like', 'inventories_checkin.created_at', $this->created_at]);
        $query->andFilterWhere(['like', 'inventories_checkin.updated_at', $this->updated_at]);
        $query->andFilterWhere(['like', 'categories.name', $this->getAttribute('category.name')]);
        $query->andFilterWhere(['like', 'inventories.quantity', $this->getAttribute('inventory.quantity')]);
        $query->andFilterWhere(['like', 'people.username', $this->getAttribute('creator.username')]);
//        $query->andFilterWhere(['like', 'inventories.card_no', $this->getAttribute('inventory_details')]);
//        $query->orFilterWhere(['like', 'inventories.code_no', $this->getAttribute('inventory_details')]);
//        $query->orFilterWhere(['like', 'inventories.description', $this->getAttribute('inventory_details')]);
        $query->orFilterWhere(['like', 'inventories.code_no', $this->inventory_details]);
        $query->orFilterWhere(['like', 'inventories.card_no', $this->inventory_details]);
        $query->orFilterWhere(['like', 'inventories.description', $this->inventory_details]);

        return $dataProvider;
    }

}
