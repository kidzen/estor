<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "INVENTORIES".
 *
 * @property string $ID
 * @property integer $CATEGORY_ID
 * @property string $CARD_NO
 * @property string $CODE_NO
 * @property string $DESCRIPTION
 * @property string $QUANTITY
 * @property integer $MIN_STOCK
 * @property string $LOCATION
 * @property integer $APPROVED
 * @property string $APPROVED_AT
 * @property integer $APPROVED_BY
 * @property string $CREATED_AT
 * @property integer $CREATED_BY
 * @property string $UPDATED_AT
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class Inventories extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

//    public $DETAIL;
    public function attributes() {
        // add related fields to searchable attributes
//        return array_merge(parent::attributes(), ['category.name', 'inventory.quantity', 'inventory.description', 'inventory.card_no', 'inventory.code_no',
//            'checkInTransaction.check_date', 'checkOutTransaction.check_date', 'inventory_details']);
//        $DETAIL = $this->getDetail();
//        $DETAIL = 0;
        return array_merge(parent::attributes(), ['detail']);
    }

//    public function setDetail() {
//        $this->detail = 1;
//    }

    public function behaviors() {
        return [
        [
        'class' => TimestampBehavior::className(),
        'createdAtAttribute' => 'created_at',
        'updatedAtAttribute' => 'updated_at',
        'value' => new \yii\db\expression('current_timestamp()'),
        ],
        [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
        'value' => Yii::$app->user->id,
        ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'inventories';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
        [['id', 'quantity'], 'number'],
        [['category_id', 'min_stock', 'approved', 'approved_by', 'created_by', 'updated_by', 'deleted'], 'integer'],
        [['approved_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        [['card_no', 'code_no', 'description', 'location'], 'string', 'max' => 255],
        [['id', 'code_no'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
        'id' => Yii::t('app', 'id'),
        'category_id' => Yii::t('app', 'ID Kategori'),
        'card_no' => Yii::t('app', 'No Kad'),
        'code_no' => Yii::t('app', 'No Kod'),
        'description' => Yii::t('app', 'Nama Inventori'),
        'quantity' => Yii::t('app', 'Kuantiti'),
        'min_stock' => Yii::t('app', 'Min  Stok'),
        'location' => Yii::t('app', 'Lokasi'),
        'approved' => Yii::t('app', 'Status Pengesahan'),
        'approved_at' => Yii::t('app', 'Disahkan Oleh'),
        'approved_by' => Yii::t('app', 'Disahkan Pada'),
        'created_at' => Yii::t('app', 'Tarikh Dijana'),
        'created_by' => Yii::t('app', 'Dijana Oleh'),
        'updated_at' => Yii::t('app', 'Tarikh Dikemaskini'),
        'updated_by' => Yii::t('app', 'Dikemaskini Oleh'),
        'deleted' => Yii::t('app', 'Status'),
        'deleted_at' => Yii::t('app', 'Tarikh Kemaskini Status'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getCategory() {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    public function getDetail() {
        return $this->code_no . ' - ' . $this->description;
    }

    public function getAllCheckInTransactions() {
        return $this->hasMany(InventoriesCheckIn::className(), ['inventory_id' => 'id']);
    }

    public function getAllCheckOutTransactions() {
        return $this->hasMany(OrderItems::className(), ['inventory_id' => 'id']);
    }

    public function getCheckInTransactions() {
        return $this->hasOne(InventoriesCheckIn::className(), ['inventory_id' => 'id']);
    }

    public function getCheckOutTransactions() {
        return $this->hasOne(OrderItems::className(), ['inventory_id' => 'id']);
    }

    public function getTransactionIn() {
        return $this->hasMany(Transactions::className(), ['id' => 'transaction_id'])
        ->via('checkInTransactions');
    }

    public function getTransactionOut() {
        return $this->hasMany(Transactions::className(), ['id' => 'transaction_id'])
        ->via('checkOutTransactions');
    }

    public function getCreator() {
        return $this->hasOne(People::className(), ['id' => 'created_by']);
    }

    public function getUpdator() {
        return $this->hasOne(People::className(), ['id' => 'updated_by']);
    }

    public function getItems() {
        return $this->hasMany(InventoryItems::className(), ['id' => 'checkin_transaction_id'])
        ->via('checkInTransactions');
    }

    public static function getActiveItemsByInventoryId($id) {
        return InventoryItems::find()->joinWith('inventory')->where(['inventories.id' => $id, 'inventory_items.deleted' => 0])->all();
    }

    public static function getQuantityInStore($id) {
        $quantity = InventoryItems::find()
        ->joinWith('inventory')
        ->where(['inventories.id' => $id])
        ->andWhere(['checkout_transaction_id' => null])
        ->andWhere(['inventory_items.deleted' => 0])
        ->asArray()
        ->count();
        return $quantity;
    }

    public static function updateQuantity($id) {
        $model = static::findOne($id);
        $model->quantity = static::getQuantityInStore($id);
        if ($model->save()) {
            return true;
        }
        return false;
    }

    public static function deletePermanent($id) {
        $model = static::findOne($id);
//        check if there is any model related to this inventory id : true -> cannot delete
        $flag = !empty($model->allCheckInTransactions) || !empty($model->allCheckOutTransactions) || !empty($model->items);
//        var_dump($flag);die();
//        delete if allowed
        if (!$flag) {
//            if ($model->deleted = 0) {
//                static::updateQuantity($id);
//            }
            $model->delete();
            return true;
        }
        return false;
    }

//    https://github.com/yiisoft/yii2/issues/1282 :get all relation
//    public function getModelRelations() {
//        $reflector = new \ReflectionClass($this->modelClass);
//        $model = new $this->modelClass;
//        $stack = array();
//        foreach ($reflector->getMethods() AS $method) {
//            if (substr($method->name, 0, 3) !== 'get')
//                continue;
//            if ($method->name === 'getRelation')
//                continue;
//            if ($method->name === 'getBehavior')
//                continue;
//            if ($method->name === 'getFirstError')
//                continue;
//            if ($method->name === 'getAttribute')
//                continue;
//            if ($method->name === 'getAttributeLabel')
//                continue;
//            if ($method->name === 'getOldAttribute')
//                continue;
//
//            $relation = call_user_func(array($model, $method->name));
//            if ($relation instanceof yii\db\ActiveRelation) {
//                $stack[] = $relation;
//            }
//        }
//        return $stack;
//    }
//    public function getAll() {
//
//        $result = \yii\helpers\ArrayHelper::map($get, 'id', 'name');
//        return $result;
//    }

    public function findList($id) {
        return $this->code_no . ' - ' . $this->description;
    }

    public static function printInventoryBarcode($id){
        $data = static::findOne($id);
        // $mpdf = new \mPDF('utf-8', array(50, 20), 0, '', 1, 1, 5, 1, 2, 0, 'p');
        $mpdf = new \mPDF('utf-8', array(50, 20), 0, '', 1, 1, 5, 0, 1.5, 0, 'p');
        $mpdf->defaultheaderfontsize = 5;
        $mpdf->defaultfooterfontsize = 5;
        $mpdf->setHeader('|© Majlis Perbandaran Seberang Perai|');
       // $mpdf->setFooter('|© MPSP|');
        $mpdf->writeHTML(''
            . 'barcode {padding: 1.5mm;margin: 0;vertical-align: top;color: #000044;}'
            . '.barcodecell {text-align: center;vertical-align: middle;color:white;text-align:center;padding:2px;}'
            . '.barcodetext {text-align:center;font-size:6px;}'
            , 1);
        $mpdf->writeHTML(''
            . '<div style="text-align:center;font-size:8px">' . $data->description . '</div>'
            . '<div class="barcodecell">'
            . '<barcode class="barcode" code="' . $data->code_no . '" type="C128A" size="0.5" height="1.5"/></div>'
            . '<div class="barcodetext">CODE : ' . $data->code_no . '</div>'
            , 2);
        $mpdf->output();

    }
    public static function printAllSku($id,$printInventoryCode){
        $model = InventoryItems::find()->where(['in','inventories.id',$id])
        ->andFilterWhere(['inventory_items.deleted' => 0])
        ->andFilterWhere(['checkout_transaction_id' => null])
        ->joinWith('inventory')->all();
        // var_dump($model);die();
        // $mpdf = new \mPDF('utf-8', array(50, 20), 0, '', 1, 1, 5, 1, 2, 0, 'p');
        $mpdf = new \mPDF('utf-8', array(50, 20), 0, '', 1, 1, 5, 0, 1.5, 0, 'p');
        $mpdf->defaultheaderfontsize = 5;
        $mpdf->defaultfooterfontsize = 5;
        $mpdf->setHeader('|© Majlis Perbandaran Seberang Perai|');
       // $mpdf->setFooter('|© MPSP|');
        $css = ''
        . 'barcode {padding: 1.5mm;margin: 0;vertical-align: top;color: #000044;}'
        . '.barcodecell {text-align: center;vertical-align: middle;color:white;text-align:center;padding:2px;}'
        . '.barcodetext {text-align:center;font-size:6px;}'
        .'';
        $mpdf->writeHTML($css, 1);
        foreach ($model as $key => $data) {
            $content1[] = ''
            . '<div style="text-align:center;font-size:8px">' . $data->inventory->description . '</div>'
            . '<div class="barcodecell"><barcode class="barcode" code="' . $data->inventory->code_no . '" type="C128A" size="0.5" height="1.5"/></div>'
            . '<div class="barcodetext">CODE : ' . $data->inventory->code_no . '</div>';
            $content2[] = ''
            . '<div style="text-align:center;font-size:8px">' . $data->inventory->description . '</div>'
            . '<div class="barcodecell"><barcode class="barcode" code="' . $data->sku . '" type="C128A" size="0.48" height="1.5"/></div>'
            . '<div class="barcodetext">SKU : ' . $data->sku . '</div>';
        }
        foreach ($content2 as $i => $c) {
            if($printInventoryCode){
                $mpdf->writeHTML($content1[$i], 2);
            }
            $mpdf->writeHTML($c, 2);
            $mpdf->addPage();
        }

        $mpdf->output();

    }

}
