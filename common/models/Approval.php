<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "APPROVAL".
 *
 * @property string $ID
 * @property string $ID_INVENTORY_ITEMS
 * @property string $ID_INVENTORIES
 * @property string $ID_CATEGORIES
 * @property string $ID_INVENTORIES_CHECKIN
 * @property string $ID_ORDER_ITEMS
 * @property string $ID_ORDERS
 * @property string $SKU
 * @property string $ODER
 * @property string $ORDER_NO
 * @property string $ITEMS_CATEGORY_NAME
 * @property string $ITEMS_INVENTORY_NAME
 * @property string $RQ_QUANTITY
 * @property string $APP_QUANTITY
 * @property integer $APPROVED
 * @property string $CURRENT_BALANCE
 * @property string $TOTAL_PRICE
 * @property string $ORDER_DATE
 * @property string $REQUIRED_DATE
 */
class Approval extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\expression('current_timestamp()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'approval';
    }

    public static function primaryKey() {
        return ['id'];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
//            [['id', 'id_inventory_items', 'id_inventories', 'id_categories', 'id_inventories_checkin', 'id_order_items', 'id_orders'
//                , 'rq_quantity', 'app_quantity', 'current_balance', 'total_price', 'unit_price'], 'number'],

            [['id', 'available_per_order', 'id_inventory_items', 'id_inventories', 'id_categories', 'id_inventories_checkin',
                'id_order_items', 'id_orders', 'unit_price', 'rq_quantity', 'app_quantity', 'current_balance', 'total_price'], 'number'],

            [['id_inventory_items'], 'required'],
            [['approved'], 'integer'],
            [['sku'], 'string', 'max' => 50],
            [['order_no'], 'string', 'max' => 20],
            [['items_category_name', 'items_inventory_name'], 'string', 'max' => 255],
            [['order_date', 'required_date'], 'string', 'max' => 7],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'id'),
            'available_per_order' => Yii::t('app', 'Jumlah Yang Ada'),
            'id_inventory_items' => Yii::t('app', 'Id  Barang Inventori'),
            'id_inventories' => Yii::t('app', 'Id  Inventori'),
            'id_categories' => Yii::t('app', 'Id  Kategori'),
            'id_inventories_checkin' => Yii::t('app', 'Id  Inventori Masuk'),
            'id_order_items' => Yii::t('app', 'Id  Barang Pesanan'),
            'id_orders' => Yii::t('app', 'Id  Pesanan'),
            'sku' => Yii::t('app', 'Sku'),
            'unit_price' => Yii::t('app', 'Harga Seunit'),
            'order_no' => Yii::t('app', 'No Pesanan'),
            'items_category_name' => Yii::t('app', 'Nama Kategori Barang'),
            'items_inventory_name' => Yii::t('app', 'Nama Inventori Barang'),
            'rq_quantity' => Yii::t('app', 'Kuantiti Dimohon'),
            'app_quantity' => Yii::t('app', 'Kuantiti Disahkan'),
            'approved' => Yii::t('app', 'Status Pengesahan'),
            'current_balance' => Yii::t('app', 'Kuantiti Semasa Pengeluaran'),
            'total_price' => Yii::t('app', 'Jumlah'),
            'order_date' => Yii::t('app', 'Tarikh Pesanan'),
            'required_date' => Yii::t('app', 'Tarikh Diperlukan'),
        ];
    }

    /**
     * @inheritdoc
     * @return ApprovalQuery the active query used by this AR class.
     */
    public static function find() {
        return new ApprovalQuery(get_called_class());
    }

    public static function fifo($orderId) {
        $data = static::find()->select('id_inventories')->where(['id_orders'=>$orderId])->groupBy('id_inventories')
                ->asArray()
                ->all();
        $itemlist = InventoryItems::find()
                ->joinWith('inventory')
                ->where(['in','inventories.id',$data[0]])
                ->andWhere(['checkout_transaction_id'=>null])
//                ->asArray()
                ->all();
        $up = InventoryItems::checkoutFifo($data[0]);
//        for
        var_dump($up);die();
        return $itemlist;
//        return $data;
    }

    /**
     * @inheritdoc
     */
    public function getInventory() {
        return $this->hasOne(Inventories::className(), ['id' => 'id_inventories']);
    }

    public function getInventoryItem() {
        return $this->hasOne(InventoryItems::className(), ['id' => 'id_inventory_items']);
    }

    public function getOrder() {
        return $this->hasOne(Orders::className(), ['id' => 'id_orders']);
    }

    public function getOrderItem() {
        return $this->hasOne(OrderItems::className(), ['id' => 'id_order_items']);
    }

    public function getCategory() {
        return $this->hasOne(Categories::className(), ['id' => 'id_categories']);
    }

    public function getCreator() {
        return $this->hasOne(People::className(), ['id' => 'created_by']);
    }

    public function getUpdator() {
        return $this->hasOne(People::className(), ['id' => 'updated_by']);
    }

}
