<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "ORDERS".
 *
 * @property string $ID
 * @property integer $TRANSACTION_ID
 * @property string $ORDER_DATE
 * @property integer $ORDERED_BY
 * @property string $ORDER_NO
 * @property integer $APPROVED
 * @property integer $APPROVED_BY
 * @property string $APPROVED_AT
 * @property integer $VEHICLE_ID
 * @property string $REQUIRED_DATE
 * @property string $CHECKOUT_DATE
 * @property integer $CHECKOUT_BY
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class Orders extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
        [
        'class' => TimestampBehavior::className(),
        'createdAtAttribute' => 'created_at',
        'updatedAtAttribute' => 'updated_at',
        'value' => new \yii\db\expression('current_timestamp()'),
        ],
        [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
        'value' => Yii::$app->user->id,
        ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
        [['id'], 'number'],
        [['ordered_by', 'order_no', 'order_date', 'required_date',], 'required', 'on' => 'checkout'],
        [['transaction_id', 'approved', 'approved_by', 'vehicle_id', 'checkout_by', 'created_by', 'updated_by', 'deleted'], 'integer'],
        [['approved_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
//            [['order_date', 'required_date', 'checkout_date'], 'string', 'max' => 7],
        [['order_date', 'required_date', 'checkout_date', 'arahan_kerja_id'], 'string'],
        ['required_date', 'compare', 'compareAttribute' => 'order_date', 'operator' => '>=',
        'enableClientValidation' => true, 'message' => '"Tarikh Diperlukan" tidak boleh mendahului "Tarikh Pesanan"'],
        [['ordered_by', 'order_no'], 'string', 'max' => 20],
        [['id'], 'unique'],
        ];
    }

//    public function scenarios() {
//        return [
//            'default' => ['ordered_by', 'order_no', 'order_date', 'required_date',],
//            'checkout' => ['ordered_by', 'order_no', 'order_date', 'required_date'],
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
        'id' => Yii::t('app', 'id'),
        'arahan_kerja_id' => Yii::t('app', 'No Arahan Kerja'),
        'transaction_id' => Yii::t('app', 'Transaction  id'),
        'order_date' => Yii::t('app', 'Tarikh Pesanan'),
        'ordered_by' => Yii::t('app', 'Dipesan Oleh'),
        'order_no' => Yii::t('app', 'No Pesanan'),
        'approved' => Yii::t('app', 'Status Pengesahan'),
        'approved_by' => Yii::t('app', 'Disahkan Oleh'),
        'approved_at' => Yii::t('app', 'Disahkan Pada'),
        'vehicle_id' => Yii::t('app', 'ID Kenderaan'),
        'required_date' => Yii::t('app', 'Tarikh Diperlukan'),
        'checkout_date' => Yii::t('app', 'Tarikh Keluar'),
        'checkout_by' => Yii::t('app', 'Dikeluarkan Oleh'),
        'created_at' => Yii::t('app', 'Tarikh Dijana'),
        'updated_at' => Yii::t('app', 'Tarikh Dikemaskini'),
        'created_by' => Yii::t('app', 'Dijana Oleh'),
        'updated_by' => Yii::t('app', 'Dikemaskini Oleh'),
        'deleted' => Yii::t('app', 'Status'),
        'deleted_at' => Yii::t('app', 'Tarikh Kemaskini Status'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getItems() {
        return $this->hasMany(OrderItems::className(), ['order_id' => 'id']);
    }
    public function getArahanKerja() {
        return $this->hasOne(LaporanPenyelengaraanSisken::className(), ['id' => 'arahan_kerja_id']);
    }

    public function getVehicle() {
        return $this->hasOne(VehicleList::className(), ['id' => 'vehicle_id']);
    }

    public function getApprovedBy() {
        return $this->hasOne(People::className(), ['id' => 'approved_by']);
    }

    public function getCreator() {
        return $this->hasOne(People::className(), ['id' => 'created_by']);
    }

    public function getUpdator() {
        return $this->hasOne(People::className(), ['id' => 'updated_by']);
    }

    public static function pdfKewps10($id) {
        static::findOne($id);
        $query = OrderItems::find()->with('inventory', 'order','orderApprovedBy')->where(['order_id' => $id]);
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $query->asArray()->all(),
            ]);
        $items = $dataProvider->models;
//        $par = array_chunk($items, 3);
//        var_dump($par);die();
        $content = \Yii::$app->controller->renderPartial('@frontend/views/request/_kewps102', [
            'items' => $items,
            'dataProvider' => $dataProvider,
            ]);
//        $filename = '@frontend/assets/dist/pdf_template/KEW.PS-82.pdf';
//        $filedir = Yii::getAlias($filename);


        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE, // leaner size using standard fonts
            'content' => $content,
            'defaultFontSize' => 10,
//            'defaultFont' => '',
//
            'marginLeft' => 13,
            'marginRight' => 13,
            'marginTop' => 10,
            'marginBottom' => 6,
            'marginHeader' => 5,
            'marginFooter' => 5,
//            'marginLeft' => 15,
//            'marginRight' => 15,
//            'marginTop' => 16,
//            'marginBottom' => 16,
//            'marginHeader' => 9,
//            'marginFooter' => 9,
            'cssFile' => '@frontend/assets/dist/css/Pdf.css',
            'format' => 'A4-l',
            'filename' => 'KEW PS 10.pdf',
            'options' => [
            'title' => 'Borang Pemesanan Dan Pengeluaran',
//                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
            'methods' => [
            'SetHeader' => ['Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran a'],
            'SetFooter' => ['|{PAGENO}|'],
            ]
            ]);
        $pdf->render();
    }

    public static function pdfKewps102($id) {
        // var_dump(strlen('BULB 24V 1141 (LAMPU SIGNAL)'));die();
        static::findOne($id);
        $query = OrderItems::find()->with('inventory','order.approvedBy.mpspProfile')->where(['order_id' => $id]);
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $query->asArray()->all(),
            ]);
        $items = $dataProvider->models;
        // var_dump($items[0]['order']);die();
//         var_dump($items);die();
        $arraylist = array_chunk($items, 5);

        $filename = '@frontend/assets/dist/css/Pdf.css';
        $filedir = Yii::getAlias($filename);
        $css = file_get_contents($filedir);
        $contentHead = \Yii::$app->controller->renderPartial('@frontend/views/request/_kewps102_1', [
            'items' => $items,
            'dataProvider' => $dataProvider,
            ]);
        $contentBody = [];
        foreach ($arraylist as $list) {
            $contentBody[] = \Yii::$app->controller->renderPartial('@frontend/views/request/_kewps102_2', [
                'items' => $list,
                'dataProvider' => $dataProvider,
                ]);
        }
        $contentFoot = \Yii::$app->controller->renderPartial('@frontend/views/request/_kewps102_3', [
            'items' => $items,
            'dataProvider' => $dataProvider,
            ]);
        // $filename = '@frontend/assets/dist/pdf_template/KEW.PS-82.pdf';
        // $filedir = Yii::getAlias($filename);
        $mpdf = new \mPDF('utf-8','A4-l');
        $mpdf->setHeader('Pekeliling Perbendaharaan Malaysia|© Majlis Perbandaran Seberang Perai|AM 6.5 Lampiran a');
        setlocale(LC_TIME, 'ms');
        $mpdf->setFooter('|{PAGENO}|Dicetak pada '.strftime("%A, %d %B %Y %H:%M:%S"));
        $mpdf->writeHTML($css, 1);
        // $mpdf->writeHTML('.row-1{white-space:nowrap;} .colw-1{ width:1%;white-space:nowrap;} .colw-10{ width:10%;white-space:nowrap;} .colw-20{ width:20%;white-space:nowrap;} .colw-40{ width:30%;white-space:nowrap;} .colw-812{ width:8.12%;white-space:nowrap;}', 1);
//        var_dump($contentHead);
       // var_dump($contentBody);
       // var_dump($contentFoot);
       //  die();
        foreach ($contentBody as $i => $body) {
            $mpdf->writeHTML($contentHead, 2);
            $mpdf->writeHTML($body, 2);
            $mpdf->writeHTML($contentFoot, 2);
            if($i != sizeof($contentBody)-1){
                $mpdf->addPage();
            }
           // var_dump($body);
        }
        // die();
        $mpdf->output();
        exit;
    }

}
