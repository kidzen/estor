<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "REPORT_ALL".
 *
 * @property string $ID
 * @property string $YEAR
 * @property string $QUARTER
 * @property string $COUNT_IN
 * @property string $PRICE_IN
 * @property string $COUNT_OUT
 * @property string $PRICE_OUT
 * @property string $COUNT_CURRENT
 * @property string $PRICE_CURRENT
 */
class ReportAll extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

//    public function behaviors() {
//        return [
//            [
//                'class' => TimestampBehavior::className(),
//                'createdAtAttribute' => 'created_at',
//                'updatedAtAttribute' => 'updated_at',
//                'value' => new \yii\db\expression('current_timestamp()'),
//            ],
//            [
//                'class' => BlameableBehavior::className(),
//                'createdByAttribute' => 'created_by',
//                'updatedByAttribute' => 'updated_by',
//                'value' => Yii::$app->user->id,
//            ],
//        ];
//    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_all';
    }
    public static function primaryKey()
    {
        return ['id'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'year', 'count_in', 'price_in', 'count_out', 'price_out', 'count_current', 'price_current'], 'number'],
            [['quarter'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
            'year' => Yii::t('app', 'Tahun'),
            'quarter' => Yii::t('app', 'Suku'),
            'count_in' => Yii::t('app', 'Bilangan Masuk'),
            'price_in' => Yii::t('app', 'Jumlah Masuk'),
            'count_out' => Yii::t('app', 'Bilangan Keluar'),
            'price_out' => Yii::t('app', 'Jumlah Keluar'),
            'count_current' => Yii::t('app', 'Bilangan Semasa'),
            'price_current' => Yii::t('app', 'Jumlah Semasa'),
        ];
    }

    /**
     * @inheritdoc
     * @return ReportAllQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ReportAllQuery(get_called_class());
    }
    /**
     * @inheritdoc
     */
//    public function getCreator() {
//        return $this->hasOne(People::className(), ['id' => 'created_by']);
//    }
//    public function getUpdator() {
//        return $this->hasOne(People::className(), ['id' => 'updated_by']);
//    }
}
