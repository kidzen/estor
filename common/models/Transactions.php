<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "TRANSACTIONS".
 *
 * @property string $ID
 * @property string $TYPE
 * @property string $CHECK_DATE
 * @property integer $CHECK_BY
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class Transactions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\expression('current_timestamp()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transactions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type'], 'number'],
            [['check_by', 'created_by', 'updated_by', 'deleted'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['check_date'], 'string'],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
            'type' => Yii::t('app', 'Jenis'),
            'check_date' => Yii::t('app', 'Tarikh Cek'),
            'check_by' => Yii::t('app', 'Cek Oleh'),
            'created_at' => Yii::t('app', 'Tarikh Dijana'),
            'updated_at' => Yii::t('app', 'Tarikh Dikemaskini'),
            'created_by' => Yii::t('app', 'Dijana Oleh'),
            'updated_by' => Yii::t('app', 'Dikemaskini Oleh'),
            'deleted' => Yii::t('app', 'Status'),
            'deleted_at' => Yii::t('app', 'Tarikh Kemaskini Status'),
        ];
    }
    /**
     * @inheritdoc
     */
    public function getInventory() {
        return $this->hasOne(Inventories::className(), ['id' => 'inventory_id'])
                ->via('inventoriesCheckIn');
    }
    public function getInInventoryItem() {
        return $this->hasMany(InventoryItems::className(), ['id' => 'checkin_transaction_id']);
    }
    public function getOutInventoryItem() {
        return $this->hasMany(InventoryItems::className(), ['id' => 'checkout_transaction_id']);
    }
    public function getOrder() {
        return $this->hasOne(Orders::className(), ['transaction_id' => 'id']);
    }
    public function getOrderItems() {
        return $this->hasOne(OrderItems::className(), ['order_id' => 'id'])
                ->via('order');
    }
    public function getInventoriesCheckIn() {
        return $this->hasOne(InventoriesCheckIn::className(), ['transaction_id' => 'id']);
    }
    public function getCheckBy() {
        return $this->hasOne(People::className(), ['id' => 'check_by']);
    }
    public function getCreator() {
        return $this->hasOne(People::className(), ['id' => 'created_by']);
    }
    public function getUpdator() {
        return $this->hasOne(People::className(), ['id' => 'updated_by']);
    }
}
