<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "REPORT_IN_QUARTER".
 *
 * @property string $YEAR
 * @property string $ID
 * @property string $QUARTER
 * @property string $COUNT
 * @property string $TOTAL_PRICE
 */
class ReportInQuarter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\expression('current_timestamp()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_in_quarter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'id', 'count', 'total_price'], 'number'],
            [['quarter'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'year' => Yii::t('app', 'Tahun'),
            'id' => Yii::t('app', 'id'),
            'quarter' => Yii::t('app', 'Suku'),
            'count' => Yii::t('app', 'Bilangan'),
            'total_price' => Yii::t('app', 'Jumlah'),
        ];
    }

    /**
     * @inheritdoc
     * @return ReportInQuarterQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ReportInQuarterQuery(get_called_class());
    }
    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(People::className(), ['id' => 'created_by']);
    }
    public function getUpdator() {
        return $this->hasOne(People::className(), ['id' => 'updated_by']);
    }
}
