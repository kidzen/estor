<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "TRANSACTIONS_ALL".
 *
 * @property string $ID
 * @property string $TYPE
 * @property string $CHECK_DATE
 * @property integer $INVENTORY_ID
 * @property string $COUNT_IN
 * @property string $PRICE_IN
 * @property string $COUNT_OUT
 * @property string $PRICE_OUT
 * @property string $COUNT_CURRENT
 * @property string $PRICE_CURRENT
 */
class TransactionsAll extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\expression('current_timestamp()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'transactions_all';
    }
    public static function primaryKey() {
        return ['id'];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'type', 'unit_price', 'count_in', 'price_in', 'count_out', 'price_out', 'count_current', 'price_current'], 'number'],
            [['inventory_id', 'check_by'], 'integer'],
            [['check_date', 'refference'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'id'),
            'type' => Yii::t('app', 'Jenis'),
            'check_date' => Yii::t('app', 'Tarikh Cek'),
            'check_by' => Yii::t('app', 'Cek Oleh'),
            'refference' => Yii::t('app', 'Tarikh Cek'),
            'inventory_id' => Yii::t('app', 'ID Inventori'),
            'unit_price' => Yii::t('app', 'Harga Seunit (RM)'),
            'count_in' => Yii::t('app', 'Bil Kemasukan'),
            'price_in' => Yii::t('app', 'Jumlah Kemasukan'),
            'count_out' => Yii::t('app', 'Bil Keluar'),
            'price_out' => Yii::t('app', 'Jumlah Keluar'),
            'count_current' => Yii::t('app', 'Bil Semasa'),
            'price_current' => Yii::t('app', 'Jumlah Semasa'),
        ];
    }

    /**
     * @inheritdoc
     * @return TransactionsAllQuery the active query used by this AR class.
     */
    public static function find() {
        return new TransactionsAllQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function getTransaction() {
        return $this->hasOne(Transactions::className(), ['id' => 'id']);
    }

    public function getInout() {
        if ($this->type == 2) {
            return $this->hasOne(Orders::className(), ['transaction_id' => 'id'])
                            ->via('transaction');
        } else if ($this->type == 1) {
            return $this->hasOne(InventoriesCheckIn::className(), ['transaction_id' => 'id'])
                            ->via('transaction');
        }
    }

    public function getOrder() {
        if ($this->type == 2) {
            return $this->hasOne(Orders::className(), ['transaction_id' => 'id'])
                            ->via('transaction');
        } else if ($this->type == 1) {
            return 'in';
        }
    }

    public function getVehicle() {
        if ($this->type == 2) {
            return $this->hasOne(VehicleList::className(), ['id' => 'vehicle_id'])
                            ->via('order');
        } else if ($this->type == 1) {
            return 'in';
        }
    }

    public function getCheckBy() {
        return $this->hasOne(People::className(), ['id' => 'check_by']);
    }


    public function getCreator() {
        return $this->hasOne(People::className(), ['id' => 'created_by']);
    }

    public function getUpdator() {
        return $this->hasOne(People::className(), ['id' => 'updated_by']);
    }

}
