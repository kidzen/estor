<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use \Exception;
use common\models\People;

/**
 * This is the model class for table "INVENTORY_ITEMS".
 *
 * @property string $ID
 * @property integer $INVENTORY_ID
 * @property integer $CHECKIN_TRANSACTION_ID
 * @property integer $CHECKOUT_TRANSACTION_ID
 * @property string $SKU
 * @property string $UNIT_PRICE
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 * @property integer $DELETED
 * @property string $DELETED_AT
 */
class InventoryItems extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
        [
        'class' => TimestampBehavior::className(),
        'createdAtAttribute' => 'created_at',
        'updatedAtAttribute' => 'updated_at',
        'value' => new \yii\db\expression('current_timestamp()'),
        ],
        [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
        'value' => Yii::$app->user->id,
        ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'inventory_items';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
        [['unit_price'], 'required', 'on' => 'entry-create'],
        [['id', 'unit_price'], 'number'],
        [['inventory_id', 'checkin_transaction_id', 'checkout_transaction_id', 'created_by', 'updated_by', 'deleted'], 'integer'],
        [['created_at', 'updated_at', 'deleted_at'], 'safe'],
        [['sku'], 'string', 'max' => 50],
        [['id', 'sku'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
        'id' => Yii::t('app', 'id'),
        'inventory_id' => Yii::t('app', 'Inventory  id'),
        'checkin_transaction_id' => Yii::t('app', 'ID Transaksi Masuk'),
        'checkout_transaction_id' => Yii::t('app', 'ID Transaksi Keluar'),
        'sku' => Yii::t('app', 'Sku'),
        'unit_price' => Yii::t('app', 'Harga Seunit'),
        'created_at' => Yii::t('app', 'Tarikh Dijana'),
        'updated_at' => Yii::t('app', 'Tarikh Dikemaskini'),
        'created_by' => Yii::t('app', 'Dijana Oleh'),
        'updated_by' => Yii::t('app', 'Dikemaskini Oleh'),
        'deleted' => Yii::t('app', 'Status'),
        'deleted_at' => Yii::t('app', 'Tarikh Kemaskini Status'),
        ];
    }

    /**
     * @inheritdoc
     */
//    public function getCheckInTransaction2() {
//        return $this->hasOne(Transactions::className(), ['id' => 'checkin_transaction_id']);
//    }
    public function getCheckInTransaction() {
        return $this->hasOne(InventoriesCheckIn::className(), ['id' => 'checkin_transaction_id']);
    }

    public function getCheckOutTransaction() {
        return $this->hasOne(OrderItems::className(), ['id' => 'checkout_transaction_id']);
    }

    public function getTransactionIn() {
        return $this->hasOne(Transactions::className(), ['id' => 'transaction_id'])
        ->via('checkInTransaction');
    }

    public function getTransactionOut() {
        return $this->hasOne(Transactions::className(), ['id' => 'transaction_id'])
        ->via('orders');
    }

    public function getInventory() {
        return $this->hasOne(Inventories::className(), ['id' => 'inventory_id'])
        ->via('checkInTransaction');
    }

    public function getCategory() {
        return $this->hasOne(Categories::className(), ['id' => 'category_id'])
        ->via('inventory');
    }

    public function getOrders() {
        return $this->hasOne(Orders::className(), ['id' => 'order_id'])
        ->via('checkOutTransaction');
    }

    public function getRequestOrderItems() {
        return $this->hasOne(OrderItems::className(), ['inventory_id' => 'id'])
        ->via('inventory');
    }

    public function getRequestInventory() {
        return $this->hasOne(Inventories::className(), ['inventory_id' => 'inventory_id'])
        ->via('orderItems');
    }

    public function getPeople($id) {
        return $this->hasOne(People::className(), ['id' => $id]);
    }

    public function getCreator() {
        return $this->hasOne(People::className(), ['id' => 'created_by']);
    }

    public function getUpdator() {
        return $this->hasOne(People::className(), ['id' => 'updated_by']);
    }

    public function getSku() {
        return $this->sku;
    }

    public static function findByInventoryId($id) {
        return static::find()->joinWith('inventory')->where(['inventories.id' => $id])->all();
    }

    public static function findByOrderItemId($id) {
        return static::find()->joinWith('checkOutTransaction')->where(['checkout_transaction_id' => $id])->all();
    }

    public static function findByIdStatusInStore($id) {
        return static::find($id)->where(['checkout_transaction_id' => null])->one();
    }

    public static function checkoutFifo($ordersId) {
        $order = Orders::findOne($ordersId);
        $dbtransac = \Yii::$app->db->beginTransaction();
        try {
            // populate all items requested
            $orderItems = $order->items;
            foreach($orderItems as $orderItem){
                $inventoryItems = InventoryItems::find()
                ->joinWith('inventory')
                ->andWhere(['inventories.id'=>$orderItem->inventory->id])
                ->andWhere(['checkout_transaction_id'=>null])
                ->orderBy(['inventories.id'=>SORT_ASC])
                ->limit($orderItem->rq_quantity)
                ->all();
                foreach ($inventoryItems as $i => $inventoryItem) {
                    if ($orderItem->app_quantity < $orderItem->rq_quantity) {
                        $inventoryItem->checkout_transaction_id = $orderItem->id;
                        $inventoryItem->save();
                    } else {
                        throw new Exception(' Transaksi gagal. Transaksi secara manual diperlukan.');
                    }
                }
                var_dump(sizeof($inventoryItems));
                $orderItem->app_quantity = sizeof($inventoryItems);
                Inventories::updateQuantity($orderItem->inventory->id);
                $orderItem->save();
            }
            $order->approved = 1;
            $order->approved_by = Yii::$app->user->id;
            $order->approved_at = date('d-M-y h.i.s a');
            $order->save();
            $dbtransac->commit();
            \Yii::$app->notify->success('Proses berjaya');
        } catch (Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        }
    }

//    public static function printDetails($id) {
////        $sku = static::find()
////                ->select('INVENTORY_ITEMS.ID,INVENTORY_ITEMS.SKU,INVENTORIES.code_no')
////                ->joinWith('inventory')
////                ->where(['inventory_items.id' => $id])
////                ->asArray()
////                ->all();
////        var_dump($sku);
//        $data = static::findOne($id);
////        var_dump($data->inventory->description);die();
////        $pdf = new \kartik\mpdf\Pdf();
////        $mpdf = $pdf->getApi('utf-8', array(190,236));
//        $mpdf = new \mPDF('utf-8', array(50, 20), 0, '', 1, 1, 4, 1, 0.5, 0, 'p');
////        $mpdf = new \mPDF('utf-8', 'A4', 0, '', 1, 1, 4, 1, 0.5, 0, 'p');
////        $mpdf = new \mPDF('utf-8', 'A4');
////        $mpdf = new \mPDF('utf-8', 'A4', 0, '', 15, 15, 16, 16, 9, 9, 'l');
//        $mpdf->defaultheaderfontsize = 5;
//        $mpdf->defaultfooterfontsize = 5;
//        $mpdf->setHeader('|© Majlis Perbandaran Seberang Perai|');
////        $mpdf->setFooter('|© Majlis Perbandaran Seberang Perai|');
////        $mpdf->showWatermarkImage = true;
//        $mpdf->writeHTML(''
//                . 'barcode {padding: 1.5mm;margin: 0;vertical-align: top;color: #000044;}'
//                . '.barcodecell {text-align: center;vertical-align: middle;}'
//                , 1);
//        $mpdf->writeHTML(''
//                . '<div style="text-align:center;font-size:8px">' . $data->inventory->description . '</div>'
//                . '<div class="barcodecell">'
//                . '<barcode class="barcode" code="' . $data->inventory->code_no . '" type="C128A" size="0.5" height="0.5"/></div>'
//                . '<div style="text-align:center;font-size:6px;">CODE : ' . $data->inventory->code_no . '</div>'
//                . '<div class="barcodecell">'
//                . '<barcode class="barcode" code="' . $data->sku . '" type="C128A" size="0.5" height="0.5"/></div>'
//                . '<div style="text-align:center;font-size:6px">SKU : ' . $data->sku . '</div>'
//                , 2);
//        $mpdf->addPage();
//        $mpdf->writeHTML(''
//                . '<div style="text-align:center;font-size:8px">' . $data->inventory->description . '</div>'
//                . '<div style="color:white;text-align:center;padding:2px;">'
//                . '<barcode code="' . $data->inventory->code_no . '" type="C128A" size="0.5" height="0.5"/></div>'
//                . '<div style="text-align:center;font-size:6px;">CODE : ' . $data->inventory->code_no . '</div>'
//                . '<div style="color:white;text-align:center;padding:2px;">'
//                . '<barcode code="' . $data->sku . '" type="C128A" size="0.5" height="0.5"/></div>'
//                . '<div style="text-align:center;font-size:6px">SKU : ' . $data->sku . '</div>'
//        );
//        $mpdf->output();
////        return $sku;
//    }


    public static function printSkuById($id, $printInventoryCode = 0) {
        $model = static::find()->where(['in', 'id' ,$id])->with('inventory')->asArray()->all();
        $mpdf = static::generatePdfBarcode($model, $printInventoryCode);
        $mpdf->output();
    }
    public static function printSku($sku, $printInventoryCode = 0) {
        $model = static::find()->where(['in', 'sku' ,$sku])->with('inventory')->asArray()->all();
        $mpdf = static::generatePdfBarcode($model, $printInventoryCode);
        $mpdf->output();
    }

    public static function generatePdfBarcode($model, $printInventoryCode) {
        $mpdf = new \mPDF('utf-8', array(50, 20), 0, '', 1, 1, 5, 1, 2, 0, 'p');
        $mpdf->defaultheaderfontsize = 5;
        $mpdf->defaultfooterfontsize = 5;
        $mpdf->setHeader('|© Majlis Perbandaran Seberang Perai|');
//        $mpdf->setFooter('|© Majlis Perbandaran Seberang Perai|');
//        $mpdf->showWatermarkImage = true;
        $css = ''
        . 'barcode {padding: 1.5mm;margin: 0;vertical-align: top;color: #000044;}'
        . '.barcodecell {text-align: center;vertical-align: middle;color:white;text-align:center;padding:2px;}'
        . '.barcodetext {text-align:center;font-size:6px;}'
        .'';
        $mpdf->writeHTML($css, 1);
        foreach ($model as $data) {
            $mpdf->writeHTML(''
                . '<div style="text-align:center;font-size:8px">' . $data['inventory']['description'] . '</div>'
                . '<div class="barcodecell"><barcode class="barcode" code="' . $data['sku'] . '" type="C128A" size="0.48" height="1.5"/></div>'
                . '<div class="barcodetext">SKU : ' . $data['sku'] . '</div>'
                , 2);
            if ($printInventoryCode) {
                $mpdf->addPage();
                $mpdf->writeHTML(''
                    . '<div style="text-align:center;font-size:8px">' . $data['inventory']['description'] . '</div>'
                    . '<div class="barcodecell"><barcode class="barcode" code="' . $data['inventory']['code_no'] . '" type="C128A" size="0.5" height="1.5"/></div>'
                    . '<div class="barcodetext">CODE : ' . $data['inventory']['code_no'] . '</div>'
                    , 2);
            }
        }
        return $mpdf;
    }

}
