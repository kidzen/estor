<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Transactions;

/**
 * TransactionsSearch represents the model behind the search form about `common\models\Transactions`.
 */
class TransactionsSearch extends Transactions {

    /**
     * @inheritdoc
     */
    public $DETAILS;

    public function rules() {
        return [
            [['id', 'type'], 'number'],
            [['check_date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['check_by', 'created_by', 'updated_by', 'deleted'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Transactions::find();
        if (!Yii::$app->user->isAdmin) {
            $query->andWhere(['transactions.deleted' => 0]);
        }

//        $query->where(['id'=>'']);
//        $query->joinWith('order');
//        $query->joinWith('inventoriesCheckIn');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
//        var_dump($dataProvider->query);die();
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'transactions.id' => $this->id,
            'transactions.type' => $this->type,
            'transactions.check_by' => $this->check_by,
            'transactions.created_at' => $this->created_at,
            'transactions.updated_at' => $this->updated_at,
            'transactions.created_by' => $this->created_by,
            'transactions.updated_by' => $this->updated_by,
            'transactions.deleted' => $this->deleted,
            'transactions.deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'transactions.check_date', $this->check_date]);

        return $dataProvider;
    }

}
