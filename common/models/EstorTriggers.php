<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "SYS.USER_TRIGGERS".
 *
 * @property string $TRIGGER_NAME
 * @property string $TRIGGER_TYPE
 * @property string $TRIGGERING_EVENT
 * @property string $TABLE_OWNER
 * @property string $BASE_OBJECT_TYPE
 * @property string $TABLE_NAME
 * @property string $COLUMN_NAME
 * @property string $REFERENCING_NAMES
 * @property string $WHEN_CLAUSE
 * @property string $STATUS
 * @property string $DESCRIPTION
 * @property string $ACTION_TYPE
 * @property string $TRIGGER_BODY
 * @property string $CROSSEDITION
 * @property string $BEFORE_STATEMENT
 * @property string $BEFORE_ROW
 * @property string $AFTER_ROW
 * @property string $AFTER_STATEMENT
 * @property string $INSTEAD_OF_ROW
 * @property string $FIRE_ONCE
 * @property string $APPLY_SERVER_ONLY
 */
class EstorTriggers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sys.user_triggers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trigger_body'], 'string'],
            [['trigger_name', 'table_owner', 'table_name'], 'string', 'max' => 30],
            [['trigger_type', 'base_object_type'], 'string', 'max' => 16],
            [['triggering_event'], 'string', 'max' => 227],
            [['column_name', 'when_clause', 'description'], 'string', 'max' => 4000],
            [['referencing_names'], 'string', 'max' => 128],
            [['status'], 'string', 'max' => 8],
            [['action_type'], 'string', 'max' => 11],
            [['crossedition'], 'string', 'max' => 7],
            [['before_statement', 'before_row', 'after_row', 'after_statement', 'instead_of_row', 'fire_once', 'apply_server_only'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'trigger_name' => Yii::t('app', 'Trigger  Name'),
            'trigger_type' => Yii::t('app', 'Trigger  Type'),
            'triggering_event' => Yii::t('app', 'Triggering  Event'),
            'table_owner' => Yii::t('app', 'Table  Owner'),
            'base_object_type' => Yii::t('app', 'Base  Object  Type'),
            'table_name' => Yii::t('app', 'Table  Name'),
            'column_name' => Yii::t('app', 'Column  Name'),
            'referencing_names' => Yii::t('app', 'Referencing  Names'),
            'when_clause' => Yii::t('app', 'When  Clause'),
            'status' => Yii::t('app', 'Status'),
            'description' => Yii::t('app', 'Description'),
            'action_type' => Yii::t('app', 'Action  Type'),
            'trigger_body' => Yii::t('app', 'Trigger  Body'),
            'crossedition' => Yii::t('app', 'Crossedition'),
            'before_statement' => Yii::t('app', 'Before  Statement'),
            'before_row' => Yii::t('app', 'Before  Row'),
            'after_row' => Yii::t('app', 'After  Row'),
            'after_statement' => Yii::t('app', 'After  Statement'),
            'instead_of_row' => Yii::t('app', 'Instead  Of  Row'),
            'fire_once' => Yii::t('app', 'Fire  Once'),
            'apply_server_only' => Yii::t('app', 'Apply  Server  Only'),
        ];
    }
}
