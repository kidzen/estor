<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "ROLES".
 *
 * @property string $ID
 * @property string $NAME
 * @property integer $DELETED
 * @property string $DELETED_DATE
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property integer $CREATED_BY
 * @property integer $UPDATED_BY
 */
class Roles extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\expression('current_timestamp()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => Yii::$app->user->id,
            ],
        ];
    }

    public static function tableName() {
        return 'roles';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
//            [['id'], 'required'],
            [['id'], 'number'],
            [['deleted', 'created_by', 'updated_by'], 'integer'],
            [['deleted_date', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'id'),
            'name' => Yii::t('app', 'Akses'),
            'deleted' => Yii::t('app', 'Status'),
            'deleted_date' => Yii::t('app', 'Tarikh Status'),
            'created_at' => Yii::t('app', 'Tarikh Dijana'),
            'updated_at' => Yii::t('app', 'Tarikh Dikemaskini'),
            'created_by' => Yii::t('app', 'Dijana Oleh'),
            'updated_by' => Yii::t('app', 'Dikemaskini Oleh'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(People::className(), ['id' => 'created_by']);
    }
    public function getUpdator() {
        return $this->hasOne(People::className(), ['id' => 'updated_by']);
    }
}
