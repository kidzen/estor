<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use common\models\People;

/**
 * This is the model class for table "ACTIVITY_LOGS".
 *
 * @property string $ID
 * @property integer $USER_ID
 * @property string $REMOTE_IP
 * @property string $ACTION
 * @property string $CONTROLLER
 * @property string $PARAMS
 * @property string $ROUTE
 * @property string $STATUS
 * @property string $MESSAGES
 * @property string $CREATED_AT
 */
class ActivityLogs extends \yii\db\ActiveRecord {

    const LOG_STATUS_SUCCESS = 'success';
    const LOG_STATUS_INFO = 'info';
    const LOG_STATUS_WARNING = 'warning';
    const LOG_STATUS_ERROR = 'error';
    const LOG_STATUS_FAIL = 'fail';

    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                ],
                'value' => new \yii\db\expression('current_timestamp()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'user_id',
                ],
                'value' => Yii::$app->user->id,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        // return 'activity_logs';
        return 'activity_log';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id'], 'number'],
            [['user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['remote_ip', 'action', 'controller', 'route', 'status', 'messages'], 'string', 'max' => 255],
            [['params'], 'string', 'max' => 4000],
//            [['id', 'id', 'id', 'id', 'id'], 'unique', 'targetAttribute' => ['id', 'id', 'id', 'id', 'id'], 'message' => 'The combination of  and ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'id',
            'user_id' => 'Author',
            'remote_ip' => 'Remote  Ip',
            'action' => 'Action',
            'controller' => 'Controller',
            'params' => 'Params',
            'route' => 'Route',
            'status' => 'Status',
            'messages' => 'Messages',
            'created_at' => 'Created  At',
        ];
    }

    /**
     * Adds a message to ActionLog model
     *
     * @param string $status The log status information
     * @param mixed $message The log message
     * @param int $uID The user id
     */
    public static function add($status = null, $message = null) {
        if (Yii::$app->params['logActivity']) {

            $model = Yii::createObject(__CLASS__);
//            $model = new ActivityLogs();
//            $model->user_id = (int) Yii::$app->user->id;
            $model->remote_ip = $_SERVER['REMOTE_ADDR'];
            $model->action = Yii::$app->requestedAction->id;
            $model->controller = Yii::$app->requestedAction->controller->id;
            if (Yii::$app->request->isPost) {
                $posted = Yii::$app->request->post();
                unset($posted['_csrf']);
                $model->params = serialize($posted);
            } else {
                $model->params = serialize(Yii::$app->requestedAction->controller->actionParams);
            }
            $model->route = Yii::$app->requestedRoute;
            $model->status = $status;
            $model->messages = ($message !== null) ? serialize($message) : null;
            if (!$model->save())
                Yii::$app->notify->warning('fail to track');
//            return $model->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function getCreator() {
        return $this->hasOne(People::className(), ['id' => 'user_id']);
    }

}
