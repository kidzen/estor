<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        
//        'user' => [
//            'identityClass' => 'common\models\People',
//            'enableAutoLogin' => true,
//        ],

//        default yii
        'cache' => [
            'class' => 'yii\caching\FileCache',     // **cache using standart file
            
//            'class' => 'yii\caching\DbCache',     // **portable
//             'cacheTable' => 'ESTOR_CACHE',       // for db cache
             
//            'class' => 'yii\caching\ApcCache',    // **fast, for centralize single server and no dedicated load balancer
//            'keyPrefix' => 'myapp',               // for apc cache

//            'class' => 'yii\caching\MemCache',    // **multiple server and load balancer 
        ],

//        'session' => [
//            'class' => 'yii\web\DbSession',
//
//            // Set the following if you want to use DB component other than
//            // default 'db'.
//            // 'db' => 'mydb',
//
//            // To override default session table, set the following
//             'sessionTable' => 'ESTOR_SESSION',
//        ],
        
//        redis
//        'cache' => [
//            'class' => 'yii\redis\Cache',
//            'redis' => [
//                'hostname' => 'localhost',
//                'port' => 6379,
//                'database' => 0,
//            ]
//        ],
        
        
        
    ],
];
