<?php
return [
    'components' => [
//         'db' => [
//             'class' => 'yii\db\Connection',
//          'dsn' => 'oci:dbname=//localhost:1521/mpsp12c;charset=UTF8',
// //            'username' => 'estor5',
//          'username' => 'estor',
//          'password' => 'estor',
//          'enableSchemaCache' => true,
//          'enableQueryCache' => true,
//          // Duration of schema cache.
//          // 'schemaCacheDuration' => 3600,
//          'schemaCacheDuration' => 0,
//          // Name of the cache component used to store schema information
//          // 'schemaCache' => 'schemaCache',
//      ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=estor',
            'username' => 'root',
            'password' => '',
            'enableSchemaCache' => true,
            'enableQueryCache' => true,
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
];
