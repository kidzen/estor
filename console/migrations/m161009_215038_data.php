<?php

use yii\db\Migration;

class m161009_215038_data extends Migration
{
    public function up() {
        $timeAll = microtime(true);
        $this->insertDefaultData();
        $this->getSchemaSql();
    }

    private function getSchemaSql() {
        //additional view
        if(Yii::$app->db->driverName == 'mysql'){
            $fileName = 'data-mysql-laporan-sisken.sql';
            $myfile = fopen($fileName, "r") or die("Unable to open file!");
            $sql = fread($myfile, filesize($fileName));
            fclose($myfile);
            // var_dump($sql);die;
            foreach (array_filter(array_map('trim', explode(';', $sql))) as $query) {
                $this->execute($query);
            }

            $fileName = 'data-mysql-profile.sql';
            $myfile = fopen($fileName, "r") or die("Unable to open file!");
            $sql = fread($myfile, filesize($fileName));
            fclose($myfile);
            // var_dump($sql);die;
            foreach (array_filter(array_map('trim', explode(';', $sql))) as $query) {
                $this->execute($query);
            }
        }

        // db oracle
        if(Yii::$app->db->driverName == 'oci'){
            $fileName = 'data-20171013.sql';
            $myfile = fopen($fileName, "r") or die("Unable to open file!");
            $sql = fread($myfile, filesize($fileName));
            fclose($myfile);
        }
        if(Yii::$app->db->driverName == 'mysql'){
            $fileName = 'data-mysql-v3.sql';
            $myfile = fopen($fileName, "r") or die("Unable to open file!");
            $sql = fread($myfile, filesize($fileName));
            fclose($myfile);
        } else {
            return 'fail';die;
        }

        foreach (array_filter(array_map('trim', explode(';', $sql))) as $query) {
            $this->execute($query);
        }

    }

    public function down() {
        // db oracle
        $this->insertDefaultData();

    }
    protected function insertDefaultData() {

        try {
            if(Yii::$app->db->driverName == 'oci'){
                $sqlObject = Yii::$app->db->createCommand("select 'truncate table ' || table_name || ' ;' as query from user_tables")->queryAll();
                foreach ($sqlObject as $query) {
                    $this->execute($query['query']);
                }

            } else if(Yii::$app->db->driverName == 'mysql'){
                $sqlObject = Yii::$app->db->createCommand("select CONCAT(CONCAT('truncate table ' , table_name) ,';')  AS query from information_schema.tables
                    WHERE table_schema = '".$this->getDbName()."' and table_name != 'migration' and table_type like '%table%'")->queryAll();
                foreach ($sqlObject as $query) {
                    $this->execute($query['query']);
                }
            }
            $i = 1;
            $this->batchInsert('{{%roles}}', ['id','name'],
                [
                [$i++,'Administrator',],
                [$i++,'Pegawai Stor',],
                [$i++,'Not User',],
                [$i++,'User',],
                [$i++,'Manager',],
                [$i++,'Vendor',],
                ]
                );
            $i = 1;
            $this->batchInsert('{{%people}}', ['id','username','email','staff_no','password','role_id','auth_key'],
                [
                [$i++,'admin1','admin2@mail.com','10000',Yii::$app->security->generatePasswordHash('admin1'),1,Yii::$app->security->generateRandomString()],
                ]
                );

        } catch (\yii\db\Exception $e) {
            echo $e;
        }

    }
    private function getDbName($name='dbname')
    {
        switch (Yii::$app->db->driverName) {
            case 'mysql':
                if (preg_match('/' . $name . '=([^;]*)/', Yii::$app->db->dsn, $match)) {
                    return $match[1];
                } else {
                    return null;
                }
                break;

            case 'oci':
                return Yii::$app->db->username;
                break;

            default:
                # code...
                return null;
                break;
        }

    }
}
