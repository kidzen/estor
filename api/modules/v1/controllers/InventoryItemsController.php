<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use common\models\InventoryItems;
use yii\web\response;
/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class InventoryItemsController extends ActiveController
{
	public function behaviors(){
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
            ],
        ];

        // $behaviors['authenticator'] = [
        //     'class' => CompositeAuth::className(),
        //     'except' => ['index', 'view', 'options'],
        //     'authMethods' => [
        //         HttpBasicAuth::className(),
        //         HttpBearerAuth::className(),
        //         QueryParamAuth::className(),
        //     ],
        // ];

        return $behaviors;
    }

    // public $modelClass = 'common\models\People';
	public $modelClass = 'api\modules\v1\models\InventoryItems';

	public function actions()
	{
		$actions = parent::actions();
		unset($actions['view']);
		return $actions;
	}

	public function actionView($id)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		// 01090102-0916-000022
		// $data = InventoryItems::findOne($id);
		$sku = substr_replace(substr_replace($id, '-', 8,0),'-', -6,0);
		$data = InventoryItems::find()->where(['SKU'=>$sku])->with('category','orders')->asArray()->one();
		// $data = InventoryItems::find()->where(['SKU'=>$sku])->with('category')->one();
		// var_dump($data);die();
		// return $sku;
		// $response = Yii::$app->response;
		// $response->format = \yii\web\Response::FORMAT_JSON;
		// $response->data = ['data' => $data];
		// return $response;
		// var_dump($data);die;
		return $data;
    // prepare and return a data provider for the "index" action
	}
}


