<?php

namespace frontend\components;

Class Enum {

	public static $transactionsType = [
		1 => 'Check In',
		2 => 'Check Out',
	];

	public static $monthList = [
		1 => 'Januari',
		2 => 'Febuari',
		3 => 'Mac',
		4 => 'April',
		5 => 'Mei',
		6 => 'Jun',
		7 => 'Julai',
		8 => 'Ogos',
		9 => 'September',
		10 => 'Oktober',
		11 => 'November',
		12 => 'Disember',
	];

	public static $formList = [
		3 => 'KEW.PS-3 ()',
		4 => 'KEW.PS-4 (KAD PETAK)',
		5 => 'KEW.PS-5 (SENARAI DAFTAR KAD KAWALAN STOK)',
		7 => 'KEW.PS-7 (PENENTUAN KUMPULAN STOK)',
		8 => 'KEW.PS-8 (LABEL FIFO)',
		9 => 'KEW.PS-9 (SENARAI STOK BERTARIKH LUPUT)',
		10 => 'KEW.PS-10 (BORANG PESANAN DAN PENGELUARAN STOK)',
		11 => 'KEW.PS-11 (BORANG PEMESANAN STOK)',
		13 => 'KEW.PS-13 (LAPORAN KEDUDUKAN STOK)',
		14 => 'KEW.PS-14 (LAPORAN PEMERIKSAAN / VERIFIKASI STOR)',
		17 => 'KEW.PS-17 (PENYATA PELARASAN STOK)',
		18 => 'KEW.PS-18 (PERAKUAN AMBIL ALIH)',
	];

	public static $approvalType = [
		0 => 'New',
		1 => 'Pending',
		2 => 'Approved',
		8 => 'Rejected',
		9 => 'Disposed',
	];

	public static $status = [
		0 => 'Active',
		1 => 'Deleted',
	];

}
