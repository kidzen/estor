<?php

namespace frontend\controllers;

use Yii;
use common\models\People;
use common\models\PeopleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\components\AccessRule;
use kartik\widgets\Alert;
use kartik\widgets\Growl;
use yii\web\UploadedFile;

/**
 * PeopleController implements the CRUD actions for People model.
 */
class PeopleController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'delete-multiple' => ['post'],
                    'recover' => ['post'],
                    'delete-permanent' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),],
                'only' => ['create', 'update', 'index', 'view',
                    'delete', 'delete-permanent', 'recover'],
                'rules' => [
                    [
                        'actions' => ['delete-permanent'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['create', 'update', 'delete', 'recover'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all People models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new PeopleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single People model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
        //return $this->redirect(['index']);
    }

    /**
     * Creates a new People model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new People();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', [
                    'type' => Growl::TYPE_SUCCESS,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-ok-sign',
                    'title' => ' Proses BERJAYA',
                    'message' => ' Data berjaya ditambah.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            } else {
                \Yii::$app->session->setFlash('error', [
                    'type' => Growl::TYPE_DANGER,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-remove-sign',
                    'title' => ' Proses GAGAL.',
                    'message' => ' Data tidak berjaya ditambah.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            return $this->redirect(['view', 'id' => $model->id]);
            //return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                        'model' => $model, 'rolesArray' => $rolesArray
            ]);
        }
    }

    /**
     * Updates an existing People model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $modelStaff = $this->findModel($id);
        $model->setScenario('admin-update');
        $roles = \common\models\Roles::find()->where(['deleted' => 0])->asArray()->all();
        $rolesArray = \yii\helpers\ArrayHelper::map($roles, 'id', 'name');

        if ($model->load(Yii::$app->request->post())) {
            $flag = true;
            //get instance of uploaded file
            $model->profile_pic_file = UploadedFile::getInstance($model, 'profile_pic_file');
            if (!empty($model->profile_pic_file)) {
                $imageName = $model->username;
                $imageUrl = \Yii::$app->params['profilePicPath'] . $imageName . '_' . time() . '.' . $model->profile_pic_file->extension;
                if ($model->profile_pic_file->extension == 'jpg' || $model->profile_pic_file->extension == 'png') {
                    //upload file to server
                    $model->profile_pic_file->saveAs($imageUrl);
                    //save path in db
                    $model->profile_pic = $imageUrl;
                } else {
                    $flag = false;
                }
            }
            if ($model->isNewRecord || (!$model->isNewRecord && $model->password_temp && $model->repassword_temp)) {
                $model->setPassword($model->password_temp);
                $model->generateAuthKey();
                $model->generatePasswordResetToken();
                \Yii::$app->session->setFlash('password', [
                    'type' => Growl::TYPE_SUCCESS,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-ok-sign',
                    'title' => ' Kata Laluan Berubah!',
                    'message' => ' Data berjaya dikemaskini.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', [
                    'type' => Growl::TYPE_SUCCESS,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-ok-sign',
                    'title' => ' Proses BERJAYA',
                    'message' => ' Data berjaya dikemaskini.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            } else {
                \Yii::$app->session->setFlash('error', [
                    'type' => Growl::TYPE_DANGER,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-remove-sign',
                    'title' => ' Proses GAGAL.',
                    'message' => ' Data tidak berjaya dikemaskini.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
//            return $this->redirect(['view', 'id' => $model->id]);
//            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model, 'rolesArray' => $rolesArray
            ]);
        }
    }

    /**
     * Deletes an existing People model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->deleted = '1';
        if ($model->save()) {
            \Yii::$app->session->setFlash('success', [
                'type' => Growl::TYPE_SUCCESS,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-ok-sign',
                'title' => ' Proses BERJAYA',
                'message' => ' Data berjaya dihapuskan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        } else {
            \Yii::$app->session->setFlash('error', [
                'type' => Growl::TYPE_DANGER,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-remove-sign',
                'title' => ' Proses GAGAL.',
                'message' => ' Data tidak berjaya dihapuskan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        }

        return $this->redirect(['index']);
    }

    public function actionRecover($id) {
        $model = $this->findModel($id);
        $model->deleted = '0';
        if ($model->save()) {
            \Yii::$app->session->setFlash('success', [
                'type' => Growl::TYPE_SUCCESS,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-ok-sign',
                'title' => ' Proses BERJAYA',
                'message' => ' Data berjaya dikembalikan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        } else {
            \Yii::$app->session->setFlash('error', [
                'type' => Growl::TYPE_DANGER,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-remove-sign',
                'title' => ' Proses GAGAL.',
                'message' => ' Data tidak berjaya dikembalikan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        }
        return $this->redirect(['index']);
    }

    public function actionDeletePermanent($id) {
        $existTransactions = \common\models\Transactions::find()
                ->where(['created_by' => $id])
                ->orWhere(['updated_by' => $id])
                ->count();
        $existOrders = \common\models\Orders::find()
                ->where(['created_by' => $id])
                ->orWhere(['updated_by' => $id])
                ->orWhere(['approved_by' => $id])
                ->count();
        $existInventoriesCheckin = \common\models\InventoriesCheckIn::find()
                ->where(['created_by' => $id])
                ->orWhere(['updated_by' => $id])
                ->orWhere(['approved_by' => $id])
                ->count();
        $existItems = \common\models\InventoryItems::find()
                ->where(['created_by' => $id])
                ->orWhere(['updated_by' => $id])
                ->count();
        $existOrderItems = \common\models\OrderItems::find()
                ->where(['created_by' => $id])
                ->orWhere(['updated_by' => $id])
                ->count();
        $existCategories = \common\models\Categories::find()
                ->where(['created_by' => $id])
                ->orWhere(['updated_by' => $id])
                ->count();
        $existRoles = \common\models\Roles::find()
                ->where(['created_by' => $id])
                ->orWhere(['updated_by' => $id])
                ->count();
        $existVehicleList = \common\models\VehicleList::find()
                ->where(['created_by' => $id])
                ->orWhere(['updated_by' => $id])
                ->count();
        $existVendors = \common\models\Vendors::find()
                ->where(['created_by' => $id])
                ->orWhere(['updated_by' => $id])
                ->count();
        $existActivityLogs = \common\models\ActivityLogs::find()
                ->where(['user_id' => $id])
                ->count();
        $existCount = $existActivityLogs + $existVendors + $existVehicleList + $existRoles + $existCategories + $existOrderItems + $existOrders + $existItems + $existInventoriesCheckin + $existTransactions;
        if ($existCount > 0) {
            \Yii::$app->session->setFlash('error', [
                'type' => Growl::TYPE_DANGER,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-remove-sign',
                'title' => ' Proses GAGAL.',
                'message' => ' Pengguna ini tidak boleh dibuang secara kekal. Terdapat ' . $existCount . ' rekod keatas pengunna.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        } else {
            $this->findModel($id)->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the People model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return People the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = People::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findModelStaff($staffNo) {
        if (($model = \common\models\MpspStaff::findOne(['staff_no'=>$staffNo])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
