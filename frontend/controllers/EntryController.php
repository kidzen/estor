<?php

namespace frontend\controllers;

use Yii;
use common\models\ActivityLogs;
use common\models\InventoriesCheckIn;
use common\models\EntrySearch;
use common\models\EntryTransactionSearch;
use common\models\InventoriesCheckInSearch;
use common\models\InventoryItems;
use common\models\InventoryItemsSearch;
use common\models\Inventories;
use common\models\InventoriesSearch;
use common\models\Vendors;
use common\models\VendorsSearch;
use common\models\Transactions;
use common\models\TransactionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\components\AccessRule;
use kartik\widgets\Alert;
use kartik\widgets\Growl;
use yii\db\Expression;
use yii\base\Exception;

/**
 * InventoryItemsController implements the CRUD actions for InventoryItems model.
 */
class EntryController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'delete-multiple' => ['post'],
                    'recover' => ['post'],
                    'delete-permanent' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),],
                'only' => ['create', 'update', 'index', 'view',
                    'delete', 'delete-permanent', 'recover'],
                'rules' => [
                    [
                        'actions' => ['delete-permanent'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['create', 'update', 'delete', 'recover'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all InventoryItems models.
     * @return mixed
     */
    public function actionIndex() {

        $searchModel = new EntryTransactionSearch();
//        $searchModel->deleted = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $inventoryDetailFilter = Inventories::find()->where(['deleted' => 0])->asArray()->all();
//        $dataProvider->query->andFilterWhere(['deleted'=>1]);
//        var_dump($dataProvider->models[0]->inventory);die();
//        die();
//        $data = Transactions::find()->all();
//        $data = \common\models\Orders::find()->all();
//        var_dump($data);die();
//        die();
        return $this->render('index', [
                    'inventoryDetailFilter' => $inventoryDetailFilter,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetQuantity($id) {
        return Inventories::getQuantityInStore($id);
    }
    public function actionItemList($id) {

//        $searchModel = new EntrySearch();
//        $searchModel->deleted = 1;
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider->query->andWhere(['checkin_transaction_id' => $id]);
//        var_dump($id);die();

        $searchModel = new InventoryItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere((['inventories_checkin.id' => $id]));

        return $this->render('item-list', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single InventoryItems model.
     * @param string $id
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findInventoriesCheckin($id),
        ]);
        //return $this->redirect(['index']);
    }

    /**
     * Creates a new InventoryItems model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
//        var_dump(Yii::$app->user->id);die();
        $date = date('my');
        $transaction = new Transactions();
        $checkIn = new InventoriesCheckIn(['scenario'=>'entry-create']);
        $inventory = new Inventories();
        $items = new InventoryItems(['scenario'=>'entry-create']);

        $inventories = \common\models\Inventories::find()->where(['deleted' => 0])->asArray()->all();
        $inventoriesArray = \yii\helpers\ArrayHelper::map($inventories, 'id', function($model, $defaultValue) {
                    return $model['code_no'] . ' - ' . $model['description'] . ' {' . $model['quantity'] . '}';
                });
        $vendors = \common\models\Vendors::find()->where(['deleted' => 0])->asArray()->all();
        $vendorsArray = \yii\helpers\ArrayHelper::map($vendors, 'id', 'name');
        $vehicles = \common\models\VehicleList::find()->where(['deleted' => 0])->asArray()->all();
        $vehiclesArray = \yii\helpers\ArrayHelper::map($vehicles, 'id', ['reg_no'], 'type');

        if ($items->load(Yii::$app->request->post()) && $checkIn->load(Yii::$app->request->post())) {

            $transaction->type = 1;     //checkIn
            $transaction->check_date = date('d-M-Y');
            $transaction->check_by = Yii::$app->user->id;

            $checkIn->items_total_price = $items->unit_price * $checkIn->items_quantity;
            $checkIn->check_date = date('d-M-Y');
            $checkIn->check_by = Yii::$app->user->id;
            $checkIn->approved = 1;
            $checkIn->approved_at = date('d-M-y h.i.s a');
            $checkIn->approved_by = Yii::$app->user->id;

            $valid = $checkIn->validate();
            $valid = $items->validate() && $valid;
            $valid = $transaction->validate() && $valid;

            if ($valid) {
                $modelInventory = Inventories::findOne([$checkIn->inventory_id]);
                $modelInventory->quantity = Inventories::getQuantityInStore($checkIn->inventory_id) + $checkIn->items_quantity;   //use update counter
                $checkIn->inventory_id = $modelInventory->id;   //use link
                $modelItems = \common\models\Model::createMultipleGenerator(InventoryItems::classname(), '', $checkIn);
            } else {
                Yii::$app->notify->fail(' Proses menyimpan data GAGAL!');
                return $this->redirect(['create']);
            }
//            generate sku
            $skuCounter = InventoryItems::find()
                    // ->select(["MAX(SUBSTR(sku, -6)) AS SKU"])               //  xxxx-xxxx-{SKU}
                    ->joinWith('inventory')
                    ->where(["date_format(inventory_items.created_at, '%m%y')" => $date])                //  xxxx-{MMYY}-xxxx
                    ->andWhere(['code_no' => $modelInventory->code_no])                //  xxxx-{MMYY}-xxxx
                    ->max('SUBSTR(sku, -6)');
                    // ->one();
            $skuCounter = isset($skuCounter) ? $skuCounter + 1 : 0; //data-type : integer
            // var_dump($skuCounter);die;
            // $skuCounter = isset($skuCounter->sku) ? substr($skuCounter->sku + 1, -6) : $modelInventory->code_no . '-' . $date . '-000000'; //data-type : integer
            foreach ($modelItems as $i => $modelItem) {
//                mask counter to 6 byte length
                $skuCounter = str_pad($skuCounter, 6, STR_PAD_LEFT, 0);   //999,999 items limit added per month
                $modelItem->sku = $modelInventory->code_no . '-' . $date . '-' . $skuCounter;
                $modelItem->unit_price = $items->unit_price;
//                var_dump($modelItem->sku);
//                increase counter
                $skuCounter = $skuCounter + 1;
            }
            $valid = $modelInventory->validate() && $valid;
            $valid = \yii\base\Model::validateMultiple($modelItems) && $valid;
//                var_dump($valid);
//            die;
            if ($valid) {
                $dbTransaction = \Yii::$app->db->beginTransaction();
                try {
                    //do the save
                    if (!$transaction->save(false)) {
                        Throw new Exception(' Transaksi gagal disimpan.');
                    }
                    $checkIn->transaction_id = $transaction->id;    //use link
                    if (!$checkIn->items_quantity) {
                        Throw new Exception(' Transaksi gagal disimpan.');
                    }    //use link
                    if (!$checkIn->save(false)) {
                        Throw new Exception(' Transaksi gagal disimpan.');
                    }    //use link
                    if (!$modelInventory->save(false)) {
                        Throw new Exception(' Data Inventori gagal dikemaskini.');
                    }
                    foreach ($modelItems as $i => $modelItem) {
                        $modelItem->checkin_transaction_id = $checkIn->id;  //use link
                        if (!$modelItem->save(false)) {
                            Throw new Exception(' Item Inventori gagal disimpan.');
                        }
                    }
                    ActivityLogs::add(ActivityLogs::LOG_STATUS_SUCCESS, ['Permohonan Kemasukan Barang Berjaya']);
                    $dbTransaction->commit();
                    Yii::$app->notify->success(' ' . $i + 1 . ' item telah berjaya disimpan.');
                    return $this->redirect(['index']);
                } catch (\yii\base\ErrorException $e) {
                    $dbTransaction->rollBack();
                    ActivityLogs::add(ActivityLogs::LOG_STATUS_FAIL, ['Permohonan Kemasukan Barang', serialize($e->getMessage()),]);
                    Yii::$app->notify->fail($e->getMessage());
                }
            } else if (!$valid) {
                ActivityLogs::add(ActivityLogs::LOG_STATUS_WARNING, ['Permohonan Kemasukan Barang', 'Proses validasi data GAGAL!',]);
                Yii::$app->notify->info(' Sila pastikan data yang diisi menepati syarat.');
//                return $this->redirect(['create']);
            }
        }
        return $this->render('create', [
//                    'model' => $model,
                    'transaction' => $transaction,
                    'checkIn' => $checkIn,
                    'inventory' => $inventory,
                    'items' => $items,
                    'inventoriesArray' => $inventoriesArray,
                    'vendorsArray' => $vendorsArray,
        ]);
    }

    public function actionUpdate($id) {
        $date = date('my');
        $checkIn = InventoriesCheckIn::findOne($id);
        $transaction = $checkIn->transaction;
        $inventory = $checkIn->inventory;
        $items = $checkIn->items;

        $inventories = \common\models\Inventories::find()->where(['deleted' => 0])->asArray()->all();
        $inventoriesArray = \yii\helpers\ArrayHelper::map($inventories, 'id', function($model, $defaultValue) {
                    return $model['code_no'] . ' - ' . $model['description'] . ' {' . $model['quantity'] . '}';
                });
        $vendors = Vendors::find()->asArray()->all();
        $vendorsArray = \yii\helpers\ArrayHelper::map($vendors, 'id', 'name');

        if ($checkIn->load(Yii::$app->request->post())) {
            if ($checkIn->approved == 2) {
                $oldIDs = ArrayHelper::map($items, 'id', 'id');
                $items = Model::createMultiple(StockItems::classname(), $items);
                Model::loadMultiple($items, Yii::$app->request->post());
                $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($items, 'id', 'id')));

                // validate all models
                $valid = $checkIn->validate();
                $valid = Model::validateMultiple($items) && $valid;

                if ($valid) {
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        if ($flag = $checkIn->save(false)) {
                            if (!empty($deletedIDs)) {
                                StockItems::deleteAll(['id' => $deletedIDs]);
                            }
                            foreach ($items as $item) {
                                $item->checkin_transaction_id = $checkIn->id;

                                if (!($flag = $item->save(false))) {
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        }
                        if ($flag) {
                            $transaction->commit();
                            return $this->redirect(['index']);
                        }
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }
            } else {
                if ($checkIn->save()) {
                    Yii::$app->notify->success(' Data berjaya disimpan.');
                } else {
                    Yii::$app->notify->fail(' Data tidak berjaya disimpan.');
                }
            }
        }
        return $this->render('update', [
//                    'model' => $model,
                    'transaction' => $transaction,
                    'checkIn' => $checkIn,
                    'inventory' => $inventory,
                    'items' => (empty($items)) ? [new InventoryItems] : $items,
                    'inventoriesArray' => $inventoriesArray,
                    'vendorsArray' => $vendorsArray,
        ]);
    }

    /**
     * Deletes an existing InventoryItems model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findInventoriesCheckin($id);
        $modelTransaction = $this->findTransactions($model->transaction_id);
        $modelItems = InventoryItems::find()->where(['checkin_transaction_id' => $model->id])->all();

//        check if items is already check out or not
        $checkoutedItems[] = '';
        foreach ($modelItems as $item) {
            $checkoutedItems = \common\models\OrderItems::findOne($item->checkout_transaction_id);
        }
        if (!empty($checkoutedItems)) {
            Yii::$app->notify->fail(' Item dalam transaksi ini telah/pernah dikeluarkan dari stor.');
            return $this->redirect(['index']);
        }

        $model->deleted = '1';
        $model->deleted_at = date('d-M-y h.i.s a');
        $modelTransaction->deleted = '1';
        $modelTransaction->deleted_at = date('d-M-y h.i.s a');

        $dbtransac = Yii::$app->db->beginTransaction();
        try {
            foreach ($modelItems as $item) {
                $item->deleted = '1';
                $item->deleted_at = date('d-M-y h.i.s a');
                if (!$item->save()) {
                    Throw new Exception(' Data berjaya dikemaskini');
                }
            }

            Inventories::updateQuantity($model->inventory_id);
            if (!$modelTransaction->save()) {
                Throw new Exception(' Data berjaya dikemaskini');
            }

            if (!$model->save()) {
                Throw new Exception(' Data tidak berjaya dihapuskan.');
            }
            Yii::$app->notify->success(' Data berjaya dihapuskan.');
            $dbtransac->commit();
        } catch (Exception $e) {
            $dbtransac->rollBack();
            Yii::$app->notify->fail(' Data tidak berjaya dihapuskan.');
        }

        return $this->redirect(['index']);
    }

    public function actionRecover($id) {
        $model = $this->findInventoriesCheckin($id);
        $modelTransaction = $this->findTransactions($model->transaction_id);
        $modelItems = InventoryItems::find()->where(['checkin_transaction_id' => $model->id])->all();

        $model->deleted = '0';
        $model->deleted_at = date('d-M-y h.i.s a');
        $modelTransaction->deleted = '0';
        $modelTransaction->deleted_at = date('d-M-y h.i.s a');

        $dbtransac = Yii::$app->db->beginTransaction();
        try {
            foreach ($modelItems as $item) {
                $item->deleted = '0';
                $item->deleted_at = date('d-M-y h.i.s a');
                $item->save();
            }

            Inventories::updateQuantity($model->inventory_id);
            $modelTransaction->save();

            if ($model->save()) {
                Throw new Exception(' Data tidak berjaya dikembalikan.');
            }
            $dbtransac->commit();
        } catch (\Exception $e) {
            $dbtransac->rollBack();
            Yii::$app->notify->fail(' Data tidak berjaya dikembalikan.');
        }
        return $this->redirect(['index']);
    }

    public function actionDeletePermanent($id) {
        $model = $this->findInventoriesCheckin($id);
        $modelTransaction = $this->findTransactions($model->transaction_id);
        $modelItems = InventoryItems::find()
                        ->select('id')
                        ->where(['checkin_transaction_id' => $model->id])
                        ->asArray()->all();
        $checkoutedItems = InventoryItems::find()
                        ->select('id')
                        ->where(['checkin_transaction_id' => $model->id])
                        ->andWhere(['not', ['checkout_transaction_id' => null]])
                        ->asArray()->all();
//        check if items is already check out or not
        if (sizeof($checkoutedItems) !== 0) {
            Yii::$app->notify->fail(' Item dalam transaksi ini telah/pernah dikeluarkan dari stor.');
            return $this->redirect(Yii::$app->request->referrer);
        }

        $dbtransac = \Yii::$app->db->beginTransaction();
        try {
            $flag = $model->delete(false);
            if ($flag = $flag && $modelTransaction->delete(false)) {
                $flag = $flag && InventoryItems::deleteAll(['in', 'id', $modelItems]);
            }
            if (!$flag) {
                throw new Exception("Data does not exist");
            }

            Yii::$app->notify->success(' Data berjaya dihapuskan.');
            $dbtransac->commit();
            return $this->redirect(['index']);
        } catch (yii\db\Exception $e) {
            $dbtransac->rollBack();
            Yii::$app->notify->fail(' Data tidak berjaya dihapuskan.');
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the InventoryItems model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return InventoryItems the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findInventoryItems($id) {
        if (($model = InventoryItems::findOne($id)) !== null) {
            return $model;
        } else {
            Yii::$app->notify->fail(' Data tidak dijumpai.');
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    protected function findInventoriesCheckin($id) {
        if (($model = InventoriesCheckIn::findOne($id)) !== null) {
            return $model;
        } else {
            Yii::$app->notify->fail(' Data tidak dijumpai.');
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    protected function findTransactions($id) {
        if (($model = Transactions::findOne($id)) !== null) {
            return $model;
        } else {
            Yii::$app->notify->fail(' Data tidak dijumpai.');
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

//    protected function findInventory($id) {
//        if (($model = Inventories::findOne($id)) !== null) {
//            return $model;
//        } else {
//            Yii::$app->notify->fail(' Data tidak dijumpai.');
//            return $this->redirect(Yii::$app->request->referrer);
//        }
//    }

}
