<?php

namespace frontend\controllers;

use Yii;
use yii\base\Model;
use common\models\Transactions;
use common\models\TransactionsSearch;
use common\models\RequestSearch;
use common\models\Orders;
use common\models\OrdersSearch;
use common\models\OrderItems;
use common\models\OrderItemsSearch;
use common\models\InventoryItems;
use common\models\InventoryItemsSearch;
use common\models\Inventories;
use common\models\InventoriesSearch;
use common\models\Vendors;
use common\models\VendorsSearch;
use common\models\Package;
use common\models\PackageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\components\AccessRule;
use kartik\widgets\Alert;
use kartik\widgets\Growl;
use yii\base\Exception;

/**
 * TransactionsController implements the CRUD actions for Transactions model.
 */
class RequestController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'delete-multiple' => ['post'],
                    'recover' => ['post'],
                    'delete-permanent' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),],
                'only' => ['create', 'update', 'index', 'view',
                    'delete', 'delete-permanent', 'recover'],
                'rules' => [
                    [
                        'actions' => ['delete-permanent'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['create', 'update', 'delete', 'recover'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Transactions models.
     * @return mixed
     */
//    public function actionIndex2() {
//        $searchModel = new TransactionsSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('index', [
//                    'searchModel' => $searchModel,
//                    'dataProvider' => $dataProvider,
//        ]);
//    }
//    in development
//    public function actionItemList2($id) {
////        $searchModel = new RequestSearch();
////        $orderItems = OrderItems::find()->where(['order_id' => $id])->all();
////        $orderItems = [0 => ['id' => 92], 1 => ['id' => 93]];
////        foreach ($orderItems as $orderItem) {
////            $orderItemIds[] = $orderItem['id'];
//////            $orderItemIds = $orderItem->id;
////        }
//////        var_dump($orderItemIds);die();
//////        $searchModel->deleted = 1;
////        $testArray = [93, 92];
////        $testArray = $orderItemIds;
////        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//////        $dataProvider->query->andWhere(['in', 'checkout_transaction_id',$testArray]);
//////        $dataProvider->query->andWhere(['not','checkout_transaction_id', null]);
//////        $dataProvider->query->andFilterWhere(['deleted'=>1]);
//////        var_dump($dataProvider->models[0]->inventory);die();
//////        die();
//////        $data = Transactions::find()->all();
//////        $data = \common\models\Orders::find()->all();
//////        var_dump($data);die();
//        $ordersId = $id;
//        $searchModel = new \common\models\ApprovalSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider->query->where(['id_orders' => $ordersId]);
////        $dataProvider->query->andWhere(['inventory_items.checkout_transaction_id' => null]);
////        $dataProvider->query->andWhere(['not',['inventory_items.checkout_transaction_id' => null]]);
////        $dataProvider->query->andWhere(['not','inventory_items.checkout_transaction_id', null]);
//        var_dump($dataProvider->models);
//        die();
//        return $this->render('item-list', [
//                    'searchModel' => $searchModel,
//                    'dataProvider' => $dataProvider,
//        ]);
//    }

    public function actionItemList($id) {
//        $ordersId = $ordersId;
        $searchModel = new \common\models\InventoryItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider->query->where(['orders.id' => $ordersId]);
        $dataProvider->query->andWhere(['inventory_items.checkout_transaction_id' => $id]);
//        $dataProvider->query->andWhere(['inventory_items.checkout_transaction_id' => null]);
//        $dataProvider->query->andWhere(['not', ['inventory_items.checkout_transaction_id' => null]]);
//        var_dump($dataProvider->models);die();
        return $this->render('item-list-grid', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
//                    'ordersId' => $ordersId,
        ]);
    }

    public function actionIndex() {
        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndex2() {
        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index2', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

//    public function actionSearch() {
//        $searchModel = new RequestSearch();
//        return $this->renderPartial('_search', [
//                    'searchModel' => $searchModel,
//        ]);
//    }

    public function actionView($id) {
        $model = $this->findOrders($id);
//        $model = $this->findModel($id);
        $searchModel = new \common\models\FormKewps10Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->filterWhere(['order_id' => $id]);
        $items = $dataProvider->models;
//        var_dump($items[0]);die();
//        $dataProvider->query->andFilterWhere(['deleted'=>1]);
        return $this->render('view', [
                    'items' => $items,
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

//    public function actionKew2() {
//        $searchModel = new RequestSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
////        $dataProvider->query->andFilterWhere(['deleted'=>1]);
//        return $this->render('_kewps10', [
////        return $this->render('_view', [
//                    'searchModel' => $searchModel,
//                    'dataProvider' => $dataProvider,
//        ]);
//    }

    public function actionPrintKewps10($id) {
        // $id = 1032;
        // Orders::pdfKewps102($id);
        \common\models\KewpsForm::pdfKewps10($id);
    }
    public function actionKew($id) {
        $model = $this->findOrders($id);
//        $model = $this->findModel($id);
        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->filterWhere(['order_id' => $id]);
        $items = $dataProvider->models;
//        $dataProvider->query->andFilterWhere(['deleted'=>1]);
        $content = $this->renderPartial('_kewps10', [
            'items' => $items,
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
//        var_dump($dataProvider);die();
//        $pdf = new \kartik\mpdf\Pdf([
//            'mode' => \kartik\mpdf\Pdf::MODE_CORE, // leaner size using standard fonts
//            'content' => $content,
//            'defaultFontSize' => $content,
////            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
////            'cssFile' => '@frontend/assets/dist/css/Pdf.css',
//            'format' => 'A4-L',
////            'format' => 'A4',
////            'defaultFont' => 'Times New Roman',
////            'orientation' => 'l',
////            'destination' => 'i',
////            'orientation' => \kartik\mpdf\Pdf::ORIENT_LANDSCAPE,
////            'orientation' => \kartik\mpdf\Pdf::ORIENT_PORTRAIT,
//            // any css to be embedded if required
////            'cssInline' => '.form-id,.form-lampiran{font-size:10px} .right-border-bold{border:10px solid #000}',
//            'filename' => 'KEW PS 10',
//            'options' => [
//                'title' => 'Borang Pemesanan Dan Pengeluaran',
////                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
//            ],
//            'methods' => [
//                'SetHeader' => ['Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran A'],
////                'SetFooter' => ['|Page {PAGENO}|'],
//            ]
////            'methods' => [
////                'SetHeader' => ['Generated By: MPSP Estor Application||Generated On: ' . date("r")],
//////                'SetFooter' => ['|Page {PAGENO}|'],
////            ]
//        ]);
        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE, // leaner size using standard fonts
            'content' => $content,
//            'defaultFontSize' => 30,
//            'defaultFont' => '',
//
            'marginLeft' => 13,
            'marginRight' => 13,
            'marginTop' => 10,
            'marginBottom' => 6,
            'marginHeader' => 5,
            'marginFooter' => 5,
//            'marginLeft' => 15,
//            'marginRight' => 15,
//            'marginTop' => 16,
//            'marginBottom' => 16,
//            'marginHeader' => 9,
//            'marginFooter' => 9,
            'cssFile' => '@frontend/assets/dist/css/Pdf.css',
            'format' => 'A4-L',
            'filename' => 'KEW PS 10.pdf',
            'options' => [
                'title' => 'Borang Pemesanan Dan Pengeluaran',
//                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
            'methods' => [
                'SetHeader' => ['Pekeliling Perbendaharaan Malaysia||AM 6.5 Lampiran A'],
                'SetFooter' => ['|{PAGENO}|'],
            ]
        ]);
//        var_dump($pdf);
//        die();
//         $pdf = new \kartik\mpdf\Pdf([
//        // set to use core fonts only
//        'mode' => \kartik\mpdf\Pdf::MODE_CORE,
//        // A4 paper format
////        'format' => \kartik\mpdf\Pdf::FORMAT_A4,
//        // portrait orientation
//        'orientation' => \kartik\mpdf\Pdf::ORIENT_LANDSCAPE,
//        // stream to browser inline
//        'destination' => \kartik\mpdf\Pdf::DEST_BROWSER,
//        // your html content input
//        'content' => $content,
//        // format content from your own css file if needed or use the
//        // enhanced bootstrap css built by Krajee for mPDF formatting
//        'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
//        // any css to be embedded if required
//        'cssInline' => '.kv-heading-1{font-size:18px}',
//         // set mPDF properties on the fly
//        'options' => ['title' => 'Krajee Report Title'],
//         // call mPDF methods on the fly
//        'methods' => [
//            'SetHeader'=>['Krajee Report Header'],
//            'SetFooter'=>['{PAGENO}'],
//        ]
//    ]);
//        var_dump(Yii::$app->response->format);die();
//        $response = Yii::$app->response;
//        $response->format = \yii\web\Response::FORMAT_RAW;
//        $headers = Yii::$app->response->headers;
//        $headers->add('Content-Type', 'application/pdf');
//        var_dump($pdf);
//        die();

        $mpdf = new \mPDF('utf-8', 'A4-L');
//        $mpdf = new \mPDF('utf-8','A4');
//        $mpdf = new \mPDF('c','A4');
        $filename = '@frontend/assets/dist/css/Pdf.css';
        $filedir = Yii::getAlias($filename);
//        $filename = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
//        $stylesheet = file_get_contents('@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css');
        $stylesheet = file_get_contents($filedir);
//        var_dump($filedir);
//        die();
//        var_dump($stylesheet);die();
//        $mpdf->writeHTML($stylesheet, 1);
//        var_dump($content);die();
//        $mpdf->writeHTML($content, 2);
//        var_dump($content);die();

        $mpdf->writeHTML($stylesheet, 1);
//        $mpdf->useDefaultCSS2 = true;
        $mpdf->writeHTML($content, 2);
//        $response = Yii::$app->response;
//        $response->format = \yii\web\Response::FORMAT_RAW;
//        $headers = Yii::$app->response->headers;
//        $headers->add('Content-Type', 'application/pdf');

        $pdf->render();
//        $mpdf->output();
        exit;

//        return $mpdf->output();
//        return $pdf->render();
    }

//    public function actionApproval2($id) {
//        $orderItemsId = $id;
////        var_dump($orderItemsId);die();
////        $debug = InventoryItems::find()->joinWith('requestOrderItems')
//////                ->where(['order_items.id'=>$id])
////                        ->asArray()->all();
//
//        $searchModel = new \common\models\ApprovalItemsSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
////        $dataProvider->query->where(['order_items.order_id' => $id]);
////        $dataProvider->query->andWhere(['checkout_transaction_id' => null]);
////        var_dump($debug);
////        die();
////        var_dump($dataProvider->models);die();
////        var_dump($dataProvider->models);die();
////        $dataProvider->query->andFilterWhere(['deleted'=>1]);
////        new test
////        $query = \common\models\InventoryItems::find();
////        $query->where(['order_id' => $id]);
////        $searchModel->leftJoin('inventories', ['inventory_id'=>1]);
////                ->rightJoin('order_items', ['id'=>$id]);
////        $dataProvider = new \yii\data\ActiveDataProvider(['query' => $query,]);
//
//        return $this->render('approval1', [
//                    'searchModel' => $searchModel,
//                    'dataProvider' => $dataProvider,
//                    'orderItemsId' => $orderItemsId,
//        ]);
//    }

    public function actionApproval($ordersId) {
        $searchModel = new \common\models\ApprovalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $ordersId);
        $dataProvider->query->orderBy('inventory_items.sku');
        if (\Yii::$app->request->post('smart-approve')) {
            $dbtransac = \Yii::$app->db->beginTransaction();
            try {
                if ($order && $orderItems && $inventory && $inventoryItems) {
                    //assign checkout id to items(link)
                    $inventoryItems->checkout_transaction_id = $orderItemsId;
                    //remove 1 items from inventory
                    $inventory->quantity = $inventory->quantity - 1;
                    //add 1 item to approved quantity
                    $orderItems->app_quantity = $orderItems->app_quantity + 1;
                    //update total price of checkout items
                    $orderItems->unit_price = $orderItems->unit_price + $inventoryItems->unit_price;
                    //save all model
                    if (!$inventoryItems->save(false))
                        Throw new Exception(' Data item inventori tidak dapat dikemaskini.');
                    if (!$inventory->save(false))
                        Throw new Exception(' Data inventori tidak dapat dikemaskini.');
                    if (!$orderItems->save(false))
                        Throw new Exception(' Data pesanan tidak dapat dikemaskini.');
                    $dbtransac->commit();
                    \Yii::$app->notify->success('Proses kemaskini data berjaya');
                } else {
                    throw new Exception(' Data tidak wujud.');
                }
            } catch (Exception $e) {
                \Yii::$app->notify->fail($e->getMessage());
                $dbtransac->rollBack();
            } catch (\yii\db\Exception $e) {
                \Yii::$app->notify->fail($e->getMessage());
                $dbtransac->rollBack();
            }
        }

        return $this->render('approval', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'ordersId' => $ordersId,
        ]);
    }

    public function actionApprove($id, $orderItemsId) {
        $inventoryItems = InventoryItems::findOne($id);
        $inventory = Inventories::findOne($inventoryItems->checkInTransaction->inventory_id);
        $orderItems = OrderItems::findOne($orderItemsId);
        $order = Orders::findOne($orderItems->order_id);

        $dbtransac = \Yii::$app->db->beginTransaction();
        try {
            if ($order && $orderItems && $inventory && $inventoryItems) {
                //assign checkout id to items(link)
                $inventoryItems->checkout_transaction_id = $orderItemsId;
                //remove 1 items from inventory
                $inventory->quantity = $inventory->quantity - 1;
                //add 1 item to approved quantity
                $orderItems->app_quantity = $orderItems->app_quantity + 1;
                //update total price of checkout items
                $orderItems->unit_price = $orderItems->unit_price + $inventoryItems->unit_price;
                //save all model
                if (!$inventoryItems->save(false))
                    Throw new Exception(' Data item inventori tidak dapat dikemaskini.');
                if (!$inventory->save(false))
                    Throw new Exception(' Data inventori tidak dapat dikemaskini.');
                if (!$orderItems->save(false))
                    Throw new Exception(' Data pesanan tidak dapat dikemaskini.');
                $dbtransac->commit();
                \Yii::$app->notify->success('Proses kemaskini data berjaya');
            } else {
                throw new Exception(' Data tidak wujud.');
            }
        } catch (Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionApproveMultiple($id, $orderItemsId) {
        $inventoryItems = InventoryItems::findOne($id);
        $inventory = Inventories::findOne($inventoryItems->checkInTransaction->inventory_id);
        $orderItems = OrderItems::findOne($orderItemsId);
        $order = Orders::findOne($orderItems->order_id);

        $inventoryItems->checkout_transaction_id = $orderItemsId;
        $inventory->quantity = $inventory->quantity - 1;
        $orderItems->app_quantity = $orderItems->app_quantity + 1;
        $orderItems->unit_price = $orderItems->unit_price + $inventoryItems->unit_price;

//        $inventoryItems->save();
//        $inventory->save();
//        $orderItems->save();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionApproveTransaction($ordersId) {
        $order = Orders::findOne($ordersId);
        //change order status to approved
        $order->approved = 1;
        $order->approved_by = Yii::$app->user->id;
        $order->approved_at = date('d-M-y h.i.s a');

        $dbtransac = Yii::$app->db->beginTransaction();
        try {
            if (!$order->save(false))
                Throw new Exception(' Data tidak dapat dikemaskini.');
            $dbtransac->commit();
        } catch (Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        }

        return $this->redirect(['index']);
    }

//    public function actionCreate2() {
//        $transaction = new Transactions();
//        $inventory = new Inventories();
//        $order = new Orders();
//        $orderItem = new OrderItems();
//
//        $inventories = \common\models\Inventories::find()->where(['deleted' => 0])->asArray()->all();
//        $inventoriesArray = \yii\helpers\ArrayHelper::map($inventories, 'id', 'description');
//        $vendors = \common\models\Vendors::find()->where(['deleted' => 0])->asArray()->all();
//        $vendorsArray = \yii\helpers\ArrayHelper::map($vendors, 'id', 'name');
//
//        if ($transaction->load(Yii::$app->request->post())) {
//            if ($transaction->save()) {
//                \Yii::$app->session->setFlash('success', [
//                    'type' => Growl::TYPE_SUCCESS,
//                    'duration' => 3000,
//                    'icon' => 'glyphicon glyphicon-ok-sign',
//                    'title' => ' Proses BERJAYA',
//                    'message' => ' Data berjaya ditambah.',
//                    'positonY' => 'top',
//                    'positonX' => 'right'
//                ]);
//            } else {
//                \Yii::$app->session->setFlash('error', [
//                    'type' => Growl::TYPE_DANGER,
//                    'duration' => 3000,
//                    'icon' => 'glyphicon glyphicon-remove-sign',
//                    'title' => ' Proses GAGAL.',
//                    'message' => ' Data tidak berjaya ditambah.',
//                    'positonY' => 'top',
//                    'positonX' => 'right'
//                ]);
//            }
//            return $this->redirect(['view', 'id' => $transaction->id]);
////return $this->redirect(['index']);
//        } else {
//            return $this->render('create', [
//                        'transaction' => $transaction,
//                        'inventory' => $inventory,
//                        'order' => $order,
//                        'orderItem' => $orderItem,
//                        'inventoriesArray' => $inventoriesArray,
//                        'vendorsArray' => $vendorsArray,
//            ]);
//        }
//    }

    public function actionCreate() {
        //create list for inventory dropdown
        $inventories = \common\models\Inventories::find()->where(['deleted' => 0])->andWhere(['not', ['quantity' => 0]])->asArray()->all();
        $inventoriesArray = \yii\helpers\ArrayHelper::map($inventories, 'id', function($model, $defaultValue) {
                    return $model['code_no'] . ' - ' . $model['description'] . ' {' . $model['quantity'] . '}';
                });
        //create list for vendor dropdown
        $vendors = \common\models\Vendors::find()->where(['deleted' => 0])->asArray()->all();
        $vendorsArray = \yii\helpers\ArrayHelper::map($vendors, 'id', 'name');
        //create list for vehicle dropdown
        $vehicles = \common\models\VehicleList::find()->where(['deleted' => 0])->asArray()->all();
        $vehiclesArray = \yii\helpers\ArrayHelper::map($vehicles, 'id', ['reg_no'], 'type');
        //itterate bill no
        $bilStock = Orders::find()
                ->where(['EXTRACT(MONTH FROM CREATED_AT)' => date('m')])
                ->andWhere(['EXTRACT(YEAR FROM CREATED_AT)' => date('y')])
                ->max('CAST(SUBSTR(ORDER_NO,9) AS INT)');
        $bilStock = $bilStock + 1;
        $inventory = new Inventories();
        $transaction = new Transactions();
        $order = new Orders(['scenario' => 'checkout']);
        $orderItems = [new OrderItems(['scenario' => 'checkout'])];
        //**need to check for orderItems to before proceed
        if ($order->load(Yii::$app->request->post())) {
            //set transaction detail for checkin
            $transaction->type = 2;     //checkIn
            $transaction->check_date = date('d-M-Y');
            $transaction->check_by = Yii::$app->user->id;
            //itterate bill no
            $bilStock = Orders::find()
                    ->where(['EXTRACT(MONTH FROM CREATED_AT)' => date('m')])
                    ->andWhere(['EXTRACT(YEAR FROM CREATED_AT)' => date('y')])
                    ->max('CAST(SUBSTR(ORDER_NO,9) AS INT)');
            $bilStock = $bilStock + 1;
            //assign bill no to order no
            $order->order_no = date('Y/m') . '-' . $bilStock;
            //change order status to pending
            $order->approved = 2;
            //create multiple order items model and load with data
            $orderItems = \common\models\Model::createMultiple(OrderItems::classname(['scenario' => 'checkout']));
//            foreach ($orderItems as $orderItem) {
//                $orderItem->scenario = 'checkout';
//            }
            Model::loadMultiple($orderItems, Yii::$app->request->post());
            //validate all model
            $valid = $transaction->validate();
            $valid = $order->validate();
            $valid = Model::validateMultiple($orderItems) && $valid;
            if ($valid) {
                $dbtransac = \Yii::$app->db->beginTransaction();

                try {
//                    var_dump($transaction->id);
                    if (!$transaction->save(false)) {
                        Throw new Exception(' Detail transaksi tidak dapat disimpan.');
                    }
//                    var_dump($transaction->id);
                    $order->transaction_id = $transaction->id;
//                    var_dump($order->id);
                    if (!$order->save(false)) {
                        Throw new Exception(' Detail pesanan tidak dapat disimpan.');
                    }
//                    var_dump($order->id);die();
                    foreach ($orderItems as $orderItem) {
                        $inventory = Inventories::findOne($orderItem->inventory_id);

                        $orderItem->order_id = $order->id;
                        $orderItem->current_balance = $inventory->quantity;
                        if (!$orderItem->save(false))
                            Throw new Exception(' Detail item pesanan tidak dapat disimpan.');
                    }

                    $dbtransac->commit();
                    return $this->redirect(['index']);
                } catch (Exception $e) {
                    \Yii::$app->notify->fail($e->getMessage());
                    $dbtransac->rollBack();
                } catch (\yii\db\Exception $e) {
                    \Yii::$app->notify->fail($e->getMessage());
                    $dbtransac->rollBack();
                }
            } else {
                \Yii::$app->notify->fail(' Validasi data gagal. Sila pastikan data yang dimasukkan.');
            }
        }
        return $this->render('create', [
                    'transaction' => $transaction,
                    'vehiclesArray' => $vehiclesArray,
                    'inventoriesArray' => $inventoriesArray,
                    'order' => $order,
                    'bilStock' => $bilStock,
                    'orderItems' => (empty($orderItems)) ? [new OrderItems] : $orderItems,
        ]);
    }

    /**
     * Updates an existing Transactions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $orderItems = $this->findOrderItems($id);
        $order = $this->findOrders($orderItems->order_id);

        $inventories = \common\models\Inventories::find()->where(['deleted' => 0])->asArray()->all();
        $inventoriesArray = \yii\helpers\ArrayHelper::map($inventories, 'id', function($model, $defaultValue) {
                    return $model['code_no'] . ' - ' . $model['description'];
                });
        $vendors = \common\models\Vendors::find()->where(['deleted' => 0])->asArray()->all();
        $vendorsArray = \yii\helpers\ArrayHelper::map($vendors, 'id', 'name');
        $vehicles = \common\models\VehicleList::find()->where(['deleted' => 0])->asArray()->all();
        $vehiclesArray = \yii\helpers\ArrayHelper::map($vehicles, 'id', ['reg_no'], 'type');

        if ($order->load(Yii::$app->request->post())) {
            if ($order->save()) {
                \Yii::$app->notify->success(' Data berjaya dikemaskini.');
                return $this->redirect(['view', 'id' => $order->id]);
            } else {
                \Yii::$app->notify->fail(' Data tidak berjaya dikemaskini.');
            }
        }
        return $this->render('update', [
                    'order' => $order,
                    'vehiclesArray' => $vehiclesArray,
                    'inventoriesArray' => $inventoriesArray,
        ]);
    }

    /**
     * Reject an existing Transactions model.
     * If rejection is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionReject($id) {
        $orderItem = $this->findOrderItems($id);
        $order = $this->findOrders($orderItem->order_id);
        $transaction = $this->findTransaction($order->transaction_id);
        $orderItems = $this->findAllOrderItems($orderItem->order_id);
        $inventoryItems = InventoryItems::find()->where(['checkout_transaction_id' => $orderItem->id])->all();

        if (empty($order) || empty($transaction) || empty($orderItems)) {
            \Yii::$app->notify->fail(' Data tidak berjaya ditemui.');
            $this->redirect(Yii::$app->request->referrer);
        }
        $dbtransac = \Yii::$app->db->beginTransaction();

        try {
            $flag = true;
            foreach ($orderItems as $item) {
                if (!empty($inventoryItems)) {
                    $flag = InventoryItems::updateAll(['checkout_transaction_id' => null], ['and',
                                ['deleted' => 0],
                                ['checkout_transaction_id' => $item->id],
                    ]);
                }
                $item->app_quantity = null;
                $item->current_balance = $item->current_balance + $item->app_quantity;
                $item->unit_price = null;
                if ($flag = $flag && $item->save(false)) {
                    Inventories::updateQuantity($item->inventory_id);
                } else {
                    Throw new Exception(' Data tidak dapat dikemaskini.');
                }
            }
            $order->arahan_kerja_id = null;
            $order->approved = 8;
            $order->approved_at = date('d-M-y h.i.s a');

            if (!$flag || !$order->save(false) || !$transaction->save(false))
                Throw new Exception(' Data tidak dapat dikemaskini.');
            $dbtransac->commit();
            \Yii::$app->notify->success(' Data berjaya dikemaskini.');
        } catch (Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Deletes an existing Transactions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $orderItem = $this->findOrderItems($id);
        $order = $this->findOrders($orderItem->order_id);
        $transaction = $this->findTransaction($order->transaction_id);
        $orderItems = $this->findAllOrderItems($orderItem->order_id);
//        $inventoryItems = InventoryItems::find()->where(['checkout_transaction_id' => $orderItem->id])->all();
//        $orderItems = OrderItems::findAllByOrderItemId($id);
//       $order = $this->findOrders($orderItem->order_id);
//        $transaction = $this->findTransaction($order->transaction_id);
        $inventoryItems = InventoryItems::findByOrderItemId($id);

        if (empty($order) || empty($transaction) || empty($orderItems)) {
            \Yii::$app->notify->fail(' Data tidak berjaya ditemui.');
            $this->redirect(Yii::$app->request->referrer);
        }

        $dbtransac = \Yii::$app->db->beginTransaction();
        try {
            $flag = true;
            foreach ($orderItems as $item) {
                if (!empty($inventoryItems)) {
                    $flag = InventoryItems::updateAll(['checkout_transaction_id' => null], ['and',
                                ['deleted' => 0],
                                ['checkout_transaction_id' => $item->id],
                    ]);
                }
                $item->app_quantity = null;
                $item->current_balance = $item->current_balance + $item->app_quantity;
                $item->deleted = 1;
                $item->deleted_at = date('d-M-y h.i.s a');

                if (!$flag = $flag && $item->save(false))
                    Throw new Exception(' Data tidak berjaya dikemaskini.');
                Inventories::updateQuantity($item->inventory_id);
            }
            $order->arahan_kerja_id = null;
            $order->deleted = 1;
            $order->deleted_at = date('d-M-y h.i.s a');
            $transaction->deleted = 1;
            $transaction->deleted_at = date('d-M-y h.i.s a');

            if (!$flag = $flag && $order->save(false) && $transaction->save(false))
                Throw new Exception(' Data tidak berjaya dikemaskini.');
            $dbtransac->commit();
            \Yii::$app->notify->success(' Data berjaya dikemaskini.');
        } catch (Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionRecover($id) {
//        var_dump(date('d-M-y h.i.s a'));
//        die();
        $orderItem = $this->findOrderItems($id);
        $order = $this->findOrders($orderItem->order_id);
        $transaction = $this->findTransaction($order->transaction_id);
        $orderItems = $this->findAllOrderItems($orderItem->order_id);
//        $orderItems = OrderItems::findOne($id);
        $inventoryItems = InventoryItems::find()->where(['checkout_transaction_id' => $orderItem->id])->all();
        if (empty($order) || empty($transaction)) {
            \Yii::$app->notify->fail(' Data tidak berjaya ditemui.');
            $this->redirect(Yii::$app->request->referrer);
        }
        $dbtransac = \Yii::$app->db->beginTransaction();
        try {
            $flag = true;
            if (!empty($orderItems)) {
                foreach ($orderItems as $item) {
                    if (!empty($inventoryItems)) {
                        $flag = InventoryItems::updateAll(['checkout_transaction_id' => null], ['and',
                                    ['deleted' => 1],
                                    ['checkout_transaction_id' => $item->id],
                        ]);
                    }
                    $item->current_balance = $item->current_balance + $item->app_quantity;
                    $item->app_quantity = null;
                    $item->deleted = 0;
                    $item->deleted_at = date('d-M-y h.i.s a');
                    if (!$flag = $flag && $item->save(false))
                        Throw new Exception(' Data tidak berjaya dikemaskini.');
                    Inventories::updateQuantity($item->inventory_id);
                }
            }
            $order->deleted = 0;
            $order->deleted_at = date('d-M-y h.i.s a');
            $transaction->deleted = 0;
            $transaction->deleted_at = date('d-M-y h.i.s a');
            $order->arahan_kerja_id = '';
            if (!$flag = $flag && $order->save(false))
                Throw new Exception(' Data Permohonan tidak berjaya dikemaskini.');
            if (!$flag = $flag && $transaction->save(false))
                Throw new Exception(' Data Transaksi tidak berjaya dikemaskini.');
            $dbtransac->commit();
            \Yii::$app->notify->success(' Data berjaya dikemaskini.');
        } catch (Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionDeletePermanent($id) {
        $orderItem = $this->findOrderItems($id);
        $orderItems = OrderItems::find()->where(['order_id' => $orderItem->order_id])->all();
        $order = $this->findOrders($orderItem->order_id);
        $transaction = $this->findTransaction($order->transaction_id);
        $dbtransac = Yii::$app->db->beginTransaction();
        try {
            $flag = true;
            if ($order->approved == 8 || $order->deleted == 1) {
                if (!empty($orderItems))
                    $flag = OrderItems::deleteAll(['order_id' => $orderItem->order_id], ['app_quantity' => 0]);
                if (!$flag || !$order->delete() || !$transaction->delete())
                    Throw new Exception(' Data tidak berjaya dipadam.');
            } else {
                Throw new Exception(' Data masih diperlukan. Hanya transaksi yang telah DIPADAM/DITOLAK sahaja boleh dipadam secara kekal.');
            }
            $dbtransac->commit();
            \Yii::$app->notify->success(' Data berjaya dikemaskini.');
        } catch (Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        } catch (\yii\db\Exception $e) {
            \Yii::$app->notify->fail($e->getMessage());
            $dbtransac->rollBack();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Transactions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Transactions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = OrderItems::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findOrderItems($id) {
        if (($model = OrderItems::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findAllOrderItems($orderId) {
        if (($model = OrderItems::find()->where(['order_id' => $orderId])->all()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findOrders($id) {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findTransaction($id) {
        if (($model = Transactions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
