<?php

namespace frontend\controllers;

use Yii;
use yii\base\Model;
use common\models\LaporanPenyelengaraanSisken;
use common\models\LaporanPenyelengaraanSiskenSearch;
use common\models\Transactions;
use common\models\TransactionsSearch;
use common\models\RequestSearch;
use common\models\Orders;
use common\models\OrdersSearch;
use common\models\OrderItems;
use common\models\OrderItemsSearch;
use common\models\InventoryItems;
use common\models\InventoryItemsSearch;
use common\models\Inventories;
use common\models\InventoriesSearch;
use common\models\Vendors;
use common\models\VendorsSearch;
use common\models\Package;
use common\models\PackageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\components\AccessRule;
use kartik\widgets\Alert;
use kartik\widgets\Growl;

/**
 * LaporanPenyelengaraanSiskenController implements the CRUD actions for LaporanPenyelengaraanSisken model.
 */
class LaporanPenyelengaraanSiskenController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'delete-multiple' => ['post'],
                    'recover' => ['post'],
                    'delete-permanent' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),],
                'only' => ['create', 'update', 'index', 'view',
                    'delete', 'delete-permanent', 'recover'],
                'rules' => [
                    [
                        'actions' => ['delete-permanent'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['create', 'update', 'delete', 'recover'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all LaporanPenyelengaraanSisken models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new LaporanPenyelengaraanSiskenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAttend($id) {
        $arahanKerja = $this->findModel($id);

        $inventories = \common\models\Inventories::find()->where(['deleted' => 0])->andWhere(['not', ['quantity' => 0]])->asArray()->all();
        $inventoriesArray = \yii\helpers\ArrayHelper::map($inventories, 'id', function($model, $defaultValue) {
                    return $model['code_no'] . ' - ' . $model['description'] . ' {' . $model['quantity'] . '}';
                });
        $vendors = \common\models\Vendors::find()->where(['deleted' => 0])->asArray()->all();
        $vendorsArray = \yii\helpers\ArrayHelper::map($vendors, 'id', 'name');
        $vehicles = \common\models\VehicleList::find()->where(['deleted' => 0])->asArray()->all();
        $vehiclesArray = \yii\helpers\ArrayHelper::map($vehicles, 'id', ['reg_no'], 'type');
//        $vehiclesArray = \yii\helpers\ArrayHelper::map($vehicles, 'reg_no', 'reg_no');
//        var_dump($vehiclesArray);die();
//        $usageList = ArrayHelper::map(VehicleList::find()->all(), 'reg_no', ['reg_no'], 'type');
        $bilStock = Orders::find()
                ->where(['EXTRACT(MONTH FROM CREATED_AT)' => date('m')])
                ->andWhere(['EXTRACT(YEAR FROM CREATED_AT)' => date('y')])
                ->max('CAST(SUBSTR(ORDER_NO,9) AS INT)');
        $bilStock = $bilStock + 1;
//        $model = new Stocks();
//        $modelItems = [new StockItems];

        $inventory = new Inventories();
        $transaction = new Transactions();
        $order = new Orders();
//        $order->vehicle_id = $arahanKerja->id_kenderaan;
        $orderItems = [new OrderItems];
//        var_dump($arahanKerja->id_kenderaan);
//        die();

        if ($order->load(Yii::$app->request->post())) {
//        if ($order->load(Yii::$app->request->post()) && $orderItems->load(Yii::$app->request->post())) {
//            var_dump(Yii::$app->request->post());
//            die();

            $transaction->type = 2;     //checkIn
            $transaction->check_date = date('d-M-Y');
            $transaction->check_by = Yii::$app->user->id;
            // order link to trans
            $bilStock = Orders::find()
                    ->where(['EXTRACT(MONTH FROM CREATED_AT)' => date('m')])
                    ->andWhere(['EXTRACT(YEAR FROM CREATED_AT)' => date('y')])
                    ->max('CAST(SUBSTR(ORDER_NO,9) AS INT)');
            $bilStock = $bilStock + 1;
            $order->order_no = date('Y/m') . '-' . $bilStock;
            $order->approved = 2;
            $order->arahan_kerja_id = $id;
//            $orderItems = Inventories::find()->
            $orderItems = \common\models\Model::createMultiple(OrderItems::classname());
            Model::loadMultiple($orderItems, Yii::$app->request->post());

            // validate all models
//            $valid = $transaction->validate();
            $valid = $order->validate();
            $valid = Model::validateMultiple($orderItems) && $valid;

            if ($valid) {
                $dbTransaction = \Yii::$app->db->beginTransaction();

                try {
                    if ($flag = $transaction->save(false)) {
                        $order->transaction_id = $transaction->id;
                        $order->save(false);
//                        if (!($flag = $order->save(false))) {
//                            $dbTransaction->rollBack();
//                            break;
//                        }
                        foreach ($orderItems as $orderItem) {
                            // orderItems link to order
                            // orderItems current bal

                            $inventory = Inventories::findOne($orderItem->inventory_id);

                            $orderItem->order_id = $order->id;
                            $orderItem->current_balance = $inventory->quantity;
//                            var_dump($order->id);die();
//                            $balance = EstorInventories::find()->where(['id' => $modelItem->inventory_id])
//                                            ->select('quantity')->asArray()->one();
//                            $modelItem->current_balance = $balance['quantity'];
                            if (!($flag = $orderItem->save(false))) {
                                $dbTransaction->rollBack();
                                break;
                            }
                        }
                    }

                    if ($flag) {
                        $dbTransaction->commit();
                        return $this->redirect(['index']);
                    }
                } catch (Exception $e) {
                    $dbTransaction->rollBack();
                }
            }
        }
        return $this->render('attend', [
                    'arahanKerja' => $this->findModel($id),
                    'transaction' => $transaction,
                    'vehiclesArray' => $vehiclesArray,
                    'inventoriesArray' => $inventoriesArray,
                    'order' => $order,
                    'bilStock' => $bilStock,
                    'orderItems' => (empty($orderItems)) ? [new OrderItems] : $orderItems,
        ]);
    }

    /**
     * Displays a single LaporanPenyelengaraanSisken model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
        //return $this->redirect(['index']);
    }

    /**
     * Creates a new LaporanPenyelengaraanSisken model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new LaporanPenyelengaraanSisken();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', [
                    'type' => Growl::TYPE_SUCCESS,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-ok-sign',
                    'title' => ' Proses BERJAYA',
                    'message' => ' Data berjaya ditambah.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            } else {
                \Yii::$app->session->setFlash('error', [
                    'type' => Growl::TYPE_DANGER,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-remove-sign',
                    'title' => ' Proses GAGAL.',
                    'message' => ' Data tidak berjaya ditambah.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            return $this->redirect(['view', 'id' => $model->id]);
            //return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LaporanPenyelengaraanSisken model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                \Yii::$app->session->setFlash('success', [
                    'type' => Growl::TYPE_SUCCESS,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-ok-sign',
                    'title' => ' Proses BERJAYA',
                    'message' => ' Data berjaya dikemaskini.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            } else {
                \Yii::$app->session->setFlash('error', [
                    'type' => Growl::TYPE_DANGER,
                    'duration' => 3000,
                    'icon' => 'glyphicon glyphicon-remove-sign',
                    'title' => ' Proses GAGAL.',
                    'message' => ' Data tidak berjaya dikemaskini.',
                    'positonY' => 'top',
                    'positonX' => 'right'
                ]);
            }
            return $this->redirect(['view', 'id' => $model->id]);
            //return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LaporanPenyelengaraanSisken model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->deleted = '1';
        if ($model->save()) {
            \Yii::$app->session->setFlash('success', [
                'type' => Growl::TYPE_SUCCESS,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-ok-sign',
                'title' => ' Proses BERJAYA',
                'message' => ' Data berjaya dihapuskan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        } else {
            \Yii::$app->session->setFlash('error', [
                'type' => Growl::TYPE_DANGER,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-remove-sign',
                'title' => ' Proses GAGAL.',
                'message' => ' Data tidak berjaya dihapuskan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        }

        return $this->redirect(['index']);
    }

    public function actionRecover($id) {
        $model = $this->findModel($id);
        $model->deleted = '0';
        if ($model->save()) {
            \Yii::$app->session->setFlash('success', [
                'type' => Growl::TYPE_SUCCESS,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-ok-sign',
                'title' => ' Proses BERJAYA',
                'message' => ' Data berjaya dikembalikan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        } else {
            \Yii::$app->session->setFlash('error', [
                'type' => Growl::TYPE_DANGER,
                'duration' => 3000,
                'icon' => 'glyphicon glyphicon-remove-sign',
                'title' => ' Proses GAGAL.',
                'message' => ' Data tidak berjaya dikembalikan.',
                'positonY' => 'top',
                'positonX' => 'right'
            ]);
        }
        return $this->redirect(['index']);
    }

    public function actionDeletePermanent($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LaporanPenyelengaraanSisken model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return LaporanPenyelengaraanSisken the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = LaporanPenyelengaraanSisken::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
