<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="mmh2050">
<!--    <link rel="icon" href="../../favicon.ico">-->

    <title>eCuti Management System</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASEDIR; ?>engine/view/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo BASEDIR; ?>engine/view/plugin/bootstrap/css/bootstrap-theme.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo BASEDIR; ?>engine/view/css/signin.css" rel="stylesheet">

    <script src="<?php echo BASEDIR; ?>engine/view/plugin/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo BASEDIR; ?>engine/view/js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="<?php echo BASEDIR; ?>engine/view/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?php echo BASEDIR; ?>engine/view/js/jquery.form.js" type="text/javascript"></script>
    <script type="text/javascript"> 
        $(document).ready(function(){   
              $('#respond').hide();
               $('#reseter').click(function()
               {
                    $('#respond').hide();
                   
                
               });//return form value to null
               
               
               
               $("#daftar").validate
               ({
			rules: {
				
				pwd: {
					required: true,
					minlength: 8
				},
				ulangpwd: {
					required: true,
					minlength: 8,
					equalTo: "#pwd"
                                }
			},
			messages: {
				
				pwd: {
					required: "Masukkan katalaluan anda",
					minlength: "Katalaluan mesti melebihi 8 aksara"
				},
				ulangpwd: {
					required: "Ulang katalaluan",
					minlength: "Katalaluan mesti melebihi 8 aksara",
					equalTo: "Katalaluan tidak sepadan"
				}
			}
                
                
                
                        });
               
               jQuery("#reseter").click(function() {
			$("#daftar").resetForm();
		});
                
              });//main document ready
       </script>  
        
        

   
  </head>


  
  <body>

    <div class="container">
<div class="panel panel-default" >
    <div class="panel-heading">
 <h1 class="panel-title">Sistem eCuti | Daftar</h1>

</div><div class="panel-body">
 <a href="index.php">   
     <img src ="<?php echo BASEDIR; ?>engine/view/img/header.png" class="list-group-item img-responsive" draggable="false"></a>
           
   
    
    
     
        <form id="daftar" method="post">
            
            
            <div class="alert alert-info" id="respond" ></div><!--alert-->
                              
<!--            <a href="index.php"><h8 style="margin-top: 10px;margin-bottom: 5px;"><-- Kembali ke menu utama</h8> </a>               -->
<h4 style="margin-top: 50px;margin-bottom: 20px;"><a href="index.php"><span class="glyphicon glyphicon-home" title="Kembali ke halaman UTAMA"></span></a> | Pendaftaran Kali Pertama</h4>
            <div class=" table-responsive">
            <table class="table  table-hover table-condensed" >
                    		
					  <!-- CONTOH TEXT FIELD -->
					<tr>
                      <td height="27" class="style20 style30">Nama Penuh</td>
                      <td>: <font size="2" face="Arial"><strong><font face="Tahoma"><strong><font face="Tahoma"><strong><font face="Tahoma"><strong><font size="2" face="Tahoma">
                                          <input type="text" name="namastaf" size=20 title="Isikan nama anda" required>  <!--      <-- DI SINI      -->
                     </font></strong></font></strong></font></strong></font></strong></font></td>
                    </tr>
					
					
					
					
					
					
                    <tr>
                      <td height="27" class="style20 style30">No. Staf</td>
                      <td>: <font size="2" face="Arial"><strong><font face="Tahoma"><strong><font face="Tahoma"><strong><font face="Tahoma"><strong><font size="2" face="Tahoma">
                      <input type="text" name="idstaf" size=20 title="Isikan id anda" required>
                     </font></strong></font></strong></font></strong></font></strong></font></td>
                    </tr>
                   
				   
				   
				   <!-- CONTOH PASSWORD FIELD -->
                    <tr>
                      <td width="98" height="24" class="style31">Katalaluan</td>
                      <td width="494">:<font size="2" face="Arial">
                        <input type="password" NAME="pwd" size="20" id="pwd" title="Isikan katalaluan anda" required></tr>  <!--      <--DI SINI     -->
						
						
						
						
						
						 <tr>
                      <td width="98" height="24" class="style31">Ulang Katalaluan</td>
                      <td width="494">:<font size="2" face="Arial">
                      <input type="password" NAME="ulangpwd" id="ulangpwd" size="20" title="Ulang katalaluan anda" required></tr>
						
						
						
						<tr>
                      <td height="27" class="style20 style30">Jabatan</td>
                      <td>: <font size="2" face="Arial"><strong><font face="Tahoma"><strong><font face="Tahoma"><strong><font face="Tahoma"><strong><font size="2" face="Tahoma">
                      
					    <!-- CONTOH DROP DOWN MENU -->
                                            <select name="jabatan" id="jabatan" title="Pilih JABATAN anda" required>
                                              <option></option>
                                              
                                              
                                         <?php      
                                          
                                         
                                         foreach ($this->senaraiDept as $row) 
                                         {
                                           echo '<option value="'.$row['dept_id'].'">'.$row['dept_name'].'</option>';   
                                         }
					 
					 
                                         ?>
                        
					 </select>
					  
					    <!-- TAMAT DROP DOWN MENU -->
                     </font></strong></font></strong></font></strong></font></strong></font></td>
                    </tr>
					
					<tr>
                      <td height="27" class="style20 style30">Jawatan</td>
                      <td>: <font size="2" face="Arial"><strong><font face="Tahoma"><strong><font face="Tahoma"><strong><font face="Tahoma"><strong><font size="2" face="Tahoma">
                                          <select name="jawatan" id="jawatan" title="Pilih JAWATAN anda" required>
					  
                       <option></option>
				
					  
					  </select>
                     </font></strong></font></strong></font></strong></font></strong></font></td>
                    </tr>
						
						<tr>
                      <td height="27" class="style20 style30">E-Mail</td>
                      <td>: <font size="2" face="Arial"><strong><font face="Tahoma"><strong><font face="Tahoma"><strong><font face="Tahoma"><strong><font size="2" face="Tahoma">
                      <input type="text" name="staffemel" size=20 data-rule-required="true" data-rule-email="true" data-msg-required="Emel diperlukan" data-msg-email="Masukkan emel yang sah!" required>
                     </font></strong></font></strong></font></strong></font></strong></font></td>
                    </tr>
						
						
                        <b>
                            <tr ><td width="180px"></td><td align="left" >
                                
                                <div class="btn-group btn-group-sm" style="padding-left: 0px;padding-top:1px">
        <button type="submit" name="submitRegister" id="submitRegister"  class="btn btn-default"><span class="glyphicon glyphicon-log-in"></span>  Daftar </button>
        <button  type="button" id="reseter" value="Reset" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span>  Reset </button>
        </div>
                                
                                
                                
                                </td></tr>
                      
            </table> </div>
        
      </form>
    
</div></div><!-- /panel-->

    </div> <!-- /container -->

  <script type="text/javascript">
$(document).ready(function() {
    
    $('#jabatan').change(function(){
    
    var deptid = $(this).val();
   
    $("#jawatan").empty();
    
  
     $.ajax({
                           url :'call/jawatanListByDept',
                           type: 'POST',
                           data: 'deptid='+deptid,
                           dataType: 'JSON',
                           cache: false,
                           
                            success: function(data,status,xhr)
                            {
                                 
                               $.each(data.result,function()
                               {
                                   
                                   $("#jawatan").append("<option value=\""+this["p_id"]+"\" >"+this["p_name"]+"</option>");
                               });
                              
                           }
                     });
  
    });
    
    
    
  });
   </script>
  </body>
</html>

