
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="mmh2050">
<!--    <link rel="icon" href="../../favicon.ico">-->

    <title>eCuti Management System</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASEDIR; ?>engine/view/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo BASEDIR; ?>engine/view/plugin/bootstrap/css/bootstrap-theme.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo BASEDIR; ?>engine/view/css/signin.css" rel="stylesheet">

    <script src="<?php echo BASEDIR; ?>engine/view/plugin/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo BASEDIR; ?>engine/view/js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="<?php echo BASEDIR; ?>engine/view/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?php echo BASEDIR; ?>engine/view/js/jquery.form.js" type="text/javascript"></script>
    <script type="text/javascript"> 
        $(document).ready(function(){   
              $('#respond').hide();
               $('#reseter').click(function()
               {
                    $('#respond').hide();
                   $('#staffno').val('');
               
                   
                
               });//return form value to null
               
               
               
              // $("#lupa").validate();
               
             
                
              });//main document ready
       </script>  
        
        

   
  </head>
  
  <body>

    <div class="container">
<div class="panel panel-default" >
    <div class="panel-heading">
 <h1 class="panel-title">Sistem eCuti | Reset Katalaluan </h1>

</div><div class="panel-body">
 <a href="index.php">   
     <img src ="<?php echo BASEDIR; ?>engine/view/img/header.png" class="img-responsive" draggable="false"></a>
           
   
    
    <div class="list-group-item" style="margin-top: 30px">
     
        <form id="lupa" method="post">
            
            
            <div class="alert alert-info" id="respond" ></div><!--alert-->
                              
            <h4 style="margin-top: 5px;"><a href="index.php"><span class="glyphicon glyphicon-home" title="Kembali ke halaman UTAMA"></span></a> | Reset Katalaluan</h4>             
              
            <div align="center" style="padding-bottom: 50px;">
                <label style="padding-top: 50px;" >Masukkan No. Staf anda</label>
            <label for="text" class="sr-only">Masukkan No. Staf anda</label>
        <div class="input-group" style="max-width: 190px;" > <span class="input-group-addon" ><span class="glyphicon glyphicon-credit-card"></span></span>
            <input  type="text" name="idstaf" id="idstaf" class="form-control" placeholder="No. Staf" required autofocus>
          
        </div>  <button  type="submit" name="submitx" id="submit"  class="btn btn-default"><span class="glyphicon glyphicon-log-in"></span>  Hantar</button>
      
            
             
            </div>
        
        </form></div>
    
</div></div><!-- /panel-->

    </div> <!-- /container -->


    
  </body>
</html>
