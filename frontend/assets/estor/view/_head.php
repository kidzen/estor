<html>
    <head>

<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="mmh2050">
<title>Selamat Datang Ke Sistem Pengurusan Cuti MIS</title>


<!--  CODE UNTUK DATEPICKER----LOKASI :HEADER ------------------------------------------------------>

<link href="<?php echo BASEDIR; ?>engine/view/js/jquery-ui-1.11.4.custom/jquery-ui.css" rel="stylesheet">
<link href="<?php echo BASEDIR; ?>engine/view/js/jquery-ui-1.11.4.custom/jquery-ui.theme.css" rel="stylesheet">
<link href="<?php echo BASEDIR; ?>engine/view/plugin/popup/pop.css" rel="stylesheet">
<link href="<?php echo BASEDIR; ?>engine/view/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASEDIR; ?>engine/view/plugin/bootstrap/css/bootstrap-theme.css" rel="stylesheet">
<link href="<?php echo BASEDIR; ?>engine/view/css/main.css" rel="stylesheet">
 <!-- DATA TABLES -->
<!-- <link href="DataTables/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />-->
<!-- <link href="DataTables/media/css/jquery.dataTables_themeroller.css" rel="stylesheet" type="text/css" />-->


 <link href="<?php echo BASEDIR; ?>engine/view/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
 <link href="<?php echo BASEDIR; ?>engine/view/plugin/DataTables/extensions/ColReorder/css/dataTables.colReorder.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo BASEDIR; ?>engine/view/plugin/DataTables/extensions/ColVis/css/dataTables.colVis.css" rel="stylesheet" type="text/css" />
<link href="<?php echo BASEDIR; ?>engine/view/plugin/DataTables/extensions/TableTools/css/dataTables.tableTools.min.css" rel="stylesheet" type="text/css" />
<!--<link href="css/style.default.css" rel="stylesheet" type="text/css" />-->


<script src="<?php echo BASEDIR; ?>engine/view/js/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="<?php echo BASEDIR; ?>engine/view/js/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo BASEDIR; ?>engine/view/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo BASEDIR; ?>engine/view/js/jquery.form.js" type="text/javascript"></script>
<script src="<?php echo BASEDIR; ?>engine/view/js/jquery.knob.min.js" type="text/javascript"></script>
<script src="<?php echo BASEDIR; ?>engine/view/plugin/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo BASEDIR; ?>engine/view/plugin/popup/pop.min.js" type="text/javascript"></script>
<script src="<?php echo BASEDIR; ?>engine/view/plugin/moment/moment.min.js" type="text/javascript"></script>
<script src="<?php echo BASEDIR; ?>engine/view/js/app.js" type="text/javascript"></script>
<script src="<?php echo BASEDIR; ?>engine/view/plugin/loader/nanobar.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function ()
{
 var nanobar = new Nanobar({bg:'#D44B47'});
 nanobar.go(30); 
 nanobar.go(100); 
 });
 </script>


<!-- DATA TABES SCRIPT -->
<script src="<?php echo BASEDIR; ?>engine/view/plugin/DataTables/media/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo BASEDIR; ?>engine/view/plugin/DataTables/extensions/ColReorder/js/dataTables.colReorder.min.js" type="text/javascript"></script>
<script src="<?php echo BASEDIR; ?>engine/view/plugin/DataTables/extensions/ColVis/js/dataTables.colVis.js" type="text/javascript"></script>
<script src="<?php echo BASEDIR; ?>engine/view/plugin/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
<script src="<?php echo BASEDIR; ?>engine/view/js/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
    
     $(function () {
          
          $('#tableCuti tbody TD').click( function () {
		         // Get the position of the current data from the node
		         var aPos = cutiTab.fnGetPosition( this );
		 
		        // Get the data array for this row
		        var aData = cutiTab.fnGetData( aPos[0] );
                        
                       // alert(aData[ aPos[1] ]);
		       });
          
          
          
        var cutiTab =$('#tableCuti').dataTable({

                        "jQueryUI": false,
                       "dom":  '<"domCutiHeader" Rlfr>rt<"bottom"pi><"clear">',
                        "pagingType": "full_numbers",
                        "lengthMenu": [[10, 20, 50,100, -1], [10, 20, 50,100, "Semua"]],
                         "tableTools":{
                                        "sRowSelect": "single"
                                     },
                        "language": {
                            "paginate": {
				      "next": ">",
                                      "first": "<<",
                                      "last": ">>",
                                      "previous": "<"
				     },
                                      "sSearch": "<span style='font-weight: bold;float:right;padding-left:5px;'></span>",
                                      "sZeroRecords": "<span style='font-weight: bold;'>Tiada rekod ditemui</span>",
                                      "info": "<span style='font-weight: bold;'>Halaman _PAGE_ dari _PAGES_</span>",
                                      "sLengthMenu": "_MENU_",
                                      "sInfoFiltered": "  (tapisan dari <span style='font-weight: bold;'> _MAX_ </span> rekod keseluruhan)"
                                     },
                        
                        "emptyTable": "Tiada data",
                        "infoEmpty": "Tiada entri",
                        "bPaginate": false,
                        "bLengthChange": true,
                        "bFilter": true,
                        "bSort": true,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "stateSave": false
//                        "columnDefs": [
//                                        { "orderData": [ 2 ], "targets": [ 1 ] }
//		         ]
		    } );
                    
               
                    
                   // alert($('.sFilterInput').attr('type'));
                    
                    cutiTab.$('tr:odd').css('backgroundColor', '#FFFFFF');//highlight setiap second row
                   // cutiTab.$('tr:event').css('backgroundColor', 'yellow');//highlight setiap first row
                   
                    $(window).bind('resize', function ()
                    {
		        cutiTab.fnAdjustColumnSizing();
		    } );
                    
//                    cutiTab.$('tr').click( function () {
//		         var sData = cutiTab.fnGetData( this );
//		         alert( sData[0] );
//		       } );
                    //cutiTab.fnDestroy();
                    //cutiTab.fnFilter( 'lulus' );
                    $('#tableCuti_filterx').attr('placeholder','Carian');
                    //alert($('#tableCuti_filter').attr('placeholder'));
                   
                    $('#bakiCuti').click(function(){cutiTab.fnFilter( '' );});
                    $('#lulusCuti').click(function(){cutiTab.fnFilter( 'lulus' );});
                     $('#tolakCuti').click(function(){cutiTab.fnFilter( 'ditolak' );});
                     $('#tungguCuti').click(function(){cutiTab.fnFilter( 'menunggu' );});
//                $('#pilih').change(function(){cutiTab.fnFilter( $(this).val());});
//                
               // $('.batal').click(function(){alert(this.value);});
                
                
                
                
                
                
              //table main
              
               var mainTab =$('#tableMain').dataTable({

                        "jQueryUI": false,
//                        "dom":  '<"H"RClfr>t<"F"pi>',
                         "dom":  '<"domCutiHeader" RClfr>rt<"bottom"pi><"clear">',
                        "pagingType": "full_numbers",
                        "lengthMenu": [[10, 20, 50,100, -1], [10, 20, 50,100, "Semua"]],
                         "tableTools":{
                                        "sRowSelect": "single"
                                     },
                        "language": {
                            "paginate": {
				      "next": ">",
                                      "first": "<<",
                                      "last": ">>",
                                      "previous": "<"
				     },
                                      "sSearch": "<span style='font-weight: bold;float:right;padding-left:5px;'></span>",
                                      "sZeroRecords": "<span style='font-weight: bold;'>Tiada rekod ditemui</span>",
                                      "info": "<span style='font-weight: bold;'>Halaman _PAGE_ dari _PAGES_</span>",
                                      "sLengthMenu": "_MENU_",
                                      "sInfoFiltered": "  (tapisan dari <span style='font-weight: bold;'> _MAX_ </span> rekod keseluruhan)"
                                     },
                        
                        "emptyTable": "Tiada data",
                        "infoEmpty": "Tiada entri",
                        "bPaginate": false,
                        "bLengthChange": true,
                        "bFilter": true,
                        "bSort": true,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "stateSave": false
              
		    } );
                    
                    
                    mainTab.$('tr:odd').css('backgroundColor', '#FFFFFF');//highlight setiap second row
                   // cutiTab.$('tr:event').css('backgroundColor', 'yellow');//highlight setiap first row
                   
                    $(window).bind('resize', function ()
                    {
                       
		        mainTab.fnAdjustColumnSizing(false);
                         //mainTab..fnClearTable();
		    } );
                    

                    $('#tableMain_filterx').attr('placeholder','Carian');

                
         //table main 2
              
               var main2Tab =$('#tableMain2').dataTable({

                        "jQueryUI": false,
//                        "dom":  '<"H"RClfr>t<"F"pi>',
                         "dom":  '<"domCutiHeader" lfr>rt<"bottom"pi><"clear">',
                        "pagingType": "full_numbers",
                        "lengthMenu": [[10, 20, 50,100, -1], [10, 20, 50,100, "Semua"]],
                        "language": {
                            "paginate": {
				      "next": ">",
                                      "first": "<<",
                                      "last": ">>",
                                      "previous": "<"
				     },
                                      "sSearch": "<span style='font-weight: bold;float:right;padding-left:5px;'></span>",
                                      "sZeroRecords": "<span style='font-weight: bold;'>Tiada rekod ditemui</span>",
                                      "info": "<span style='font-weight: bold;'>Halaman _PAGE_ dari _PAGES_</span>",
                                      "sLengthMenu": "_MENU_",
                                      "sInfoFiltered": "  (tapisan dari <span style='font-weight: bold;'> _MAX_ </span> rekod keseluruhan)"
                                     },
                        
                        "emptyTable": "Tiada data",
                        "infoEmpty": "Tiada entri",
                        "bPaginate": false,
                        "bLengthChange": true,
                        "bFilter": true,
                        "bSort": true,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "stateSave": false
                       
              
		    } );
                    
                    
                    main2Tab.$('tr:odd').css('backgroundColor', '#FFFFFF');//highlight setiap second row
                   // cutiTab.$('tr:event').css('backgroundColor', 'yellow');//highlight setiap first row
                   
                    $(window).bind('resize', function ()
                    {
                       
		        main2Tab.fnAdjustColumnSizing(false);
                         //mainTab..fnClearTable();
		    } );
                    

                    $('#tableMain2_filterx').attr('placeholder','Carian');        
                
                
                
                
                
                         //table main 3
              
               var main3Tab =$('#tableMain3').dataTable({

                        "jQueryUI": false,
//                        "dom":  '<"H"RClfr>t<"F"pi>',
                         "dom":  '<"domCutiHeader" lfr>rt<"bottom"pi><"clear">',
                        "pagingType": "full_numbers",
                        "lengthMenu": [[10, 20, 50,100, -1], [10, 20, 50,100, "Semua"]],
                         "tableTools":{
                                        "sRowSelect": "single"
                                     },
                        "language": {
                            "paginate": {
				      "next": ">",
                                      "first": "<<",
                                      "last": ">>",
                                      "previous": "<"
				     },
                                      "sSearch": "<span style='font-weight: bold;float:right;padding-left:5px;'></span>",
                                      "sZeroRecords": "<span style='font-weight: bold;'>Tiada rekod ditemui</span>",
                                      "info": "<span style='font-weight: bold;'>Halaman _PAGE_ dari _PAGES_</span>",
                                      "sLengthMenu": "_MENU_",
                                      "sInfoFiltered": "  (tapisan dari <span style='font-weight: bold;'> _MAX_ </span> rekod keseluruhan)"
                                     },
                        
                        "emptyTable": "Tiada data",
                        "infoEmpty": "Tiada entri",
                        "bPaginate": false,
                        "bLengthChange": true,
                        "bFilter": true,
                        "bSort": true,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "stateSave": false
              
		    } );
                    
                    
                    main3Tab.$('tr:odd').css('backgroundColor', '#FFFFFF');//highlight setiap second row
                   // cutiTab.$('tr:event').css('backgroundColor', 'yellow');//highlight setiap first row
                   
                    $(window).bind('resize', function ()
                    {
                       
		        main2Tab.fnAdjustColumnSizing(false);
                         //mainTab..fnClearTable();
		    } );
                    

                    $('#tableMain3_filterx').attr('placeholder','Carian');        
                
                
                
                
                
                
            //table admin
              
               var adminTab =$('#tableAdmin').dataTable({

                        "jQueryUI": false,
//                        "dom":  '<"H"RClfr>t<"F"pi>',
                         "dom":  '<"domCutiHeader" lfr>rt<"bottom"pi><"clear">',
                        "pagingType": "full_numbers",
                        "lengthMenu": [[10, 20, 50,100, -1], [10, 20, 50,100, "Semua"]],
                         "tableTools":{
                                        "sRowSelect": "single"
                                     },
                        "language": {
                            "paginate": {
				      "next": ">",
                                      "first": "<<",
                                      "last": ">>",
                                      "previous": "<"
				     },
                                      "sSearch": "<span style='font-weight: bold;float:right;padding-left:5px;'></span>",
                                      "sZeroRecords": "<span style='font-weight: bold;'>Tiada rekod ditemui</span>",
                                      "info": "<span style='font-weight: bold;'>Halaman _PAGE_ dari _PAGES_</span>",
                                      "sLengthMenu": "_MENU_",
                                      "sInfoFiltered": "  (tapisan dari <span style='font-weight: bold;'> _MAX_ </span> rekod keseluruhan)"
                                     },
                        
                        "emptyTable": "Tiada data",
                        "infoEmpty": "Tiada entri",
                        "bPaginate": false,
                        "bLengthChange": true,
                        "bFilter": true,
                        "bSort": true,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "stateSave": false
              
		    } );
                    
                    
                    adminTab.$('tr:odd').css('backgroundColor', '#FFFFFF');//highlight setiap second row
                   // cutiTab.$('tr:event').css('backgroundColor', 'yellow');//highlight setiap first row
                   
                    $(window).bind('resize', function ()
                    {
                       
		        adminTab.fnAdjustColumnSizing(false);
                         //mainTab..fnClearTable();
		    } );
                    

                    $('#tableAdmin_filterx').attr('placeholder','Carian');      
                
                
                
                
                
                
                
                
                
                
                
      });
    </script>
 
<script type="text/javascript">
  $(document).ready(function ()
  { 
    
 
                
                // $('.delay a').click(function()
                 //{
                     
                    //alert($(this).attr('href'));
                   //alert(urlRegExp);
                  //$(this).siblings().removeClass('active');
                  //$(this).addClass('active');
                     
                // });
                
//                  
    
    $(".datepicker" ).datepicker({ dateFormat: "dd/mm/yy" });  //.datepicker bermaksud CLASS ...kalau #datepicker bermaksud id
    
    $("#cutiForm").validate();
               
    $("#resetCutiForm").click(function() {
			$("#cutiForm").resetForm();
		});
    
    
     $("#logout").click(function(e) 
              {
               
                e.preventDefault();
                //alert('hehe');
                
                swal({   title: "Anda pasti?", 
                         text:"Log keluar dari sistem." , 
                         type: "warning",   
                         showCancelButton: true,   
                         confirmButtonColor: "#DD6B55",   
                         confirmButtonText: "Ya",
                         cancelButtonText: "Batal",
                         closeOnConfirm: true }, 
                     function(){ 
                                  $(location).attr('href','auth/logout');
                               });

              });
              
              
              
              
              
              
              
              
              
              
              
    
         
                var type = 'fadeInUp';
		var count = 0;
		$('.delay li').each(function(){
			jQuery(this).addClass('animate'+count+' '+type);
			jQuery(this).on('animationend', function(e) {
				//jQuery(this).removeAttr('class');
			});
			count++;
                        
                
                //var pageName = $(location).attr('href').split('\\').pop();
                var pageName = $('#pageTitle').attr('value');
                
                $('.delay li').each(function()
                {
                  
                  var matchx = $(this).find('a').attr('title');
                  if(pageName.match(matchx))
                  {
                      $(this).addClass('active');  
                  }
                });
                       
		});
		return true;
              
              
              
              
              
              
        });
  </script>

<!--  CODE UNTUK DATEPICKER----TAMAT------------------------------------------------------>






<script type="text/javascript">
$(document).ready(function ()
  {
      
       
	var fixd;
	
	function isGregLeapYear(year)
	{
		return year%4 == 0 && year%100 != 0 || year%400 == 0;
	}

	function gregToFixed(year, month, day)
	{
		var a = Math.floor((year - 1) / 4);
		var b = Math.floor((year - 1) / 100);
		var c = Math.floor((year - 1) / 400);
		var d = Math.floor((367 * month - 362) / 12);
	
		if (month <= 2)
			e = 0;
		else if (month > 2 && isGregLeapYear(year))
			e = -1;
		else
			e = -2;
	
		return 1 - 1 + 365 * (year - 1) + a - b + c + d + e + day;
	}

	function Hijri(year, month, day)
	{
		this.year = year;
		this.month = month;
		this.day = day;
		this.toFixed = hijriToFixed;
		this.toString = hijriToString;
	}

	function hijriToFixed()
	{
		return this.day -1 + Math.ceil(29.5 * (this.month - 1)) + (this.year - 1) * 354 + 
 			Math.floor((3 + 11 * this.year) / 30) + 227015 - 1;
	}

	function hijriToString()
	{
		var months = new Array("Muharam","Safar","Rabi'ul Awal","Rabiul Akhir","Jamadil Awal","Jamadil Akhir","Rejab","Sya'ban","Ramadan","Syawal","Dzulka'edah","Dzulhijjah");
  	return this.day + " " + months[this.month - 1]+ " " + this.year;
}

	function fixedToHijri(f)
	{
  	var i=new Hijri(1100, 1, 1);
   	i.year = Math.floor((30 * (f - 227015) + 10646) / 10631);
   	var i2=new Hijri(i.year, 1, 1);
   	var m = Math.ceil((f - 29 - i2.toFixed()) / 29.5) + 1;
   	i.month = Math.min(m, 12);
   	i2.year = i.year;
	 	i2.month = i.month;
	 	
	 	<!-- asal i2.day = 1; -->
	 	i2.day = 1+1;
   	i.day = f - i2.toFixed() + 1;
   	return i;
	}
	
	var tod=new Date();
	var weekday=new Array("Ahad","Isnin ","Selasa","Rabu","Khamis","Jumaat","Sabtu")
	var monthname=new Array("Januari","Februari","Mac","April","Mei","Jun","Julai","Ogos","September","Oktober","November","Disember");

	var y = tod.getFullYear();
	var m = tod.getMonth();
	var d = tod.getDate();
	var dow = tod.getDay();
        
       	m++;
	fixd=gregToFixed(y, m, d);
	var h=new Hijri(1421, 11, 28);
	h = fixedToHijri(fixd);
	
	//$("#timenow").html(h.toString() + " H ");
        
      
       $('#timenow').html( (moment().locale('ms-my').format(' h:mm:ss a'))); $('#datenow').html( (moment().locale('ms-my').format('dddd , Do MMM YYYY '))   ) ;
       //timeDat();
     setInterval("$('#timenow').html( (moment().locale('ms-my').format(' h:mm:ss a'))); $('#datenow').html( (moment().locale('ms-my').format('dddd , Do MMM YYYY '))   ) ;",1000);

 function timeDat(){
      //$('#datenow').html( (moment().locale('ms-my').format('dddd , Do MMM YYYY ')) + " | " + h.toString() + " H "  ) ;
      //$('#timenow').html( (moment().locale('ms-my').format(' h:mm:ss a'))) ;
      //return true;
      //setInterval("$('#datenow').html( (moment().locale('ms-my').format('dddd , Do MMM YYYY '))); $('#timenow').html( (moment().locale('ms-my').format(' h:mm:ss a'))) ;",1000);
       }
});
             </script>
			 
	


	


			 
			 

			 
			 
			 
                     
                  
              
</head>

 

<body >
<!--    <div id="loaderTop" style="float: top"></div> -->
  <form id="logKeluar" method="post">
         <input type="hidden" name="logout" value="-">
    </form>  
    
     
     <div class="wrapper">
     <header class="main-header">
          
          
    
            
   <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
           <button type="button" class="navbar-toggle" id="close_sidebar" data-toggle="offcanvas">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            
          
          <a class="navbar-brand" href="index.php">eCUTI PICOMS v1.0 | <?php echo $this->accLevelN;  ?>
          
          </a>
              </div>
        <div id="navbar" class="navbar-collapse">
          
          <div class="navbar-form navbar-right">
<!--              <button type="button" class="btn btn-link"  ></button>-->
              <button type="button" class="btn btn-link"  ><span class="glyphicon glyphicon-calendar" style="color: white"></span> <a  id="datenow" style="color:white;font-weight: bold;font-size: 14px;padding-right: 10px;"> </a>   <span class="glyphicon glyphicon-time" style="color: white"></span> <a  id="timenow" style="color:white;padding-right: 10px;font-weight: bold;font-size: 14px"> </a></button> 
              <button type="button" class="btn btn-danger" id="logout" ><span class="glyphicon glyphicon-log-out"></span> Log keluar</button>
            
             
               
                 
               
               
          </div>
        </div>
      </div>
    </nav>
     </header>
    
         <aside class="main-sidebar" style="position: fixed;">
        <section class="sidebar">
           
            <ul class="sidebar-menu delay staf" > <li class="headerTop "></li>
              
                
<!--             NO. STAF : <?php //echo $_SESSION['usr']; ?>  -->
                            
   <?php 
//          $conn=$current->connectToDb();
//          $query=$conn->prepare("SELECT staff_namapenuh,staff_jabatan,staff_jawatan,baki_cuti,total_cuti,staff_gambar from staff WHERE staff_staffno=:staffno AND staff_accstatus='active' LIMIT 1");
//          $query->execute(array(':staffno'=>$_SESSION['usr']));

    
    foreach ($this->detailStaf as $row)
    {
        echo'<script type=\'text/javascript\'> 
                    $(document).ready(function()
                    { 
                        $("#bakiCuti").attr("data-max",'.$row['total_cuti'].');
                        $("#bakiCuti").attr("value",'.$row['baki_cuti'].');
                        $("#bakiCutiTotal").html("dari total '.$row['total_cuti'].' hari");  
                    });//main document ready
                </script>';
        
        // echo ' <li ><img draggable="false" style="margin-bottom: 15px;margin-top: 20px;margin-left: 38px"  src="engine/view/img/staf/'.$row['staff_gambar'].'" class="img-thumbnail img-responsive" width="120" height="120" alt="'.$_SESSION['usr'].'" title="'.$_SESSION['usr'].'"></li>';
//                <div class="caption"><a href="#tukarProfil" data-toggle="modal">
//		<div class="blur kemaskiniGambar" width="120" height="120"></div>
//		<div class="caption-text" >Kemaskini profil anda.</div></a>
//                </div>
                
        
         
          echo '<br><ul  class="ctree"><li style="text-align:center;">Selamat datang <br><b>'.$row['staff_namapenuh'].' !</b></li><br><li><center><button class="glyphicon glyphicon-edit" id="editProfileAnda" title="Kemaskini Profil Anda"></center></li><hr><li style="margin-left:10px;">Jabatan <br> - '.$row['dept_name'].'</li><br><li style="margin-left:10px;">Jawatan <br> - '.$row['p_name'].'</li>';
          echo '</ul>';

          
    }     
       //$conn=null; 
//       $query=null;
//       $row=null;
          ?>
             
           
        
            
            
                <li class="header ">MENU UTAMA</li>
<!--              <li class="active"><a href="main.php"><span class="glyphicon glyphicon-th-list"></span> Halaman Utama <span class="sr-only">(current)</span></a></li>-->
                <li class="treeview"><a href="index.php" title="utama"><span class="glyphicon glyphicon-th-list"></span> Halaman Utama <span class="sr-only">(current)</span></a>
               <!--<ul class="treeview-menu">
                <li><a href="../../index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
              </ul>-->
              </li>
             
              
              <?php
              
             
              if($this->accLevel ==2)  //2 for HOD
              {
                  echo '  <li class="treeview"><a href="pohoncuti" title="cuti"><span class="glyphicon glyphicon-calendar"></span> Mohon Cuti</a></li>';
                  echo ' <li class="treeview"><a href="pentadbir" title="pentadbir"><span class="glyphicon glyphicon-briefcase"></span> Pentadbir/HOD</a></li>';
              }
              else if($this->accLevel ==3)  //3 for HR
              {
                  echo '  <li class="treeview"><a href="pohoncuti" title="cuti"><span class="glyphicon glyphicon-calendar"></span> Mohon Cuti</a></li>';
                 echo ' <li class="treeview"><a href="admin_hr" title="hr"><span class="glyphicon glyphicon-lock"></span> Pentadbir/HR</a></li>';
              }
              else if($this->accLevel ==4)  //4 for STAF
              {
                  echo '  <li class="treeview"><a href="pohoncuti" title="cuti"><span class="glyphicon glyphicon-calendar"></span> Mohon Cuti</a></li>';
              }
              else if($this->accLevel ==5)  //5 for ADMIN
              {
                
              }
              else if($this->accLevel ==1)  //1 for SUPER DUPER ADMIN  aka TUAN TANAH
              {
                    echo '  <li class="treeview"><a href="pohoncuti" title="cuti"><span class="glyphicon glyphicon-calendar"></span> Mohon Cuti</a></li>';
                    echo ' <li class="treeview"><a href="pentadbir" title="pentadbir"><span class="glyphicon glyphicon-briefcase"></span> Pentadbir/HOD</a></li>';
                    echo ' <li class="treeview"><a href="admin_hr" title="hr"><span class="glyphicon glyphicon-lock"></span> Pentadbir/HR</a></li>';
                    echo ' <li class="treeview"><a href="admin_staff" title="admin"><span class="glyphicon glyphicon-lock"></span> Administrator</a></li>';
   
                  
              
              }
              
              
            
              
               ?>
   
          </ul>
      
        </section></aside>


         
             
     <!--    ########################  CODE UNTUK BOOTSTRAP POPUP MULA ########################     -->      
    
    
         <div id="tukarProfil" class="modal fade">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
	<button data-dismiss="modal" class="close" type="button">×</button>
        <h5><b>Kemaskini Profil</b></h5>
	</div>
	<div class="modal-body">
	
<!--         ------------------------------   -->
        <div class=" table-responsive">
            
                <table class="table table-condensed" >
                <tr><td colspan="2" align="left" style="padding-top: 10px;padding-bottom: 10px;"><b>PROFIL</b></td></tr>
                    <tr>
                      <td >Staf</td>
                      <td><font size="2" face="Arial" >
                          <strong>
                              <input disabled style="text-align: center" type="text" size="4" name="stafnoz" id="stafnoz" title="Isikan id anda" placeholder="No. Staf" >
                              : 
                              <input type="text" style="text-align: center" name="namaz" id="namaz" title="Isikan nama anda" placeholder="Nama penuh anda" size="40" >
                                     </strong>
                                     </font>
                                     
                                     </td>
                    </tr>
		
					
                <tr>
                      <td> Emel</td>
                      <td><font size="2" face="Arial"><strong>
                            <input type="text" name="emelz" id="emelz" size=50 data-rule-required="true" data-rule-email="true" data-msg-required="Emel diperlukan" data-msg-email="Masukkan emel yang sah!" placeholder="Emel" value="">
                          </strong></font>
                      </td>
                </tr>			
						
                    <tr><td colspan="2" align="left" style="padding-top: 10px;padding-bottom: 10px;"><b>KATALALUAN</b></td></tr>
		    <tr>
                      <td >Katalaluan</td>
                      <td ><font size="2" face="Arial">
                      <input type="password" NAME="pwdz" size="20" id="pwdz" title="Isikan katalaluan anda" placeholder="Katalaluan BARU" >
                    </tr>
						
						
						
						
						
		      <tr>
                      <td >Ulang Katalaluan</td>
                      <td ><font size="2" face="Arial">
                      <input type="password" name="ulangpwdz" id="ulangpwdz" size="20" title="Ulang katalaluan anda" placeholder="Ulang katalaluan BARU"  >
                      </tr>				
			
                      
            </table> </div>
            
        
<!--         -----------------------------   -->
	</div>
	      <div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button"  id="confirmStaffz" class="btn btn-primary">Kemaskini</button>
	</div>
       
        </div>
</div>

</div>
         
  <!--    ########################  CODE UNTUK BOOTSTRAP POPUP TAMAT  ########################     -->        
         
         
         
<!--    ########################  CODE UNTUK BOOTSTRAP POPUP  ########################     -->
         
    <script>
		$(document).ready(function (e) {
			
                        
                        
                    $('#editProfileAnda').click(function()
                    {
                        
                 
            
          $('#tukarProfil').modal(
            {
                backdrop: 'static',
                keyboard: true
              
              
            });
            
            
            
            
            
                      $.ajax({
                       url: 'call/callStaffById',
                       data: {stafid:<?php echo $_SESSION['sid']; ?>},
                       dataType: 'json',
                       cache: false,
                       type: 'POST',
                       success: function(data){
                       //alert(<?php //echo $_SESSION['tempJab'];?>);
                       //$(location).attr('href','main');
                               $.each(data ,   function(k,v)
                               {
                                                  
                                                   $('#stafnoz').val(v.staffno);
                                                   $('#namaz').val(v.nama);
                                                   $('#emelz').val(v.emel);
                                                
                                                
                                                    
                                                
                              });
                               
                               
                               
                                           
   
                                            
                     
                     
                     
                     
                     
                  
                       },
                        error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus+" "+errorThrown);
                      }
                   });
            
                 
                    });    
                        
                        
                        
      
      
      
      
      
      
      
      
      
      
      
       //simpan data selepas update
       $('#confirmStaffz').click(function()
        { 
            
            var namaBaru = $('#namaz').val();
            var emelBaru = $('#emelz').val();
            
            var pwdBaru = $('#pwdz').val(); 
            var upwdBaru = $('#ulangpwdz').val(); 
            
        
            
            
        if (pwdBaru!=upwdBaru)
        {
          alert('Katalaluan tidak sepadan!')  ;
        }
        
        
          else
          {
       
             $.ajax({
                       
                       url: 'call/updateProfileSelf',
                       data: {id:<?php echo $_SESSION['sid']; ?>,nama:namaBaru,emel:emelBaru,pass:pwdBaru},
                       cache: false,
                       type: 'POST',
                       success: function(data)
                       {
                           
                          if(data>0)
                          {
                              alert('Data berjaya dikemaskini!');
                              $('#tukarProfil').modal('hide');
            
                          }
                          else
                          {
                            alert('Gagal! Mohon cuba sekali lagi.');  
                          }
                     
                       
                  
                       },
                        error: function(jqXHR, textStatus, errorThrown) {
                            
                            
                        console.log(textStatus+" "+errorThrown);
                      }
                   });
                
                
              }  
        
       
                
                
                
                
                
                
                
                
                
             
            }); 
    
      
      
      
      
      
      
      
      
      
                        

			// Function to preview image after validation
			$(function() {
				$("#file").change(function() {
					$("#message").empty(); // To remove the previous error message
					var file = this.files[0];
					var imagefile = file.type;
					var match= ["image/jpeg","image/png","image/jpg"];
					if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
					{
						
						$("#message").html("<font style=\"color:red;font-weight:bold;\">ERROR: Hanya format foto ini dibenarkan : JPG,JPEG,PNG</font>");
						return false;
					}
					else
					{
						var reader = new FileReader();
						reader.onload = imageIsLoaded;
						reader.readAsDataURL(this.files[0]);
                                                $("#message").html("<font style=\"color:green;font-weight:bold;\">OK!</font>");
						
					}
				});
			});
			function imageIsLoaded(e) {
				$('#previewing').attr('src', e.target.result);
			};
		});
	</script>




   
         
         
<!-- ----------------------------HEADER TAMAT--------------------------------------------->
    
     <div class="content-wrapper">
<!-- Main content -->
<section class="content">
