<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InventoryItems */

$this->title = Yii::t('app', 'Create Inventory Items');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inventory Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventory-items-create">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
        </div>

        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>

    </div>
</div>
