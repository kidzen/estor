<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

//var_dump(Yii::$app->request->referrer);
//var_dump(preg_match('/*inventory-items*/', Yii::$app->request->referrer));
//die();
/* @var $this yii\web\View */
/* @var $model common\models\InventoryItems */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inventory Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info inventory-items-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>inventory-items : <?= Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a(Yii::t('app', 'Kembali'), Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
            <?= Html::a(Yii::t('app', 'Kemaskini'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Cetak Barkod Inventori & sku'), ['/inventory-items/print-sku-by-id','id'=>$model->id,'printInventoryCode'=>true], ['class' => 'btn btn-info','target'=>'_blank']) ?>
            <?= Html::a(Yii::t('app', 'Cetak Barkod Inventori'), ['/inventories/print-barcode','id'=>$model->inventory->id], ['class' => 'btn btn-info','target'=>'_blank']) ?>
            <?= Html::a(Yii::t('app', 'Cetak Barkod sku'), ['/inventory-items/print-sku-by-id','id'=>$model->id,'printInventoryCode'=>false], ['class' => 'btn btn-info','target'=>'_blank']) ?>
            <?=
            Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </p>
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'inventory_id',
                'checkin_transaction_id',
                'checkout_transaction_id',
                'sku',
                'unit_price',
                'created_at',
                'updated_at',
                'created_by',
                'updated_by',
                [
                    'attribute' => 'deleted',
                    'vAlign' => 'middle',
                    'value' => $model->deleted == 1 ? 'Deleted' : 'Active',
                ],
                'deleted_at',
            ],
        ])
        ?>

    </div>
