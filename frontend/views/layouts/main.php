<?php

    use yii\helpers\Html;

    /* @var $this \yii\web\View */
    /* @var $content string */
    $username   = Yii::$app->user->name ? Yii::$app->user->name : 'Guest';
    $activeBool = Yii::$app->user->id ? 'Online' : 'Offline';
    $roles      = Yii::$app->user->role;

    //dmstr\web\AdminLteAsset::register($this);
    frontend\assets\AdminLteAsset::register($this);
    //frontend\assets\AppAsset::register($this);
    //AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@frontend/assets/dist');
    //var_dump(readdir(opendir($directoryAsset)));
    //die();

    //$file = Yii::$app->assetManager->getPublishedUrl('@frontend/assets/dist/css/Pdf.css');
    //$file = Yii::getAlias('@frontend/assets/dist/css/Pdf.css');
    //$file = Yii::getAlias('@web/uploads/profile_pic/admin1.jpg');
    $file = Yii::getAlias('@frontend/assets/dist/pdf_template/KEW.PS-8.pdf');
    //$file = Yii::getAlias('@web/pdf_template/KEW.PS-8.pdf');

    //$directoryAsset = Yii::$app->assetManager->getPublishedUrl('');
?>
<?php $this->beginPage();?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language;?>">
    <head>
        <meta charset="<?=Yii::$app->charset;?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?=Html::csrfMetaTags();?>
        <title><?=Html::encode($this->title);?></title>
          <link rel="manifest" href="manifest.json">
          <meta name="mobile-web-app-capable" content="yes">
          <link rel="shortcut icon" href="<?php echo $directoryAsset; ?>/img/favicon.ico" type="image/x-icon" />
        <?php $this->head();?>
    </head>
    <!--<body class="hold-transition skin-purple-light sidebar-mini">-->
    <body class="hold-transition skin-purple-light fixed sidebar-mini">
        <?php $this->beginBody();?>
        <div class="wrapper">

            <?=
$this->render(
    'header.php', [
        'directoryAsset' => $directoryAsset,
        'username'       => isset(Yii::$app->user->username) ? Yii::$app->user->username : 'Guest',
        'activeBool'     => Yii::$app->user->status,
        'roles'          => isset(Yii::$app->user->role) ? Yii::$app->user->role : 'Guest',
    ]
)
;?>

            <?=
$this->render(
    'left.php', [
        'directoryAsset' => $directoryAsset,
        'username'       => isset(Yii::$app->user->username) ? Yii::$app->user->username : 'Guest',
        'activeBool'     => Yii::$app->user->status,
        'roles'          => isset(Yii::$app->user->role) ? Yii::$app->user->role : 'Guest',
    ]
)
;?>

            <?=
$this->render(
    'content.php', [
        'content'        => $content,
        'directoryAsset' => $directoryAsset,
        'username'       => isset(Yii::$app->user->username) ? Yii::$app->user->username : 'Guest',
        'activeBool'     => Yii::$app->user->status,
        'roles'          => isset(Yii::$app->user->role) ? Yii::$app->user->role : 'Guest',
    ]
)
;?>

        </div>

        <?php $this->endBody();?>
    </body>
</html>
<?php $this->endPage();?>
