<?php

//use Yii;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\People */

$this->title = Yii::t('app', 'Kemaskini {modelClass} : ', [
            'modelClass' => 'Profil',
        ]) . $model->id;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Peoples'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
//$this->params['breadcrumbs'][] = Yii::t('app', 'Kemaskini');
?>
<div class="people-update">
    <div class="box box-info people-view">
        <div class="box-header with-border">
            <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>Profil : <?= Html::encode($model->username) ?></strong></h3>
        </div>
        <div class="box-body">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title"><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
                </div>

                <?=
                $this->render('_form', [
                    'model' => $model, 'rolesArray' => $rolesArray
                ])
                ?>

            </div>
<!--            <p>
            <?=
            Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
            </p>-->
            <?php Pjax::begin(['id' => 'people']) ?>
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'username',
                    'email:email',
                    'mpspProfile.nama',
                    'mpspProfile.jawatan',
                    'mpspProfile.keterangan',
                    [
                        'attribute' => 'profile_pic',
                        'format' => ['image', ['width' => '100', 'height' => '100']],
                        'value' => isset($model->profile_pic) ? Yii::getAlias('@web/') .$model->profile_pic : null,
                    ],
                    [
                        'label' => 'Url Gambar',
                        'attribute' => 'profile_pic',
                    ],
                    [
                        'label' => 'Roles',
                        'attribute' => 'roles.name',
                        'vAlign' => 'middle',
                    ],
                    [
                        'attribute' => 'deleted',
                        'vAlign' => 'middle',
                        'value' => $model->deleted == 1 ? 'Deleted' : 'Active',
                    ],
//                    'deleted_at',
//                    'created_at',
//                    'updated_at',
//                    'password',
//                    'password_reset_token',
//                    'auth_key',
                ],
            ])
            ?>
            <?php Pjax::end() ?>

        </div>

    </div>

    <?php
    $this->registerJs(
            '$("document").ready(function(){
        $("#update_people").on("pjax:end", function() {
            $.pjax.reload({container:"#people"});  //Reload GridView
        });
    });'
    );
    ?>

