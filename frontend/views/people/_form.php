<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\People */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="people-form">

    <?php yii\widgets\Pjax::begin(['id' => 'update_people']) ?>
    <?php
    $form = ActiveForm::begin(['options' => ['data-pjax' => true, 'enctype' => 'multipart/form-data']]);
    ?>
    <div class="panel-body">
        <div class="row">
            <!--            <div class="col-md-4">
            <?= $form->field($model, 'id')->textInput(['maxlength' => true]) ?>
                        </div>-->
            <div class="col-md-4">
                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'staff_no')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-4">
                <?= $form->field($model, 'role_id')->dropDownList($rolesArray)->label('Akses') ?>
            </div>

            <div class="col-md-4">
                <?= $form->field($model, 'password_temp')->passwordInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'repassword_temp')->passwordInput(['maxlength' => true]) ?>
            </div>
            <?php if (Yii::$app->user->isAdmin) { ?>
                <div class="col-md-4">
                    <?= $form->field($model, 'deleted')->dropDownList([0 => 'Active', 1 => 'Deactivate']) ?>
                </div>
            <?php } ?>

        </div>
        <div class="row">
            <div class="col-md-4">
                <?php
                if (empty($model->profile_pic)) {
                    // if you do not want a placeholder
                    $image = null;

                    // else if you want to display a placeholder
                    $image = Html::img($model->profile_pic, [
                                'alt' => Yii::t('app', 'Tiada Gambar'),
                                'title' => Yii::t('app', 'Upload your avatar by selecting browse below'),
//                                'class' => 'user-header'
//                                'style' => 'width:100px'
                                    // add a CSS class to make your image styling consistent
                    ]);
                    $image = Html::img(Yii::$app->assetManager->getPublishedUrl('@frontend/assets/dist') . '/img/tiada_gambar.jpg', [
                                'alt' => Yii::t('app', 'Avatar for ') . $model->username,
                                'title' => Yii::t('app', 'Click remove button below to remove this image'),
                                'class' => 'profile-user-img img-circle',
//                                'class' => 'profile-user-img img-responsive img-circle',
//                                'style' => 'width:100px'
                                    // add a CSS class to make your image styling consistent
                    ]);
                } else {
                    $image = Html::img(Yii::$app->urlManager->baseUrl . '/' . $model->profile_pic, [
                                'alt' => Yii::t('app', 'Avatar for ') . $model->username,
                                'title' => Yii::t('app', 'Click remove button below to remove this image'),
                                'class' => 'profile-user-img img-circle',
//                                'class' => 'profile-user-img img-responsive img-circle',
//                                'style' => 'width:100px'
                                    // add a CSS class to make your image styling consistent
                    ]);
                }

                // enclose in a container if you wish with appropriate styles
                echo Html::tag('div', $image, ['class' => 'user-image']);
                ?>
                <?=
                $form->field($model, 'profile_pic_file')->fileInput()
                ?>

            </div>
        </div>
        <div>
            <?php echo Html::a(Yii::t('app', 'Batal'), ['index'], ['class' => 'btn btn-default']) ?>
            <?php // echo Html::a(Yii::t('app', 'Batal'), Yii::$app->request->referrer, ['class' => 'btn btn-default'])  ?>
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Kemaskini'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <?php yii\widgets\Pjax::end() ?>

</div>
