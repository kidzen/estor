<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\People */

$this->title = Yii::t('app', 'Create People');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Peoples'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="people-create">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
        </div>

        <?= $this->render('_form', [
        'model' => $model, 'rolesArray'=>$rolesArray
        ]) ?>

    </div>
</div>
