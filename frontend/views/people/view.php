<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\People */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Peoples'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info people-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>people : <?= Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a(Yii::t('app', 'Kembali'), Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
            <?= Html::a(Yii::t('app', 'Kemaskini'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </p>
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'username',
                'email:email',
                'staff_no',
                'mpspProfile.nama',
                'mpspProfile.jawatan',
                'mpspProfile.keterangan',
                [
                    'attribute' => 'profile_pic',
                    'format' => ['image', ['width' => '100', 'height' => '100']],
                    'value' => isset($model->profile_pic) ? Yii::getAlias('@web/') . $model->profile_pic : null,
                ],
                [
                    'label' => 'Url Gambar',
                    'attribute' => 'profile_pic',
                ],
                [
                    'label' => 'Roles',
                    'attribute' => 'roles.name',
                    'vAlign' => 'middle',
                ],
                [
                    'attribute' => 'deleted',
                    'vAlign' => 'middle',
                    'value' => $model->deleted == 1 ? 'Deleted' : 'Active',
                ],
                'deleted_at',
                'created_at',
                'updated_at',
                'password_reset_token',
                'auth_key',
            ],
        ])
        ?>

    </div>
