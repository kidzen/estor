<!-- link https://m2.mymi1.com/app/workplace/member.do -->

<!-- page source -->

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>MYMI1 - Member Back Office</title>
    <script type="text/javascript">
        window.onbeforeunload = function (e) {
            location.href = "/auth.do?task=doLogout";
                    //alert("You have been logged out");
                };
            </script>
        </head>

        <frameset cols="*" border="5" framespacing="5">
        <frame src="/app/workplace/member.do?task=main&t=1486438093303">
    </frameset>

    </html>


    <!-- frame source -->

    <html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <title>MYMI1 - Member Back Office</title>
    <link rel="stylesheet" type="text/css" media="screen" href="/css/default.css">
    <link rel="StyleSheet" type="text/css" href="/lib/calendar.css">
    <script type="text/Javascript" src="/lib/calendar.js"></script>
    <script type="text/Javascript" src="/lib/script.js"></script>
    <script type="text/Javascript" src="/lib/validate.js"></script>

    <script language="Javascript" src="/lib/order.js"></script>

    <script type="text/Javascript">

        function doSubmit(thisform) {
            var form_mobile = "Please enter Mobile Number.";
            var form_accno = "Please enter No Meter PLN.";
            var form_replymobile = "Please enter No HP Terima SMS.";

            var i=0;
            var selectedItem = false;
            if (thisform.SI_.length > 0) {
                for (; thisform.SI_!=null && i < thisform.SI_.length; i++) {
                    if (thisform.SI_[i].checked == "1") {
                        selectedItem = true;
                        break;
                    }
                }
            } else {
                if (thisform.SI_.checked == "1") {
                    selectedItem = true;
                }
            }
            if (!selectedItem) {
                alert("Please select Service.");
                return false;
            }
            if (thisform.SI_[i].value == "137") {
                                    // utility bill , pln

                                    if (thisform.Remark.value == "" || !thisform.Remark.value.match(/^[0-9]+$/)) {
                                        focusAndSelect(thisform.Remark);
                                        alert(form_accno);

                                        return false;
                                    }
                                    if (thisform.Receiver.value == "" || !thisform.Receiver.value.match(/^[0-9]+$/)) {
                                        focusAndSelect(thisform.Receiver);
                                        alert(form_replymobile);

                                        return false;
                                    }

                                } else {
                                    // mobile bill

                                    if (thisform.Remark.value == "" || !thisform.Remark.value.match(/^[0-9]+$/)) {
                                        focusAndSelect(thisform.Remark);
                                        alert(form_mobile);
                                        return false;
                                    }
                                    if (thisform.MobileTelArea.value != "65" && thisform.Remark.value.length < 9) {
                                        focusAndSelect(thisform.Remark);
                                        alert(form_mobile);
                                        return false;
                                    }
                                    if (thisform.MobileTelArea.value == "6" && thisform.Remark.value.charAt(0) != "0") {
                                    // my
                                    focusAndSelect(thisform.Remark);
                                    alert(form_mobile);
                                    return false;
                                }
                                if (thisform.MobileTelArea.value == "62" && thisform.Remark.value.charAt(0) != "0") {
                                    // indo
                                    focusAndSelect(thisform.Remark);
                                    alert(form_mobile);
                                    return false;
                                }
                                if (thisform.MobileTelArea.value == "880" && thisform.Remark.value.charAt(0) != "0") {
                                    // vertnam
                                    focusAndSelect(thisform.Remark);
                                    alert(form_mobile);
                                    return false;
                                }
                                if (thisform.MobileTelArea.value == "65" && !(thisform.Remark.value.charAt(0) == "8" || thisform.Remark.value.charAt(0) == "9")) {
                                    // sg
                                    focusAndSelect(thisform.Remark);
                                    alert(form_mobile);
                                    return false;
                                }
                            }
                            return true;
                        }
                        function pservice(thisform) {
                            var si_id = "";

                            if (thisform.SI_!=null && thisform.SI_.length > 0) {
                                for (var i=0; i < thisform.SI_.length; i++) {
                                    if (thisform.SI_[i].checked == "1") {
                                        si_id = thisform.SI_[i].value;
                                        break;
                                    }
                                }
                            }

                            if (si_id == "137") {
                                lbl_service.innerText = "No Meter PLN";
                                showhideDO(1);
                            } else {
                                lbl_service.innerText = "Mobile Tel. No.";
                                showhideDO(0);
                            }
                        }
                        function showhideDO(id) {
                            var doN = document.getElementById("tab_replymobile_n");
                            var doV = document.getElementById("tab_replymobile_v");

                            if (id == 1) {
                                doN.style.display = 'block';
                                doV.style.display = 'block';
                            } else {
                                doN.style.display = 'none';
                                doV.style.display = 'none';
                            }
                        }
                        window.onload = function() {pservice(document.salesform);self.focus();}

                        var capturedOnLoad = window.onload;
                        function body_load() {if (capturedOnLoad != null){setTimeout(capturedOnLoad,100);} addUrlTimestamp();}
                    </script>
                </head>
                <body onload="body_load()" id="dashoard">

                    <div id="dashboard-head">
                        <div id="pl_wrap">
                            <div id="pl_head" class="clearfix">
                                <h1 class="pl_logo pos-a"></h1>

                                <div class="pl_menu">
                                    <ul class="pl_navi float-li">
                                        <li><a href="https://m2.mymi1.com/app/workplace/member.do?task=main&amp;t=1486438347620"><span>Dashboard</span></a></li>
                                        <li><a href="#"><span>Personal Profile</span></a>
                                            <ul>
                                                <li><a href="https://m2.mymi1.com/app/member/maintain.do?task=doInfo&amp;t=1486438347620"><span>Personal Profile</span></a></li>
                                                <li><a href="https://m2.mymi1.com/app/member/maintain2.do?task=doChangePwd&amp;t=1486438347620"><span>Change Password</span></a></li>
                                                <li><a href="https://m2.mymi1.com/app/member/maintain2.do?task=doChangePin&amp;t=1486438347620"><span>Change Password Trx</span></a></li>
                                                <li><a href="https://m2.mymi1.com/app/member/maintain2.do?task=doChgSetting&amp;t=1486438347620"><span>Change Setting</span></a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#"><span>Business Center</span></a>
                                            <ul>
                                                <li><a href="https://m2.mymi1.com/app/member/tree/sponsor.do?task=doView&amp;ViewType=1&amp;t=1486438347620"><span>Sponsor Tree</span></a></li>
                                                <li><a href="https://m2.mymi1.com/app/member/tree/placement.do?task=doView&amp;ViewType=1&amp;t=1486438347620"><span>Placement Tree</span></a></li>

                                                <li><a href="https://m2.mymi1.com/app/member/tree/sponsor.do?task=doDownline&amp;t=1486438347620"><span>List Direct Sponsor</span></a></li>

                                                <li><span>&nbsp;</span></li>
                                                <li><a href="https://m2.mymi1.com/app/member/register.do?task=doPre&amp;t=1486438347620"><span>eRegistration</span></a></li>

                                                <li><a href="https://m2.mymi1.com/app/member/pinreg.do?task=doPre&amp;t=1486438347620"><span>eRegistration (AI)</span></a></li>
                                                <li><a href="https://m2.mymi1.com/app/stock/pin/pin.do?task=doBal&amp;t=1486438347620"><span>Package PIN Balance</span></a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#"><span>Purchase</span></a>
                                            <ul>
                                                <li><a href="https://m2.mymi1.com/app/pos/order.do?task=doOrderHistory&amp;t=1486438347620"><span>Order History (in)</span></a></li>
                                                <li><a href="https://m2.mymi1.com/app/pos/order.do?task=doKeyinHistory&amp;t=1486438347620"><span>Order History (out)</span></a></li>
                                                <li><a href="https://m2.mymi1.com/app/pos/order/pinTransfer.do?task=doPre&amp;t=1486438347620"><span>Transfer Package PIN</span></a></li>

                                                <li><a href="https://m2.mymi1.com/app/pos/order/redemption.do?task=doForm&amp;t=1486438347620"><span>Product Redemption</span></a></li>


                                                <li><a href="https://m2.mymi1.com/app/pos/order/upgLots.do?task=doPre&amp;t=1486438347620"><span>Upgrade Member</span></a></li>

                                                <li><a href="https://m2.mymi1.com/app/pos/order/reloadPrepaidOrder2.do?task=doForm&amp;t=1486438347620"><span>Prepaid Reload</span></a></li>






                                                <li><a href="https://m2.mymi1.com/app/pos/order/reloadIntOrder.do?task=doPre&amp;t=1486438347620"><span>International Reload</span></a></li>


                                                <li><a href="https://m2.mymi1.com/app/share/trade.do?task=doMain&amp;t=1486438347620">Share Market</a></li>

                                                <li><span>&nbsp;</span></li>
                                                <li><a href="https://m2.mymi1.com/app/member/register3.do?task=doPre&amp;t=1486438347620"><span>eRegistration (TC)</span></a></li>

                                            </ul>
                                        </li>
                                        <li><a href="#"><span>Bonus</span></a>
                                            <ul>
                                                <li><a href="https://m2.mymi1.com/app/bonus/stmt.do?task=doPayday&amp;t=1486438347620"><span>Pay Day Listing</span></a></li>
                                                <li><a href="https://m2.mymi1.com/app/bonus/bingo.do?task=doSummary&amp;t=1486438347620"><span>Pairing Status</span></a></li>
                                                <li><a href="https://m2.mymi1.com/app/bonus/sponsor.do?task=doSummary&amp;t=1486438347620"><span>Sponsor Bonus</span></a></li>

                                                <li><a href="https://m2.mymi1.com/app/bonus/matching.do?task=doSummary&amp;t=1486438347620"><span>Matching Bonus</span></a></li>

                                                <li><a href="https://m2.mymi1.com/app/bonus/sponsor.do?task=doSummary3&amp;t=1486438347620"><span>Entertain Bonus</span></a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#"><span>e-Wallet</span></a>
                                            <ul>
                                                <li><a href="https://m2.mymi1.com/app/wallet/stmt.do?task=doViewMbrTrx&amp;Type=10&amp;t=1486438347620"><span>Wallet Statement</span></a></li>
                                                <li><a href="https://m2.mymi1.com/app/impl/wallet/trf.do?task=doForm&amp;t=1486438347620"><span>Wallet Transfer</span></a></li>
                                                <li><a href="https://m2.mymi1.com/app/impl/wallet/exchange.do?task=doForm&amp;t=1486438347620"><span>Wallet Exchange</span></a></li>

                                                <li><a href="https://m2.mymi1.com/app/stock/pin/reloadStatus.do?task=doListing&amp;t=1486438347620"><span>Reload Statement</span></a></li>
                                            </ul>
                                        </li>
                                        <li><a href="https://m2.mymi1.com/auth.do?task=doLogout&amp;t=1486438347620" target="_top"><span>Sign out</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div id="dashboard-body">
                        <div id="pl_wrap">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tbody><tr>
                                    <td><h1>
                                        Purchase Prepaid Reload
                                    </h1></td>
                                </tr>
                                <tr>
                                    <td height="400" valign="top">
                                        <div id="error"></div>


                                        <form name="salesform" method="post" action="/app/pos/order/reloadPrepaidOrder2.do?task=doForm" onsubmit="return doSubmit(this)"><div><input type="hidden" name="tKEy" value="bee827495e25e7e0f1860dfcb0d24370"></div>

                                            <b><u>Member Information</u></b>
                                            <table width="100%" border="0">
                                                <tbody><tr>
                                                    <td width="20%">Member ID</td>
                                                    <td width="1%">:</td>
                                                    <td width="29%">MY261535</td>
                                                    <td width="20%">Order Date</td>
                                                    <td width="1%">:</td>
                                                    <td width="29%">07-02-2017</td>
                                                </tr>
                                                <tr>
                                                    <td>Name</td>
                                                    <td>:</td>
                                                    <td>Azmuddin Bin Ah Shahimi </td>
                                                    <td>Cash Point (CP)</td>
                                                    <td>:</td>
                                                    <td>0.00</td>
                                                </tr>
                                                <tr>
                                                    <td>Telephone No.</td>
                                                    <td>:</td>
                                                    <td>60123331807</td>
                                                    <td>Topup Credit (TC)</td>
                                                    <td>:</td>
                                                    <td>212.30</td>
                                                </tr>
                                            </tbody></table>
                                            <br>

                                            <div class="form-panel p50">
                                                <table>
                                                    <tbody><tr valign="top">
                                                        <th>Service <font color="red">*</font></th>
                                                        <td>

                                                            <input type="radio" name="SI_" value="132" onclick="pservice(this.form)"> &nbsp; Celcom Prepaid &nbsp; (RM 10,<strike>30,50</strike>)<br>

                                                            <input type="radio" name="SI_" value="131" disabled="disabled" onclick="pservice(this.form)"> &nbsp; Digi Prepaid &nbsp; (RM 10,<strike>30,50</strike>) *<br>

                                                            <input type="radio" name="SI_" value="130" disabled="disabled" onclick="pservice(this.form)"> &nbsp; Maxis Prepaid &nbsp; (RM 10,<strike>30,60</strike>) *<br>

                                                            <input type="radio" name="SI_" value="133" disabled="disabled" onclick="pservice(this.form)"> &nbsp; TuneTalk Prepaid &nbsp; (RM 10,<strike>30,50</strike>) *<br>

                                                            <input type="radio" name="SI_" value="134" disabled="disabled" onclick="pservice(this.form)"> &nbsp; UMobile Prepaid &nbsp; (RM 10,<strike>30,50</strike>) *<br>

                                                            <input type="radio" name="SI_" value="135" disabled="disabled" onclick="pservice(this.form)"> &nbsp; XOX Prepaid &nbsp; (RM 10,<strike>30,50</strike>) *<br>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th width="150">Reload Amount <font color="red">*</font></th>
                                                        <td><select name="Qty">

                                                            <option value="10">RM 10 = 3.40TC/CP</option>


                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th><label id="lbl_service">Mobile Tel. No.</label> <font color="red">*</font></th>
                                                    <td><input type="text" name="Remark" size="15" value=""> Eg:

                                                        0121234567

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Wallet</th>
                                                    <td><input type="radio" name="Wallet" value="30" checked="checked"> Topup Credit (TC) &nbsp;
                                                        <input type="radio" name="Wallet" value="10"> Cash Point (CP) &nbsp;

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th><span id="tab_replymobile_n" style="display: none">No HP Terima SMS<font color="red">*</font></span></th>
                                                    <td><span id="tab_replymobile_v" style="display: none"><input type="text" name="Receiver" size="15" value=""> Eg: 081812345678</span></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">Note : Reload process required about 3-5 minute to process.<br>
                                                        Apabila TOPUP anda tiada terima, RC anda akan dalam 24jam auto return back to E-Wallet<br>
                                                        <font color="red">Topup problem pls call or whatapps 013-6643121</font>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">Multiple recharge same mobile phone number must be carried out after 30 minutes.<br><br>
                                                        重复同一个手机号码充值必须在30分钟之后才可以进行.<br><br>
                                                        Isi ulang yang sama nomor telephon bimbit ponsel harus dilakukan setelah waktu 30 menit.
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"><font color="red">Company did not take any responsible for invalid mobile number. Please make sure the reload mobile number is correct.<br><br>
                                                        请确定你的手机号码是正确后才充值，如有任何错误公司不负责<br><br>
                                                        Silakan menentukan nomor telepon bimbit anda sudah benar sebelum pengisian, perusahaan tidak bertanggung jawab atas kesalahan
                                                    </font></td>
                                                </tr>
                                            </tbody></table>
                                        </div>
                                        <input type="hidden" name="SubmitData" value="true">
                                        <input type="hidden" name="MobileTelArea" value="6">
                                        <input type="button" value="«« Back" onclick="history.back()">
                                        <input type="submit" value="Next Step »»">

                                    </form>


                                </td>
                            </tr>
                        </tbody></table>
                    </div>
                </div>
                <div id="pl_foot">
                    <div id="pl_copy" class="small">© 2014-2017 MYMI1.com All Right Reserved</div>
                </div>


            </body></html>
