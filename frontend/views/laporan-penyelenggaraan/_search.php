<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LaporanPenyelenggaraanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="laporan-penyelengaraan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tahun') ?>

    <?= $form->field($model, 'bulan') ?>

    <?= $form->field($model, 'no_kerja') ?>

    <?= $form->field($model, 'arahan_kerja') ?>

    <?php // echo $form->field($model, 'jk_jg') ?>

    <?php // echo $form->field($model, 'bengkel_panel_id') ?>

    <?php // echo $form->field($model, 'nama_panel') ?>

    <?php // echo $form->field($model, 'kategori_panel') ?>

    <?php // echo $form->field($model, 'selenggara_id') ?>

    <?php // echo $form->field($model, 'tempoh_selenggara') ?>

    <?php // echo $form->field($model, 'alasan_id') ?>

    <?php // echo $form->field($model, 'alasan') ?>

    <?php // echo $form->field($model, 'tarikh_arahan') ?>

    <?php // echo $form->field($model, 'no_sebutharga') ?>

    <?php // echo $form->field($model, 'tarikh_kenderaan_tiba') ?>

    <?php // echo $form->field($model, 'tarikh_jangka_siap') ?>

    <?php // echo $form->field($model, 'tarikh_sebutharga') ?>

    <?php // echo $form->field($model, 'status_sebutharga') ?>

    <?php // echo $form->field($model, 'no_do') ?>

    <?php // echo $form->field($model, 'tarikh_siap') ?>

    <?php // echo $form->field($model, 'tempoh_jaminan') ?>

    <?php // echo $form->field($model, 'tarikh_do') ?>

    <?php // echo $form->field($model, 'tarikh_cetak') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'catatan_sebutharga') ?>

    <?php // echo $form->field($model, 'catatan') ?>

    <?php // echo $form->field($model, 'nama_pic') ?>

    <?php // echo $form->field($model, 'tarikh_permohonan') ?>

    <?php // echo $form->field($model, 'id_kenderaan') ?>

    <?php // echo $form->field($model, 'kategori_kerosakan_id') ?>

    <?php // echo $form->field($model, 'kategori_kerosakan') ?>

    <?php // echo $form->field($model, 'kategori') ?>

    <?php // echo $form->field($model, 'isservice') ?>

    <?php // echo $form->field($model, 'ispancit') ?>

    <?php // echo $form->field($model, 'odometer_terkini') ?>

    <?php // echo $form->field($model, 'no_plat') ?>

    <?php // echo $form->field($model, 'model') ?>

    <?php // echo $form->field($model, 'kod_jabatan') ?>

    <?php // echo $form->field($model, 'nama_jabatan') ?>

    <?php // echo $form->field($model, 'jumlah_kos') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
