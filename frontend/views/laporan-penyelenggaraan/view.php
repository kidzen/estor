<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\LaporanPenyelengaraan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Laporan Penyelengaraans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info laporan-penyelengaraan-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>laporan-penyelengaraan : <?=  Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">
    <p>
        <?= Html::a(Yii::t('app', 'Kembali'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('app', 'Kemaskini'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
        'method' => 'post',
        ],
        ]) ?>
    </p>
    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                'id',
            'tahun',
            'bulan',
            'no_kerja',
            'arahan_kerja',
            'jk_jg',
            'bengkel_panel_id',
            'nama_panel',
            'kategori_panel',
            'selenggara_id',
            'tempoh_selenggara',
            'alasan_id',
            'alasan',
            'tarikh_arahan',
            'no_sebutharga',
            'tarikh_kenderaan_tiba',
            'tarikh_jangka_siap',
            'tarikh_sebutharga',
            'status_sebutharga',
            'no_do',
            'tarikh_siap',
            'tempoh_jaminan',
            'tarikh_do',
            'tarikh_cetak',
            'status',
            'catatan_sebutharga',
            'catatan',
            'nama_pic',
            'tarikh_permohonan',
            'id_kenderaan',
            'kategori_kerosakan_id',
            'kategori_kerosakan',
            'kategori',
            'isservice',
            'ispancit',
            'odometer_terkini',
            'no_plat',
            'model',
            'kod_jabatan',
            'nama_jabatan',
            'jumlah_kos',
    ],
    ]) ?>

</div>
