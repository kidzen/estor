<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\widgets\Growl;

//use yii\web\User;
/* @var $this yii\web\View */
//echo '<pre>';var_dump(\Yii::$app->user->id);echo '</pre>';
//$this->title = 'My Yii Application';
//echo Yii::$app->user->name;
//echo Yii::$app->user->role;
//echo Yii::$app->user->isAdmin;
//echo Yii::$app->user->isPegawaiStor;
//foreach(PDO::getAvailableDrivers() as $driver)
//    echo $driver, '<br>';
//var_dump(PDO::ATTR_DRIVER_NAME);
////var_dump(Yii::$app->db);
//var_dump(Yii::$app->db->driverName);
//var_dump(Yii::$app->db->masterPdo);
//var_dump(Yii::$app->db->migrationTable);
//var_dump(Yii::$app->db->schema->getTableSchema($this->migrationTable, true));
//var_dump(Yii::$app->db->schema->getTableSchema('{{%migration}}', true));
?>
<div class="site-index">
<!--    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>-->

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">CPU Traffic</span>
                        <span class="info-box-number">90<small>%</small></span>
                    </div>
                     <!--/.info-box-content-->
                </div>
                 <!--/.info-box-->
            </div>
             <!--/.col-->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Likes</span>
                        <span class="info-box-number">41,410</span>
                    </div>
                     <!--/.info-box-content-->
                </div>
                 <!--/.info-box-->
            </div>
             <!--/.col-->

             <!--fix for small devices only-->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Sales</span>
                        <span class="info-box-number">760</span>
                    </div>
                     <!--/.info-box-content-->
                </div>
                 <!--/.info-box-->
            </div>
             <!--/.col-->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">New Members</span>
                        <span class="info-box-number">2,000</span>
                    </div>
                     <!--/.info-box-content-->
                </div>
                 <!--/.info-box-->
            </div>
             <!--/.col-->
        </div>
        <!-- /.row -->

<!--        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Monthly Recap Report</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <div class="btn-group">
                                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-wrench"></i></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                     /.box-header
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-8">
                                <p class="text-center">
                                    <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong>
                                </p>

                                <div class="chart">
                                     Sales Chart Canvas
                                    <canvas id="salesChart" style="height: 180px;"></canvas>
                                </div>
                                 /.chart-responsive
                            </div>
                             /.col
                            <div class="col-md-4">
                                <p class="text-center">
                                    <strong>Goal Completion</strong>
                                </p>

                                <div class="progress-group">
                                    <span class="progress-text">Add Products to Cart</span>
                                    <span class="progress-number"><b>160</b>/200</span>

                                    <div class="progress sm">
                                        <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                                    </div>
                                </div>
                                 /.progress-group
                                <div class="progress-group">
                                    <span class="progress-text">Complete Purchase</span>
                                    <span class="progress-number"><b>310</b>/400</span>

                                    <div class="progress sm">
                                        <div class="progress-bar progress-bar-red" style="width: 80%"></div>
                                    </div>
                                </div>
                                 /.progress-group
                                <div class="progress-group">
                                    <span class="progress-text">Visit Premium Page</span>
                                    <span class="progress-number"><b>480</b>/800</span>

                                    <div class="progress sm">
                                        <div class="progress-bar progress-bar-green" style="width: 80%"></div>
                                    </div>
                                </div>
                                 /.progress-group
                                <div class="progress-group">
                                    <span class="progress-text">Send Inquiries</span>
                                    <span class="progress-number"><b>250</b>/500</span>

                                    <div class="progress sm">
                                        <div class="progress-bar progress-bar-yellow" style="width: 80%"></div>
                                    </div>
                                </div>
                                 /.progress-group
                            </div>
                             /.col
                        </div>
                         /.row
                    </div>
                     ./box-body
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>
                                    <h5 class="description-header">$35,210.43</h5>
                                    <span class="description-text">TOTAL REVENUE</span>
                                </div>
                                 /.description-block
                            </div>
                             /.col
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                                    <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span>
                                    <h5 class="description-header">$10,390.90</h5>
                                    <span class="description-text">TOTAL COST</span>
                                </div>
                                 /.description-block
                            </div>
                             /.col
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                                    <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span>
                                    <h5 class="description-header">$24,813.53</h5>
                                    <span class="description-text">TOTAL PROFIT</span>
                                </div>
                                 /.description-block
                            </div>
                             /.col
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block">
                                    <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span>
                                    <h5 class="description-header">1200</h5>
                                    <span class="description-text">GOAL COMPLETIONS</span>
                                </div>
                                 /.description-block
                            </div>
                        </div>
                         /.row
                    </div>
                     /.box-footer
                </div>
                 /.box
            </div>
             /.col
        </div>-->
        <!-- /.row -->

        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-8">
                <!-- MAP & BOX PANE -->
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Visitors Report</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="row">
                            <div class="col-md-9 col-sm-8">
                                <div class="pad">
                                    <!-- Map will be created here -->
                                    <div id="world-map-markers" style="height: 325px;"></div>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-3 col-sm-4">
                                <div class="pad box-pane-right bg-green" style="min-height: 280px">
                                    <div class="description-block margin-bottom">
                                        <div class="sparkbar pad" data-color="#fff">90,70,90,70,75,80,70</div>
                                        <h5 class="description-header">8390</h5>
                                        <span class="description-text">Visits</span>
                                    </div>
                                    <!-- /.description-block -->
                                    <div class="description-block margin-bottom">
                                        <div class="sparkbar pad" data-color="#fff">90,50,90,70,61,83,63</div>
                                        <h5 class="description-header">30%</h5>
                                        <span class="description-text">Referrals</span>
                                    </div>
                                    <!-- /.description-block -->
                                    <div class="description-block">
                                        <div class="sparkbar pad" data-color="#fff">90,50,90,70,61,83,63</div>
                                        <h5 class="description-header">70%</h5>
                                        <span class="description-text">Organic</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <div class="row">
                    <div class="col-md-6">
                        <!-- DIRECT CHAT -->
                        <div class="box box-warning direct-chat direct-chat-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Direct Chat</h3>

                                <div class="box-tools pull-right">
                                    <span data-toggle="tooltip" title="3 New Messages" class="badge bg-yellow">3</span>
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">
                                        <i class="fa fa-comments"></i></button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <!-- Conversations are loaded here -->
                                <div class="direct-chat-messages">
                                    <!-- Message. Default to the left -->
                                    <div class="direct-chat-msg">
                                        <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-name pull-left">Alexander Pierce</span>
                                            <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                                        </div>
                                        <!-- /.direct-chat-info -->
                                        <img class="direct-chat-img" src="dist/img/user1-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
                                        <div class="direct-chat-text">
                                            Is this template really for free? That's unbelievable!
                                        </div>
                                        <!-- /.direct-chat-text -->
                                    </div>
                                    <!-- /.direct-chat-msg -->

                                    <!-- Message to the right -->
                                    <div class="direct-chat-msg right">
                                        <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-name pull-right">Sarah Bullock</span>
                                            <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
                                        </div>
                                        <!-- /.direct-chat-info -->
                                        <img class="direct-chat-img" src="dist/img/user3-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
                                        <div class="direct-chat-text">
                                            You better believe it!
                                        </div>
                                        <!-- /.direct-chat-text -->
                                    </div>
                                    <!-- /.direct-chat-msg -->

                                    <!-- Message. Default to the left -->
                                    <div class="direct-chat-msg">
                                        <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-name pull-left">Alexander Pierce</span>
                                            <span class="direct-chat-timestamp pull-right">23 Jan 5:37 pm</span>
                                        </div>
                                        <!-- /.direct-chat-info -->
                                        <img class="direct-chat-img" src="dist/img/user1-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
                                        <div class="direct-chat-text">
                                            Working with AdminLTE on a great new app! Wanna join?
                                        </div>
                                        <!-- /.direct-chat-text -->
                                    </div>
                                    <!-- /.direct-chat-msg -->

                                    <!-- Message to the right -->
                                    <div class="direct-chat-msg right">
                                        <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-name pull-right">Sarah Bullock</span>
                                            <span class="direct-chat-timestamp pull-left">23 Jan 6:10 pm</span>
                                        </div>
                                        <!-- /.direct-chat-info -->
                                        <img class="direct-chat-img" src="dist/img/user3-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
                                        <div class="direct-chat-text">
                                            I would love to.
                                        </div>
                                        <!-- /.direct-chat-text -->
                                    </div>
                                    <!-- /.direct-chat-msg -->

                                </div>
                                <!--/.direct-chat-messages-->

                                <!-- Contacts are loaded here -->
                                <div class="direct-chat-contacts">
                                    <ul class="contacts-list">
                                        <li>
                                            <a href="#">
                                                <img class="contacts-list-img" src="dist/img/user1-128x128.jpg" alt="User Image">

                                                <div class="contacts-list-info">
                                                    <span class="contacts-list-name">
                                                        Count Dracula
                                                        <small class="contacts-list-date pull-right">2/28/2015</small>
                                                    </span>
                                                    <span class="contacts-list-msg">How have you been? I was...</span>
                                                </div>
                                                <!-- /.contacts-list-info -->
                                            </a>
                                        </li>
                                        <!-- End Contact Item -->
                                        <li>
                                            <a href="#">
                                                <img class="contacts-list-img" src="dist/img/user7-128x128.jpg" alt="User Image">

                                                <div class="contacts-list-info">
                                                    <span class="contacts-list-name">
                                                        Sarah Doe
                                                        <small class="contacts-list-date pull-right">2/23/2015</small>
                                                    </span>
                                                    <span class="contacts-list-msg">I will be waiting for...</span>
                                                </div>
                                                <!-- /.contacts-list-info -->
                                            </a>
                                        </li>
                                        <!-- End Contact Item -->
                                        <li>
                                            <a href="#">
                                                <img class="contacts-list-img" src="dist/img/user3-128x128.jpg" alt="User Image">

                                                <div class="contacts-list-info">
                                                    <span class="contacts-list-name">
                                                        Nadia Jolie
                                                        <small class="contacts-list-date pull-right">2/20/2015</small>
                                                    </span>
                                                    <span class="contacts-list-msg">i'll call you back at...</span>
                                                </div>
                                                <!-- /.contacts-list-info -->
                                            </a>
                                        </li>
                                        <!-- End Contact Item -->
                                        <li>
                                            <a href="#">
                                                <img class="contacts-list-img" src="dist/img/user5-128x128.jpg" alt="User Image">

                                                <div class="contacts-list-info">
                                                    <span class="contacts-list-name">
                                                        Nora S. Vans
                                                        <small class="contacts-list-date pull-right">2/10/2015</small>
                                                    </span>
                                                    <span class="contacts-list-msg">Where is your new...</span>
                                                </div>
                                                <!-- /.contacts-list-info -->
                                            </a>
                                        </li>
                                        <!-- End Contact Item -->
                                        <li>
                                            <a href="#">
                                                <img class="contacts-list-img" src="dist/img/user6-128x128.jpg" alt="User Image">

                                                <div class="contacts-list-info">
                                                    <span class="contacts-list-name">
                                                        John K.
                                                        <small class="contacts-list-date pull-right">1/27/2015</small>
                                                    </span>
                                                    <span class="contacts-list-msg">Can I take a look at...</span>
                                                </div>
                                                <!-- /.contacts-list-info -->
                                            </a>
                                        </li>
                                        <!-- End Contact Item -->
                                        <li>
                                            <a href="#">
                                                <img class="contacts-list-img" src="dist/img/user8-128x128.jpg" alt="User Image">

                                                <div class="contacts-list-info">
                                                    <span class="contacts-list-name">
                                                        Kenneth M.
                                                        <small class="contacts-list-date pull-right">1/4/2015</small>
                                                    </span>
                                                    <span class="contacts-list-msg">Never mind I found...</span>
                                                </div>
                                                <!-- /.contacts-list-info -->
                                            </a>
                                        </li>
                                        <!-- End Contact Item -->
                                    </ul>
                                    <!-- /.contatcts-list -->
                                </div>
                                <!-- /.direct-chat-pane -->
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <form action="#" method="post">
                                    <div class="input-group">
                                        <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-warning btn-flat">Send</button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                    <!-- /.col -->

                    <div class="col-md-6">
                        <!-- USERS LIST -->
                        <div class="box box-danger">
                            <div class="box-header with-border">
                                <h3 class="box-title">Latest Members</h3>

                                <div class="box-tools pull-right">
                                    <span class="label label-danger">8 New Members</span>
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body no-padding">
                                <ul class="users-list clearfix">
                                    <li>
                                        <img src="dist/img/user1-128x128.jpg" alt="User Image">
                                        <a class="users-list-name" href="#">Alexander Pierce</a>
                                        <span class="users-list-date">Today</span>
                                    </li>
                                    <li>
                                        <img src="dist/img/user8-128x128.jpg" alt="User Image">
                                        <a class="users-list-name" href="#">Norman</a>
                                        <span class="users-list-date">Yesterday</span>
                                    </li>
                                    <li>
                                        <img src="dist/img/user7-128x128.jpg" alt="User Image">
                                        <a class="users-list-name" href="#">Jane</a>
                                        <span class="users-list-date">12 Jan</span>
                                    </li>
                                    <li>
                                        <img src="dist/img/user6-128x128.jpg" alt="User Image">
                                        <a class="users-list-name" href="#">John</a>
                                        <span class="users-list-date">12 Jan</span>
                                    </li>
                                    <li>
                                        <img src="dist/img/user2-160x160.jpg" alt="User Image">
                                        <a class="users-list-name" href="#">Alexander</a>
                                        <span class="users-list-date">13 Jan</span>
                                    </li>
                                    <li>
                                        <img src="dist/img/user5-128x128.jpg" alt="User Image">
                                        <a class="users-list-name" href="#">Sarah</a>
                                        <span class="users-list-date">14 Jan</span>
                                    </li>
                                    <li>
                                        <img src="dist/img/user4-128x128.jpg" alt="User Image">
                                        <a class="users-list-name" href="#">Nora</a>
                                        <span class="users-list-date">15 Jan</span>
                                    </li>
                                    <li>
                                        <img src="dist/img/user3-128x128.jpg" alt="User Image">
                                        <a class="users-list-name" href="#">Nadia</a>
                                        <span class="users-list-date">15 Jan</span>
                                    </li>
                                </ul>
                                <!-- /.users-list -->
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer text-center">
                                <a href="javascript:void(0)" class="uppercase">View All Users</a>
                            </div>
                            <!-- /.box-footer -->
                        </div>
                        <!--/.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Latest Orders</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th>Order ID</th>
                                        <th>Item</th>
                                        <th>Status</th>
                                        <th>Popularity</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><a href="pages/examples/invoice.html">OR9842</a></td>
                                        <td>Call of Duty IV</td>
                                        <td><span class="label label-success">Shipped</span></td>
                                        <td>
                                            <div class="sparkbar" data-color="#00a65a" data-height="20">90,80,90,-70,61,-83,63</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><a href="pages/examples/invoice.html">OR1848</a></td>
                                        <td>Samsung Smart TV</td>
                                        <td><span class="label label-warning">Pending</span></td>
                                        <td>
                                            <div class="sparkbar" data-color="#f39c12" data-height="20">90,80,-90,70,61,-83,68</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><a href="pages/examples/invoice.html">OR7429</a></td>
                                        <td>iPhone 6 Plus</td>
                                        <td><span class="label label-danger">Delivered</span></td>
                                        <td>
                                            <div class="sparkbar" data-color="#f56954" data-height="20">90,-80,90,70,-61,83,63</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><a href="pages/examples/invoice.html">OR7429</a></td>
                                        <td>Samsung Smart TV</td>
                                        <td><span class="label label-info">Processing</span></td>
                                        <td>
                                            <div class="sparkbar" data-color="#00c0ef" data-height="20">90,80,-90,70,-61,83,63</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><a href="pages/examples/invoice.html">OR1848</a></td>
                                        <td>Samsung Smart TV</td>
                                        <td><span class="label label-warning">Pending</span></td>
                                        <td>
                                            <div class="sparkbar" data-color="#f39c12" data-height="20">90,80,-90,70,61,-83,68</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><a href="pages/examples/invoice.html">OR7429</a></td>
                                        <td>iPhone 6 Plus</td>
                                        <td><span class="label label-danger">Delivered</span></td>
                                        <td>
                                            <div class="sparkbar" data-color="#f56954" data-height="20">90,-80,90,70,-61,83,63</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><a href="pages/examples/invoice.html">OR9842</a></td>
                                        <td>Call of Duty IV</td>
                                        <td><span class="label label-success">Shipped</span></td>
                                        <td>
                                            <div class="sparkbar" data-color="#00a65a" data-height="20">90,80,90,-70,61,-83,63</div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
                        <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->

            <div class="col-md-4">
                <!-- Info Boxes Style 2 -->
                <div class="info-box bg-yellow">
                    <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Inventory</span>
                        <span class="info-box-number">5,200</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 50%"></div>
                        </div>
                        <span class="progress-description">
                            50% Increase in 30 Days
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
                <div class="info-box bg-green">
                    <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Mentions</span>
                        <span class="info-box-number">92,050</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 20%"></div>
                        </div>
                        <span class="progress-description">
                            20% Increase in 30 Days
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
                <div class="info-box bg-red">
                    <span class="info-box-icon"><i class="ion ion-ios-cloud-download-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Downloads</span>
                        <span class="info-box-number">114,381</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                            70% Increase in 30 Days
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
                <div class="info-box bg-aqua">
                    <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Direct Messages</span>
                        <span class="info-box-number">163,921</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 40%"></div>
                        </div>
                        <span class="progress-description">
                            40% Increase in 30 Days
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->

                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Browser Usage</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="chart-responsive">
                                    <canvas id="pieChart" height="150"></canvas>
                                </div>
                                <!-- ./chart-responsive -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <ul class="chart-legend clearfix">
                                    <li><i class="fa fa-circle-o text-red"></i> Chrome</li>
                                    <li><i class="fa fa-circle-o text-green"></i> IE</li>
                                    <li><i class="fa fa-circle-o text-yellow"></i> FireFox</li>
                                    <li><i class="fa fa-circle-o text-aqua"></i> Safari</li>
                                    <li><i class="fa fa-circle-o text-light-blue"></i> Opera</li>
                                    <li><i class="fa fa-circle-o text-gray"></i> Navigator</li>
                                </ul>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer no-padding">
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">United States of America
                                    <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 12%</span></a></li>
                            <li><a href="#">India <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 4%</span></a>
                            </li>
                            <li><a href="#">China
                                    <span class="pull-right text-yellow"><i class="fa fa-angle-left"></i> 0%</span></a></li>
                        </ul>
                    </div>
                    <!-- /.footer -->
                </div>
                <!-- /.box -->

                <!-- PRODUCT LIST -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Recently Added Products</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <ul class="products-list product-list-in-box">
                            <li class="item">
                                <div class="product-img">
                                    <img src="dist/img/default-50x50.gif" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <a href="javascript:void(0)" class="product-title">Samsung TV
                                        <span class="label label-warning pull-right">$1800</span></a>
                                    <span class="product-description">
                                        Samsung 32" 1080p 60Hz LED Smart HDTV.
                                    </span>
                                </div>
                            </li>
                            <!-- /.item -->
                            <li class="item">
                                <div class="product-img">
                                    <img src="dist/img/default-50x50.gif" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <a href="javascript:void(0)" class="product-title">Bicycle
                                        <span class="label label-info pull-right">$700</span></a>
                                    <span class="product-description">
                                        26" Mongoose Dolomite Men's 7-speed, Navy Blue.
                                    </span>
                                </div>
                            </li>
                            <!-- /.item -->
                            <li class="item">
                                <div class="product-img">
                                    <img src="dist/img/default-50x50.gif" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <a href="javascript:void(0)" class="product-title">Xbox One <span class="label label-danger pull-right">$350</span></a>
                                    <span class="product-description">
                                        Xbox One Console Bundle with Halo Master Chief Collection.
                                    </span>
                                </div>
                            </li>
                            <!-- /.item -->
                            <li class="item">
                                <div class="product-img">
                                    <img src="dist/img/default-50x50.gif" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <a href="javascript:void(0)" class="product-title">PlayStation 4
                                        <span class="label label-success pull-right">$399</span></a>
                                    <span class="product-description">
                                        PlayStation 4 500GB Console (PS4)
                                    </span>
                                </div>
                            </li>
                            <!-- /.item -->
                        </ul>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a href="javascript:void(0)" class="uppercase">View All Products</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>150</h3>

                        <p>New Orders</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>53<sup style="font-size: 20px">%</sup></h3>

                        <p>Bounce Rate</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>44</h3>

                        <p>User Registrations</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>65</h3>

                        <p>Unique Visitors</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-7 connectedSortable">
                <!-- Custom tabs (Charts with tabs)-->
                <div class="nav-tabs-custom">
                    <!-- Tabs within a box -->
                    <ul class="nav nav-tabs pull-right">
                        <li class="active"><a href="#revenue-chart" data-toggle="tab">Area</a></li>
                        <li><a href="#sales-chart" data-toggle="tab">Donut</a></li>
                        <li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li>
                    </ul>
                    <div class="tab-content no-padding">
                        <!-- Morris chart - Sales -->
                        <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
                        <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;"></div>
                    </div>
                </div>
                <!-- /.nav-tabs-custom -->

                <!-- Chat box -->
                <div class="box box-success">
                    <div class="box-header">
                        <i class="fa fa-comments-o"></i>

                        <h3 class="box-title">Chat</h3>

                        <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
                            <div class="btn-group" data-toggle="btn-toggle">
                                <button type="button" class="btn btn-default btn-sm active"><i class="fa fa-square text-green"></i>
                                </button>
                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-square text-red"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="box-body chat" id="chat-box">
                        <!-- chat item -->
                        <div class="item">
                            <img src="dist/img/user4-128x128.jpg" alt="user image" class="online">

                            <p class="message">
                                <a href="#" class="name">
                                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 2:15</small>
                                    Mike Doe
                                </a>
                                I would like to meet you to discuss the latest news about
                                the arrival of the new theme. They say it is going to be one the
                                best themes on the market
                            </p>
                            <div class="attachment">
                                <h4>Attachments:</h4>

                                <p class="filename">
                                    Theme-thumbnail-image.jpg
                                </p>

                                <div class="pull-right">
                                    <button type="button" class="btn btn-primary btn-sm btn-flat">Open</button>
                                </div>
                            </div>
                            <!-- /.attachment -->
                        </div>
                        <!-- /.item -->
                        <!-- chat item -->
                        <div class="item">
                            <img src="dist/img/user3-128x128.jpg" alt="user image" class="offline">

                            <p class="message">
                                <a href="#" class="name">
                                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:15</small>
                                    Alexander Pierce
                                </a>
                                I would like to meet you to discuss the latest news about
                                the arrival of the new theme. They say it is going to be one the
                                best themes on the market
                            </p>
                        </div>
                        <!-- /.item -->
                        <!-- chat item -->
                        <div class="item">
                            <img src="dist/img/user2-160x160.jpg" alt="user image" class="offline">

                            <p class="message">
                                <a href="#" class="name">
                                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:30</small>
                                    Susan Doe
                                </a>
                                I would like to meet you to discuss the latest news about
                                the arrival of the new theme. They say it is going to be one the
                                best themes on the market
                            </p>
                        </div>
                        <!-- /.item -->
                    </div>
                    <!-- /.chat -->
                    <div class="box-footer">
                        <div class="input-group">
                            <input class="form-control" placeholder="Type message...">

                            <div class="input-group-btn">
                                <button type="button" class="btn btn-success"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box (chat box) -->

                <!-- TO DO List -->
                <div class="box box-primary">
                    <div class="box-header">
                        <i class="ion ion-clipboard"></i>

                        <h3 class="box-title">To Do List</h3>

                        <div class="box-tools pull-right">
                            <ul class="pagination pagination-sm inline">
                                <li><a href="#">&laquo;</a></li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <ul class="todo-list">
                            <li>
                                <!-- drag handle -->
                                <span class="handle">
                                    <i class="fa fa-ellipsis-v"></i>
                                    <i class="fa fa-ellipsis-v"></i>
                                </span>
                                <!-- checkbox -->
                                <input type="checkbox" value="">
                                <!-- todo text -->
                                <span class="text">Design a nice theme</span>
                                <!-- Emphasis label -->
                                <small class="label label-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
                                <!-- General tools such as edit or delete-->
                                <div class="tools">
                                    <i class="fa fa-edit"></i>
                                    <i class="fa fa-trash-o"></i>
                                </div>
                            </li>
                            <li>
                                <span class="handle">
                                    <i class="fa fa-ellipsis-v"></i>
                                    <i class="fa fa-ellipsis-v"></i>
                                </span>
                                <input type="checkbox" value="">
                                <span class="text">Make the theme responsive</span>
                                <small class="label label-info"><i class="fa fa-clock-o"></i> 4 hours</small>
                                <div class="tools">
                                    <i class="fa fa-edit"></i>
                                    <i class="fa fa-trash-o"></i>
                                </div>
                            </li>
                            <li>
                                <span class="handle">
                                    <i class="fa fa-ellipsis-v"></i>
                                    <i class="fa fa-ellipsis-v"></i>
                                </span>
                                <input type="checkbox" value="">
                                <span class="text">Let theme shine like a star</span>
                                <small class="label label-warning"><i class="fa fa-clock-o"></i> 1 day</small>
                                <div class="tools">
                                    <i class="fa fa-edit"></i>
                                    <i class="fa fa-trash-o"></i>
                                </div>
                            </li>
                            <li>
                                <span class="handle">
                                    <i class="fa fa-ellipsis-v"></i>
                                    <i class="fa fa-ellipsis-v"></i>
                                </span>
                                <input type="checkbox" value="">
                                <span class="text">Let theme shine like a star</span>
                                <small class="label label-success"><i class="fa fa-clock-o"></i> 3 days</small>
                                <div class="tools">
                                    <i class="fa fa-edit"></i>
                                    <i class="fa fa-trash-o"></i>
                                </div>
                            </li>
                            <li>
                                <span class="handle">
                                    <i class="fa fa-ellipsis-v"></i>
                                    <i class="fa fa-ellipsis-v"></i>
                                </span>
                                <input type="checkbox" value="">
                                <span class="text">Check your messages and notifications</span>
                                <small class="label label-primary"><i class="fa fa-clock-o"></i> 1 week</small>
                                <div class="tools">
                                    <i class="fa fa-edit"></i>
                                    <i class="fa fa-trash-o"></i>
                                </div>
                            </li>
                            <li>
                                <span class="handle">
                                    <i class="fa fa-ellipsis-v"></i>
                                    <i class="fa fa-ellipsis-v"></i>
                                </span>
                                <input type="checkbox" value="">
                                <span class="text">Let theme shine like a star</span>
                                <small class="label label-default"><i class="fa fa-clock-o"></i> 1 month</small>
                                <div class="tools">
                                    <i class="fa fa-edit"></i>
                                    <i class="fa fa-trash-o"></i>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix no-border">
                        <button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
                    </div>
                </div>
                <!-- /.box -->

                <!-- quick email widget -->
                <div class="box box-info">
                    <div class="box-header">
                        <i class="fa fa-envelope"></i>

                        <h3 class="box-title">Quick Email</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <div class="box-body">
                        <form action="#" method="post">
                            <div class="form-group">
                                <input type="email" class="form-control" name="emailto" placeholder="Email to:">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="subject" placeholder="Subject">
                            </div>
                            <div>
                                <textarea class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="box-footer clearfix">
                        <button type="button" class="pull-right btn btn-default" id="sendEmail">Send
                            <i class="fa fa-arrow-circle-right"></i></button>
                    </div>
                </div>

            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-5 connectedSortable">

                <!-- Map box -->
                <div class="box box-solid bg-light-blue-gradient">
                    <div class="box-header">
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-primary btn-sm daterange pull-right" data-toggle="tooltip" title="Date range">
                                <i class="fa fa-calendar"></i></button>
                            <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                                <i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /. tools -->

                        <i class="fa fa-map-marker"></i>

                        <h3 class="box-title">
                            Visitors
                        </h3>
                    </div>
                    <div class="box-body">
                        <div id="world-map" style="height: 250px; width: 100%;"></div>
                    </div>
                    <!-- /.box-body-->
                    <div class="box-footer no-border">
                        <div class="row">
                            <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                                <div id="sparkline-1"></div>
                                <div class="knob-label">Visitors</div>
                            </div>
                            <!-- ./col -->
                            <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                                <div id="sparkline-2"></div>
                                <div class="knob-label">Online</div>
                            </div>
                            <!-- ./col -->
                            <div class="col-xs-4 text-center">
                                <div id="sparkline-3"></div>
                                <div class="knob-label">Exists</div>
                            </div>
                            <!-- ./col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
                <!-- /.box -->

                <!-- solid sales graph -->
                <div class="box box-solid bg-teal-gradient">
                    <div class="box-header">
                        <i class="fa fa-th"></i>

                        <h3 class="box-title">Sales Graph</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body border-radius-none">
                        <div class="chart" id="line-chart" style="height: 250px;"></div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer no-border">
                        <div class="row">
                            <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                                <input type="text" class="knob" data-readonly="true" value="20" data-width="60" data-height="60" data-fgColor="#39CCCC">

                                <div class="knob-label">Mail-Orders</div>
                            </div>
                            <!-- ./col -->
                            <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                                <input type="text" class="knob" data-readonly="true" value="50" data-width="60" data-height="60" data-fgColor="#39CCCC">

                                <div class="knob-label">Online</div>
                            </div>
                            <!-- ./col -->
                            <div class="col-xs-4 text-center">
                                <input type="text" class="knob" data-readonly="true" value="30" data-width="60" data-height="60" data-fgColor="#39CCCC">

                                <div class="knob-label">In-Store</div>
                            </div>
                            <!-- ./col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->

                <!-- Calendar -->
                <div class="box box-solid bg-green-gradient">
                    <div class="box-header">
                        <i class="fa fa-calendar"></i>

                        <h3 class="box-title">Calendar</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <!-- button with a dropdown -->
                            <div class="btn-group">
                                <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bars"></i></button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="#">Add new event</a></li>
                                    <li><a href="#">Clear events</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">View calendar</a></li>
                                </ul>
                            </div>
                            <button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                            </button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <!--The calendar -->
                        <div id="calendar" style="width: 100%"></div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-black">
                        <div class="row">
                            <div class="col-sm-6">
                                <!-- Progress bars -->
                                <div class="clearfix">
                                    <span class="pull-left">Task #1</span>
                                    <small class="pull-right">90%</small>
                                </div>
                                <div class="progress xs">
                                    <div class="progress-bar progress-bar-green" style="width: 90%;"></div>
                                </div>

                                <div class="clearfix">
                                    <span class="pull-left">Task #2</span>
                                    <small class="pull-right">70%</small>
                                </div>
                                <div class="progress xs">
                                    <div class="progress-bar progress-bar-green" style="width: 70%;"></div>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="clearfix">
                                    <span class="pull-left">Task #3</span>
                                    <small class="pull-right">60%</small>
                                </div>
                                <div class="progress xs">
                                    <div class="progress-bar progress-bar-green" style="width: 60%;"></div>
                                </div>

                                <div class="clearfix">
                                    <span class="pull-left">Task #4</span>
                                    <small class="pull-right">40%</small>
                                </div>
                                <div class="progress xs">
                                    <div class="progress-bar progress-bar-green" style="width: 40%;"></div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
                <!-- /.box -->

            </section>
            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            General UI
            <small>Preview of UI elements</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">UI</a></li>
            <li class="active">General</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- COLOR PALETTE -->
        <div class="box box-default color-palette-box">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-tag"></i> Color Palette</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-4 col-md-2">
                        <h4 class="text-center">Primary</h4>

                        <div class="color-palette-set">
                            <div class="bg-light-blue disabled color-palette"><span>Disabled</span></div>
                            <div class="bg-light-blue color-palette"><span>#3c8dbc</span></div>
                            <div class="bg-light-blue-active color-palette"><span>Active</span></div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 col-md-2">
                        <h4 class="text-center">Info</h4>

                        <div class="color-palette-set">
                            <div class="bg-aqua disabled color-palette"><span>Disabled</span></div>
                            <div class="bg-aqua color-palette"><span>#00c0ef</span></div>
                            <div class="bg-aqua-active color-palette"><span>Active</span></div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 col-md-2">
                        <h4 class="text-center">Success</h4>

                        <div class="color-palette-set">
                            <div class="bg-green disabled color-palette"><span>Disabled</span></div>
                            <div class="bg-green color-palette"><span>#00a65a</span></div>
                            <div class="bg-green-active color-palette"><span>Active</span></div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 col-md-2">
                        <h4 class="text-center">Warning</h4>

                        <div class="color-palette-set">
                            <div class="bg-yellow disabled color-palette"><span>Disabled</span></div>
                            <div class="bg-yellow color-palette"><span>#f39c12</span></div>
                            <div class="bg-yellow-active color-palette"><span>Active</span></div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 col-md-2">
                        <h4 class="text-center">Danger</h4>

                        <div class="color-palette-set">
                            <div class="bg-red disabled color-palette"><span>Disabled</span></div>
                            <div class="bg-red color-palette"><span>#f56954</span></div>
                            <div class="bg-red-active color-palette"><span>Active</span></div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 col-md-2">
                        <h4 class="text-center">Gray</h4>

                        <div class="color-palette-set">
                            <div class="bg-gray disabled color-palette"><span>Disabled</span></div>
                            <div class="bg-gray color-palette"><span>#d2d6de</span></div>
                            <div class="bg-gray-active color-palette"><span>Active</span></div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-sm-4 col-md-2">
                        <h4 class="text-center">Navy</h4>

                        <div class="color-palette-set">
                            <div class="bg-navy disabled color-palette"><span>Disabled</span></div>
                            <div class="bg-navy color-palette"><span>#001F3F</span></div>
                            <div class="bg-navy-active color-palette"><span>Active</span></div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 col-md-2">
                        <h4 class="text-center">Teal</h4>

                        <div class="color-palette-set">
                            <div class="bg-teal disabled color-palette"><span>Disabled</span></div>
                            <div class="bg-teal color-palette"><span>#39CCCC</span></div>
                            <div class="bg-teal-active color-palette"><span>Active</span></div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 col-md-2">
                        <h4 class="text-center">Purple</h4>

                        <div class="color-palette-set">
                            <div class="bg-purple disabled color-palette"><span>Disabled</span></div>
                            <div class="bg-purple color-palette"><span>#605ca8</span></div>
                            <div class="bg-purple-active color-palette"><span>Active</span></div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 col-md-2">
                        <h4 class="text-center">Orange</h4>

                        <div class="color-palette-set">
                            <div class="bg-orange disabled color-palette"><span>Disabled</span></div>
                            <div class="bg-orange color-palette"><span>#ff851b</span></div>
                            <div class="bg-orange-active color-palette"><span>Active</span></div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 col-md-2">
                        <h4 class="text-center">Maroon</h4>

                        <div class="color-palette-set">
                            <div class="bg-maroon disabled color-palette"><span>Disabled</span></div>
                            <div class="bg-maroon color-palette"><span>#D81B60</span></div>
                            <div class="bg-maroon-active color-palette"><span>Active</span></div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 col-md-2">
                        <h4 class="text-center">Black</h4>

                        <div class="color-palette-set">
                            <div class="bg-black disabled color-palette"><span>Disabled</span></div>
                            <div class="bg-black color-palette"><span>#111111</span></div>
                            <div class="bg-black-active color-palette"><span>Active</span></div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <!-- START ALERTS AND CALLOUTS -->
        <h2 class="page-header">Alerts and Callouts</h2>

        <div class="row">
            <div class="col-md-6">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <i class="fa fa-warning"></i>

                        <h3 class="box-title">Alerts</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            Danger alert preview. This alert is dismissable. A wonderful serenity has taken possession of my entire
                            soul, like these sweet mornings of spring which I enjoy with my whole heart.
                        </div>
                        <div class="alert alert-info alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-info"></i> Alert!</h4>
                            Info alert preview. This alert is dismissable.
                        </div>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            Warning alert preview. This alert is dismissable.
                        </div>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Alert!</h4>
                            Success alert preview. This alert is dismissable.
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->

            <div class="col-md-6">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <i class="fa fa-bullhorn"></i>

                        <h3 class="box-title">Callouts</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="callout callout-danger">
                            <h4>I am a danger callout!</h4>

                            <p>There is a problem that we need to fix. A wonderful serenity has taken possession of my entire soul,
                                like these sweet mornings of spring which I enjoy with my whole heart.</p>
                        </div>
                        <div class="callout callout-info">
                            <h4>I am an info callout!</h4>

                            <p>Follow the steps to continue to payment.</p>
                        </div>
                        <div class="callout callout-warning">
                            <h4>I am a warning callout!</h4>

                            <p>This is a yellow callout.</p>
                        </div>
                        <div class="callout callout-success">
                            <h4>I am a success callout!</h4>

                            <p>This is a green callout.</p>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <!-- END ALERTS AND CALLOUTS -->
        <!-- START CUSTOM TABS -->
        <h2 class="page-header">AdminLTE Custom Tabs</h2>

        <div class="row">
            <div class="col-md-6">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Tab 1</a></li>
                        <li><a href="#tab_2" data-toggle="tab">Tab 2</a></li>
                        <li><a href="#tab_3" data-toggle="tab">Tab 3</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Dropdown <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                                <li role="presentation" class="divider"></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                            </ul>
                        </li>
                        <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <b>How to use:</b>

                            <p>Exactly like the original bootstrap tabs except you should use
                                the custom wrapper <code>.nav-tabs-custom</code> to achieve this style.</p>
                            A wonderful serenity has taken possession of my entire soul,
                            like these sweet mornings of spring which I enjoy with my whole heart.
                            I am alone, and feel the charm of existence in this spot,
                            which was created for the bliss of souls like mine. I am so happy,
                            my dear friend, so absorbed in the exquisite sense of mere tranquil existence,
                            that I neglect my talents. I should be incapable of drawing a single stroke
                            at the present moment; and yet I feel that I never was a greater artist than now.
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            The European languages are members of the same family. Their separate existence is a myth.
                            For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ
                            in their grammar, their pronunciation and their most common words. Everyone realizes why a
                            new common language would be desirable: one could refuse to pay expensive translators. To
                            achieve this, it would be necessary to have uniform grammar, pronunciation and more common
                            words. If several languages coalesce, the grammar of the resulting language is more simple
                            and regular than that of the individual languages.
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_3">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                            It has survived not only five centuries, but also the leap into electronic typesetting,
                            remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop publishing software
                            like Aldus PageMaker including versions of Lorem Ipsum.
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
            <!-- /.col -->

            <div class="col-md-6">
                <!-- Custom Tabs (Pulled to the right) -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs pull-right">
                        <li class="active"><a href="#tab_1-1" data-toggle="tab">Tab 1</a></li>
                        <li><a href="#tab_2-2" data-toggle="tab">Tab 2</a></li>
                        <li><a href="#tab_3-2" data-toggle="tab">Tab 3</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Dropdown <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                                <li role="presentation" class="divider"></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                            </ul>
                        </li>
                        <li class="pull-left header"><i class="fa fa-th"></i> Custom Tabs</li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1-1">
                            <b>How to use:</b>

                            <p>Exactly like the original bootstrap tabs except you should use
                                the custom wrapper <code>.nav-tabs-custom</code> to achieve this style.</p>
                            A wonderful serenity has taken possession of my entire soul,
                            like these sweet mornings of spring which I enjoy with my whole heart.
                            I am alone, and feel the charm of existence in this spot,
                            which was created for the bliss of souls like mine. I am so happy,
                            my dear friend, so absorbed in the exquisite sense of mere tranquil existence,
                            that I neglect my talents. I should be incapable of drawing a single stroke
                            at the present moment; and yet I feel that I never was a greater artist than now.
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2-2">
                            The European languages are members of the same family. Their separate existence is a myth.
                            For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ
                            in their grammar, their pronunciation and their most common words. Everyone realizes why a
                            new common language would be desirable: one could refuse to pay expensive translators. To
                            achieve this, it would be necessary to have uniform grammar, pronunciation and more common
                            words. If several languages coalesce, the grammar of the resulting language is more simple
                            and regular than that of the individual languages.
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_3-2">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                            It has survived not only five centuries, but also the leap into electronic typesetting,
                            remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                            sheets containing Lorem Ipsum passages, and more recently with desktop publishing software
                            like Aldus PageMaker including versions of Lorem Ipsum.
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <!-- END CUSTOM TABS -->
        <!-- START PROGRESS BARS -->
        <h2 class="page-header">Progress Bars</h2>

        <div class="row">
            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Progress Bars Different Sizes</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <p><code>.progress</code></p>

                        <div class="progress">
                            <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                <span class="sr-only">40% Complete (success)</span>
                            </div>
                        </div>
                        <p>Class: <code>.sm</code></p>

                        <div class="progress progress-sm active">
                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                <span class="sr-only">20% Complete</span>
                            </div>
                        </div>
                        <p>Class: <code>.xs</code></p>

                        <div class="progress progress-xs">
                            <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                <span class="sr-only">60% Complete (warning)</span>
                            </div>
                        </div>
                        <p>Class: <code>.xxs</code></p>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                <span class="sr-only">60% Complete (warning)</span>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col (left) -->
            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Progress bars</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="progress">
                            <div class="progress-bar progress-bar-green" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                <span class="sr-only">40% Complete (success)</span>
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-aqua" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                <span class="sr-only">20% Complete</span>
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-yellow" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                <span class="sr-only">60% Complete (warning)</span>
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-red" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                <span class="sr-only">80% Complete</span>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col (right) -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Vertical Progress Bars Different Sizes</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body text-center">
                        <p>By adding the class <code>.vertical</code> and <code>.progress-sm</code>, <code>.progress-xs</code> or
                            <code>.progress-xxs</code> we achieve:</p>

                        <div class="progress vertical active">
                            <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="height: 40%">
                                <span class="sr-only">40%</span>
                            </div>
                        </div>
                        <div class="progress vertical progress-sm">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="height: 100%">
                                <span class="sr-only">100%</span>
                            </div>
                        </div>
                        <div class="progress vertical progress-xs">
                            <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="height: 60%">
                                <span class="sr-only">60%</span>
                            </div>
                        </div>
                        <div class="progress vertical progress-xxs">
                            <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="height: 60%">
                                <span class="sr-only">60%</span>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col (left) -->
            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Vertical Progress bars</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body text-center">
                        <p>By adding the class <code>.vertical</code> we achieve:</p>

                        <div class="progress vertical">
                            <div class="progress-bar progress-bar-green" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="height: 40%">
                                <span class="sr-only">40%</span>
                            </div>
                        </div>
                        <div class="progress vertical">
                            <div class="progress-bar progress-bar-aqua" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="height: 20%">
                                <span class="sr-only">20%</span>
                            </div>
                        </div>
                        <div class="progress vertical">
                            <div class="progress-bar progress-bar-yellow" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="height: 60%">
                                <span class="sr-only">60%</span>
                            </div>
                        </div>
                        <div class="progress vertical">
                            <div class="progress-bar progress-bar-red" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="height: 80%">
                                <span class="sr-only">80%</span>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col (right) -->
        </div>
        <!-- /.row -->
        <!-- END PROGRESS BARS -->

        <!-- START ACCORDION & CAROUSEL-->
        <h2 class="page-header">Bootstrap Accordion & Carousel</h2>

        <div class="row">
            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Collapsible Accordion</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="box-group" id="accordion">
                            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                            <div class="panel box box-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            Collapsible Group Item #1
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="box-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                                        wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                                        eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
                                        assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                                        nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                                        farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus
                                        labore sustainable VHS.
                                    </div>
                                </div>
                            </div>
                            <div class="panel box box-danger">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                            Collapsible Group Danger
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="box-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                                        wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                                        eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
                                        assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                                        nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                                        farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus
                                        labore sustainable VHS.
                                    </div>
                                </div>
                            </div>
                            <div class="panel box box-success">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                            Collapsible Group Success
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="box-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                                        wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                                        eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
                                        assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                                        nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                                        farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus
                                        labore sustainable VHS.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Carousel</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="http://placehold.it/900x500/39CCCC/ffffff&text=I+Love+Bootstrap" alt="First slide">

                                    <div class="carousel-caption">
                                        First Slide
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="http://placehold.it/900x500/3c8dbc/ffffff&text=I+Love+Bootstrap" alt="Second slide">

                                    <div class="carousel-caption">
                                        Second Slide
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="http://placehold.it/900x500/f39c12/ffffff&text=I+Love+Bootstrap" alt="Third slide">

                                    <div class="carousel-caption">
                                        Third Slide
                                    </div>
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="fa fa-angle-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="fa fa-angle-right"></span>
                            </a>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <!-- END ACCORDION & CAROUSEL-->

        <!-- START TYPOGRAPHY -->
        <h2 class="page-header">Typography</h2>

        <div class="row">
            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <i class="fa fa-text-width"></i>

                        <h3 class="box-title">Headlines</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <h1>h1. Bootstrap heading</h1>

                        <h2>h2. Bootstrap heading</h2>

                        <h3>h3. Bootstrap heading</h3>
                        <h4>h4. Bootstrap heading</h4>
                        <h5>h5. Bootstrap heading</h5>
                        <h6>h6. Bootstrap heading</h6>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- ./col -->
            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <i class="fa fa-text-width"></i>

                        <h3 class="box-title">Text Emphasis</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <p class="lead">Lead to emphasize importance</p>

                        <p class="text-green">Text green to emphasize success</p>

                        <p class="text-aqua">Text aqua to emphasize info</p>

                        <p class="text-light-blue">Text light blue to emphasize info (2)</p>

                        <p class="text-red">Text red to emphasize danger</p>

                        <p class="text-yellow">Text yellow to emphasize warning</p>

                        <p class="text-muted">Text muted to emphasize general</p>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <i class="fa fa-text-width"></i>

                        <h3 class="box-title">Block Quotes</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <blockquote>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                            <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                        </blockquote>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- ./col -->
            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <i class="fa fa-text-width"></i>

                        <h3 class="box-title">Block Quotes Pulled Right</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body clearfix">
                        <blockquote class="pull-right">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                            <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                        </blockquote>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-md-4">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <i class="fa fa-text-width"></i>

                        <h3 class="box-title">Unordered List</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <ul>
                            <li>Lorem ipsum dolor sit amet</li>
                            <li>Consectetur adipiscing elit</li>
                            <li>Integer molestie lorem at massa</li>
                            <li>Facilisis in pretium nisl aliquet</li>
                            <li>Nulla volutpat aliquam velit
                                <ul>
                                    <li>Phasellus iaculis neque</li>
                                    <li>Purus sodales ultricies</li>
                                    <li>Vestibulum laoreet porttitor sem</li>
                                    <li>Ac tristique libero volutpat at</li>
                                </ul>
                            </li>
                            <li>Faucibus porta lacus fringilla vel</li>
                            <li>Aenean sit amet erat nunc</li>
                            <li>Eget porttitor lorem</li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- ./col -->
            <div class="col-md-4">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <i class="fa fa-text-width"></i>

                        <h3 class="box-title">Ordered Lists</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <ol>
                            <li>Lorem ipsum dolor sit amet</li>
                            <li>Consectetur adipiscing elit</li>
                            <li>Integer molestie lorem at massa</li>
                            <li>Facilisis in pretium nisl aliquet</li>
                            <li>Nulla volutpat aliquam velit
                                <ol>
                                    <li>Phasellus iaculis neque</li>
                                    <li>Purus sodales ultricies</li>
                                    <li>Vestibulum laoreet porttitor sem</li>
                                    <li>Ac tristique libero volutpat at</li>
                                </ol>
                            </li>
                            <li>Faucibus porta lacus fringilla vel</li>
                            <li>Aenean sit amet erat nunc</li>
                            <li>Eget porttitor lorem</li>
                        </ol>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- ./col -->
            <div class="col-md-4">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <i class="fa fa-text-width"></i>

                        <h3 class="box-title">Unstyled List</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <ul class="list-unstyled">
                            <li>Lorem ipsum dolor sit amet</li>
                            <li>Consectetur adipiscing elit</li>
                            <li>Integer molestie lorem at massa</li>
                            <li>Facilisis in pretium nisl aliquet</li>
                            <li>Nulla volutpat aliquam velit
                                <ul>
                                    <li>Phasellus iaculis neque</li>
                                    <li>Purus sodales ultricies</li>
                                    <li>Vestibulum laoreet porttitor sem</li>
                                    <li>Ac tristique libero volutpat at</li>
                                </ul>
                            </li>
                            <li>Faucibus porta lacus fringilla vel</li>
                            <li>Aenean sit amet erat nunc</li>
                            <li>Eget porttitor lorem</li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <i class="fa fa-text-width"></i>

                        <h3 class="box-title">Description</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <dl>
                            <dt>Description lists</dt>
                            <dd>A description list is perfect for defining terms.</dd>
                            <dt>Euismod</dt>
                            <dd>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
                            <dd>Donec id elit non mi porta gravida at eget metus.</dd>
                            <dt>Malesuada porta</dt>
                            <dd>Etiam porta sem malesuada magna mollis euismod.</dd>
                        </dl>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- ./col -->
            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <i class="fa fa-text-width"></i>

                        <h3 class="box-title">Description Horizontal</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <dl class="dl-horizontal">
                            <dt>Description lists</dt>
                            <dd>A description list is perfect for defining terms.</dd>
                            <dt>Euismod</dt>
                            <dd>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
                            <dd>Donec id elit non mi porta gravida at eget metus.</dd>
                            <dt>Malesuada porta</dt>
                            <dd>Etiam porta sem malesuada magna mollis euismod.</dd>
                            <dt>Felis euismod semper eget lacinia</dt>
                            <dd>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo
                                sit amet risus.
                            </dd>
                        </dl>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- END TYPOGRAPHY -->

    </section>
    <!-- /.content -->

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Buttons
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">UI</a></li>
            <li class="active">Buttons</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <i class="fa fa-edit"></i>

                        <h3 class="box-title">Buttons</h3>
                    </div>
                    <div class="box-body pad table-responsive">
                        <p>Various types of buttons. Using the base class <code>.btn</code></p>
                        <table class="table table-bordered text-center">
                            <tr>
                                <th>Normal</th>
                                <th>Large <code>.btn-lg</code></th>
                                <th>Small <code>.btn-sm</code></th>
                                <th>X-Small <code>.btn-xs</code></th>
                                <th>Flat <code>.btn-flat</code></th>
                                <th>Disabled <code>.disabled</code></th>
                            </tr>
                            <tr>
                                <td>
                                    <button type="button" class="btn btn-block btn-default">Default</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-default btn-lg">Default</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-default btn-sm">Default</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-default btn-xs">Default</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-default btn-flat">Default</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-default disabled">Default</button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <button type="button" class="btn btn-block btn-primary">Primary</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-primary btn-lg">Primary</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-primary btn-sm">Primary</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-primary btn-xs">Primary</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-primary btn-flat">Primary</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-primary disabled">Primary</button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <button type="button" class="btn btn-block btn-success">Success</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-success btn-lg">Success</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-success btn-sm">Success</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-success btn-xs">Success</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-success btn-flat">Success</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-success disabled">Success</button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <button type="button" class="btn btn-block btn-info">Info</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-info btn-lg">Info</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-info btn-sm">Info</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-info btn-xs">Info</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-info btn-flat">Info</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-info disabled">Info</button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <button type="button" class="btn btn-block btn-danger">Danger</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-danger btn-lg">Danger</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-danger btn-sm">Danger</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-danger btn-xs">Danger</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-danger btn-flat">Danger</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-danger disabled">Danger</button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <button type="button" class="btn btn-block btn-warning">Warning</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-warning btn-lg">Warning</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-warning btn-sm">Warning</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-warning btn-xs">Warning</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-warning btn-flat">Warning</button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-block btn-warning disabled">Warning</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- ./row -->
        <div class="row">
            <div class="col-md-6">
                <!-- Block buttons -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Block Buttons</h3>
                    </div>
                    <div class="box-body">
                        <button type="button" class="btn btn-default btn-block">.btn-block</button>
                        <button type="button" class="btn btn-default btn-block btn-flat">.btn-block .btn-flat</button>
                        <button type="button" class="btn btn-default btn-block btn-sm">.btn-block .btn-sm</button>
                    </div>
                </div>
                <!-- /.box -->

                <!-- Horizontal grouping -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Horizontal Button Group</h3>
                    </div>
                    <div class="box-body table-responsive pad">
                        <p>
                            Horizontal button groups are easy to create with bootstrap. Just add your buttons
                            inside <code>&lt;div class="btn-group"&gt;&lt;/div&gt;</code>
                        </p>

                        <table class="table table-bordered">
                            <tr>
                                <th>Button</th>
                                <th>Icons</th>
                                <th>Flat</th>
                                <th>Dropdown</th>
                            </tr>
                            <!-- Default -->
                            <tr>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default">Left</button>
                                        <button type="button" class="btn btn-default">Middle</button>
                                        <button type="button" class="btn btn-default">Right</button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-default"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-default"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-flat"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-default btn-flat"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-default btn-flat"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default">1</button>
                                        <button type="button" class="btn btn-default">2</button>

                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Dropdown link</a></li>
                                                <li><a href="#">Dropdown link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <!-- ./default -->
                            <!-- Info -->
                            <tr>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-info">Left</button>
                                        <button type="button" class="btn btn-info">Middle</button>
                                        <button type="button" class="btn btn-info">Right</button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-info"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-info"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-info"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-info btn-flat"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-info btn-flat"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-info btn-flat"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-info">1</button>
                                        <button type="button" class="btn btn-info">2</button>

                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Dropdown link</a></li>
                                                <li><a href="#">Dropdown link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <!-- /. info -->
                            <!-- /.danger -->
                            <tr>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger">Left</button>
                                        <button type="button" class="btn btn-danger">Middle</button>
                                        <button type="button" class="btn btn-danger">Right</button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-danger"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-danger"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger btn-flat"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-danger btn-flat"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-danger btn-flat"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger">1</button>
                                        <button type="button" class="btn btn-danger">2</button>

                                        <div class="btn-group">
                                            <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Dropdown link</a></li>
                                                <li><a href="#">Dropdown link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <!-- /.danger -->
                            <!-- warning -->
                            <tr>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-warning">Left</button>
                                        <button type="button" class="btn btn-warning">Middle</button>
                                        <button type="button" class="btn btn-warning">Right</button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-warning"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-warning"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-warning"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-warning btn-flat"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-warning btn-flat"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-warning btn-flat"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-warning">1</button>
                                        <button type="button" class="btn btn-warning">2</button>

                                        <div class="btn-group">
                                            <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Dropdown link</a></li>
                                                <li><a href="#">Dropdown link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <!-- /.warning -->
                            <!-- success -->
                            <tr>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success">Left</button>
                                        <button type="button" class="btn btn-success">Middle</button>
                                        <button type="button" class="btn btn-success">Right</button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-success"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-success"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success btn-flat"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-success btn-flat"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-success btn-flat"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success">1</button>
                                        <button type="button" class="btn btn-success">2</button>

                                        <div class="btn-group">
                                            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Dropdown link</a></li>
                                                <li><a href="#">Dropdown link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <!-- /.success -->
                        </table>
                    </div>
                </div>
                <!-- /.box -->

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Button Addon</h3>
                    </div>
                    <div class="box-body">
                        <p>With dropdown</p>

                        <div class="input-group margin">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action
                                    <span class="fa fa-caret-down"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <!-- /btn-group -->
                            <input type="text" class="form-control">
                        </div>
                        <!-- /input-group -->
                        <p>Normal</p>

                        <div class="input-group margin">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-danger">Action</button>
                            </div>
                            <!-- /btn-group -->
                            <input type="text" class="form-control">
                        </div>
                        <!-- /input-group -->
                        <p>Flat</p>

                        <div class="input-group margin">
                            <input type="text" class="form-control">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-info btn-flat">Go!</button>
                            </span>
                        </div>
                        <!-- /input-group -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <!-- split buttons box -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Split buttons</h3>
                    </div>
                    <div class="box-body">
                        <!-- Split button -->
                        <p>Normal split buttons:</p>

                        <div class="margin">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default">Action</button>
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-info">Action</button>
                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-danger">Action</button>
                                <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-success">Action</button>
                                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-warning">Action</button>
                                <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- flat split buttons -->
                        <p>Flat split buttons:</p>

                        <div class="margin">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-flat">Action</button>
                                <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-info btn-flat">Action</button>
                                <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-danger btn-flat">Action</button>
                                <button type="button" class="btn btn-danger btn-flat dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-success btn-flat">Action</button>
                                <button type="button" class="btn btn-success btn-flat dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-warning btn-flat">Action</button>
                                <button type="button" class="btn btn-warning btn-flat dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- end split buttons box -->

                <!-- social buttons -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Social Buttons (By <a href="https://github.com/lipis/bootstrap-social">Lipis</a>)
                        </h3>
                    </div>
                    <div class="box-body">
                        <a class="btn btn-block btn-social btn-bitbucket">
                            <i class="fa fa-bitbucket"></i> Sign in with Bitbucket
                        </a>
                        <a class="btn btn-block btn-social btn-dropbox">
                            <i class="fa fa-dropbox"></i> Sign in with Dropbox
                        </a>
                        <a class="btn btn-block btn-social btn-facebook">
                            <i class="fa fa-facebook"></i> Sign in with Facebook
                        </a>
                        <a class="btn btn-block btn-social btn-flickr">
                            <i class="fa fa-flickr"></i> Sign in with Flickr
                        </a>
                        <a class="btn btn-block btn-social btn-foursquare">
                            <i class="fa fa-foursquare"></i> Sign in with Foursquare
                        </a>
                        <a class="btn btn-block btn-social btn-github">
                            <i class="fa fa-github"></i> Sign in with GitHub
                        </a>
                        <a class="btn btn-block btn-social btn-google">
                            <i class="fa fa-google-plus"></i> Sign in with Google
                        </a>
                        <a class="btn btn-block btn-social btn-instagram">
                            <i class="fa fa-instagram"></i> Sign in with Instagram
                        </a>
                        <a class="btn btn-block btn-social btn-linkedin">
                            <i class="fa fa-linkedin"></i> Sign in with LinkedIn
                        </a>
                        <a class="btn btn-block btn-social btn-tumblr">
                            <i class="fa fa-tumblr"></i> Sign in with Tumblr
                        </a>
                        <a class="btn btn-block btn-social btn-twitter">
                            <i class="fa fa-twitter"></i> Sign in with Twitter
                        </a>
                        <a class="btn btn-block btn-social btn-vk">
                            <i class="fa fa-vk"></i> Sign in with VK
                        </a>
                        <br>

                        <div class="text-center">
                            <a class="btn btn-social-icon btn-bitbucket"><i class="fa fa-bitbucket"></i></a>
                            <a class="btn btn-social-icon btn-dropbox"><i class="fa fa-dropbox"></i></a>
                            <a class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook"></i></a>
                            <a class="btn btn-social-icon btn-flickr"><i class="fa fa-flickr"></i></a>
                            <a class="btn btn-social-icon btn-foursquare"><i class="fa fa-foursquare"></i></a>
                            <a class="btn btn-social-icon btn-github"><i class="fa fa-github"></i></a>
                            <a class="btn btn-social-icon btn-google"><i class="fa fa-google-plus"></i></a>
                            <a class="btn btn-social-icon btn-instagram"><i class="fa fa-instagram"></i></a>
                            <a class="btn btn-social-icon btn-linkedin"><i class="fa fa-linkedin"></i></a>
                            <a class="btn btn-social-icon btn-tumblr"><i class="fa fa-tumblr"></i></a>
                            <a class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></a>
                            <a class="btn btn-social-icon btn-vk"><i class="fa fa-vk"></i></a>
                        </div>
                    </div>
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
            <div class="col-md-6">
                <!-- Application buttons -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Application Buttons</h3>
                    </div>
                    <div class="box-body">
                        <p>Add the classes <code>.btn.btn-app</code> to an <code>&lt;a></code> tag to achieve the following:</p>
                        <a class="btn btn-app">
                            <i class="fa fa-edit"></i> Edit
                        </a>
                        <a class="btn btn-app">
                            <i class="fa fa-play"></i> Play
                        </a>
                        <a class="btn btn-app">
                            <i class="fa fa-repeat"></i> Repeat
                        </a>
                        <a class="btn btn-app">
                            <i class="fa fa-pause"></i> Pause
                        </a>
                        <a class="btn btn-app">
                            <i class="fa fa-save"></i> Save
                        </a>
                        <a class="btn btn-app">
                            <span class="badge bg-yellow">3</span>
                            <i class="fa fa-bullhorn"></i> Notifications
                        </a>
                        <a class="btn btn-app">
                            <span class="badge bg-green">300</span>
                            <i class="fa fa-barcode"></i> Products
                        </a>
                        <a class="btn btn-app">
                            <span class="badge bg-purple">891</span>
                            <i class="fa fa-users"></i> Users
                        </a>
                        <a class="btn btn-app">
                            <span class="badge bg-teal">67</span>
                            <i class="fa fa-inbox"></i> Orders
                        </a>
                        <a class="btn btn-app">
                            <span class="badge bg-aqua">12</span>
                            <i class="fa fa-envelope"></i> Inbox
                        </a>
                        <a class="btn btn-app">
                            <span class="badge bg-red">531</span>
                            <i class="fa fa-heart-o"></i> Likes
                        </a>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <!-- Various colors -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Different colors</h3>
                    </div>
                    <div class="box-body">
                        <p>Mix and match with various background colors. Use base class <code>.btn</code> and add any available
                            <code>.bg-*</code> class</p>
                        <!-- You may notice a .margin class added
                        here but that's only to make the content
                        display correctly without having to use a table-->
                        <p>
                            <button type="button" class="btn bg-maroon btn-flat margin">.btn.bg-maroon.btn-flat</button>
                            <button type="button" class="btn bg-purple btn-flat margin">.btn.bg-purple.btn-flat</button>
                            <button type="button" class="btn bg-navy btn-flat margin">.btn.bg-navy.btn-flat</button>
                            <button type="button" class="btn bg-orange btn-flat margin">.btn.bg-orange.btn-flat</button>
                            <button type="button" class="btn bg-olive btn-flat margin">.btn.bg-olive.btn-flat</button>
                        </p>

                        <p>
                            <button type="button" class="btn bg-maroon margin">.btn.bg-maroon</button>
                            <button type="button" class="btn bg-purple margin">.btn.bg-purple</button>
                            <button type="button" class="btn bg-navy margin">.btn.bg-navy</button>
                            <button type="button" class="btn bg-orange margin">.btn.bg-orange</button>
                            <button type="button" class="btn bg-olive margin">.btn.bg-olive</button>
                        </p>
                    </div>
                </div>
                <!-- /.box -->

                <!-- Vertical grouping -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Vertical Button Group</h3>
                    </div>
                    <div class="box-body table-responsive pad">

                        <p>
                            Vertical button groups are easy to create with bootstrap. Just add your buttons
                            inside <code>&lt;div class="btn-group-vertical"&gt;&lt;/div&gt;</code>
                        </p>

                        <table class="table table-bordered">
                            <tr>
                                <th>Button</th>
                                <th>Icons</th>
                                <th>Flat</th>
                                <th>Dropdown</th>
                            </tr>
                            <!-- Default -->
                            <tr>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-default">Top</button>
                                        <button type="button" class="btn btn-default">Middle</button>
                                        <button type="button" class="btn btn-default">Bottom</button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-default"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-default"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-default"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-default btn-flat"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-default btn-flat"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-default btn-flat"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-default">1</button>
                                        <button type="button" class="btn btn-default">2</button>

                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Dropdown link</a></li>
                                                <li><a href="#">Dropdown link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <!-- ./default -->
                            <!-- Info -->
                            <tr>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-info">Top</button>
                                        <button type="button" class="btn btn-info">Middle</button>
                                        <button type="button" class="btn btn-info">Bottom</button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-info"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-info"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-info"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-info btn-flat"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-info btn-flat"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-info btn-flat"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-info">1</button>
                                        <button type="button" class="btn btn-info">2</button>

                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Dropdown link</a></li>
                                                <li><a href="#">Dropdown link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <!-- /. info -->
                            <!-- /.danger -->
                            <tr>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-danger">Top</button>
                                        <button type="button" class="btn btn-danger">Middle</button>
                                        <button type="button" class="btn btn-danger">Bottom</button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-danger"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-danger"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-danger"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-danger btn-flat"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-danger btn-flat"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-danger btn-flat"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-danger">1</button>
                                        <button type="button" class="btn btn-danger">2</button>

                                        <div class="btn-group">
                                            <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Dropdown link</a></li>
                                                <li><a href="#">Dropdown link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <!-- /.danger -->
                            <!-- warning -->
                            <tr>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-warning">Top</button>
                                        <button type="button" class="btn btn-warning">Middle</button>
                                        <button type="button" class="btn btn-warning">Bottom</button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-warning"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-warning"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-warning"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-warning btn-flat"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-warning btn-flat"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-warning btn-flat"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-warning">1</button>
                                        <button type="button" class="btn btn-warning">2</button>

                                        <div class="btn-group">
                                            <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Dropdown link</a></li>
                                                <li><a href="#">Dropdown link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <!-- /.warning -->
                            <!-- success -->
                            <tr>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-success">Top</button>
                                        <button type="button" class="btn btn-success">Middle</button>
                                        <button type="button" class="btn btn-success">Bottom</button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-success"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-success"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-success"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-success btn-flat"><i class="fa fa-align-left"></i></button>
                                        <button type="button" class="btn btn-success btn-flat"><i class="fa fa-align-center"></i></button>
                                        <button type="button" class="btn btn-success btn-flat"><i class="fa fa-align-right"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group-vertical">
                                        <button type="button" class="btn btn-success">1</button>
                                        <button type="button" class="btn btn-success">2</button>

                                        <div class="btn-group">
                                            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Dropdown link</a></li>
                                                <li><a href="#">Dropdown link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <!-- /.success -->
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /. row -->
    </section>
    <!-- /.content -->

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Sliders
            <small>range sliders</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">UI</a></li>
            <li class="active">Sliders</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Ion Slider</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row margin">
                            <div class="col-sm-6">
                                <input id="range_1" type="text" name="range_1" value="">
                            </div>

                            <div class="col-sm-6">
                                <input id="range_2" type="text" name="range_2" value="1000;100000" data-type="double" data-step="500" data-postfix=" &euro;" data-from="30000" data-to="90000" data-hasgrid="true">
                            </div>
                        </div>
                        <div class="row margin">
                            <div class="col-sm-6">
                                <input id="range_5" type="text" name="range_5" value="">
                            </div>
                            <div class="col-sm-6">
                                <input id="range_6" type="text" name="range_6" value="">
                            </div>
                        </div>
                        <div class="row margin">
                            <div class="col-sm-12">
                                <input id="range_4" type="text" name="range_4" value="10000;100000">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Bootstrap Slider</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row margin">
                            <div class="col-sm-6">
                                <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200" data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="red">

                                <p>data-slider-id="red"</p>
                                <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200" data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="blue">

                                <p>data-slider-id="blue"</p>
                                <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200" data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="green">

                                <p>data-slider-id="green"</p>
                                <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200" data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="yellow">

                                <p>data-slider-id="yellow"</p>
                                <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200" data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="aqua">

                                <p>data-slider-id="aqua"</p>
                                <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200" data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="purple">

                                <p style="margin-top: 10px">data-slider-id="purple"</p>
                            </div>
                            <div class="col-sm-6 text-center">
                                <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200" data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="vertical" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="red">
                                <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200" data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="vertical" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="blue">
                                <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200" data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="vertical" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="green">
                                <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200" data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="vertical" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="yellow">
                                <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200" data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="vertical" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="aqua">
                                <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200" data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="vertical" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="purple">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Invoice
            <small>#007612</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Invoice</li>
        </ol>
    </section>

    <div class="pad margin no-print">
        <div class="callout callout-info" style="margin-bottom: 0!important;">
            <h4><i class="fa fa-info"></i> Note:</h4>
            This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
        </div>
    </div>

    <!-- Main content -->
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i> AdminLTE, Inc.
                    <small class="pull-right">Date: 2/10/2014</small>
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                From
                <address>
                    <strong>Admin, Inc.</strong><br>
                    795 Folsom Ave, Suite 600<br>
                    San Francisco, CA 94107<br>
                    Phone: (804) 123-5432<br>
                    Email: info@almasaeedstudio.com
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                To
                <address>
                    <strong>John Doe</strong><br>
                    795 Folsom Ave, Suite 600<br>
                    San Francisco, CA 94107<br>
                    Phone: (555) 539-1037<br>
                    Email: john.doe@example.com
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <b>Invoice #007612</b><br>
                <br>
                <b>Order ID:</b> 4F3S8J<br>
                <b>Payment Due:</b> 2/22/2014<br>
                <b>Account:</b> 968-34567
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Qty</th>
                            <th>Product</th>
                            <th>Serial #</th>
                            <th>Description</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Call of Duty</td>
                            <td>455-981-221</td>
                            <td>El snort testosterone trophy driving gloves handsome</td>
                            <td>$64.50</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Need for Speed IV</td>
                            <td>247-925-726</td>
                            <td>Wes Anderson umami biodiesel</td>
                            <td>$50.00</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Monsters DVD</td>
                            <td>735-845-642</td>
                            <td>Terry Richardson helvetica tousled street art master</td>
                            <td>$10.70</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Grown Ups Blue Ray</td>
                            <td>422-568-642</td>
                            <td>Tousled lomo letterpress</td>
                            <td>$25.99</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
                <p class="lead">Payment Methods:</p>
                <img src="../../dist/img/credit/visa.png" alt="Visa">
                <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
                <img src="../../dist/img/credit/american-express.png" alt="American Express">
                <img src="../../dist/img/credit/paypal2.png" alt="Paypal">

                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                    Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
                    dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                </p>
            </div>
            <!-- /.col -->
            <div class="col-xs-6">
                <p class="lead">Amount Due 2/22/2014</p>

                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th style="width:50%">Subtotal:</th>
                            <td>$250.30</td>
                        </tr>
                        <tr>
                            <th>Tax (9.3%)</th>
                            <td>$10.34</td>
                        </tr>
                        <tr>
                            <th>Shipping:</th>
                            <td>$5.80</td>
                        </tr>
                        <tr>
                            <th>Total:</th>
                            <td>$265.24</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">
                <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
                </button>
                <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                    <i class="fa fa-download"></i> Generate PDF
                </button>
            </div>
        </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Calendar
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Calendar</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h4 class="box-title">Draggable Events</h4>
                    </div>
                    <div class="box-body">
                        <!-- the events -->
                        <div id="external-events">
                            <div class="external-event bg-green">Lunch</div>
                            <div class="external-event bg-yellow">Go home</div>
                            <div class="external-event bg-aqua">Do homework</div>
                            <div class="external-event bg-light-blue">Work on UI design</div>
                            <div class="external-event bg-red">Sleep tight</div>
                            <div class="checkbox">
                                <label for="drop-remove">
                                    <input type="checkbox" id="drop-remove">
                                    remove after drop
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /. box -->
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Create Event</h3>
                    </div>
                    <div class="box-body">
                        <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
                          <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
                            <ul class="fc-color-picker" id="color-chooser">
                                <li><a class="text-aqua" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-blue" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-light-blue" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-teal" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-yellow" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-orange" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-green" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-lime" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-red" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-purple" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-fuchsia" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-navy" href="#"><i class="fa fa-square"></i></a></li>
                            </ul>
                        </div>
                        <!-- /btn-group -->
                        <div class="input-group">
                            <input id="new-event" type="text" class="form-control" placeholder="Event Title">

                            <div class="input-group-btn">
                                <button id="add-new-event" type="button" class="btn btn-primary btn-flat">Add</button>
                            </div>
                            <!-- /btn-group -->
                        </div>
                        <!-- /input-group -->
                    </div>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="box box-primary">
                    <div class="box-body no-padding">
                        <!-- THE CALENDAR -->
                        <div id="calendar"></div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

    <section class="content-header">
        <h1>
            Widgets
            <small>Preview page</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Widgets</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Messages</span>
                        <span class="info-box-number">1,410</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-flag-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Bookmarks</span>
                        <span class="info-box-number">410</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-files-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Uploads</span>
                        <span class="info-box-number">13,648</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Likes</span>
                        <span class="info-box-number">93,139</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- =========================================================== -->

        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-aqua">
                    <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Bookmarks</span>
                        <span class="info-box-number">41,410</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                            70% Increase in 30 Days
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-green">
                    <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Likes</span>
                        <span class="info-box-number">41,410</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                            70% Increase in 30 Days
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-yellow">
                    <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Events</span>
                        <span class="info-box-number">41,410</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                            70% Increase in 30 Days
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-red">
                    <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Comments</span>
                        <span class="info-box-number">41,410</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                            70% Increase in 30 Days
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- =========================================================== -->

        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>150</h3>

                        <p>New Orders</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>53<sup style="font-size: 20px">%</sup></h3>

                        <p>Bounce Rate</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>44</h3>

                        <p>User Registrations</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>65</h3>

                        <p>Unique Visitors</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->

        <!-- =========================================================== -->

        <div class="row">
            <div class="col-md-3">
                <div class="box box-default collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Expandable</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                            </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        The body of the box
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Removable</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        The body of the box
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Collapsable</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        The body of the box
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3">
                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title">Loading state</h3>
                    </div>
                    <div class="box-body">
                        The body of the box
                    </div>
                    <!-- /.box-body -->
                    <!-- Loading (remove the following to stop the loading)-->
                    <div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>
                    <!-- end loading -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- =========================================================== -->

        <div class="row">
            <div class="col-md-3">
                <div class="box box-default collapsed-box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Expandable</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                            </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        The body of the box
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3">
                <div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Removable</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        The body of the box
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3">
                <div class="box box-warning box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Collapsable</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        The body of the box
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3">
                <div class="box box-danger box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Loading state</h3>
                    </div>
                    <div class="box-body">
                        The body of the box
                    </div>
                    <!-- /.box-body -->
                    <!-- Loading (remove the following to stop the loading)-->
                    <div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>
                    <!-- end loading -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- =========================================================== -->

        <!-- Direct Chat -->
        <div class="row">
            <div class="col-md-3">
                <!-- DIRECT CHAT PRIMARY -->
                <div class="box box-primary direct-chat direct-chat-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Direct Chat</h3>

                        <div class="box-tools pull-right">
                            <span data-toggle="tooltip" title="3 New Messages" class="badge bg-light-blue">3</span>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">
                                <i class="fa fa-comments"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Conversations are loaded here -->
                        <div class="direct-chat-messages">
                            <!-- Message. Default to the left -->
                            <div class="direct-chat-msg">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-left">Alexander Pierce</span>
                                    <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                                </div>
                                <!-- /.direct-chat-info -->
                                <img class="direct-chat-img" src="../dist/img/user1-128x128.jpg" alt="Message User Image"><!-- /.direct-chat-img -->
                                <div class="direct-chat-text">
                                    Is this template really for free? That's unbelievable!
                                </div>
                                <!-- /.direct-chat-text -->
                            </div>
                            <!-- /.direct-chat-msg -->

                            <!-- Message to the right -->
                            <div class="direct-chat-msg right">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-right">Sarah Bullock</span>
                                    <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
                                </div>
                                <!-- /.direct-chat-info -->
                                <img class="direct-chat-img" src="../dist/img/user3-128x128.jpg" alt="Message User Image"><!-- /.direct-chat-img -->
                                <div class="direct-chat-text">
                                    You better believe it!
                                </div>
                                <!-- /.direct-chat-text -->
                            </div>
                            <!-- /.direct-chat-msg -->
                        </div>
                        <!--/.direct-chat-messages-->

                        <!-- Contacts are loaded here -->
                        <div class="direct-chat-contacts">
                            <ul class="contacts-list">
                                <li>
                                    <a href="#">
                                        <img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="User Image">

                                        <div class="contacts-list-info">
                                            <span class="contacts-list-name">
                                                Count Dracula
                                                <small class="contacts-list-date pull-right">2/28/2015</small>
                                            </span>
                                            <span class="contacts-list-msg">How have you been? I was...</span>
                                        </div>
                                        <!-- /.contacts-list-info -->
                                    </a>
                                </li>
                                <!-- End Contact Item -->
                            </ul>
                            <!-- /.contatcts-list -->
                        </div>
                        <!-- /.direct-chat-pane -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <form action="#" method="post">
                            <div class="input-group">
                                <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-primary btn-flat">Send</button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!--/.direct-chat -->
            </div>
            <!-- /.col -->

            <div class="col-md-3">
                <!-- DIRECT CHAT SUCCESS -->
                <div class="box box-success direct-chat direct-chat-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Direct Chat</h3>

                        <div class="box-tools pull-right">
                            <span data-toggle="tooltip" title="3 New Messages" class="badge bg-green">3</span>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">
                                <i class="fa fa-comments"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Conversations are loaded here -->
                        <div class="direct-chat-messages">
                            <!-- Message. Default to the left -->
                            <div class="direct-chat-msg">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-left">Alexander Pierce</span>
                                    <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                                </div>
                                <!-- /.direct-chat-info -->
                                <img class="direct-chat-img" src="../dist/img/user1-128x128.jpg" alt="Message User Image"><!-- /.direct-chat-img -->
                                <div class="direct-chat-text">
                                    Is this template really for free? That's unbelievable!
                                </div>
                                <!-- /.direct-chat-text -->
                            </div>
                            <!-- /.direct-chat-msg -->

                            <!-- Message to the right -->
                            <div class="direct-chat-msg right">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-right">Sarah Bullock</span>
                                    <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
                                </div>
                                <!-- /.direct-chat-info -->
                                <img class="direct-chat-img" src="../dist/img/user3-128x128.jpg" alt="Message User Image"><!-- /.direct-chat-img -->
                                <div class="direct-chat-text">
                                    You better believe it!
                                </div>
                                <!-- /.direct-chat-text -->
                            </div>
                            <!-- /.direct-chat-msg -->
                        </div>
                        <!--/.direct-chat-messages-->

                        <!-- Contacts are loaded here -->
                        <div class="direct-chat-contacts">
                            <ul class="contacts-list">
                                <li>
                                    <a href="#">
                                        <img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="User Image">

                                        <div class="contacts-list-info">
                                            <span class="contacts-list-name">
                                                Count Dracula
                                                <small class="contacts-list-date pull-right">2/28/2015</small>
                                            </span>
                                            <span class="contacts-list-msg">How have you been? I was...</span>
                                        </div>
                                        <!-- /.contacts-list-info -->
                                    </a>
                                </li>
                                <!-- End Contact Item -->
                            </ul>
                            <!-- /.contatcts-list -->
                        </div>
                        <!-- /.direct-chat-pane -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <form action="#" method="post">
                            <div class="input-group">
                                <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-success btn-flat">Send</button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!--/.direct-chat -->
            </div>
            <!-- /.col -->

            <div class="col-md-3">
                <!-- DIRECT CHAT WARNING -->
                <div class="box box-warning direct-chat direct-chat-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Direct Chat</h3>

                        <div class="box-tools pull-right">
                            <span data-toggle="tooltip" title="3 New Messages" class="badge bg-yellow">3</span>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">
                                <i class="fa fa-comments"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Conversations are loaded here -->
                        <div class="direct-chat-messages">
                            <!-- Message. Default to the left -->
                            <div class="direct-chat-msg">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-left">Alexander Pierce</span>
                                    <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                                </div>
                                <!-- /.direct-chat-info -->
                                <img class="direct-chat-img" src="../dist/img/user1-128x128.jpg" alt="Message User Image"><!-- /.direct-chat-img -->
                                <div class="direct-chat-text">
                                    Is this template really for free? That's unbelievable!
                                </div>
                                <!-- /.direct-chat-text -->
                            </div>
                            <!-- /.direct-chat-msg -->

                            <!-- Message to the right -->
                            <div class="direct-chat-msg right">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-right">Sarah Bullock</span>
                                    <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
                                </div>
                                <!-- /.direct-chat-info -->
                                <img class="direct-chat-img" src="../dist/img/user3-128x128.jpg" alt="Message User Image"><!-- /.direct-chat-img -->
                                <div class="direct-chat-text">
                                    You better believe it!
                                </div>
                                <!-- /.direct-chat-text -->
                            </div>
                            <!-- /.direct-chat-msg -->
                        </div>
                        <!--/.direct-chat-messages-->

                        <!-- Contacts are loaded here -->
                        <div class="direct-chat-contacts">
                            <ul class="contacts-list">
                                <li>
                                    <a href="#">
                                        <img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="User Image">

                                        <div class="contacts-list-info">
                                            <span class="contacts-list-name">
                                                Count Dracula
                                                <small class="contacts-list-date pull-right">2/28/2015</small>
                                            </span>
                                            <span class="contacts-list-msg">How have you been? I was...</span>
                                        </div>
                                        <!-- /.contacts-list-info -->
                                    </a>
                                </li>
                                <!-- End Contact Item -->
                            </ul>
                            <!-- /.contatcts-list -->
                        </div>
                        <!-- /.direct-chat-pane -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <form action="#" method="post">
                            <div class="input-group">
                                <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-warning btn-flat">Send</button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!--/.direct-chat -->
            </div>
            <!-- /.col -->

            <div class="col-md-3">
                <!-- DIRECT CHAT DANGER -->
                <div class="box box-danger direct-chat direct-chat-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Direct Chat</h3>

                        <div class="box-tools pull-right">
                            <span data-toggle="tooltip" title="3 New Messages" class="badge bg-red">3</span>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">
                                <i class="fa fa-comments"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Conversations are loaded here -->
                        <div class="direct-chat-messages">
                            <!-- Message. Default to the left -->
                            <div class="direct-chat-msg">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-left">Alexander Pierce</span>
                                    <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                                </div>
                                <!-- /.direct-chat-info -->
                                <img class="direct-chat-img" src="../dist/img/user1-128x128.jpg" alt="Message User Image"><!-- /.direct-chat-img -->
                                <div class="direct-chat-text">
                                    Is this template really for free? That's unbelievable!
                                </div>
                                <!-- /.direct-chat-text -->
                            </div>
                            <!-- /.direct-chat-msg -->

                            <!-- Message to the right -->
                            <div class="direct-chat-msg right">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-right">Sarah Bullock</span>
                                    <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
                                </div>
                                <!-- /.direct-chat-info -->
                                <img class="direct-chat-img" src="../dist/img/user3-128x128.jpg" alt="Message User Image"><!-- /.direct-chat-img -->
                                <div class="direct-chat-text">
                                    You better believe it!
                                </div>
                                <!-- /.direct-chat-text -->
                            </div>
                            <!-- /.direct-chat-msg -->
                        </div>
                        <!--/.direct-chat-messages-->

                        <!-- Contacts are loaded here -->
                        <div class="direct-chat-contacts">
                            <ul class="contacts-list">
                                <li>
                                    <a href="#">
                                        <img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="User Image">

                                        <div class="contacts-list-info">
                                            <span class="contacts-list-name">
                                                Count Dracula
                                                <small class="contacts-list-date pull-right">2/28/2015</small>
                                            </span>
                                            <span class="contacts-list-msg">How have you been? I was...</span>
                                        </div>
                                        <!-- /.contacts-list-info -->
                                    </a>
                                </li>
                                <!-- End Contact Item -->
                            </ul>
                            <!-- /.contatcts-list -->
                        </div>
                        <!-- /.direct-chat-pane -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <form action="#" method="post">
                            <div class="input-group">
                                <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-danger btn-flat">Send</button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!--/.direct-chat -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <h2 class="page-header">Social Widgets</h2>

        <div class="row">
            <div class="col-md-4">
                <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user-2">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-yellow">
                        <div class="widget-user-image">
                            <img class="img-circle" src="../dist/img/user7-128x128.jpg" alt="User Avatar">
                        </div>
                        <!-- /.widget-user-image -->
                        <h3 class="widget-user-username">Nadia Carmichael</h3>
                        <h5 class="widget-user-desc">Lead Developer</h5>
                    </div>
                    <div class="box-footer no-padding">
                        <ul class="nav nav-stacked">
                            <li><a href="#">Projects <span class="pull-right badge bg-blue">31</span></a></li>
                            <li><a href="#">Tasks <span class="pull-right badge bg-aqua">5</span></a></li>
                            <li><a href="#">Completed Projects <span class="pull-right badge bg-green">12</span></a></li>
                            <li><a href="#">Followers <span class="pull-right badge bg-red">842</span></a></li>
                        </ul>
                    </div>
                </div>
                <!-- /.widget-user -->
            </div>
            <!-- /.col -->
            <div class="col-md-4">
                <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-aqua-active">
                        <h3 class="widget-user-username">Alexander Pierce</h3>
                        <h5 class="widget-user-desc">Founder &amp; CEO</h5>
                    </div>
                    <div class="widget-user-image">
                        <img class="img-circle" src="../dist/img/user1-128x128.jpg" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">3,200</h5>
                                    <span class="description-text">SALES</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">13,000</h5>
                                    <span class="description-text">FOLLOWERS</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div class="description-block">
                                    <h5 class="description-header">35</h5>
                                    <span class="description-text">PRODUCTS</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
                <!-- /.widget-user -->
            </div>
            <!-- /.col -->
            <div class="col-md-4">
                <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black" style="background: url('../dist/img/photo1.png') center center;">
                        <h3 class="widget-user-username">Elizabeth Pierce</h3>
                        <h5 class="widget-user-desc">Web Designer</h5>
                    </div>
                    <div class="widget-user-image">
                        <img class="img-circle" src="../dist/img/user3-128x128.jpg" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">3,200</h5>
                                    <span class="description-text">SALES</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">13,000</h5>
                                    <span class="description-text">FOLLOWERS</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div class="description-block">
                                    <h5 class="description-header">35</h5>
                                    <span class="description-text">PRODUCTS</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
                <!-- /.widget-user -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-md-6">
                <!-- Box Comment -->
                <div class="box box-widget">
                    <div class="box-header with-border">
                        <div class="user-block">
                            <img class="img-circle" src="../dist/img/user1-128x128.jpg" alt="User Image">
                            <span class="username"><a href="#">Jonathan Burke Jr.</a></span>
                            <span class="description">Shared publicly - 7:30 PM Today</span>
                        </div>
                        <!-- /.user-block -->
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
                                <i class="fa fa-circle-o"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <img class="img-responsive pad" src="../dist/img/photo2.png" alt="Photo">

                        <p>I took this photo this morning. What do you guys think?</p>
                        <button type="button" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Share</button>
                        <button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button>
                        <span class="pull-right text-muted">127 likes - 3 comments</span>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer box-comments">
                        <div class="box-comment">
                            <!-- User image -->
                            <img class="img-circle img-sm" src="../dist/img/user3-128x128.jpg" alt="User Image">

                            <div class="comment-text">
                                <span class="username">
                                    Maria Gonzales
                                    <span class="text-muted pull-right">8:03 PM Today</span>
                                </span><!-- /.username -->
                                It is a long established fact that a reader will be distracted
                                by the readable content of a page when looking at its layout.
                            </div>
                            <!-- /.comment-text -->
                        </div>
                        <!-- /.box-comment -->
                        <div class="box-comment">
                            <!-- User image -->
                            <img class="img-circle img-sm" src="../dist/img/user4-128x128.jpg" alt="User Image">

                            <div class="comment-text">
                                <span class="username">
                                    Luna Stark
                                    <span class="text-muted pull-right">8:03 PM Today</span>
                                </span><!-- /.username -->
                                It is a long established fact that a reader will be distracted
                                by the readable content of a page when looking at its layout.
                            </div>
                            <!-- /.comment-text -->
                        </div>
                        <!-- /.box-comment -->
                    </div>
                    <!-- /.box-footer -->
                    <div class="box-footer">
                        <form action="#" method="post">
                            <img class="img-responsive img-circle img-sm" src="../dist/img/user4-128x128.jpg" alt="Alt Text">
                            <!-- .img-push is used to add margin to elements next to floating images -->
                            <div class="img-push">
                                <input type="text" class="form-control input-sm" placeholder="Press enter to post comment">
                            </div>
                        </form>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-6">
                <!-- Box Comment -->
                <div class="box box-widget">
                    <div class="box-header with-border">
                        <div class="user-block">
                            <img class="img-circle" src="../dist/img/user1-128x128.jpg" alt="User Image">
                            <span class="username"><a href="#">Jonathan Burke Jr.</a></span>
                            <span class="description">Shared publicly - 7:30 PM Today</span>
                        </div>
                        <!-- /.user-block -->
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read">
                                <i class="fa fa-circle-o"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- post text -->
                        <p>Far far away, behind the word mountains, far from the
                            countries Vokalia and Consonantia, there live the blind
                            texts. Separated they live in Bookmarksgrove right at</p>

                        <p>the coast of the Semantics, a large language ocean.
                            A small river named Duden flows by their place and supplies
                            it with the necessary regelialia. It is a paradisematic
                            country, in which roasted parts of sentences fly into
                            your mouth.</p>

                        <!-- Attachment -->
                        <div class="attachment-block clearfix">
                            <img class="attachment-img" src="../dist/img/photo1.png" alt="Attachment Image">

                            <div class="attachment-pushed">
                                <h4 class="attachment-heading"><a href="http://www.lipsum.com/">Lorem ipsum text generator</a></h4>

                                <div class="attachment-text">
                                    Description about the attachment can be placed here.
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry... <a href="#">more</a>
                                </div>
                                <!-- /.attachment-text -->
                            </div>
                            <!-- /.attachment-pushed -->
                        </div>
                        <!-- /.attachment-block -->

                        <!-- Social sharing buttons -->
                        <button type="button" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Share</button>
                        <button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button>
                        <span class="pull-right text-muted">45 likes - 2 comments</span>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer box-comments">
                        <div class="box-comment">
                            <!-- User image -->
                            <img class="img-circle img-sm" src="../dist/img/user3-128x128.jpg" alt="User Image">

                            <div class="comment-text">
                                <span class="username">
                                    Maria Gonzales
                                    <span class="text-muted pull-right">8:03 PM Today</span>
                                </span><!-- /.username -->
                                It is a long established fact that a reader will be distracted
                                by the readable content of a page when looking at its layout.
                            </div>
                            <!-- /.comment-text -->
                        </div>
                        <!-- /.box-comment -->
                        <div class="box-comment">
                            <!-- User image -->
                            <img class="img-circle img-sm" src="../dist/img/user5-128x128.jpg" alt="User Image">

                            <div class="comment-text">
                                <span class="username">
                                    Nora Havisham
                                    <span class="text-muted pull-right">8:03 PM Today</span>
                                </span><!-- /.username -->
                                The point of using Lorem Ipsum is that it has a more-or-less
                                normal distribution of letters, as opposed to using
                                'Content here, content here', making it look like readable English.
                            </div>
                            <!-- /.comment-text -->
                        </div>
                        <!-- /.box-comment -->
                    </div>
                    <!-- /.box-footer -->
                    <div class="box-footer">
                        <form action="#" method="post">
                            <img class="img-responsive img-circle img-sm" src="../dist/img/user4-128x128.jpg" alt="Alt Text">
                            <!-- .img-push is used to add margin to elements next to floating images -->
                            <div class="img-push">
                                <input type="text" class="form-control input-sm" placeholder="Press enter to post comment">
                            </div>
                        </form>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>


    <!-- Content Header (Page header) -->
    <div class="content-header">
        <h1>
            AdminLTE Documentation
            <small>Current version 2.3.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Documentation</li>
        </ol>
    </div>

    <!-- Main content -->
    <div class="content body">

        <section id="introduction">
            <h2 class="page-header"><a href="#introduction">Introduction</a></h2>
            <p class="lead">
                <b>AdminLTE</b> is a popular open source WebApp template for admin dashboards and control panels.
                It is a responsive HTML template that is based on the CSS framework Bootstrap 3.
                It utilizes all of the Bootstrap components in its design and re-styles many
                commonly used plugins to create a consistent design that can be used as a user
                interface for backend applications. AdminLTE is based on a modular design, which
                allows it to be easily customized and built upon. This documentation will guide you through
                installing the template and exploring the various components that are bundled with the template.
            </p>
        </section><!-- /#introduction -->


        <!-- ============================================================= -->

        <section id="download">
            <h2 class="page-header"><a href="#download">Download</a></h2>
            <p class="lead">
                AdminLTE can be downloaded in two different versions, each appealing to different skill levels and use case.
            </p>
            <div class="row">
                <div class="col-sm-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Ready</h3>
                            <span class="label label-primary pull-right"><i class="fa fa-html5"></i></span>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <p>Compiled and ready to use in production. Download this version if you don't want to customize Adminlte's LESS files.</p>
                            <a href="http://almsaeedstudio.com/download/AdminLTE-dist" class="btn btn-primary"><i class="fa fa-download"></i> Download</a>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title">Source Code</h3>
                            <span class="label label-danger pull-right"><i class="fa fa-database"></i></span>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <p>All files including the compiled CSS. Download this version if you plan on customizing the template. <b>Requires a LESS compiler.</b></p>
                            <a href="http://almsaeedstudio.com/download/AdminLTE" class="btn btn-danger"><i class="fa fa-download"></i> Download</a>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
            <pre class="hierarchy bring-up"><code class="language-bash" data-lang="bash">File Hierarchy of the Source Code Package

AdminLTE/
├── dist/
│   ├── CSS/
│   ├── JS
│   ├── img
├── build/
│   ├── less/
│   │   ├── Adminlte's Less files
│   └── Bootstrap-less/ (Only for reference. No modifications have been made)
│       ├── mixins/
│       ├── variables.less
│       ├── mixins.less
└── plugins/
    ├── All the customized plugins CSS and JS files</code></pre>
        </section>


        <!-- ============================================================= -->

        <section id="dependencies">
            <h2 class="page-header"><a href="#dependencies">Dependencies</a></h2>
            <p class="lead">AdminLTE depends on two main frameworks.
                The downloadable package contains both of these libraries, so you don't have to manually download them.</p>
            <ul class="bring-up">
                <li><a href="http://getbootstrap.com" target="_blank">Bootstrap 3</a></li>
                <li><a href="http://jquery.com/" target="_blank">jQuery 1.11+</a></li>
                <li><a href="#plugins">All other plugins are listed below</a></li>
            </ul>
        </section>


        <!-- ============================================================= -->

        <section id="advice">
            <h2 class="page-header"><a href="#advice">A Word of Advice</a></h2>
            <p class="lead">
                Before you go to see your new awesome theme, here are few tips on how to familiarize yourself with it:
            </p>

            <ul>
                <li><b>AdminLTE is based on <a href="http://getbootstrap.com/" target="_blank">Bootstrap 3</a>.</b> If you are unfamiliar with Bootstrap, visit their website and read through the documentation. All of Bootstrap components have been modified to fit the style of AdminLTE and provide a consistent look throughout the template. This way, we guarantee you will get the best of AdminLTE.</li>
                <li><b>Go through the pages that are bundled with the theme.</b> Most of the template example pages contain quick tips on how to create or use a component which can be really helpful when you need to create something on the fly.</li>
                <li><b>Documentation.</b> We are trying our best to make your experience with AdminLTE be smooth. One way to achieve that is to provide documentation and support. If you think that something is missing from the documentation, please do not hesitate to create an issue to tell us about it. Also, if you would like to contribute, email the support team at <a href="mailto:support@almsaeedstudio.com">support@almsaeedstudio.com</a>.</li>
                <li><b>Built with <a href="http://lesscss.org/" target="_blank">LESS</a>.</b> This theme uses the LESS compiler to make it easier to customize and use. LESS is easy to learn if you know CSS or SASS. It is not necessary to learn LESS but it will benefit you a lot in the future.</li>
                <li><b>Hosted on <a href="https://github.com/almasaeed2010/AdminLTE/" target="_blank">GitHub</a>.</b> Visit our GitHub repository to view issues, make requests, or contribute to the project.</li>
            </ul>
            <p>
                <b>Note:</b> LESS files are better commented than the compiled CSS file.
            </p>
        </section>


        <!-- ============================================================= -->

        <section id="layout">
            <h2 class="page-header"><a href="#layout">Layout</a></h2>
            <p class="lead">The layout consists of four major parts:</p>
            <ul>
                <li>Wrapper <code>.wrapper</code>. A div that wraps the whole site.</li>
                <li>Main Header <code>.main-header</code>. Contains the logo and navbar.</li>
                <li>Sidebar <code>.sidebar-wrapper</code>. Contains the user panel and sidebar menu.</li>
                <li>Content <code>.content-wrapper</code>. Contains the page header and content.</li>
            </ul>
            <div class="callout callout-danger lead">
                <h4>Tip!</h4>
                <p>The <a href="../starter.html">starter page</a> is a good place to start building your app if you'd like to start from scratch.</p>
            </div>

            <h3>Layout Options</h3>
            <p class="lead">AdminLTE 2.0 provides a set of options to apply to your main layout. Each on of these classes can be added
                to the body tag to get the desired goal.</p>
            <ul>
                <li><b>Fixed:</b> use the class <code>.fixed</code> to get a fixed header and sidebar.</li>
                <li><b>Collapsed Sidebar:</b> use the class <code>.sidebar-collapse</code> to have a collapsed sidebar upon loading.</li>
                <li><b>Boxed Layout:</b> use the class <code>.layout-boxed</code> to get a boxed layout that stretches only to 1250px.</li>
                <li><b>Top Navigation</b> use the class <code>.layout-top-nav</code> to remove the sidebar and have your links at the top navbar.</li>
            </ul>
            <p><b>Note:</b> you cannot use both layout-boxed and fixed at the same time. Anything else can be mixed together.</p>

            <h3>Skins</h3>
            <p class="lead">Skins can be found in the dist/css/skins folder.
                Choose and the skin file that you want then add the appropriate
                class to the body tag to change the template's appearance. Here is the list of available skins:</p>
            <div class="box box-solid" style="max-width: 300px;">
                <div class="box-body no-padding">
                    <table id="layout-skins-list" class="table table-striped bring-up nth-2-center">
                        <thead>
                            <tr>
                                <th style="width: 210px;">Skin Class</th>
                                <th>Preview</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><code>skin-blue</code></td>
                                <td><a href="#" data-skin="skin-blue" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            <tr>
                                <td><code>skin-blue-light</code></td>
                                <td><a href="#" data-skin="skin-blue-light" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            <tr>
                                <td><code>skin-yellow</code></td>
                                <td><a href="#" data-skin="skin-yellow" class="btn btn-warning btn-xs"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            <tr>
                                <td><code>skin-yellow-light</code></td>
                                <td><a href="#" data-skin="skin-yellow-light" class="btn btn-warning btn-xs"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            <tr>
                                <td><code>skin-green</code></td>
                                <td><a href="#" data-skin="skin-green" class="btn btn-success btn-xs"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            <tr>
                                <td><code>skin-green-light</code></td>
                                <td><a href="#" data-skin="skin-green-light" class="btn btn-success btn-xs"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            <tr>
                                <td><code>skin-purple</code></td>
                                <td><a href="#" data-skin="skin-purple" class="btn bg-purple btn-xs"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            <tr>
                                <td><code>skin-purple-light</code></td>
                                <td><a href="#" data-skin="skin-purple-light" class="btn bg-purple btn-xs"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            <tr>
                                <td><code>skin-red</code></td>
                                <td><a href="#" data-skin="skin-red" class="btn btn-danger btn-xs"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            <tr>
                                <td><code>skin-red-light</code></td>
                                <td><a href="#" data-skin="skin-red-light" class="btn btn-danger btn-xs"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            <tr>
                                <td><code>skin-black</code></td>
                                <td><a href="#" data-skin="skin-black" class="btn bg-black btn-xs"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            <tr>
                                <td><code>skin-black-light</code></td>
                                <td><a href="#" data-skin="skin-black-light" class="btn bg-black btn-xs"><i class="fa fa-eye"></i></a></td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </section>


        <!-- ============================================================= -->

        <section id="adminlte-options">
            <h2 class="page-header"><a href="#adminlte-options">AdminLTE Javascript Options</a></h2>
            <p class="lead">Modifying the options of Adminlte's app.js can be done using one of the following ways.</p>

            <h3>Editing app.js</h3>
            <p>Within the main Javascript file, modify the <code>$.AdminLTE.options</code> object to suit your use case.</p>

            <h3>Defining AdminLTEOptions</h3>
            <p>Alternatively, you can define a global options variable named <code>AdminLTEOptions</code> and initialize it before loading app.js.</p>
            <p>Example</p>
            <pre class="prettyprint"><code class="html">&LT;script>
  var AdminLTEOptions = {
    //Enable sidebar expand on hover effect for sidebar mini
    //This option is forced to true if both the fixed layout and sidebar mini
    //are used together
    sidebarExpandOnHover: true,
    //BoxRefresh Plugin
    enableBoxRefresh: true,
    //Bootstrap.js tooltip
    enableBSToppltip: true
  };
&LT;/script>
&LT;script src="dist/js/app.js" type="text/javascript">&LT;/script></code></pre>

            <h3>Available AdminLTE Options</h3>
            <pre class="prettyprint"><code class="javascript">{
  //Add slimscroll to navbar menus
  //This requires you to load the slimscroll plugin
  //in every page before app.js
  navbarMenuSlimscroll: true,
  navbarMenuSlimscrollWidth: "3px", //The width of the scroll bar
  navbarMenuHeight: "200px", //The height of the inner menu
  //General animation speed for JS animated elements such as box collapse/expand and
  //sidebar treeview slide up/down. This options accepts an integer as milliseconds,
  //'fast', 'normal', or 'slow'
  animationSpeed: 500,
  //Sidebar push menu toggle button selector
  sidebarToggleSelector: "[data-toggle='offcanvas']",
  //Activate sidebar push menu
  sidebarPushMenu: true,
  //Activate sidebar slimscroll if the fixed layout is set (requires SlimScroll Plugin)
  sidebarSlimScroll: true,
  //Enable sidebar expand on hover effect for sidebar mini
  //This option is forced to true if both the fixed layout and sidebar mini
  //are used together
  sidebarExpandOnHover: false,
  //BoxRefresh Plugin
  enableBoxRefresh: true,
  //Bootstrap.js tooltip
  enableBSToppltip: true,
  BSTooltipSelector: "[data-toggle='tooltip']",
  //Enable Fast Click. Fastclick.js creates a more
  //native touch experience with touch devices. If you
  //choose to enable the plugin, make sure you load the script
  //before Adminlte's app.js
  enableFastclick: true,
  //Control Sidebar Options
  enableControlSidebar: true,
  controlSidebarOptions: {
    //Which button should trigger the open/close event
    toggleBtnSelector: "[data-toggle='control-sidebar']",
    //The sidebar selector
    selector: ".control-sidebar",
    //Enable slide over content
    slide: true
  },
  //Box Widget Plugin. Enable this plugin
  //to allow boxes to be collapsed and/or removed
  enableBoxWidget: true,
  //Box Widget plugin options
  boxWidgetOptions: {
    boxWidgetIcons: {
      //Collapse icon
      collapse: 'fa-minus',
      //Open icon
      open: 'fa-plus',
      //Remove icon
      remove: 'fa-times'
    },
    boxWidgetSelectors: {
      //Remove button selector
      remove: '[data-widget="remove"]',
      //Collapse button selector
      collapse: '[data-widget="collapse"]'
    }
  },
  //Direct Chat plugin options
  directChat: {
    //Enable direct chat by default
    enable: true,
    //The button to open and close the chat contacts pane
    contactToggleSelector: '[data-widget="chat-pane-toggle"]'
  },
  //Define the set of colors to use globally around the website
  colors: {
    lightBlue: "#3c8dbc",
    red: "#f56954",
    green: "#00a65a",
    aqua: "#00c0ef",
    yellow: "#f39c12",
    blue: "#0073b7",
    navy: "#001F3F",
    teal: "#39CCCC",
    olive: "#3D9970",
    lime: "#01FF70",
    orange: "#FF851B",
    fuchsia: "#F012BE",
    purple: "#8E24AA",
    maroon: "#D81B60",
    black: "#222222",
    gray: "#d2d6de"
  },
  //The standard screen sizes that bootstrap uses.
  //If you change these in the variables.less file, change
  //them here too.
  screenSizes: {
    xs: 480,
    sm: 768,
    md: 992,
    lg: 1200
  }
}</code></pre>
        </section>


        <!-- ============================================================= -->

        <section id="components" data-spy="scroll" data-target="#scrollspy-components">
            <h2 class="page-header"><a href="#components">Components</a></h2>
            <div class="callout callout-info lead">
                <h4>Reminder!</h4>
                <p>
                    AdminLTE uses all of Bootstrap 3 components. It's a good start to review
                    the <a href="http://getbootstrap.com">Bootstrap documentation</a> to get an idea of the various components
                    that this documentation <b>does not</b> cover.
                </p>
            </div>
            <div class="callout callout-danger lead">
                <h4>Tip!</h4>
                <p>
                    If you go through the example pages and would like to copy a component, right-click on
                    the component and choose "inspect element" to get to the HTML quicker than scanning
                    the HTML page.
                </p>
            </div>
            <h3 id="component-main-header">Main Header</h3>
            <p class="lead">The main header contains the logo and navbar. Construction of the
                navbar differs slightly from Bootstrap because it has components that Bootstrap doesn't provide.
                The navbar can be constructed in two way. This an example for the normal navbar and next we will provide an example for
                the top nav layout.</p>
            <div class="box box-solid">
                <div class="box-body" style="position: relative;">
                    <span class="eg">Main Header Example</span>
                    <header class="main-header" style="position: relative;">
                        <!-- Logo -->
                        <a href="index2.html" class="logo" style="position: relative;"><b>Admin</b>LTE</a>
                        <!-- Header Navbar: style can be found in header.less -->
                        <nav class="navbar" role="navigation" style="border: 0;max-height: 50px;">
                            <!-- Sidebar toggle button-->
                            <a href="#" class="sidebar-toggle" role="button">
                                <span class="sr-only">Toggle navigation</span>
                            </a>
                            <!-- Navbar Right Menu -->
                            <div class="navbar-custom-menu">
                                <ul class="nav navbar-nav">
                                    <!-- Messages: style can be found in dropdown.less-->
                                    <li class="dropdown messages-menu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-envelope-o"></i>
                                            <span class="label label-success">4</span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="header">You have 4 messages</li>
                                            <li>
                                                <!-- inner menu: contains the actual data -->
                                                <ul class="menu">
                                                    <li><!-- start message -->
                                                        <a href="#">
                                                            <div class="pull-left">
                                                                <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                                            </div>
                                                            <h4>
                                                                Support Team
                                                                <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                                            </h4>
                                                            <p>Why not buy a new awesome theme?</p>
                                                        </a>
                                                    </li><!-- end message -->
                                                </ul>
                                            </li>
                                            <li class="footer"><a href="#">See All Messages</a></li>
                                        </ul>
                                    </li>
                                    <!-- Notifications: style can be found in dropdown.less -->
                                    <li class="dropdown notifications-menu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-bell-o"></i>
                                            <span class="label label-warning">10</span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="header">You have 10 notifications</li>
                                            <li>
                                                <!-- inner menu: contains the actual data -->
                                                <ul class="menu">
                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="footer"><a href="#">View all</a></li>
                                        </ul>
                                    </li>
                                    <!-- Tasks: style can be found in dropdown.less -->
                                    <li class="dropdown tasks-menu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-flag-o"></i>
                                            <span class="label label-danger">9</span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="header">You have 9 tasks</li>
                                            <li>
                                                <!-- inner menu: contains the actual data -->
                                                <ul class="menu">
                                                    <li><!-- Task item -->
                                                        <a href="#">
                                                            <h3>
                                                                Design some buttons
                                                                <small class="pull-right">20%</small>
                                                            </h3>
                                                            <div class="progress xs">
                                                                <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                                    <span class="sr-only">20% Complete</span>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li><!-- end task item -->
                                                </ul>
                                            </li>
                                            <li class="footer">
                                                <a href="#">View all tasks</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <!-- User Account: style can be found in dropdown.less -->
                                    <li class="dropdown user user-menu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <img src="../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                                            <span class="hidden-xs">Alexander Pierce</span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <!-- User image -->
                                            <li class="user-header">
                                                <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                                <p>
                                                    Alexander Pierce - Web Developer
                                                    <small>Member since Nov. 2012</small>
                                                </p>
                                            </li>
                                            <!-- Menu Body -->
                                            <li class="user-body">
                                                <div class="col-xs-4 text-center">
                                                    <a href="#">Followers</a>
                                                </div>
                                                <div class="col-xs-4 text-center">
                                                    <a href="#">Sales</a>
                                                </div>
                                                <div class="col-xs-4 text-center">
                                                    <a href="#">Friends</a>
                                                </div>
                                            </li>
                                            <!-- Menu Footer-->
                                            <li class="user-footer">
                                                <div class="pull-left">
                                                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                                                </div>
                                                <div class="pull-right">
                                                    <a href="#" class="btn btn-default btn-flat">Sign out</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </header>
                </div>
            </div>
            <pre class="prettyprint">&LT;header class="main-header">
  &LT;a href="../../index2.html" class="logo">
    &LT;!-- LOGO -->
    AdminLTE
  &LT;/a>
  &LT;!-- Header Navbar: style can be found in header.less -->
  &LT;nav class="navbar navbar-static-top" role="navigation">
    &LT;!-- Navbar Right Menu -->
    &LT;div class="navbar-custom-menu">
      &LT;ul class="nav navbar-nav">
        &LT;!-- Messages: style can be found in dropdown.less-->
        &LT;li class="dropdown messages-menu">
          &LT;a href="#" class="dropdown-toggle" data-toggle="dropdown">
            &LT;i class="fa fa-envelope-o">&LT;/i>
            &LT;span class="label label-success">4&LT;/span>
          &LT;/a>
          &LT;ul class="dropdown-menu">
            &LT;li class="header">You have 4 messages&LT;/li>
            &LT;li>
              &LT;!-- inner menu: contains the actual data -->
              &LT;ul class="menu">
                &LT;li>&LT;!-- start message -->
                  &LT;a href="#">
                    &LT;div class="pull-left">
                      &LT;img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                    &LT;/div>
                    &LT;h4>
                      Sender Name
                      &LT;small>&LT;i class="fa fa-clock-o">&LT;/i> 5 mins&LT;/small>
                    &LT;/h4>
                    &LT;p>Message Excerpt&LT;/p>
                  &LT;/a>
                &LT;/li>&LT;!-- end message -->
                ...
              &LT;/ul>
            &LT;/li>
            &LT;li class="footer">&LT;a href="#">See All Messages&LT;/a>&LT;/li>
          &LT;/ul>
        &LT;/li>
        &LT;!-- Notifications: style can be found in dropdown.less -->
        &LT;li class="dropdown notifications-menu">
          &LT;a href="#" class="dropdown-toggle" data-toggle="dropdown">
            &LT;i class="fa fa-bell-o">&LT;/i>
            &LT;span class="label label-warning">10&LT;/span>
          &LT;/a>
          &LT;ul class="dropdown-menu">
            &LT;li class="header">You have 10 notifications&LT;/li>
            &LT;li>
              &LT;!-- inner menu: contains the actual data -->
              &LT;ul class="menu">
                &LT;li>
                  &LT;a href="#">
                    &LT;i class="ion ion-ios-people info">&LT;/i> Notification title
                  &LT;/a>
                &LT;/li>
                ...
              &LT;/ul>
            &LT;/li>
            &LT;li class="footer">&LT;a href="#">View all&LT;/a>&LT;/li>
          &LT;/ul>
        &LT;/li>
        &LT;!-- Tasks: style can be found in dropdown.less -->
        &LT;li class="dropdown tasks-menu">
          &LT;a href="#" class="dropdown-toggle" data-toggle="dropdown">
            &LT;i class="fa fa-flag-o">&LT;/i>
            &LT;span class="label label-danger">9&LT;/span>
          &LT;/a>
          &LT;ul class="dropdown-menu">
            &LT;li class="header">You have 9 tasks&LT;/li>
            &LT;li>
              &LT;!-- inner menu: contains the actual data -->
              &LT;ul class="menu">
                &LT;li>&LT;!-- Task item -->
                  &LT;a href="#">
                    &LT;h3>
                      Design some buttons
                      &LT;small class="pull-right">20%&LT;/small>
                    &LT;/h3>
                    &LT;div class="progress xs">
                      &LT;div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                        &LT;span class="sr-only">20% Complete&LT;/span>
                      &LT;/div>
                    &LT;/div>
                  &LT;/a>
                &LT;/li>&LT;!-- end task item -->
                ...
              &LT;/ul>
            &LT;/li>
            &LT;li class="footer">
              &LT;a href="#">View all tasks&LT;/a>
            &LT;/li>
          &LT;/ul>
        &LT;/li>
        &LT;!-- User Account: style can be found in dropdown.less -->
        &LT;li class="dropdown user user-menu">
          &LT;a href="#" class="dropdown-toggle" data-toggle="dropdown">
            &LT;img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
            &LT;span class="hidden-xs">Alexander Pierce&LT;/span>
          &LT;/a>
          &LT;ul class="dropdown-menu">
            &LT;!-- User image -->
            &LT;li class="user-header">
              &LT;img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
              &LT;p>
                Alexander Pierce - Web Developer
                &LT;small>Member since Nov. 2012&LT;/small>
              &LT;/p>
            &LT;/li>
            &LT;!-- Menu Body -->
            &LT;li class="user-body">
              &LT;div class="col-xs-4 text-center">
                &LT;a href="#">Followers&LT;/a>
              &LT;/div>
              &LT;div class="col-xs-4 text-center">
                &LT;a href="#">Sales&LT;/a>
              &LT;/div>
              &LT;div class="col-xs-4 text-center">
                &LT;a href="#">Friends&LT;/a>
              &LT;/div>
            &LT;/li>
            &LT;!-- Menu Footer-->
            &LT;li class="user-footer">
              &LT;div class="pull-left">
                &LT;a href="#" class="btn btn-default btn-flat">Profile&LT;/a>
              &LT;/div>
              &LT;div class="pull-right">
                &LT;a href="#" class="btn btn-default btn-flat">Sign out&LT;/a>
              &LT;/div>
            &LT;/li>
          &LT;/ul>
        &LT;/li>
      &LT;/ul>
    &LT;/div>
  &LT;/nav>
&LT;/header></pre>
            <h4>Top Nav Layout. Main Header Example.</h4>
            <div class="callout callout-info lead">
                <h4>Reminder!</h4>
                <p>To use this main header instead of the regular one, you must add the <code>layout-top-nav</code> class to the body tag.</p>
            </div>

            <pre class="prettyprint">
&LT;header class="main-header">
  &LT;nav class="navbar navbar-static-top">
    &LT;div class="container-fluid">
    &LT;div class="navbar-header">
      &LT;a href="../../index2.html" class="navbar-brand">&LT;b>Admin&LT;/b>LTE&LT;/a>
      &LT;button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
        &LT;i class="fa fa-bars">&LT;/i>
      &LT;/button>
    &LT;/div>

    &LT;!-- Collect the nav links, forms, and other content for toggling -->
    &LT;div class="collapse navbar-collapse" id="navbar-collapse">
      &LT;ul class="nav navbar-nav">
        &LT;li class="active">&LT;a href="#">Link &LT;span class="sr-only">(current)&LT;/span>&LT;/a>&LT;/li>
        &LT;li>&LT;a href="#">Link&LT;/a>&LT;/li>
        &LT;li class="dropdown">
          &LT;a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown &LT;span class="caret">&LT;/span>&LT;/a>
          &LT;ul class="dropdown-menu" role="menu">
            &LT;li>&LT;a href="#">Action&LT;/a>&LT;/li>
            &LT;li>&LT;a href="#">Another action&LT;/a>&LT;/li>
            &LT;li>&LT;a href="#">Something else here&LT;/a>&LT;/li>
            &LT;li class="divider">&LT;/li>
            &LT;li>&LT;a href="#">Separated link&LT;/a>&LT;/li>
            &LT;li class="divider">&LT;/li>
            &LT;li>&LT;a href="#">One more separated link&LT;/a>&LT;/li>
          &LT;/ul>
        &LT;/li>
      &LT;/ul>
      &LT;form class="navbar-form navbar-left" role="search">
        &LT;div class="form-group">
          &LT;input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
        &LT;/div>
      &LT;/form>
      &LT;ul class="nav navbar-nav navbar-right">
        &LT;li>&LT;a href="#">Link&LT;/a>&LT;/li>
        &LT;li class="dropdown">
          &LT;a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown &LT;span class="caret">&LT;/span>&LT;/a>
          &LT;ul class="dropdown-menu" role="menu">
            &LT;li>&LT;a href="#">Action&LT;/a>&LT;/li>
            &LT;li>&LT;a href="#">Another action&LT;/a>&LT;/li>
            &LT;li>&LT;a href="#">Something else here&LT;/a>&LT;/li>
            &LT;li class="divider">&LT;/li>
            &LT;li>&LT;a href="#">Separated link&LT;/a>&LT;/li>
          &LT;/ul>
        &LT;/li>
      &LT;/ul>
    &LT;/div>&LT;!-- /.navbar-collapse -->
    &LT;/div>&LT;!-- /.container-fluid -->
  &LT;/nav>
&LT;/header></pre>

            <!-- ===================================================================== -->

            <h3 id="component-sidebar">Sidebar</h3>
            <p class="lead">
                The sidebar used in this page to the left provides an example of what your sidebar should like.
                Construction of a sidebar:
            </p>
            <pre class="prettyprint">
&LT;div class="main-sidebar">
  &LT;!-- Inner sidebar -->
  &LT;div class="sidebar">
    &LT;!-- user panel (Optional) -->
    &LT;div class="user-panel">
      &LT;div class="pull-left image">
        &LT;img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      &LT;/div>
      &LT;div class="pull-left info">
        &LT;p>User Name&LT;/p>

        &LT;a href="#">&LT;i class="fa fa-circle text-success">&LT;/i> Online&LT;/a>
      &LT;/div>
    &LT;/div>&LT;!-- /.user-panel -->

    &LT;!-- Search Form (Optional) -->
    &LT;form action="#" method="get" class="sidebar-form">
      &LT;div class="input-group">
        &LT;input type="text" name="q" class="form-control" placeholder="Search...">
        &LT;span class="input-group-btn">
          &LT;button type="submit" name="search" id="search-btn" class="btn btn-flat">&LT;i class="fa fa-search">&LT;/i>&LT;/button>
        &LT;/span>
      &LT;/div>
    &LT;/form>&LT;!-- /.sidebar-form -->

    &LT;!-- Sidebar Menu -->
    &LT;ul class="sidebar-menu">
      &LT;li class="header">HEADER&LT;/li>
      &LT;!-- Optionally, you can add icons to the links -->
      &LT;li class="active">&LT;a href="#">&LT;span>Link&LT;/span>&LT;/a>&LT;&LT;/li>
      &LT;li>&LT;a href="#">&LT;span>Another Link&LT;/span>&LT;/a>&LT;/li>
      &LT;li class="treeview">
        &LT;a href="#">&LT;span>Multilevel&LT;/span> &LT;i class="fa fa-angle-left pull-right">&LT;/i>&LT;/a>
        &LT;ul class="treeview-menu">
          &LT;li>&LT;a href="#">Link in level 2&LT;/a>&LT;/li>
          &LT;li>&LT;a href="#">Link in level 2&LT;/a>&LT;/li>
        &LT;/ul>
      &LT;/li>
    &LT;/ul>&LT;!-- /.sidebar-menu -->

  &LT;/div>&LT;!-- /.sidebar -->
&LT;/div>&LT;!-- /.main-sidebar --></pre>

            <h3 id="component-control-sidebar">Control Sidebar</h3>
            <p class="lead">Control sidebar is the right side bar. It can be used for many purposes and is extremely easy
                to create. The sidebar ships with two different show/hide styles. The first allows the sidebar to
                slide over the content. The second pushes the content to make space for the sidebar. Either of
                these methods can be set through the <a href="#adminlte-options">Javascript options</a>.</p>
            <p class="lead">The following code should be placed within the <code>.wrapper</code> div. I prefer
                to place it right after the footer.</p>
            <p class="lead">Dark Sidebar Markup</p>
            <pre class="prettyprint"><code class="lang-html">&LT;!-- The Right Sidebar -->
&LT;aside class="control-sidebar control-sidebar-dark">
  &LT;!-- Content of the sidebar goes here -->
&LT;/aside>
&LT;!-- The sidebar's background -->
&LT;!-- This div must placed right after the sidebar for it to work-->
&LT;div class="control-sidebar-bg">&LT;/div></code></pre>

            <p class="lead">Light Sidebar Markup</p>
            <pre class="prettyprint"><code class="lang-html">&LT;!-- The Right Sidebar -->
&LT;aside class="control-sidebar control-sidebar-light">
  &LT;!-- Content of the sidebar goes here -->
&LT;/aside>
&LT;!-- The sidebar's background -->
&LT;!-- This div must placed right after the sidebar for it to work-->
&LT;div class="control-sidebar-bg">&LT;/div></code></pre>

            <p class="lead">Once you create the sidebar, you will need a toggle button to open/close it.
                By adding the attribute <code>data-toggle="control-sidebar"</code> to any button, it will
                automatically act as the toggle button.</p>

            <p class="lead">Toggle Button Example</p>
            <button class="btn btn-primary" data-toggle="control-sidebar">Toggle Right Sidebar</button><br><br>

            <p class="lead">Sidebar Toggle Markup</p>
            <pre class="prettyprint"><code class="lang-html">&LT;button class="btn btn-default" data-toggle="control-sidebar">Toggle Right Sidebar&LT;/button></code></pre>
            <!-- ===================================================================== -->

            <h3 id="component-info-box">Info Box</h3>
            <p class="lead">Info boxes are used to display statistical snippets. There are two types of info boxes.</p>
            <h4>First Type of Info Boxes</h4>
            <!-- Info Boxes -->
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Messages</span>
                            <span class="info-box-number">1,410</span>
                        </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->
                </div><!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-flag-o"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Bookmarks</span>
                            <span class="info-box-number">410</span>
                        </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->
                </div><!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="fa fa-files-o"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Uploads</span>
                            <span class="info-box-number">13,648</span>
                        </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->
                </div><!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Likes</span>
                            <span class="info-box-number">93,139</span>
                        </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
            <p class="lead">Markup</p>
            <pre class="prettyprint"><code class="lang-html">&LT;div class="info-box">
  &LT;!-- Apply any bg-* class to to the icon to color it -->
  &LT;span class="info-box-icon bg-red">&LT;i class="fa fa-star-o">&LT;/i>&LT;/span>
  &LT;div class="info-box-content">
    &LT;span class="info-box-text">Likes&LT;/span>
    &LT;span class="info-box-number">93,139&LT;/span>
  &LT;/div>&LT;!-- /.info-box-content -->
&LT;/div>&LT;!-- /.info-box --></code></pre>

            <h4>Second Type of Info Boxes</h4>
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-aqua">
                        <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Bookmarks</span>
                            <span class="info-box-number">41,410</span>
                            <div class="progress">
                                <div class="progress-bar" style="width: 70%"></div>
                            </div>
                            <span class="progress-description">
                                70% Increase in 30 Days
                            </span>
                        </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->
                </div><!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-green">
                        <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Likes</span>
                            <span class="info-box-number">41,410</span>
                            <div class="progress">
                                <div class="progress-bar" style="width: 70%"></div>
                            </div>
                            <span class="progress-description">
                                70% Increase in 30 Days
                            </span>
                        </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->
                </div><!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-yellow">
                        <span class="info-box-icon"><i class="fa fa-calendar"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Events</span>
                            <span class="info-box-number">41,410</span>
                            <div class="progress">
                                <div class="progress-bar" style="width: 70%"></div>
                            </div>
                            <span class="progress-description">
                                70% Increase in 30 Days
                            </span>
                        </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->
                </div><!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-red">
                        <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Comments</span>
                            <span class="info-box-number">41,410</span>
                            <div class="progress">
                                <div class="progress-bar" style="width: 70%"></div>
                            </div>
                            <span class="progress-description">
                                70% Increase in 30 Days
                            </span>
                        </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
            <p class="lead">Markup</p>
            <pre class="prettyprint"><code class="lang-html">&LT;!-- Apply any bg-* class to to the info-box to color it -->
&LT;div class="info-box bg-red">
  &LT;span class="info-box-icon">&LT;i class="fa fa-comments-o">&LT;/i>&LT;/span>
  &LT;div class="info-box-content">
    &LT;span class="info-box-text">Likes&LT;/span>
    &LT;span class="info-box-number">41,410&LT;/span>
    &LT;!-- The progress section is optional -->
    &LT;div class="progress">
      &LT;div class="progress-bar" style="width: 70%">&LT;/div>
    &LT;/div>
    &LT;span class="progress-description">
      70% Increase in 30 Days
    &LT;/span>
  &LT;/div>&LT;!-- /.info-box-content -->
&LT;/div>&LT;!-- /.info-box --></code></pre>
            <p class="lead">The only thing you need to change to alternate between these style is change the placement of the bg-* class. For the
                first style apply any bg-* class to the icon itself. For the other style, apply the bg-* class to the info-box div.</p>
            <!-- ===================================================================== -->

            <h3 id="component-box">Box</h3>
            <p class="lead">The box component is the most widely used component through out this template. You can
                use it for anything from displaying charts to just blocks of text. It comes in many different
                styles that we will explore below.</p>
            <h4>Default Box Markup</h4>
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Default Box Example</h3>
                    <div class="box-tools pull-right">
                        <!-- Buttons, labels, and many other things can be placed here! -->
                        <!-- Here is a label for example -->
                        <span class="label label-primary">Label</span>
                    </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                    The body of the box
                </div><!-- /.box-body -->
                <div class="box-footer">
                    The footer of the box
                </div><!-- box-footer -->
            </div><!-- /.box -->
            <pre class="prettyprint">&LT;div class="box">
  &LT;div class="box-header with-border">
    &LT;h3 class="box-title">Default Box Example&LT;/h3>
    &LT;div class="box-tools pull-right">
      &LT;!-- Buttons, labels, and many other things can be placed here! -->
      &LT;!-- Here is a label for example -->
      &LT;span class="label label-primary">Label&LT;/span>
    &LT;/div>&LT;!-- /.box-tools -->
  &LT;/div>&LT;!-- /.box-header -->
  &LT;div class="box-body">
    The body of the box
  &LT;/div>&LT;!-- /.box-body -->
  &LT;div class="box-footer">
    The footer of the box
  &LT;/div>&LT;!-- box-footer -->
&LT;/div>&LT;!-- /.box --></pre>
            <h4>Box Variants</h4>
            <p class="lead">You can change the style of the box by adding any of the contextual classes.</p>
            <div class="row">
                <div class="col-md-4">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Default Box Example</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            The body of the box
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                <div class="col-md-4">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Primary Box Example</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            The body of the box
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                <div class="col-md-4">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Info Box Example</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            The body of the box
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                <div class="clearfix"></div>
                <div class="col-md-4">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Warning Box Example</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            The body of the box
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                <div class="col-md-4">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Success Box Example</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            The body of the box
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                <div class="col-md-4">
                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title">Danger Box Example</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            The body of the box
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div><!-- /.row -->
            <pre class="prettyprint">&LT;div class="box box-default">...&LT;/div>
&LT;div class="box box-primary">...&LT;/div>
&LT;div class="box box-info">...&LT;/div>
&LT;div class="box box-warning">...&LT;/div>
&LT;div class="box box-success">...&LT;/div>
&LT;div class="box box-danger">...&LT;/div></pre>

            <h4>Solid Box</h4>
            <p class="lead">Solid Boxes are alternative ways to display boxes.
                They can be created by simply adding the box-solid class to the box component.
                You may also use contextual classes with you solid boxes.</p>
            <div class="row">
                <div class="col-md-4">
                    <div class="box box-solid box-default">
                        <div class="box-header">
                            <h3 class="box-title">Default Solid Box Example</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            The body of the box
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                <div class="col-md-4">
                    <div class="box box-solid box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Primary Solid Box Example</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            The body of the box
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                <div class="col-md-4">
                    <div class="box box-solid box-info">
                        <div class="box-header">
                            <h3 class="box-title">Info Solid Box Example</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            The body of the box
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                <div class="clearfix"></div>
                <div class="col-md-4">
                    <div class="box box-solid box-warning">
                        <div class="box-header">
                            <h3 class="box-title">Warning Solid Box Example</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            The body of the box
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                <div class="col-md-4">
                    <div class="box box-solid box-success">
                        <div class="box-header">
                            <h3 class="box-title">Success Solid Box Example</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            The body of the box
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                <div class="col-md-4">
                    <div class="box box-solid box-danger">
                        <div class="box-header">
                            <h3 class="box-title">Danger Solid Box Example</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            The body of the box
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div><!-- /.row -->
            <pre class="prettyprint">
&LT;div class="box box-solid box-default">...&LT;/div>
&LT;div class="box box-solid box-primary">...&LT;/div>
&LT;div class="box box-solid box-info">...&LT;/div>
&LT;div class="box box-solid box-warning">...&LT;/div>
&LT;div class="box box-solid box-success">...&LT;/div>
&LT;div class="box box-solid box-danger">...&LT;/div></pre>
            <h4>Box Tools</h4>
            <p class="lead">Boxes can contain tools to deploy a specific event or provide simple info. The following examples makes use
                of multiple AdminLTE components within the header of the box.</p>
            <p>AdminLTE data-widget attribute provides boxes with the ability to collapse or be removed. The buttons
                are placed in the box-tools which is placed in the box-header.</p>
            <pre class="prettyprint">
&LT;!-- This will cause the box to be removed when clicked -->
&LT;button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">&LT;i class="fa fa-times">&LT;/i>&LT;/button>
&LT;!-- This will cause the box to collapse when clicked -->
&LT;button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">&LT;i class="fa fa-minus">&LT;/i>&LT;/button></pre>
            <div class="row">
                <div class="col-md-4">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Collapsable</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div><!-- /.box-tools -->
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            The body of the box
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <pre class="prettyprint">
&LT;div class="box box-default">
  &LT;div class="box-header with-border">
    &LT;h3 class="box-title">Collapsable&LT;/h3>
    &LT;div class="box-tools pull-right">
      &LT;button class="btn btn-box-tool" data-widget="collapse">&LT;i class="fa fa-minus">&LT;/i>&LT;/button>
    &LT;/div>&LT;!-- /.box-tools -->
  &LT;/div>&LT;!-- /.box-header -->
  &LT;div class="box-body">
    The body of the box
  &LT;/div>&LT;!-- /.box-body -->
&LT;/div>&LT;!-- /.box --></pre>
                </div>
                <div class="col-md-4">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Removable</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div><!-- /.box-tools -->
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            The body of the box
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <pre class="prettyprint">
&LT;div class="box box-default">
  &LT;div class="box-header with-border">
    &LT;h3 class="box-title">Removable&LT;/h3>
    &LT;div class="box-tools pull-right">
      &LT;button class="btn btn-box-tool" data-widget="remove">&LT;i class="fa fa-times">&LT;/i>&LT;/button>
    &LT;/div>&LT;!-- /.box-tools -->
  &LT;/div>&LT;!-- /.box-header -->
  &LT;div class="box-body">
    The body of the box
  &LT;/div>&LT;!-- /.box-body -->
&LT;/div>&LT;!-- /.box --></pre>
                </div>
                <div class="col-md-4">
                    <div class="box box-default collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Expandable</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            </div><!-- /.box-tools -->
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            The body of the box
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <pre class="prettyprint">
&LT;div class="box box-default collapsed-box">
  &LT;div class="box-header with-border">
    &LT;h3 class="box-title">Expandable&LT;/h3>
    &LT;div class="box-tools pull-right">
      &LT;button class="btn btn-box-tool" data-widget="collapse">&LT;i class="fa fa-plus">&LT;/i>&LT;/button>
    &LT;/div>&LT;!-- /.box-tools -->
  &LT;/div>&LT;!-- /.box-header -->
  &LT;div class="box-body">
    The body of the box
  &LT;/div>&LT;!-- /.box-body -->
&LT;/div>&LT;!-- /.box --></pre>
                </div>
            </div><!-- /.row -->
            <p>We can also add labels, badges, pagination, tooltips, inputs and many more in the box tools. A few examples:</p>
            <div class="row">
                <div class="col-md-4">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Labels</h3>
                            <div class="box-tools pull-right">
                                <span class="label label-default">Some Label</span>
                            </div><!-- /.box-tools -->
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            The body of the box
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <pre class="prettyprint">
&LT;div class="box box-default">
  &LT;div class="box-header with-border">
    &LT;h3 class="box-title">Labels&LT;/h3>
    &LT;div class="box-tools pull-right">
      &LT;span class="label label-default">8 New Messages&LT;/span>
    &LT;/div>&LT;!-- /.box-tools -->
  &LT;/div>&LT;!-- /.box-header -->
  &LT;div class="box-body">
    The body of the box
  &LT;/div>&LT;!-- /.box-body -->
&LT;/div>&LT;!-- /.box --></pre>
                </div>
                <div class="col-md-4">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Input</h3>
                            <div class="box-tools pull-right">
                                <div class="has-feedback">
                                    <input type="text" class="form-control input-sm" placeholder="Search...">
                                    <span class="glyphicon glyphicon-search form-control-feedback text-muted"></span>
                                </div>
                            </div><!-- /.box-tools -->
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            The body of the box
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <pre class="prettyprint">
&LT;div class="box box-default">
  &LT;div class="box-header with-border">
    &LT;h3 class="box-title">Input&LT;/h3>
    &LT;div class="box-tools pull-right">
      &LT;div class="has-feedback">
        &LT;input type="text" class="form-control input-sm" placeholder="Search...">
        &LT;span class="glyphicon glyphicon-search form-control-feedback">&LT;/span>
      &LT;/div>
    &LT;/div>&LT;!-- /.box-tools -->
  &LT;/div>&LT;!-- /.box-header -->
  &LT;div class="box-body">
    The body of the box
  &LT;/div>&LT;!-- /.box-body -->
&LT;/div>&LT;!-- /.box --></pre>
                </div>
                <div class="col-md-4">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tootips on buttons</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                            </div><!-- /.box-tools -->
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            The body of the box
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <pre class="prettyprint">
&LT;div class="box box-default">
  &LT;div class="box-header with-border">
    &LT;h3 class="box-title">Tooltips on buttons&LT;/h3>
    &LT;div class="box-tools pull-right">
      &LT;button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">&LT;i class="fa fa-minus">&LT;/i>&LT;/button>
      &LT;button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">&LT;i class="fa fa-times">&LT;/i>&LT;/button>
    &LT;/div>&LT;!-- /.box-tools -->
  &LT;/div>&LT;!-- /.box-header -->
  &LT;div class="box-body">
    The body of the box
  &LT;/div>&LT;!-- /.box-body -->
&LT;/div>&LT;!-- /.box --></pre>
                </div><!-- /.col -->
            </div><!-- /.row -->
            <p>
                If you inserted a box into the document after <code>app.js</code> was loaded, you have to activate
                the collapse/remove buttons explicitly by calling <code>.activateBox()</code>:
            </p>
            <pre class="prettyprint"><code class="html">&LT;script>
    $("#box-widget").activateBox();
&LT;/script></code></pre>

            <h4>Loading States</h4>
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Loading state</h3>
                        </div>
                        <div class="box-body">
                            The body of the box
                        </div><!-- /.box-body -->
                        <!-- Loading (remove the following to stop the loading)-->
                        <div class="overlay">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                        <!-- end loading -->
                    </div><!-- /.box -->
                </div><!-- /.col -->

                <div class="col-md-6">
                    <div class="box box-default box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Loading state (.box-solid)</h3>
                        </div>
                        <div class="box-body">
                            The body of the box
                        </div><!-- /.box-body -->
                        <!-- Loading (remove the following to stop the loading)-->
                        <div class="overlay">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                        <!-- end loading -->
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
            <p class="lead">
                To simulate a loading state, simply place this code before the <code>.box</code> closing tag.
            </p>
            <pre class="prettyprint"><code class="html">&LT;div class="overlay">
  &LT;i class="fa fa-refresh fa-spin">&LT;/i>
&LT;/div>
</code></pre>
            <h3 id="component-direct-chat">Direct Chat</h3>
            <p class="lead">The direct chat widget extends the box component to create a beautiful chat interface. This widget
                consists of a required messages pane and an <b>optional</b> contacts pane. Examples:</p>
            <!-- Direct Chat -->
            <div class="row">
                <div class="col-md-3">
                    <!-- DIRECT CHAT PRIMARY -->
                    <div class="box box-primary direct-chat direct-chat-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Direct Chat</h3>
                            <div class="box-tools pull-right">
                                <span data-toggle="tooltip" title="3 New Messages" class="badge bg-light-blue">3</span>
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
                                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <!-- Conversations are loaded here -->
                            <div class="direct-chat-messages">
                                <!-- Message. Default to the left -->
                                <div class="direct-chat-msg">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-left">Alexander Pierce</span>
                                        <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                                    </div><!-- /.direct-chat-info -->
                                    <img class="direct-chat-img" src="../dist/img/user1-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
                                    <div class="direct-chat-text">
                                        Is this template really for free? That's unbelievable!
                                    </div><!-- /.direct-chat-text -->
                                </div><!-- /.direct-chat-msg -->

                                <!-- Message to the right -->
                                <div class="direct-chat-msg right">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-right">Sarah Bullock</span>
                                        <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
                                    </div><!-- /.direct-chat-info -->
                                    <img class="direct-chat-img" src="../dist/img/user3-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
                                    <div class="direct-chat-text">
                                        You better believe it!
                                    </div><!-- /.direct-chat-text -->
                                </div><!-- /.direct-chat-msg -->
                            </div><!--/.direct-chat-messages-->

                            <!-- Contacts are loaded here -->
                            <div class="direct-chat-contacts">
                                <ul class="contacts-list">
                                    <li>
                                        <a href="#">
                                            <img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="Contact Avatar">
                                            <div class="contacts-list-info">
                                                <span class="contacts-list-name">
                                                    Count Dracula
                                                    <small class="contacts-list-date pull-right">2/28/2015</small>
                                                </span>
                                                <span class="contacts-list-msg">How have you been? I was...</span>
                                            </div><!-- /.contacts-list-info -->
                                        </a>
                                    </li><!-- End Contact Item -->
                                </ul><!-- /.contatcts-list -->
                            </div><!-- /.direct-chat-pane -->
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <form action="#" method="post">
                                <div class="input-group">
                                    <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-flat">Send</button>
                                    </span>
                                </div>
                            </form>
                        </div><!-- /.box-footer-->
                    </div><!--/.direct-chat -->
                </div><!-- /.col -->

                <div class="col-md-3">
                    <!-- DIRECT CHAT SUCCESS -->
                    <div class="box box-success direct-chat direct-chat-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Direct Chat</h3>
                            <div class="box-tools pull-right">
                                <span data-toggle="tooltip" title="3 New Messages" class="badge bg-green">3</span>
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
                                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <!-- Conversations are loaded here -->
                            <div class="direct-chat-messages">
                                <!-- Message. Default to the left -->
                                <div class="direct-chat-msg">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-left">Alexander Pierce</span>
                                        <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                                    </div><!-- /.direct-chat-info -->
                                    <img class="direct-chat-img" src="../dist/img/user1-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
                                    <div class="direct-chat-text">
                                        Is this template really for free? That's unbelievable!
                                    </div><!-- /.direct-chat-text -->
                                </div><!-- /.direct-chat-msg -->

                                <!-- Message to the right -->
                                <div class="direct-chat-msg right">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-right">Sarah Bullock</span>
                                        <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
                                    </div><!-- /.direct-chat-info -->
                                    <img class="direct-chat-img" src="../dist/img/user3-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
                                    <div class="direct-chat-text">
                                        You better believe it!
                                    </div><!-- /.direct-chat-text -->
                                </div><!-- /.direct-chat-msg -->
                            </div><!--/.direct-chat-messages-->

                            <!-- Contacts are loaded here -->
                            <div class="direct-chat-contacts">
                                <ul class="contacts-list">
                                    <li>
                                        <a href="#">
                                            <img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="Contact Avatar">
                                            <div class="contacts-list-info">
                                                <span class="contacts-list-name">
                                                    Count Dracula
                                                    <small class="contacts-list-date pull-right">2/28/2015</small>
                                                </span>
                                                <span class="contacts-list-msg">How have you been? I was...</span>
                                            </div><!-- /.contacts-list-info -->
                                        </a>
                                    </li><!-- End Contact Item -->
                                </ul><!-- /.contatcts-list -->
                            </div><!-- /.direct-chat-pane -->
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <form action="#" method="post">
                                <div class="input-group">
                                    <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-success btn-flat">Send</button>
                                    </span>
                                </div>
                            </form>
                        </div><!-- /.box-footer-->
                    </div><!--/.direct-chat -->
                </div><!-- /.col -->

                <div class="col-md-3">
                    <!-- DIRECT CHAT WARNING -->
                    <div class="box box-warning direct-chat direct-chat-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Direct Chat</h3>
                            <div class="box-tools pull-right">
                                <span data-toggle="tooltip" title="3 New Messages" class="badge bg-yellow">3</span>
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
                                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <!-- Conversations are loaded here -->
                            <div class="direct-chat-messages">
                                <!-- Message. Default to the left -->
                                <div class="direct-chat-msg">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-left">Alexander Pierce</span>
                                        <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                                    </div><!-- /.direct-chat-info -->
                                    <img class="direct-chat-img" src="../dist/img/user1-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
                                    <div class="direct-chat-text">
                                        Is this template really for free? That's unbelievable!
                                    </div><!-- /.direct-chat-text -->
                                </div><!-- /.direct-chat-msg -->

                                <!-- Message to the right -->
                                <div class="direct-chat-msg right">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-right">Sarah Bullock</span>
                                        <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
                                    </div><!-- /.direct-chat-info -->
                                    <img class="direct-chat-img" src="../dist/img/user3-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
                                    <div class="direct-chat-text">
                                        You better believe it!
                                    </div><!-- /.direct-chat-text -->
                                </div><!-- /.direct-chat-msg -->
                            </div><!--/.direct-chat-messages-->

                            <!-- Contacts are loaded here -->
                            <div class="direct-chat-contacts">
                                <ul class="contacts-list">
                                    <li>
                                        <a href="#">
                                            <img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="Contact Avatar">
                                            <div class="contacts-list-info">
                                                <span class="contacts-list-name">
                                                    Count Dracula
                                                    <small class="contacts-list-date pull-right">2/28/2015</small>
                                                </span>
                                                <span class="contacts-list-msg">How have you been? I was...</span>
                                            </div><!-- /.contacts-list-info -->
                                        </a>
                                    </li><!-- End Contact Item -->
                                </ul><!-- /.contatcts-list -->
                            </div><!-- /.direct-chat-pane -->
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <form action="#" method="post">
                                <div class="input-group">
                                    <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-warning btn-flat">Send</button>
                                    </span>
                                </div>
                            </form>
                        </div><!-- /.box-footer-->
                    </div><!--/.direct-chat -->
                </div><!-- /.col -->

                <div class="col-md-3">
                    <!-- DIRECT CHAT DANGER -->
                    <div class="box box-danger direct-chat direct-chat-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title">Direct Chat</h3>
                            <div class="box-tools pull-right">
                                <span data-toggle="tooltip" title="3 New Messages" class="badge bg-red">3</span>
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
                                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <!-- Conversations are loaded here -->
                            <div class="direct-chat-messages">
                                <!-- Message. Default to the left -->
                                <div class="direct-chat-msg">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-left">Alexander Pierce</span>
                                        <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                                    </div><!-- /.direct-chat-info -->
                                    <img class="direct-chat-img" src="../dist/img/user1-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
                                    <div class="direct-chat-text">
                                        Is this template really for free? That's unbelievable!
                                    </div><!-- /.direct-chat-text -->
                                </div><!-- /.direct-chat-msg -->

                                <!-- Message to the right -->
                                <div class="direct-chat-msg right">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-right">Sarah Bullock</span>
                                        <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
                                    </div><!-- /.direct-chat-info -->
                                    <img class="direct-chat-img" src="../dist/img/user3-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
                                    <div class="direct-chat-text">
                                        You better believe it!
                                    </div><!-- /.direct-chat-text -->
                                </div><!-- /.direct-chat-msg -->
                            </div><!--/.direct-chat-messages-->

                            <!-- Contacts are loaded here -->
                            <div class="direct-chat-contacts">
                                <ul class="contacts-list">
                                    <li>
                                        <a href="#">
                                            <img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="Contact Avatar">
                                            <div class="contacts-list-info">
                                                <span class="contacts-list-name">
                                                    Count Dracula
                                                    <small class="contacts-list-date pull-right">2/28/2015</small>
                                                </span>
                                                <span class="contacts-list-msg">How have you been? I was...</span>
                                            </div><!-- /.contacts-list-info -->
                                        </a>
                                    </li><!-- End Contact Item -->
                                </ul><!-- /.contatcts-list -->
                            </div><!-- /.direct-chat-pane -->
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <form action="#" method="post">
                                <div class="input-group">
                                    <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-danger btn-flat">Send</button>
                                    </span>
                                </div>
                            </form>
                        </div><!-- /.box-footer-->
                    </div><!--/.direct-chat -->
                </div><!-- /.col -->
            </div><!-- /.row -->
            <p class="lead">Direct Chat Markup</p>
            <pre class="prettyprint"><code class="html">
&LT;!-- Construct the box with style you want. Here we are using box-danger -->
&LT;!-- Then add the class direct-chat and choose the direct-chat-* contexual class -->
&LT;!-- The contextual class should match the box, so we are using direct-chat-danger -->
&LT;div class="box box-danger direct-chat direct-chat-danger">
  &LT;div class="box-header with-border">
    &LT;h3 class="box-title">Direct Chat&LT;/h3>
    &LT;div class="box-tools pull-right">
      &LT;span data-toggle="tooltip" title="3 New Messages" class="badge bg-red">3&LT;/span>
      &LT;button class="btn btn-box-tool" data-widget="collapse">&LT;i class="fa fa-minus">&LT;/i>&LT;/button>
      &LT;!-- In box-tools add this button if you intend to use the contacts pane -->
      &LT;button class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">&LT;i class="fa fa-comments">&LT;/i>&LT;/button>
      &LT;button class="btn btn-box-tool" data-widget="remove">&LT;i class="fa fa-times">&LT;/i>&LT;/button>
    &LT;/div>
  &LT;/div>&LT;!-- /.box-header -->
  &LT;div class="box-body">
    &LT;!-- Conversations are loaded here -->
    &LT;div class="direct-chat-messages">
      &LT;!-- Message. Default to the left -->
      &LT;div class="direct-chat-msg">
        &LT;div class="direct-chat-info clearfix">
          &LT;span class="direct-chat-name pull-left">Alexander Pierce&LT;/span>
          &LT;span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm&LT;/span>
        &LT;/div>&LT;!-- /.direct-chat-info -->
        &LT;img class="direct-chat-img" src="../dist/img/user1-128x128.jpg" alt="message user image">&LT;!-- /.direct-chat-img -->
        &LT;div class="direct-chat-text">
          Is this template really for free? That's unbelievable!
        &LT;/div>&LT;!-- /.direct-chat-text -->
      &LT;/div>&LT;!-- /.direct-chat-msg -->

      &LT;!-- Message to the right -->
      &LT;div class="direct-chat-msg right">
        &LT;div class="direct-chat-info clearfix">
          &LT;span class="direct-chat-name pull-right">Sarah Bullock&LT;/span>
          &LT;span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm&LT;/span>
        &LT;/div>&LT;!-- /.direct-chat-info -->
        &LT;img class="direct-chat-img" src="../dist/img/user3-128x128.jpg" alt="message user image">&LT;!-- /.direct-chat-img -->
        &LT;div class="direct-chat-text">
          You better believe it!
        &LT;/div>&LT;!-- /.direct-chat-text -->
      &LT;/div>&LT;!-- /.direct-chat-msg -->
    &LT;/div>&LT;!--/.direct-chat-messages-->

    &LT;!-- Contacts are loaded here -->
    &LT;div class="direct-chat-contacts">
      &LT;ul class="contacts-list">
        &LT;li>
          &LT;a href="#">
            &LT;img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="Contact Avatar">
            &LT;div class="contacts-list-info">
              &LT;span class="contacts-list-name">
                Count Dracula
                &LT;small class="contacts-list-date pull-right">2/28/2015&LT;/small>
              &LT;/span>
              &LT;span class="contacts-list-msg">How have you been? I was...&LT;/span>
            &LT;/div>&LT;!-- /.contacts-list-info -->
          &LT;/a>
        &LT;/li>&LT;!-- End Contact Item -->
      &LT;/ul>&LT;!-- /.contatcts-list -->
    &LT;/div>&LT;!-- /.direct-chat-pane -->
  &LT;/div>&LT;!-- /.box-body -->
  &LT;div class="box-footer">
    &LT;div class="input-group">
      &LT;input type="text" name="message" placeholder="Type Message ..." class="form-control">
      &LT;span class="input-group-btn">
        &LT;button type="button" class="btn btn-danger btn-flat">Send&LT;/button>
      &LT;/span>
    &LT;/div>
  &LT;/div>&LT;!-- /.box-footer-->
&LT;/div>&LT;!--/.direct-chat -->
</code></pre>

            <p>Of course you can use direct chat with a solid box by adding the class <code>solid-box</code> to the box. Here are a couple of examples:</p>

            <!-- Direct Chat With Solid Boxes -->
            <div class="row">
                <div class="col-md-6">
                    <!-- DIRECT CHAT WARNING -->
                    <div class="box box-primary box-solid direct-chat direct-chat-primary">
                        <div class="box-header">
                            <h3 class="box-title">Direct Chat in a Solid Box</h3>
                            <div class="box-tools pull-right">
                                <span data-toggle="tooltip" title="3 New Messages" class="badge bg-light-blue">3</span>
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
                                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <!-- Conversations are loaded here -->
                            <div class="direct-chat-messages">
                                <!-- Message. Default to the left -->
                                <div class="direct-chat-msg">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-left">Alexander Pierce</span>
                                        <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                                    </div><!-- /.direct-chat-info -->
                                    <img class="direct-chat-img" src="../dist/img/user1-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
                                    <div class="direct-chat-text">
                                        Is this template really for free? That's unbelievable!
                                    </div><!-- /.direct-chat-text -->
                                </div><!-- /.direct-chat-msg -->

                                <!-- Message to the right -->
                                <div class="direct-chat-msg right">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-right">Sarah Bullock</span>
                                        <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
                                    </div><!-- /.direct-chat-info -->
                                    <img class="direct-chat-img" src="../dist/img/user3-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
                                    <div class="direct-chat-text">
                                        You better believe it!
                                    </div><!-- /.direct-chat-text -->
                                </div><!-- /.direct-chat-msg -->
                            </div><!--/.direct-chat-messages-->

                            <!-- Contacts are loaded here -->
                            <div class="direct-chat-contacts">
                                <ul class="contacts-list">
                                    <li>
                                        <a href="#">
                                            <img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="Contact Avatar">
                                            <div class="contacts-list-info">
                                                <span class="contacts-list-name">
                                                    Count Dracula
                                                    <small class="contacts-list-date pull-right">2/28/2015</small>
                                                </span>
                                                <span class="contacts-list-msg">How have you been? I was...</span>
                                            </div><!-- /.contacts-list-info -->
                                        </a>
                                    </li><!-- End Contact Item -->
                                </ul><!-- /.contatcts-list -->
                            </div><!-- /.direct-chat-pane -->
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <form action="#" method="post">
                                <div class="input-group">
                                    <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-flat">Send</button>
                                    </span>
                                </div>
                            </form>
                        </div><!-- /.box-footer-->
                    </div><!--/.direct-chat -->
                </div><!-- /.col -->

                <div class="col-md-6">
                    <!-- DIRECT CHAT DANGER -->
                    <div class="box box-info box-solid direct-chat direct-chat-info">
                        <div class="box-header">
                            <h3 class="box-title">Direct Chat in a Solid Box</h3>
                            <div class="box-tools pull-right">
                                <span data-toggle="tooltip" title="3 New Messages" class="badge bg-aqua">3</span>
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
                                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <!-- Conversations are loaded here -->
                            <div class="direct-chat-messages">
                                <!-- Message. Default to the left -->
                                <div class="direct-chat-msg">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-left">Alexander Pierce</span>
                                        <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                                    </div><!-- /.direct-chat-info -->
                                    <img class="direct-chat-img" src="../dist/img/user1-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
                                    <div class="direct-chat-text">
                                        Is this template really for free? That's unbelievable!
                                    </div><!-- /.direct-chat-text -->
                                </div><!-- /.direct-chat-msg -->

                                <!-- Message to the right -->
                                <div class="direct-chat-msg right">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-right">Sarah Bullock</span>
                                        <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
                                    </div><!-- /.direct-chat-info -->
                                    <img class="direct-chat-img" src="../dist/img/user3-128x128.jpg" alt="message user image"><!-- /.direct-chat-img -->
                                    <div class="direct-chat-text">
                                        You better believe it!
                                    </div><!-- /.direct-chat-text -->
                                </div><!-- /.direct-chat-msg -->
                            </div><!--/.direct-chat-messages-->

                            <!-- Contacts are loaded here -->
                            <div class="direct-chat-contacts">
                                <ul class="contacts-list">
                                    <li>
                                        <a href="#">
                                            <img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="Contact Avatar">
                                            <div class="contacts-list-info">
                                                <span class="contacts-list-name">
                                                    Count Dracula
                                                    <small class="contacts-list-date pull-right">2/28/2015</small>
                                                </span>
                                                <span class="contacts-list-msg">How have you been? I was...</span>
                                            </div><!-- /.contacts-list-info -->
                                        </a>
                                    </li><!-- End Contact Item -->
                                </ul><!-- /.contatcts-list -->
                            </div><!-- /.direct-chat-pane -->
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <form action="#" method="post">
                                <div class="input-group">
                                    <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-info btn-flat">Send</button>
                                    </span>
                                </div>
                            </form>
                        </div><!-- /.box-footer-->
                    </div><!--/.direct-chat -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section>


        <!-- ============================================================= -->

        <section id="plugins">
            <h2 class="page-header"><a href="#plugins">Plugins</a></h2>
            <p class="lead">AdminLTE makes use of the following plugins. For documentation, updates or license information, please visit the provided links.</p>
            <div class="row bring-up">
                <div class="col-sm-3">
                    <ul class="list-unstyled">
                        <li><h4>Charts</h4></li>
                        <li><a href="http://www.chartjs.org/" target="_blank">ChartJS</a></li>
                        <li><a href="http://www.flotcharts.org/" target="_blank">Flot</a></li>
                        <li><a href="http://morrisjs.github.io/morris.js/" target="_blank">Morris.js</a></li>
                        <li><a href="http://omnipotent.net/jquery.sparkline/" target="_blank">Sparkline</a></li>
                    </ul>
                </div><!-- /.col -->
                <div class="col-sm-3">
                    <ul class="list-unstyled">
                        <li><h4>Form Elements</h4></li>
                        <li><a href="https://github.com/seiyria/bootstrap-slider/">Bootstrap Slider</a></li>
                        <li><a href="http://ionden.com/a/plugins/ion.rangeSlider/en.html" target="_blank">Ion Slider</a></li>
                        <li><a href="http://bootstrap-datepicker.readthedocs.org/" target="_blank">Date Picker</a></li>
                        <li><a href="http://www.daterangepicker.com/" target="_blank">Date Range Picker</a></li>
                        <li><a href="http://mjolnic.com/bootstrap-colorpicker/" target="_blank">Color Picker</a></li>
                        <li><a href="https://github.com/jdewit/bootstrap-timepicker/" target="_blank">Time Picker</a></li>
                        <li><a href="http://fronteed.com/iCheck/" target="_blank">iCheck</a></li>
                        <li><a href="https://github.com/RobinHerbots/jquery.inputmask/" target="_blank">Input Mask</a></li>
                    </ul>
                </div><!-- /.col -->
                <div class="col-sm-3">
                    <ul class="list-unstyled">
                        <li><h4>Editors</h4></li>
                        <li><a href="https://github.com/bootstrap-wysiwyg/bootstrap3-wysiwyg/" target="_blank">Bootstrap WYSIHTML5</a></li>
                        <li><a href="http://ckeditor.com/" target="_blank">CK Editor</a></li>
                    </ul>
                </div><!-- /. col -->
                <div class="col-sm-3">
                    <ul class="list-unstyled">
                        <li><h4>Other</h4></li>
                        <li><a href="https://datatables.net/examples/styling/bootstrap.html" target="_blank">DataTables</a></li>
                        <li><a href="http://fullcalendar.io/" target="_blank">Full Calendar</a></li>
                        <li><a href="http://jqueryui.com/" target="_blank">jQuery UI</a></li>
                        <li><a href="http://anthonyterrien.com/knob/" target="_blank">jQuery Knob</a></li>
                        <li><a href="http://jvectormap.com/" target="_blank">jVector Map</a></li>
                        <li><a href="http://rocha.la/jQuery-slimScroll/" target="_blank">Slim Scroll</a></li>
                        <li><a href="http://github.hubspot.com/pace/docs/welcome/" target="_blank">Pace</a></li>
                    </ul>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section>


        <!-- ============================================================= -->

        <section id="browsers">
            <h2 class="page-header"><a href="#browsers">Browser Support</a></h2>
            <p class="lead">AdminLTE supports the following browsers:</p>
            <ul>
                <li>IE9+</li>
                <li>Firefox (latest)</li>
                <li>Safari (latest)</li>
                <li>Chrome (latest)</li>
                <li>Opera (latest)</li>
            </ul>
            <p><b>Note:</b> IE9 does not support transitions or animations. The template will function properly but it won't use animations/transitions on IE9.</p>
        </section>


        <!-- ============================================================= -->

        <section id="upgrade">
            <h2 class="page-header"><a href="#upgrade">Upgrade Guide</a></h2>
            <p class="lead">To upgrade from version 1.x to the lateset version, follow this guide.</p>
            <h3>New Files</h3>
            <p>Make sure you update all CSS and JS files that are related to AdminLTE. Otherwise, the layout will not
                function properly. Most important files are AdminLTE.css, skins CSS files, and app.js.</p>
            <h3>Layout Changes</h3>
            <ol>
                <li>The .wrapper div must be placed immediately after the body tag rather than after the header</li>
                <li>Change the .header div to .main-header <code>&LT;div class="main-header"></code></li>
                <li>Change the .right-side class to .content-wrapper <code>&LT;div class="content-wrapper"></code></li>
                <li>Change the .left-side class to .main-sidebar <code>&LT;div class="main-sidebar"></code></li>
                <li>In the navbar, change .navbar-right to .navbar-custom-menu <code>&LT;div class="navbar-custom-menu"></code></li>
            </ol>
            <h3>Navbar Custom Dropdown Menus</h3>
            <ol>
                <li>The icons in the notification menu do not need bg-* classes. They should be replaced with contextual text color class such as text-aqua or text-red.</li>
            </ol>
            <h3>Login, Registration and Lockscreen Pages</h3>
            <p>There are major changes to the HTML markup and style to these pages. The best way is to copy the page's code and customize it.</p>
            <p>And you should be set to go!</p>
            <h3>Mailbox</h3>
            <p>Mailbox got an upgrade to include three different views. The views are inbox, read mail, and compose new email. To use these views,
                you should use the provided HTML files in the pages/mailbox folder.</p>
            <p><b class="text-red">Note:</b> the old mailbox layout has been deprecated in favor of the new one and will be removed by the next release.</p>
        </section>


        <!-- ============================================================= -->

        <section id="implementations">
            <h2 class="page-header"><a href="#implementations">Implementations</a></h2>
            <p class="lead">Thanks to many of AdminLTE users, there are multiple implementations of the template
                for easy integration with back-end frameworks. The following are some of them:</p>

            <ul>
                <li><a href="https://github.com/mmdsharifi/AdminLTE-RTL">Persian RTL</a> by <a href="https://github.com/mmdsharifi">Mohammad Sharifi</a></li>
                <li><a href="https://github.com/dmstr/yii2-adminlte-asset" target="_blank">Yii 2</a> by <a href="https://github.com/schmunk42" target="_blank">Tobias Munk</a></li>
                <li><a href="https://github.com/yajra/laravel-admin-template" target="_blank">Laravel</a> by <a href="https://github.com/yajra" target="_blank">Arjay Angeles</a></li>
                <li><a href="https://github.com/acacha/adminlte-laravel" target="_blank">Laravel 5 package</a> by <a href="https://github.com/acacha" target="_blank">Sergi Tur Badenas</a></li>
                <li><a href="https://github.com/avanzu/AdminThemeBundle" target="_blank">Symfony</a> by <a href="https://github.com/avanzu" target="_blank">Marc Bach</a></li>
                <li>Rails gems: <a href="https://github.com/nicolas-besnard/adminlte2-rails" target="_blank">adminlte2-rails</a> by <a href="https://github.com/nicolas-besnard" target="_blank">Nicolas Besnard</a> and <a href="https://github.com/racketlogger/lte-rails" target="_blank">lte-rails</a> (using AdminLTE sources) by <a href="https://github.com/racketlogger" target="_blank">Carlos at RacketLogger</a></li>
            </ul>

            <p><b class="text-red">Note:</b> these implementations are not supported by Almsaeed Studio. However,
                they do provide a good example of how to integrate AdminLTE into different frameworks. For the latest release
                of AdminLTE, please visit our <a href="https://github.com/almasaeed2010/AdminLTE">repository</a> or <a href="https://almsaeedstudio.com">website</a></p>
        </section>


        <!-- ============================================================= -->

        <section id="faq">
            <h2 class="page-header"><a href="#faq">FAQ</a></h2>
            <h3>Can AdminLTE be used with Wordpress?</h3>
            <p class="lead">AdminLTE is an HTML template that can be used for any purpose. However, it is not made to be easily installed on Wordpress. It will require some effort and enough knowledge of the Wordpress script to do so.</p>

            <h3>Is there an integration guide for PHP frameworks such as Yii or Symfony?</h3>
            <p class="lead">Short answer, no. However, there are forks and tutorials around the web that provide info on how to integrate with many different frameworks. There are even versions of AdminLTE that are integrated with jQuery ajax, AngularJS and/or MVC5 ASP .NET.</p>

            <h3>How do I get notified of new AdminLTE versions?</h3>
            <p class="lead">The best option is to subscribe to our mailing list using the <a href="http://almsaeedstudio.com/#subscribe">subscription form on Almsaeed Studio</a>.
                If that's not appealing to you, you may watch the <a href="https://github.com/almasaeed2010/AdminLTE">repository on Github</a> or visit <a href="http://almsaeedstudio.com">Almsaeed Studio</a> every now and then for updates and announcements.</p>
        </section>


        <!-- ============================================================= -->

        <section id="license">
            <h2 class="page-header"><a href="#license">License</a></h2>
            <h3>AdminLTE</h3>
            <p class="lead">
                AdminLTE is an open source project that is licensed under the <a href="http://opensource.org/licenses/MIT">MIT license</a>.
                This allows you to do pretty much anything you want as long as you include
                the copyright in "all copies or substantial portions of the Software."
                Attribution is not required (though very much appreciated).
            </p>
        </section>


    </div><!-- /.content -->


    <!--    <div class="jumbotron">
            <br>
            <h1><b>MPSP </b> e-Store </h1>
            <h1 style="color: #cc99ff;
                -webkit-text-fill-color: #cc99ff; /* Will override color (regardless of order) */
                -webkit-text-stroke-width: 2px;
                -webkit-text-stroke-color: #000099;">
                <strong>MPSP e-Store</strong>
                <strong>SISTEM PENGURUSAN KONTRAKTORISASI AGSE / AGSV</strong>
            </h1>

            <h1 style="color: black;
                letter-spacing: -3px;
                -webkit-text-fill-color: white; /* Will override color (regardless of order) */
                -webkit-text-stroke-width: 2px;
                -webkit-text-stroke-color: blue;">
                <strong>Pangkalan Udara Gong Kedak</strong></h1>
                <strong>MAJLIS PERBANDARAN SEBERANG PERAI</strong></h1>
            <p class="lead">MAJLIS PERBANDARAN SEBERANG PERAI</p>
            <p><a class="btn btn-lg btn-success" href="site/index">Pengenalan Sistem</a></p>
        </div>-->



    <!--    <div class="body-content">

            <div class="row">
                <div class="col-lg-4">
                    <h2>Heading</h2>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur.</p>

                    <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
                </div>
                <div class="col-lg-4">
                    <h2>Heading</h2>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur.</p>

                    <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
                </div>
                <div class="col-lg-4">
                    <h2>Heading</h2>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur.</p>

                    <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
                </div>
            </div>

        </div>-->
</div>
