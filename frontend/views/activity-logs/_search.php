<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ActivityLogsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-logs-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'remote_ip') ?>

    <?= $form->field($model, 'action') ?>

    <?= $form->field($model, 'controller') ?>

    <?php // echo $form->field($model, 'params') ?>

    <?php // echo $form->field($model, 'route') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'messages') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
