<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Roles */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Roles',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Kemaskini');
?>
<div class="roles-update">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
        </div>

        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>

    </div>
</div>
