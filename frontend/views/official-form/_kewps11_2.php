            <tbody>
                <?php if (sizeof($items) >= 1) { ?>
                    <?php foreach ($items as $i => $item) { ?>
                        <tr>
                            <td class="text-center"><?= $i + 1 ?></td>
                            <td class="" colspan="2"><?= $item['inventory']['description'] ?></td>
                            <td class="text-center" ><?= $item['rq_quantity'] ?></td>
                            <td class="text-center" ><?= $item['app_quantity'] ?></td>
                            <td class="text-center" ><?= $item['current_balance'] ?></td>
                            <td class=""></td>
                        </tr>
                    <?php } ?>
                    <?php for ($j = sizeof($items); $j < 5; $j++) { ?>
                        <tr>
                            <td class="text-center"> <?= $j + 1 ?></td>
                            <td class="text-center" colspan="2"> </td>
                            <td class="text-center" > </td>
                            <td class="text-center" > </td>
                            <td class="text-center" > </td>
                            <td class="text-center">&nbsp;</td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <?php for ($j = 0; $j < 5; $j++) { ?>
                        <tr>
                            <td class="text-center"> <?= $j + 1 ?></td>
                            <td class="text-center" colspan="2"> </td>
                            <td class="text-center" > </td>
                            <td class="text-center" > </td>
                            <td class="text-center" > </td>
                            <td class="text-center">&nbsp;</td>
                        </tr>
                    <?php } ?>
                <?php } ?>
            <tbody>
