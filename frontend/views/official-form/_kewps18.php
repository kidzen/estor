


<div class="box-body table-responsive">
    <div>
        <!--<p><span>Pekeliling Perbendaharaan Malaysia</span><span class="pull-right">AM 6.5 Lampiran A</span></p>-->
        <p class="pull-right form-lampiran"><strong>LAMPIRAN E</strong><br><strong>KEW.PS-18</strong></p>
        <!--<div class="clearfix"></div>-->
        <p class="text-center form-name"><strong>PERAKUAN AMBIL ALIH</strong></p>
        <p><strong>Kementerian/Jabatan: Kategori Stor:</strong></p>
        <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
        <!--<table bordered>-->
            <thead>
                <tr>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">Bil</th>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">No Kad Kawalan Stok</th>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">Perihal Stok</th>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">Harga Seunit (RM)</th>
                    <th class="col-2 vertical-align-top text-center text-center info" colspan="2">Kad Kawalan Stok</th>
                    <th class="col-2 vertical-align-top text-center text-center info" colspan="2">Baki Fizikal Stok</th>
                    <th class="col-2 vertical-align-top text-center text-center info" colspan="2">Kuantiti Fizikal Stok</th>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">Keadaan Stok (usang/rosak/tidak digunakan lagi/ tidak aktif/luput tempoh/ dll)</th>
                </tr>
                <tr>
                    <th class="col-1 vertical-align-top text-center text-center info">Kuantiti</th>
                    <th class="col-2 vertical-align-top text-center text-center info">Nilai (RM)</th>
                    <th class="col-1 vertical-align-top text-center text-center info">Kuantiti</th>
                    <th class="col-2 vertical-align-top text-center text-center info">Nilai (RM)</th>
                    <th class="col-3 vertical-align-top text-center text-center info">Lebih</th>
                    <th class="col-4 vertical-align-top text-center text-center info">Kurang</th>
                </tr>
            </thead>
            <tbody>
                <?php if (sizeof($items) >= 1) { ?>
                    <?php foreach ($items as $item) { ?>
                        <tr>
                            <td class="col-1 pull-left"><?= $item->inventory->code_no ?></td>
                            <td class="col-2 pull-left" colspan="2"><?= $item->inventory->description ?></td>
                            <td class="col-3 pull-left"><?= $item->rq_quantity ?></td>
                            <td class="col-4 pull-left"><?= $item->inventory->card_no ?></td>
                            <td class="col-5 pull-left"><?= $item->current_balance ?></td>
                            <td class="col-6 pull-left"><?= $item->app_quantity ?></td>
                            <td class="col-7 pull-left"><?= $item->unit_price / $item->app_quantity ?></td>
                            <td class="no-border-right text-center pull-left"><?= $item->unit_price ?></td>
                            <td class="col-9 pull-left"><?= $item->app_quantity ?></td>
                            <td class="col-10 pull-left"></td>
                        </tr>
                    <?php } ?>
                    <?php for ($j = sizeof($items); $j < 5; $j++) { ?>
                        <tr>
                            <td class="col-1 pull-left"> </td>
                            <td class="col-2 pull-left" colspan="2"> </td>
                            <td class="col-3 pull-left"> </td>
                            <td class="col-4 pull-left"> </td>
                            <td class="col-5 pull-left"> </td>
                            <td class="col-6 pull-left"> </td>
                            <td class="col-7 pull-left"> </td>
                            <td class="no-border-right text-center pull-left"> </td>
                            <td class="col-9 pull-left"> </td>
                            <td class="col-10 pull-left">&nbsp;</td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <?php for ($j = 0; $j < 5; $j++) { ?>
                        <tr>
                            <td class="col-1 pull-left"> </td>
                            <td class="col-2 pull-left"> </td>
                            <td class="col-2 pull-left"> </td>
                            <td class="col-2 pull-left"> </td>
                            <td class="col-2 pull-left"> </td>
                            <td class="col-1 pull-left"> </td>
                            <td class="col-2 pull-left"> </td>
                            <td class="col-2 pull-left"> </td>
                            <td class="col-2 pull-left"> </td>
                            <td class="col-2 pull-left"> </td>
                            <td class="col-10 pull-left">&nbsp;</td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td class="col-2 pull-left" colspan="4">JUMLAH:</td>
                        <td class="col-2 pull-left"> </td>
                        <td class="col-1 pull-left"> </td>
                        <td class="col-2 pull-left"> </td>
                        <td class="col-2 pull-left"> </td>
                        <td class="col-2 pull-left"> </td>
                        <td class="col-2 pull-left"> </td>
                        <td class="col-10 pull-left info">&nbsp;</td>
                    </tr>
                <?php } ?>
            </tbody>

        </table><br>
        <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
        <!--<table bordered>-->
            <tbody>

                    <tr>
                        <td class="col-2 pull-left"> </td>
                        <td class="col-2 pull-left"> </td>
                        <td class="col-10 pull-left">&nbsp;</td>
                    </tr>
            </tbody>

        </table>
    </div>
</div>
