            <tfoot>
                <tr>
                    <td class="no-border-bottom" colspan="4">
                    </td>
                    <td class="no-border-bottom" colspan="3"><strong>Kelulusan :</strong> <br>Permohonan diluluskan / tidak diluluskan*
                    </td>
                </tr>
                <tr>
                    <td class="no-border-bottom no-border-top" colspan="4">
                        <br><br>
                        <br><br>
                    </td>
                    <td class="no-border-bottom no-border-top" colspan="3">
                        <br><br>
                        <br><br>
                    </td>
                </tr>
                <tr>
                    <td class="no-border-top" colspan="4">
                        <p>............................................</p>
                        <p>(Tandatangan Pemohon)</p>
                    </td>
                    <td class="no-border-top" colspan="3">
                        <p>............................................</p>
                        <p>(Tandatangan Pegawai Pelulus)</p>
                    </td>
                </tr>
                <tr>
                    <td class="no-border-right" colspan="2">NAMA</td>
                    <td class="no-border-left" style="font-size:12;" colspan="2">: <?= mb_strimwidth($items[0]['order']['ordered_by'],0,20,"...") ?></td>
                    <td class="no-border-right">NAMA</td>
                    <td class="no-border-left" style="font-size:12;" colspan="2">: <?= mb_strimwidth($items[0]['order']['approvedBy']['mpspProfile']['nama'],0,40,"...") ?></td>
                </tr>
                <tr>
                    <td class="no-border-right" colspan="2">JAWATAN</td>
                    <td class="no-border-left" colspan="2">:</td>
                    <td class="no-border-right">JAWATAN</td>
                    <td class="no-border-left" style="font-size:12;" colspan="2">: <?= mb_strimwidth($items[0]['order']['approvedBy']['mpspProfile']['jawatan'],0,40,"...") ?></td>
                </tr>
                <tr>
                    <td class="no-border-right" colspan="2">TARIKH</td>
                    <td class="no-border-left" colspan="2">: <?= $items[0]['order']['order_date'] ?></td>
                    <td class="no-border-right">TARIKH</td>
                    <td class="no-border-left" colspan="2">: <?= $items[0]['order']['approved_at'] ?></td>
                </tr>
            </tfoot>
        </table>
        * sila potong yang berkenaan
        <br>
        <br>
        <table class="kv-grid-table table table-hover table-bordered ">
            <tbody>
                <tr>
                    <td class="no-border-bottom" colspan="4"><span><strong>Kemaskini Rekod :</strong></span></td>
                    <td class="no-border-bottom" colspan="3"><span><strong>Perakuan Penerimaan :</strong></span></td>
                </tr>
                <tr>
                    <td class="no-border-bottom wrap vertical-align-top" colspan="4"><span>Stok telah dikeluarkan dan direkod <br>di Kad Petak No  ...............................</span></td>
                    <td class="no-border-bottom wrap vertical-align-top" colspan="3"><span>Disahkan bahawa stok yang diluluskan telah diterima.</span></td>
                </tr>
                <tr>
                    <td class="no-border-bottom no-border-top" colspan="4"><br></td>
                    <td class="no-border-bottom no-border-top" colspan="3"><br></td>
                </tr>
                <tr>
                    <td class="no-border-top" colspan="4">
                        <p>............................................</p>
                        <p>(Tandatangan Pegawai Stor)</p>
                    </td>
                    <td class="no-border-top" colspan="3">
                        <p>............................................</p>
                        <p>(Tandatangan Pemohon / Wakil)</p>
                    </td>
                </tr>
                <tr>
                    <td class="no-border-right" colspan="2">NAMA</td>
                    <td class="no-border-left" style="font-size:12;" colspan="2">: <?= mb_strimwidth($items[0]['order']['approvedBy']['mpspProfile']['nama'],0,40,"...") ?></td>
                    <td class="no-border-right">NAMA</td>
                    <td class="no-border-left" style="font-size:12;" colspan="2">: <?= $items[0]['order']['ordered_by'] ?></td>
                </tr>
                <tr>
                    <td class="no-border-right" colspan="2">JAWATAN</td>
                    <td class="no-border-left" style="font-size:12;" colspan="2">: <?= mb_strimwidth($items[0]['order']['approvedBy']['mpspProfile']['jawatan'],0,40,"...") ?></td>
                    <td class="no-border-right">JAWATAN</td>
                    <td class="no-border-left" colspan="2">:</td>
                </tr>
                <tr>
                    <td class="no-border-right" colspan="2">TARIKH</td>
                    <td class="no-border-left" colspan="2">: <?= $items[0]['order']['approved_at'] ?></td>
                    <td class="no-border-right">TARIKH</td>
                    <td class="no-border-left" colspan="2">: <?= $items[0]['order']['order_date'] ?></td>
                </tr>
            </tbody>>
        </table>

    </div>
</div>
