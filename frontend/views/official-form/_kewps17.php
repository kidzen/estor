


<div class="box-body table-responsive">
    <div>
        <!--<p><span>Pekeliling Perbendaharaan Malaysia</span><span class="pull-right">AM 6.5 Lampiran A</span></p>-->
        <p class="pull-right form-lampiran"><strong>LAMPIRAN D</strong><br><strong>KEW.PS-17</strong></p>
        <!--<div class="clearfix"></div>-->
        <p class="text-center form-name"><strong>PENYATA PELARASAN STOK</strong></p>
        <p><strong>Kementerian/Jabatan: <br>Kategori Stor:</strong></p>
        <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
        <!--<table bordered>-->
            <thead>
                <tr>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">Bil</th>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">Perihal Stok</th>
                    <th class="col-2 vertical-align-top text-center text-center info" rowspan="2">No Kad Kawalan Stok</th>
                    <th class="col-2 vertical-align-top text-center text-center info" rowspan="2">Tarikh Penemuan (Pengiraan/Pemeriksaan/Verifikasi)</th>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">Harga Seunit (RM)</th>
                    <th class="col-2 vertical-align-top text-center text-center info" colspan="2">Kekurangan</th>
                    <th class="col-2 vertical-align-top text-center text-center info" colspan="2">Lebihan</th>
                    <th class="col-2 vertical-align-top text-center text-center info" rowspan="2">Justifikasi Siap Pengiraan</th>
                </tr>
                <tr>
                    <th class="col-1 vertical-align-top text-center text-center info">Kuantiti</th>
                    <th class="col-2 vertical-align-top text-center text-center info">Nilai (RM)</th>
                    <th class="col-1 vertical-align-top text-center text-center info">Kuantiti</th>
                    <th class="col-2 vertical-align-top text-center text-center info">Nilai (RM)</th>
                </tr>
            </thead>
            <tbody>
                <?php if (sizeof($items) >= 1) { ?>
                    <?php foreach ($items as $i => $item) { ?>
                        <tr>
                            <td class="center"><?= $i + 1 ?></td>
                            <td class="pull-left"><?= $item->description ?></td>
                            <td class="center"><?= $item->card_no ?></td>
                            <td class="center"> </td>
                            <td class="center"> </td>
                            <td class="center"> </td>
                            <td class="center"> </td>
                            <td class="center"> </td>
                            <td class="center"> </td>
                            <td class="center">&nbsp;</td>
                        </tr>
                        <?php
                        if ($i + 1 >= 10) {
                            break;
                        }
                        ?>
                    <?php } ?>
                    <?php
                    for ($j = sizeof($items); $j < 5; $j++) {
                        ?>
                        <tr>
                            <td class="center"> <?= $j + 1 ?></td>
                            <td class="center"> </td>
                            <td class="center"> </td>
                            <td class="center"> </td>
                            <td class="center"> </td>
                            <td class="center"> </td>
                            <td class="center"> </td>
                            <td class="center"> </td>
                            <td class="center"> </td>
                            <td class="center">&nbsp;</td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <?php for ($j = 0; $j < 5; $j++) { ?>
                        <tr>
                            <td class="center"> <?= $j + 1 ?></td>
                            <td class="center"> </td>
                            <td class="center"> </td>
                            <td class="center"> </td>
                            <td class="center"> </td>
                            <td class="center"> </td>
                            <td class="center"> </td>
                            <td class="center"> </td>
                            <td class="center"> </td>
                            <td class="center">&nbsp;</td>
                        </tr>
                    <?php } ?>
                <?php } ?>
            </tbody>

        </table><br>
        <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
        <!--<table bordered>-->
            <tbody>

                <tr>
                    <td class="" colspan="5">
                        <strong>Disediakan oleh: </strong><br><br><br>
                        ...............................................................<br>
                        (Tandatangan Pegawai Stor)<br>
                        <strong>Nama: </strong><br>
                        <strong>Jawatan: </strong><br>
                        <strong>Jabatan: </strong><br>
                        <strong>Tarikh:	</strong><br>

                    </td>
                    <td class="" colspan="5">
                        <strong>(DILULUSKAN/ TIDAK DILULUSKAN)*</strong><br><br><br>
                        ...............................................................<br>
                        (Tandatangan Ketua Jabatan)<br>
                        <strong>Nama: </strong><br>
                        <strong>Jawatan: </strong><br>
                        <strong>Jabatan: </strong><br>
                        <strong>Tarikh:	</strong><br>

                    </td>
                </tr>
            </tbody>

        </table>
    </div>
</div>
