
            <tbody>
                <?php $total = 0; ?>
                <?php if (sizeof($items) >= 1) { ?>
                    <?php foreach ($items as $item) { ?>
                        <?php $total += $item['quantity']; ?>
                        <tr>
                            <td class="text-center"><?= $item['code_no'] ?></td>
                            <td class=""><?= mb_strimwidth($item['description'],0,30,"...",'UTF-8') ?></td>
                            <td class="text-center border-right-bold"></td>
                            <td class="text-center"></td>
                            <td class="text-center"><?= $item['quantity'] ?></td>
                            <td class="text-center"></td>
                            <td class="text-center border-right-bold"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                        </tr>
                    <?php } ?>
                    <?php for ($j = sizeof($items); $j < 1; $j++) { ?>
                        <tr>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center border-right-bold"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center border-right-bold"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center">&nbsp;</td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <?php for ($j = 0; $j < 3-sizeof($items); $j++) { ?>
                        <tr>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center border-right-bold"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center border-right-bold"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center"> </td>
                            <td class="text-center">&nbsp;</td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td class="border-right-bold" colspan="3">JUMLAH</td>
                        <td class="text-center"> </td>
                        <td class="text-center"><?= $total ?></td>
                        <td class="text-center"> </td>
                        <td class="text-center border-right-bold"> </td>
                        <td class="text-center"> </td>
                        <td class="text-center"> </td>
                        <td class="text-center">&nbsp;</td>
                        <td class="text-center"> </td>
                        <td class="text-center"> </td>
                        <td class="text-center" colspan="2">&nbsp;</td>
                    </tr>
                <?php } ?>
                    <tr>
                        <td class="border-right-bold" colspan="3">JUMLAH</td>
                        <td class="text-center"> </td>
                        <td class="text-center"><?= $total ?></td>
                        <td class="text-center"> </td>
                        <td class="text-center border-right-bold"> </td>
                        <td class="text-center"> </td>
                        <td class="text-center"> </td>
                        <td class="text-center">&nbsp;</td>
                        <td class="text-center"> </td>
                        <td class="text-center"> </td>
                        <td class="text-center" colspan="2">&nbsp;</td>
                    </tr>
            </tbody>
