


            <tbody>
                <?php if (sizeof($items) >= 1) { ?>
                    <?php foreach ($items as $i => $item) { ?>
                        <tr>
                            <td class="text-center"><?= $i +1 ?></td>
                            <td class="text-center"><?= $item['card_no'] ?></td>
                            <td class="pull-left"><?= $item['description'] ?></td>
                            <td class="text-center"></td>
                            <td class="text-center"><?= $item['quantity'] ?></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                        </tr>
                    <?php } ?>
                    <?php for ($j = sizeof($items); $j < $batchSize; $j++) { ?>
                        <tr>
                            <td class="text-center"><?= $j +1 ?></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <?php for ($j = 0; $j < $batchSize; $j++) { ?>
                        <tr>
                            <td class="text-center"><?= $j +1 ?></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                        </tr>
                    <?php } ?>
                        <tr>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                        </tr>
                <?php } ?>
            </tbody>
