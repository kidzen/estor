


<div class="box-body table-responsive">
    <div>
        <!--<p><span>Pekeliling Perbendaharaan Malaysia</span><span class="pull-right">AM 6.5 Lampiran A</span></p>-->
        <p class="pull-right form-lampiran"><strong>LAMPIRAN B</strong><br><strong>KEW.PS-11</strong></p>
        <p class="pull-right form-lampiran"><strong>No. Permohonan : <?= $model->order_no ?></strong></p>
        <!--<div class="clearfix"></div>-->
        <p class="text-center form-name"><strong>BORANG PEMESANAN STOK</strong></p>
        <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
        <!--<table bordered>-->
            <thead>
                <tr>
                    <th class="col-1 vertical-align-top text-center text-center default" rowspan="2">Bil</th>
                    <th class="col-2 vertical-align-top text-center text-center default" colspan="3">Pemohonan</th>
                    <th class="col-2 vertical-align-top text-center text-center default" colspan="2">Pegawai Pelulus</th>
                    <th class="no-border-right text-center vertical-align-top text-center text-center default" rowspan="2">Catatan</th>
                </tr>
                <tr>
                    <th class="col-1 vertical-align-top text-center text-center default" colspan="2">Perihal Stok</th>
                    <th class="col-2 vertical-align-top text-center text-center default" >Kuantiti Dipesan</th>
                    <th class="col-3 vertical-align-top text-center text-center default" >Kuantiti Diluluskan</th>
                    <th class="col-4 vertical-align-top text-center text-center default" >Baki Kuantiti Dipesan</th>
                </tr>
            </thead>
            <tbody>
                <?php if (sizeof($items) >= 1) { ?>
                    <?php foreach ($items as $i => $item) { ?>
                        <tr>
                            <td class="center"><?= $i + 1 ?></td>
                            <td class="" colspan="2"><?= $item->inventory->description ?></td>
                            <td class="center" ><?= $item->rq_quantity ?></td>
                            <td class="center" ><?= $item->app_quantity ?></td>
                            <td class="center" ><?= $item->current_balance ?></td>
                            <td class=""></td>
                        </tr>
                    <?php } ?>
                    <?php for ($j = sizeof($items); $j < 5; $j++) { ?>
                        <tr>
                            <td class="center"> <?= $j + 1 ?></td>
                            <td class="col-2 pull-left" colspan="2"> </td>
                            <td class="col-2 pull-left" > </td>
                            <td class="col-2 pull-left" > </td>
                            <td class="col-2 pull-left" > </td>
                            <td class="col-10 pull-left">&nbsp;</td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <?php for ($j = 0; $j < 5; $j++) { ?>
                        <tr>
                            <td class="center"> <?= $j + 1 ?></td>
                            <td class="col-2 pull-left" colspan="2"> </td>
                            <td class="col-2 pull-left" > </td>
                            <td class="col-2 pull-left" > </td>
                            <td class="col-2 pull-left" > </td>
                            <td class="col-10 pull-left">&nbsp;</td>
                        </tr>
                    <?php } ?>
                <?php } ?>
            <tbody>
            <tfoot>
                <tr>
                    <td class="no-border-bottom" colspan="4">
                        <br><br>
                        <br><br>
                    </td>
                    <td class="no-border-bottom" colspan="3">
                        <br><br>
                        <br><br>
                    </td>
                </tr>
                <tr>
                    <td class="no-border-top" colspan="4">
                        <p>......................</p>
                        <p>(Tandatangan Pemohon)</p>
                    </td>
                    <td class="no-border-top" colspan="3">
                        <p>......................</p>
                        <p>(Tandatangan Pemohon)</p>
                    </td>
                </tr>
                <tr>
                    <td class="no-border-right" colspan="2">NAMA</td>
                    <td class="no-border-left" colspan="2">: <?= $model->ordered_by ?></td>
                    <td class="no-border-right">NAMA</td>
                    <td class="no-border-left" colspan="2">: <?= $model->approvedBy->name ?></td>
                </tr>
                <tr>
                    <td class="no-border-right" colspan="2">JAWATAN</td>
                    <td class="no-border-left" colspan="2">:</td>
                    <td class="no-border-right">JAWATAN</td>
                    <td class="no-border-left" colspan="2">: <?= $model->approvedBy->mpspProfile->jawatan ?></td>
                </tr>
                <tr>
                    <td class="no-border-right" colspan="2">TARIKH</td>
                    <td class="no-border-left" colspan="2">: <?= $model->order_date ?></td>
                    <td class="no-border-right">TARIKH</td>
                    <td class="no-border-left" colspan="2">: <?= $model->approved_at ?></td>
                </tr>
            </tfoot>
        </table><br>
        <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
            <tbody>
                <tr>
                    <td class="no-border-bottom" colspan="4">
                        <br><br>
                        <br><br>
                    </td>
                    <td class="no-border-bottom" colspan="3">
                        <br><br>
                        <br><br>
                    </td>
                </tr>
                <tr>
                    <td class="no-border-top" colspan="4">
                        <p>......................</p>
                        <p>(Tandatangan Pemohon)</p>
                    </td>
                    <td class="no-border-top" colspan="3">
                        <p>......................</p>
                        <p>(Tandatangan Pemohon)</p>
                    </td>
                </tr>
                <tr>
                    <td class="no-border-right" colspan="2">NAMA</td>
                    <td class="no-border-left" colspan="2">: <?= $model->ordered_by ?></td>
                    <td class="no-border-right">NAMA</td>
                    <td class="no-border-left" colspan="2">: <?= $model->approvedBy->name ?></td>
                </tr>
                <tr>
                    <td class="no-border-right" colspan="2">JAWATAN</td>
                    <td class="no-border-left" colspan="2">:</td>
                    <td class="no-border-right">JAWATAN</td>
                    <td class="no-border-left" colspan="2">: <?= $model->approvedBy->mpspProfile->jawatan ?></td>
                </tr>
                <tr>
                    <td class="no-border-right" colspan="2">TARIKH</td>
                    <td class="no-border-left" colspan="2">: <?= $model->order_date ?></td>
                    <td class="no-border-right">TARIKH</td>
                    <td class="no-border-left" colspan="2">: <?= $model->approved_at ?></td>
                </tr>
            </tbody>>
        </table>

    </div>
</div>
