


<div class="box-body table-responsive">
    <div>
        <!--<p><span>Pekeliling Perbendaharaan Malaysia</span><span class="pull-right">AM 6.5 Lampiran A</span></p>-->
        <p class="pull-right form-lampiran"><strong>LAMPIRAN D</strong><br><strong>KEW.PS-17</strong></p>
        <!--<div class="clearfix"></div>-->
        <p class="text-center form-name"><strong>PENYATA PELARASAN STOK</strong></p>
        <p><strong>Kementerian/Jabatan: <br>Kategori Stor:</strong></p>
        <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
        <!--<table bordered>-->
            <thead>
                <tr>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">Bil</th>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">Perihal Stok</th>
                    <th class="col-2 vertical-align-top text-center text-center info" rowspan="2">No Kad Kawalan Stok</th>
                    <th class="col-2 vertical-align-top text-center text-center info" rowspan="2">Tarikh Penemuan (Pengiraan/Pemeriksaan/Verifikasi)</th>
                    <th class="col-1 vertical-align-top text-center text-center info" rowspan="2">Harga Seunit (RM)</th>
                    <th class="col-2 vertical-align-top text-center text-center info" colspan="2">Kekurangan</th>
                    <th class="col-2 vertical-align-top text-center text-center info" colspan="2">Lebihan</th>
                    <th class="col-2 vertical-align-top text-center text-center info" rowspan="2">Justifikasi Siap Pengiraan</th>
                </tr>
                <tr>
                    <th class="col-1 vertical-align-top text-center text-center info">Kuantiti</th>
                    <th class="col-2 vertical-align-top text-center text-center info">Nilai (RM)</th>
                    <th class="col-1 vertical-align-top text-center text-center info">Kuantiti</th>
                    <th class="col-2 vertical-align-top text-center text-center info">Nilai (RM)</th>
                </tr>
            </thead>
