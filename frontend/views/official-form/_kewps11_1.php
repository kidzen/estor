


<div class="box-body table-responsive">
    <div>
        <!--<p><span>Pekeliling Perbendaharaan Malaysia</span><span class="pull-right">AM 6.5 Lampiran A</span></p>-->
        <p class="pull-right form-lampiran"><strong>LAMPIRAN B</strong><br><strong>KEW.PS-11</strong></p>
        <p class="pull-right form-lampiran"><strong>No. Permohonan : <?= $items[0]['order']['order_no'] ?></strong></p>
        <!--<div class="clearfix"></div>-->
        <p class="text-center form-name"><strong>BORANG PEMESANAN STOK</strong></p>
        <table class="kv-grid-table table table-hover table-bordered table-condensed kv-table-wrap">
        <!--<table bordered>-->
            <thead>
                <tr>
                    <th class="col-1 vertical-align-top text-center text-center default" rowspan="2">Bil</th>
                    <th class="col-2 vertical-align-top text-center text-center default" colspan="3">Pemohonan</th>
                    <th class="col-2 vertical-align-top text-center text-center default" colspan="2">Pegawai Pelulus</th>
                    <th class="no-border-right text-center vertical-align-top text-center text-center default" rowspan="2">Catatan</th>
                </tr>
                <tr>
                    <th class="col-1 vertical-align-top text-center text-center default" colspan="2">Perihal Stok</th>
                    <th class="col-2 vertical-align-top text-center text-center default" >Kuantiti Dipesan</th>
                    <th class="col-3 vertical-align-top text-center text-center default" >Kuantiti Diluluskan</th>
                    <th class="col-4 vertical-align-top text-center text-center default" >Baki Kuantiti Dipesan</th>
                </tr>
            </thead>
