<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ApprovalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="approval-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_inventory_items') ?>

    <?= $form->field($model, 'id_inventories') ?>

    <?= $form->field($model, 'id_categories') ?>

    <?= $form->field($model, 'id_inventories_checkin') ?>

    <?php // echo $form->field($model, 'id_order_items') ?>

    <?php // echo $form->field($model, 'id_orders') ?>

    <?php // echo $form->field($model, 'sku') ?>

    <?php // echo $form->field($model, 'oder') ?>

    <?php // echo $form->field($model, 'order_no') ?>

    <?php // echo $form->field($model, 'items_category_name') ?>

    <?php // echo $form->field($model, 'items_inventory_name') ?>

    <?php // echo $form->field($model, 'rq_quantity') ?>

    <?php // echo $form->field($model, 'app_quantity') ?>

    <?php // echo $form->field($model, 'approved') ?>

    <?php // echo $form->field($model, 'current_balance') ?>

    <?php // echo $form->field($model, 'total_price') ?>

    <?php // echo $form->field($model, 'order_date') ?>

    <?php // echo $form->field($model, 'required_date') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
