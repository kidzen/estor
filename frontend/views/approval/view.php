<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Approval */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Approvals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info approval-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>approval : <?=  Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">
    <p>
        <?= Html::a(Yii::t('app', 'Kembali'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('app', 'Kemaskini'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
        'method' => 'post',
        ],
        ]) ?>
    </p>
    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                'id',
            'id_inventory_items',
            'id_inventories',
            'id_categories',
            'id_inventories_checkin',
            'id_order_items',
            'id_orders',
            'sku',
            'oder',
            'order_no',
            'items_category_name',
            'items_inventory_name',
            'rq_quantity',
            'app_quantity',
            'approved',
            'current_balance',
            'total_price',
            'order_date',
            'required_date',
    ],
    ]) ?>

</div>
