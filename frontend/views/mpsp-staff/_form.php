<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MpspStaff */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mpsp-staff-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel-body">
        <div class="row">
                        <div class="col-md-4">
                <?= $form->field($model, 'no_pekerja')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'jawatan')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'keterangan')->textInput(['maxlength' => true]) ?>
            </div>

        </div>
        <div>
            <?= Html::a('Cancel' , Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
