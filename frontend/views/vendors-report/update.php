<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VendorsReport */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Vendors Report',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vendors Reports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->name]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Kemaskini');
?>
<div class="vendors-report-update">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
        </div>

        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>

    </div>
</div>
