<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\VendorsReport */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vendors Reports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info vendors-report-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>vendors-report : <?=  Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">
    <p>
        <?= Html::a(Yii::t('app', 'Kembali'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('app', 'Kemaskini'), ['update', 'id' => $model->name], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->name], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
        'method' => 'post',
        ],
        ]) ?>
    </p>
    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                'name',
            'total',
    ],
    ]) ?>

</div>
