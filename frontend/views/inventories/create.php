<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Inventories */

$this->title = Yii::t('app', 'Create Inventories');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inventories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inventories-create">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
        </div>

        <?=
        $this->render('_form', [
            'model' => $model,
            'categoryArray' => $categoryArray,
        ])
        ?>

    </div>
</div>
