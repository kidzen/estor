<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\EstorInventories */

//$this->title = $inventory->id;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Estor Inventories'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
//var_dump($inventory);
//var_dump($listItem[0]);die();
//die();
?>


<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <!--<h3 class="box-title">Kad Petak</h3>-->

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <!--                    <div class="btn-group">
                                            <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-wrench"></i></button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>-->
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!--/.box-header-->
            <div class="box-body table-responsive">
                <div>
                    <?php ?>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td colspan="2" >Rujuk Kawalan Stok :</th>
                                <td colspan="5" ><?= $inventory->card_no ?></th>
                                <td colspan="2" >Nombor Kod:</th>
                                <td colspan="4" ><?= $inventory->code_no ?></th>
                            </tr>
                            <tr>
                                <td colspan="2">Perihal Stok:</th>
                                <td colspan="11"><?= $inventory->description ?></th>
                            </tr>
                            <tr>
                                <td colspan="2">Kumpulan Stok:</th>
                                <td colspan="5" ><?= $inventory->category->name ?></th>
                                <td colspan="2" >Lokasi Stok:</th>
                                <td colspan="4"><?= $inventory->location ?></th>
                            </tr>
                        </tbody>
                    </table>
                    <!--/.row-->
                </div>
            </div>
            <div class="box-body table-responsive">
                <div>
                    <table class="kv-grid-table table table-hover table-bordered table-striped table-condensed kv-table-wrap">
                    <!--<table class="table table-bordered table-hover">-->
                        <thead>
                            <!--<tr class="kartik-sheet-style">-->
                            <tr>
                                <th rowspan="2" style="text-align: center;vertical-align: middle">Bil</th>
                                <th rowspan="2" style="text-align: center;vertical-align: middle">Tarikh</th>
                                <th rowspan="2" style="text-align: center;vertical-align: middle">Keterangan</th>
                                <th rowspan="2" style="text-align: center;vertical-align: middle">No Rujukan</th>
                                <th colspan="4" style="text-align: center;vertical-align: middle">Kuantiti</span></th>
                                <th rowspan="2" style="text-align: center;vertical-align: middle;vertical-align: middle">Nama Pegawai Stor</th>
                            </tr>
                            <tr>
                                <th style="text-align: center;vertical-align: middle">Terima</th>
                                <th style="text-align: center;vertical-align: middle">Seunit (RM)</th>
                                <th style="text-align: center;vertical-align: middle">Keluar</th>
                                <th style="text-align: center;vertical-align: middle">Baki</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="text-align: center;vertical-align: middle">/</td>
                                <td></td>
                                <td></td>
                                <td colspan="4" style="text-align: right;vertical-align: middle">Baki dibawa kehadapan</td>
                                <!--<td style="text-align: right;vertical-align: middle">baki kad lepas</td>-->
                                <td style="text-align: center;vertical-align: middle"><?= $transactions[0]->count_current - $transactions[0]->count_in ?></td>
                            </tr>
                            <?php
                            foreach ($transactions as $i => $transaction) {
                                ?>
                                <tr>
                                    <td style="text-align: center;vertical-align: middle"><?= $i + 1 ?></td>
                                    <td style="text-align: center;vertical-align: middle"><?= $transaction->check_date; ?></td>
                                    <td style="text-align: center;vertical-align: middle"><?php
                                        if ($transaction->type == 1) {
                                            echo 'in';
                                        } else {
                                            if (isset($transaction->order->vehicle)) {
                                                echo 'OUT for : ' . $transaction->order->vehicle->reg_no;
                                            }
                                            echo 'out';
                                        }
                                        ?></td>
                                    <td style="text-align: center;vertical-align: middle"><?= $transaction->refference; ?></td>
                                    <td style="text-align: center;vertical-align: middle"><?= $transaction->count_in; ?></td>
                                    <td style="text-align: center;vertical-align: middle"><?php
                                        if ($transaction->type == 1) {
                                            echo $transaction->price_in / $transaction->count_in;
                                            ;
                                        } else {
                                            echo $transaction->price_out / $transaction->count_out;
                                            ;
                                        }
                                        ?></td>
                                    <td style="text-align: center;vertical-align: middle"><?= $transaction->count_out; ?></td>
                                    <td style="text-align: center;vertical-align: middle"><?= $transaction->count_current; ?></td>
                                    <!-- <td style="text-align: center;vertical-align: middle"><?= $transaction->checkBy->mpspProfile->nama; ?></td>  -->
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!--/.row-->
                </div>
            </div>
            <!--./box-body-->
            <div class="box-footer">
                <!--footer-->
                <!--/.row-->
            </div>
            <!--/.box-footer-->
        </div>
        <!--/.box-->
    </div>
    <!--/.col-->
</div>

