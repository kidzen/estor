<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\EstorInventories */

//$this->title = $inventory->id;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Estor Inventories'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;

//var_dump($inventory);
//var_dump($listItem[0]);die();
//die();
?>


<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Kad Petak</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <!--                    <div class="btn-group">
                                            <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-wrench"></i></button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>-->
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!--/.box-header-->
            <div class="box-body table-responsive">
                <div>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td colspan="2" style="width:25%">Rujuk Kawalan Stok :</th>
                                <td colspan="4" style="width:40%"><?= $inventory->card_no ?></th>
                                <td colspan="2" style="width:18%">Nombor Kod:</th>
                                <td colspan="4" style="width:auto"><?= $inventory->code_no ?></th>
                            </tr>
                            <tr>
                                <td colspan="2">Perihal Stok:</th>
                                <td colspan="10"><?= $inventory->description ?></th>
                            </tr>
                            <tr>
                                <td colspan="2">Kumpulan Stok:</th>
                                <td colspan="1" style="width:13%"><?= $inventory->category->name ?></th>
                                <td colspan="2" style="width:15%">Lokasi Stok:</th>
                                <td colspan="8"><?= $inventory->location ?></th>
                            </tr>
                        </tbody>
                    </table>
                    <!--/.row-->
                </div>
            </div>
            <div class="box-body table-responsive">
                <div>
                    <table class="kv-grid-table table table-hover table-bordered table-striped table-condensed kv-table-wrap">
                    <!--<table class="table table-bordered table-hover">-->
                        <thead>
                            <!--<tr class="kartik-sheet-style">-->
                            <tr>
                                <th rowspan="2">Bil</th>
                                <th rowspan="2">Tarikh</th>
                                <th rowspan="2">Keterangan</th>
                                <th rowspan="2">No Rujukan</th>
                                <th colspan="4" style="text-align: center">Kuantiti</span></th>
                                <th rowspan="2">Nama Pegawai Stor</th>
                            </tr>
                            <tr>
                                <th>Terima</th>
                                <th>Seunit (RM)</th>
                                <th>Keluar</th>
                                <th>Baki</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>/</td>
                                <td></td>
                                <td></td>
                                <td colspan="3" style="text-align: right">Baki</td>
                                <td style="text-align: right">baki kad lepas</td>
                                <td></td>
                            </tr>
                            <?php
                            foreach ($transactions as $i=>$transaction) {
                                ?>
                                <tr>
                                    <td class="kv-align-center"><?= $i+1 ?></td>
                                    <td><?= $transaction['check_date']; ?></td>
                                    <td><?= $transaction['type']; ?></td>
                                    <td><?= $transaction['id']; ?></td>
                                    <td><?= $transaction['inventoriesCheckIn']['id']; ?></td>
                                    <td><?= $transaction['inventory']['quantity']; ?></td>
                                    <td><?= $transaction['orderItems']['app_quantity']; ?></td>
                                    <td><?= $transaction['id']; ?></td>
                                    <td><?= $transaction['id']; ?></td>
                                    <td><?= $transaction['id']; ?></td>
                                    <td><?= $transaction['id']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!--/.row-->
                </div>
            </div>
            <!--./box-body-->
            <div class="box-footer">
                <!--footer-->
                <!--/.row-->
            </div>
            <!--/.box-footer-->
        </div>
        <!--/.box-->
    </div>
    <!--/.col-->
</div>

