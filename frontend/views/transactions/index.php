<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TransactionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Transactions');
$this->params['breadcrumbs'][] = $this->title;

$title = empty($this->title) ? Yii::t('app', 'Report') : $this->title . ' Report';
$pdfHeader = [
    'l' => [
        'content' => Yii::t('app', 'Sistem e-Store mpsp'),
        'font-size' => 8,
        'font-weight' => '600',
        'color' => '#333333'
    ],
    'c' => [
        'content' => $title,
        'font-size' => 16,
//        'font-weight' => 'bold',
        'color' => '#333333'
    ],
    'r' => [
        'content' => Yii::t('app', 'Generated') . ': ' . date("D, d-M-Y g:i a T"),
        'font-size' => 8,
        'color' => '#333333'
    ]
];
$pdfFooter = [
    'l' => [
        'content' => Yii::t('app', "Copyright © Majlis Perbandaran Seberang Perai"),
        'font-size' => 8,
        'font-style' => 'b',
        'color' => '#999999'
    ],
    'r' => [
        'content' => '[ {PAGENO} ]',
        'font-size' => 10,
        'font-style' => 'b',
        'font-family' => 'serif',
        'color' => '#333333'
    ],
    'line' => true,
];
$pdf = [
    'label' => Yii::t('app', 'pdf'),
    'icon' => 'file-pdf-o',
    'iconOptions' => ['class' => 'text-danger'],
    'showHeader' => true,
    'showPageSummary' => true,
    'showFooter' => true,
    'showCaption' => true,
    'filename' => Yii::t('app', 'grid-export'),
    'alertMsg' => Yii::t('app', 'The PDF export file will be generated for download.'),
    'options' => ['title' => Yii::t('app', 'Portable Document Format')],
    'mime' => 'application/pdf',
    'config' => [
        'mode' => 'c',
        'format' => 'A4-l',
        'destination' => 'd',
        'marginTop' => 20,
        'marginBottom' => 20,
        'cssInline' => '.kv-wrap{padding:20px;}' .
        '.kv-align-center{text-align:center;}' .
        '.kv-align-left{text-align:left;}' .
        '.kv-align-right{text-align:right;}' .
        '.kv-align-top{vertical-align:top!important;}' .
        '.kv-align-bottom{vertical-align:bottom!important;}' .
        '.kv-align-middle{vertical-align:middle!important;}' .
        '.kv-page-summary{border-top:4px double #ddd;font-weight: bold;}' .
        '.kv-table-footer{border-top:4px double #ddd;font-weight: bold;}' .
        '.kv-table-caption{font-size:1.5em;padding:8px;border:1px solid #ddd;border-bottom:none;}',
        'methods' => [
            'SetHeader' => [
                ['odd' => $pdfHeader, 'even' => $pdfHeader]
            ],
            'SetFooter' => [
                ['odd' => $pdfFooter, 'even' => $pdfFooter]
            ],
        ],
        'options' => [
            'title' => $title,
            'subject' => Yii::t('app', 'PDF export generated by kartik-v/yii2-grid extension'),
            'keywords' => Yii::t('app', 'krajee, grid, export, yii2-grid, pdf')
        ],
        'contentBefore' => '',
        'contentAfter' => ''
    ]
];
?>
<div class="transactions-index">

    <?php Pjax::begin(); ?>            <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            ['content' =>
                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success', 'title' => Yii::t('app', 'Create transactions'),])//. ' ' .
            //                Html::a(Yii::t('app', 'Create Categories'), ['create'], ['class' => 'btn btn-success']) //. ' ' .
            //                Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type' => 'button', 'title' => Yii::t('app', 'Add Book'), 'class' => 'btn btn-success', 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' ' .
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
        'exportConfig' => [
            'pdf' => $pdf,
            'csv' => '{csv}',
            'xls' => '{xls}',
        ],
        'export' => [
            'fontAwesome' => true,
            'target' => '_self',
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        //        'showPageSummary' => true,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => $this->title,
        ],
        'responsiveWrap' => false,
        'persistResize' => false,
        //        'exportConfig' => $exportConfig,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'hAlign' => 'center', 'vAlign' => 'middle',
            ],
            [
                'attribute' => 'type',
                'value' => function ($model) {
                    if ($model->type == 1) {
                        return 'in';
                    } else if ($model->type == 2) {
                        return 'out';
                    } else {
                        return 'undefined';
                    }
                },
                'hAlign' => 'center', 'vAlign' => 'middle',
            ],
            [
                'attribute' => 'details',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->type == 1) {
                        return $model['inventoriesCheckIn']['id'];
                    } else if ($model->type == 2) {
                        return $model['order']['order_no'];
                    } else {
                        return 'undefined';
                    }
                },
                'hAlign' => 'center', 'vAlign' => 'middle',
            ],
            [
                'label' => 'Total Price (RM)',
                'attribute' => 'total_price',
                'format' => ['decimal', 2],
                'value' => function ($model) {
            if ($model->type == 1) {
                return $model['inventoriesCheckIn']['items_total_price'];
            } else if ($model->type == 2) {
                foreach ($model['orderItems'] as $i => $order) {
                    return isset($order['unit_price'])?$order['unit_price']:0;
                }
            } else {
                return 'undefined';
            }
        },
                'hAlign' => 'right', 'vAlign' => 'middle',
            ],
            [
                'attribute' => 'check_date',
                'hAlign' => 'center', 'vAlign' => 'middle',
            ],
            [
                'attribute' => 'checkBy.username',
                'hAlign' => 'center', 'vAlign' => 'middle',
            ],
            [
                'attribute' => 'created_at',
                'hAlign' => 'center', 'vAlign' => 'middle',
            ],
            /* [
              'attribute'=>'updated_at',
              'hAlign' => 'center', 'vAlign' => 'middle',
              ], */
            /* [
              'attribute'=>'created_by',
              'hAlign' => 'center', 'vAlign' => 'middle',
              ], */
            /* [
              'attribute'=>'updated_by',
              'hAlign' => 'center', 'vAlign' => 'middle',
              ], */
            /* [
              'attribute'=>'deleted',
              'hAlign' => 'center', 'vAlign' => 'middle',
              ], */
            /* [
              'attribute'=>'deleted_at',
              'hAlign' => 'center', 'vAlign' => 'middle',
              ], */
//            [
//                'class' => 'kartik\grid\ActionColumn',
//                'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isPegawaiStor,
//                'template' => '{view}{update}{delete}{recover}',
//                'viewOptions' => ['title' => 'View', 'data-toggle' => 'tooltip'],
//                'updateOptions' => ['title' => 'Update', 'data-toggle' => 'tooltip'],
//                'buttons' => [
//                    'recover' => function ($url, $model) {
//                        if ($model->deleted === 1) {
//                            return Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, ['title' => Yii::t('yii', 'Recover'), 'data-toggle' => 'tooltip','data-method' => 'post']);
//                        }
//                    },
//                            'delete' => function ($url, $model) {
//                        if ($model->deleted === 0) {
//                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'), 'data-toggle' => 'tooltip',
//                                        'data-method' => 'post']);
//                        }
//                    },
//                        ],
//                    ],
                    [
                        'header' => 'Permanent Delete',
                        'class' => 'kartik\grid\ActionColumn',
                        'template' => '{delete-permanent}',
                        'buttons' => [
                            'delete-permanent' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-trash" style="color:red;"></span>', $url, [
                                            'title' => Yii::t('yii', 'Permanent Delete'), 'data-toggle' => 'tooltip','data-method' => 'post'
                                ]);
                            }
                                ],
                                'visible' => Yii::$app->user->isAdmin,
                            ],
                        ],
                    ]);
                    ?>
                    <?php Pjax::end(); ?></div>
