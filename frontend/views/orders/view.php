<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info orders-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>orders : <?=  Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">
    <p>
        <?= Html::a(Yii::t('app', 'Kembali'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('app', 'Kemaskini'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
        'method' => 'post',
        ],
        ]) ?>
    </p>
    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                'id',
            'transaction_id',
            'order_date',
            'ordered_by',
            'order_no',
            'approved',
            'approved_by',
            'approved_at',
            'vehicle_id',
            'required_date',
            'checkout_date',
            'checkout_by',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
            [
            'attribute'=>'deleted',
            'vAlign' => 'middle',
            'value' => $model->deleted == 1 ? 'Deleted' : 'Active',
            ],
            'deleted_at',
    ],
    ]) ?>

</div>
