<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->items,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        ['attribute' => 'inventory.card_no', 'vAlign' => 'middle','hAlign'=>'center'],
        ['attribute' => 'inventory.code_no', 'vAlign' => 'middle','hAlign'=>'center'],
        'inventory.description',
        ['attribute' => 'rq_quantity', 'vAlign' => 'middle','hAlign'=>'right'],
        ['attribute' => 'app_quantity', 'vAlign' => 'middle','hAlign'=>'right'],
        ['label'=>'Kuantiti Sedia Ada','attribute' => 'inventory.quantity', 'vAlign' => 'middle','hAlign'=>'center'],
        // [
        //         'attribute' => 'department.name',
        //         'label' => 'Department'
        //     ],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'order-items'
        ],
    ];

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'responsiveWrap' => false,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
