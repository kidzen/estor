<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info orders-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i>orders : <?= Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a(Yii::t('app', 'Kembali'), ['index'], ['class' => 'btn btn-default']) ?>
            <?= Html::a(Yii::t('app', 'Kemaskini'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </p>
        <div class="order-items-index">

            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                'pjax' => true, // pjax is set to always true for this demo
                // set your toolbar
                'toolbar' => [
                    ['content' =>
                        Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success', 'title' => Yii::t('app', 'Create order-items'),])//. ' ' .
                    //                Html::a(Yii::t('app', 'Create Categories'), ['create'], ['class' => 'btn btn-success']) //. ' ' .
                    //                Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type' => 'button', 'title' => Yii::t('app', 'Add Book'), 'class' => 'btn btn-success', 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' ' .
                    ],
                    '{export}',
                    '{toggleData}',
                ],
                // set export properties
//                'exportConfig' => [
//                    'pdf' => $pdf,
//                    'csv' => '{csv}',
//                    'xls' => '{xls}',
//                ],
                'export' => [
                    'fontAwesome' => true,
                    'target' => '_self',
                ],
                // parameters from the demo form
                'bordered' => true,
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'hover' => true,
                //        'showPageSummary' => true,
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => $this->title,
                ],
                'responsiveWrap' => false,
                'persistResize' => false,
                //        'exportConfig' => $exportConfig,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'id',
                        'hAlign' => 'center', 'vAlign' => 'middle',
                    ],
                    [

                        'attribute' => 'order.order_no',
                        'hAlign' => 'center', 'vAlign' => 'middle',
                        'group' => true
                    ],
                    [

                        'attribute' => 'order.arahan_kerja_id',
                        'hAlign' => 'center', 'vAlign' => 'middle',
                        'group' => true, 'subGroupOf' => 2,
                    ],
                    [
                        'label' => 'Inventory Details',
                        'attribute' => 'inventory_details',
                        'format' => 'raw',
                        'value' => function($model) {
                            $card = 'Card No : ' . $model->inventory->card_no;
                            $code = 'Code No : ' . $model->inventory->code_no;
                            $description = 'Description : <br>' . $model->inventory->description;
                            return Html::tag('span', $card . '<br>' . $code . '<br>' . $description);
                        },
                        'hAlign' => 'left', 'vAlign' => 'middle',
                    ],
////            [
////                'attribute' => 'current_balance',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
////            [
////                'attribute' => 'transaction.check_by',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
//            [
//                'attribute' => 'transaction.check_date',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
                    [
                        'attribute' => 'order.order_date',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::tag('span', 'Date : ' . $model->order->order_date . '<br>' . 'Order by : ' . $model->order->ordered_by);
                        },
                        'hAlign' => 'center', 'vAlign' => 'middle',
                        'group' => true, 'subGroupOf' => 2,
                    ],
                    [
                        'attribute' => 'order.required_date',
                        'hAlign' => 'center', 'vAlign' => 'middle',
                        'group' => true, 'subGroupOf' => 2,
                    ],
//            [
//                'attribute' => 'order.ordered_by',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
                    [
                        'label' => 'Inventory Quantity',
                        'attribute' => 'inventory.quantity',
                        'hAlign' => 'center', 'vAlign' => 'middle',
                    ],
//            [
//                'attribute' => 'rq_quantity',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
                    [
                        'attribute' => 'quantity',
                        'format' => 'raw',
                        'value' => function ($model) {
                            if ($model->app_quantity) {
                                return Html::tag('span', 'Requested : ' . $model->rq_quantity . '<br>' . 'Approved : ' . $model->app_quantity);
                            } else {
                                return Html::tag('span', 'Requested : ' . $model->rq_quantity . '<br>' . 'Approved : ' . 0);
                            }
                        },
                        'hAlign' => 'center', 'vAlign' => 'middle',
                    ],
//            [
//
//                'attribute' => 'items',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//                'format' => 'raw',
//                'value' => function($model) {
//                    $data = \yii\helpers\ArrayHelper::map($model->items, 'id', 'sku');
//                    return implode("<br>", $data);
//                },
//            ],
                    [
                        'label' => 'Total Price',
                        'attribute' => 'unit_price',
                        'hAlign' => 'center', 'vAlign' => 'middle',
                    ],
                    [
                        'label' => 'Usage',
                        'attribute' => 'usage_detail',
                        'format' => 'raw',
                        'value' => function($model) {
                            $type = 'ID : ' . $model['vehicle']['id'];
                            $regNo = 'Reg No : ' . $model['vehicle']['reg_no'];
                            $description = 'Model : ' . $model['vehicle']['model'];
                            return Html::tag('span', $type . '<br>' . $regNo . '<br>' . $description);
                        },
                        'hAlign' => 'left', 'vAlign' => 'middle',
                        'group' => true, 'subGroupOf' => 2,
                    ],
//            [
//                'attribute' => 'order.checkout_date',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
//            [
//                'attribute' => 'order.checkout_by',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
////            [
////                'attribute' => 'created_at',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
////            [
////                'attribute' => 'updated_at',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
////            [
////                'attribute' => 'created_by',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
////            [
////                'attribute' => 'updated_by',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
////            [
////                'attribute' => 'deleted',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
////            [
////                'attribute' => 'deleted_at',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
                    [
                        'attribute' => 'order.approved',
                        'format' => 'raw',
                        'value' => function($model) {
                            if ($model['order']['approved'] == 2) {
                                return Html::label('pending', null, ['class' => 'label label-warning']);
                            } else if ($model['order']['approved'] == 1) {
                                return Html::label('approved', null, ['class' => 'label label-success']);
                            } else if ($model['order']['approved'] == 8) {
                                return Html::label('rejected', null, ['class' => 'label label-danger']);
                            } else {
                                return Html::label('undefined', null, ['class' => 'label label-primary']);
                            }
                        },
                                'hAlign' => 'center', 'vAlign' => 'middle',
                                'group' => true, 'subGroupOf' => 2,
                            ],
////            [
////                'attribute' => 'order.approved_by',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
////            [
////                'attribute' => 'order.approved_at',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
                            [
                                'class' => 'kartik\grid\ActionColumn',
                                'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isPegawaiStor,
//                        'group' => true,
//                        'subGroupOf' => 2,
                                'template' => '{approval} {reject} {item-list} {view}{update}{delete}{recover}',
                                'viewOptions' => ['title' => 'View', 'data-toggle' => 'tooltip'],
                                'updateOptions' => ['title' => 'Update', 'data-toggle' => 'tooltip'],
//                        'group' => true, 'subGroupOf' => 2,
                                'buttons' => [
                                    'approval' => function ($url, $model) {
                                        if ($model['order']['approved'] === 2) {
                                            return Html::a('<span class="glyphicon glyphicon-check" style="color:green;"></span>', ['approval', 'ordersId' => $model->order->id], ['title' => Yii::t('yii', 'Approval'), 'data-toggle' => 'tooltip']);
                                        }
                                    },
                                            'reject' => function ($url, $model) {
                                        if ($model['order']['approved'] === 2) {
//                                    return Html::a('<span class="glyphicon glyphicon-remove" style="color:red;"></span>', ['reject', 'id' => $model->order->id], ['title' => Yii::t('yii', 'Reject'), 'data-toggle' => 'tooltip']);
                                            return Html::a('<span class="glyphicon glyphicon-remove" style="color:red;"></span>', ['reject', 'id' => $model->id], ['title' => Yii::t('yii', 'Reject'), 'data-toggle' => 'tooltip']);
                                        }
                                    },
//                                    'item-list' => function ($url, $model) {
//                                return Html::a('<span class="glyphicon glyphicon-list"></span>', $url, ['title' => Yii::t('yii', 'Item List'), 'data-toggle' => 'tooltip']);
//                            },
                                            'recover' => function ($url, $model) {
                                        if ($model->deleted === 1) {
                                            return Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, ['title' => Yii::t('yii', 'Recover'), 'data-toggle' => 'tooltip','data-method' => 'post']);
                                        }
                                    },
                                            'delete' => function ($url, $model) {
                                        if ($model->deleted === 0) {
                                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'), 'data-toggle' => 'tooltip',
                                                        'data-method' => 'post']);
                                        }
                                    },
                                        ],
                                    ],
                                    [
                                        'header' => 'Permanent Delete',
                                        'class' => 'kartik\grid\ActionColumn',
                                        'template' => '{delete-permanent}',
                                        'buttons' => [
                                            'delete-permanent' => function ($url, $model) {
                                                return Html::a('<span class="glyphicon glyphicon-trash" style="color:red;"></span>', $url, [
                                                            'title' => Yii::t('yii', 'Permanent Delete'), 'data-toggle' => 'tooltip','data-method' => 'post'
                                                ]);
                                            }
                                                ],
                                                'visible' => Yii::$app->user->isAdmin,
                                            ],
                                        ],
                                    ]);
                                    ?>
                                    <?php Pjax::end(); ?></div>
    </div>
