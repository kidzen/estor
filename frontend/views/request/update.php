<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Transactions */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
            'modelClass' => 'Transactions',
        ]) . $order->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Transactions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $order->id, 'url' => ['view', 'id' => $order->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Kemaskini');
?>
<div class="transactions-update">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4><i class="fa fa-download"> </i><?= Html::encode($this->title) ?></h4>
        </div>

        <?=
        $this->render('_form', [
            'order' => $order,
            'vehiclesArray' => $vehiclesArray,
            'inventoriesArray' => $inventoriesArray,
        ])
        ?>

    </div>
</div>
