<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Transactions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transactions-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="panel-body">
        <div class="row">
            <?php if (!$order->isNewRecord) { ?>
                <div class="col-sm-4">
                    <?= $form->field($order, 'order_no')->textInput(['maxlength' => true, 'readOnly' => true, 'value' => $order->order_no]) ?>
                </div>
            <?php } else { ?>
                <div class="col-sm-4">
                    <?= $form->field($order, 'order_no')->textInput(['maxlength' => true, 'readOnly' => true, 'value' => date('Y/m') . '-' . $bilStock]) ?>
                </div>
            <?php } ?>
            <div class="col-sm-4">
                <?php
                echo $form->field($order, 'ordered_by')->widget(kartik\select2\Select2::className(), [
                    'data' => yii\helpers\ArrayHelper::map(\common\models\People::find()->asArray()->all(), 'id', 'username'),
//                            'theme' => 'default',
                    'options' => ['placeholder' => 'Select an inventory ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]);
                ?>
            </div>
            <div class="col-sm-4">
                <?php
                echo $form->field($order, 'vehicle_id')->widget(kartik\select2\Select2::className(), [
                    'data' => $vehiclesArray,
//                            'theme' => 'default',
                    'options' => ['placeholder' => 'Select an inventory ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]);
                ?>
            </div>
            <div class="col-sm-6">
                <?=
                $form->field($order, 'order_date')->widget(kartik\widgets\DatePicker::className(), [
                    'value' => date('dd-M-yy'),
                    'options' => ['placeholder' => 'Select a date ...', 'value' => date('d-M-y'),],
                    'pluginOptions' => [
                        'format' => 'dd-M-yy',
                        'startDate' => 'today',
                        'todayBtn' => 'linked',
//                            'todayHighlight' => 'true',
                        'daysOfWeekDisabled' => '06',
                        'daysOfWeekHighlighted' => '06',
//                            'defaultViewDate' => 'today',
//                            'assumeNearbyYear' => 'true',
                    ]
                ]);
                ?>
            </div>
            <div class="col-sm-6">
                <?=
                $form->field($order, 'required_date')->widget(
                        kartik\widgets\DatePicker::className(), [
//                        'value' => date('dd-M-yy'),
                    'options' => ['placeholder' => 'Select a date ...', 'value' => date('d-M-y'),],
                    'pluginOptions' => [
                        'format' => 'dd-M-yy',
                        'startDate' => 'today',
                        'todayBtn' => 'linked',
//                            'todayHighlight' => 'true',
                        'daysOfWeekDisabled' => '06',
                        'daysOfWeekHighlighted' => '06',
//                            'defaultViewDate' => 'today',
//                            'assumeNearbyYear' => 'true',
                    ]
                ]);
                ?>
            </div>

        </div>
        <div>
            <?= Html::a(Yii::t('app', 'Batal'), Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
            <?= Html::submitButton($order->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Kemaskini'), ['class' => $order->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>



</div>
