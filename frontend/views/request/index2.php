<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel common\models\OrderItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Order Items');
$this->params['breadcrumbs'][] = $this->title;

$title = empty($this->title) ? Yii::t('app', 'Report') : $this->title . ' Report';
$pdfHeader = [
    'l' => [
        'content' => Yii::t('app', 'Sistem e-Store mpsp'),
        'font-size' => 8,
        'font-weight' => '600',
        'color' => '#333333'
    ],
    'c' => [
        'content' => $title,
        'font-size' => 16,
//        'font-weight' => 'bold',
        'color' => '#333333'
    ],
    'r' => [
        'content' => Yii::t('app', 'Generated') . ': ' . date("D, d-M-Y g:i a T"),
        'font-size' => 8,
        'color' => '#333333'
    ]
];
$pdfFooter = [
    'l' => [
        'content' => Yii::t('app', "Copyright © Majlis Perbandaran Seberang Perai"),
        'font-size' => 8,
        'font-style' => 'b',
        'color' => '#999999'
    ],
    'r' => [
        'content' => '[ {PAGENO} ]',
        'font-size' => 10,
        'font-style' => 'b',
        'font-family' => 'serif',
        'color' => '#333333'
    ],
    'line' => true,
];
$pdf = [
    'label' => Yii::t('app', 'pdf'),
    'icon' => 'file-pdf-o',
    'iconOptions' => ['class' => 'text-danger'],
    'showHeader' => true,
    'showPageSummary' => true,
    'showFooter' => true,
    'showCaption' => true,
    'filename' => Yii::t('app', 'grid-export'),
    'alertMsg' => Yii::t('app', 'The PDF export file will be generated for download.'),
    'options' => ['title' => Yii::t('app', 'Portable Document Format')],
    'mime' => 'application/pdf',
    'config' => [
        'mode' => 'c',
        'format' => 'A4-l',
        'destination' => 'd',
        'marginTop' => 20,
        'marginBottom' => 20,
        'cssInline' => '.kv-wrap{padding:20px;}' .
        '.kv-align-center{text-align:center;}' .
        '.kv-align-left{text-align:left;}' .
        '.kv-align-right{text-align:right;}' .
        '.kv-align-top{vertical-align:top!important;}' .
        '.kv-align-bottom{vertical-align:bottom!important;}' .
        '.kv-align-middle{vertical-align:middle!important;}' .
        '.kv-page-summary{border-top:4px double #ddd;font-weight: bold;}' .
        '.kv-table-footer{border-top:4px double #ddd;font-weight: bold;}' .
        '.kv-table-caption{font-size:1.5em;padding:8px;border:1px solid #ddd;border-bottom:none;}',
        'methods' => [
            'SetHeader' => [
                ['odd' => $pdfHeader, 'even' => $pdfHeader]
            ],
            'SetFooter' => [
                ['odd' => $pdfFooter, 'even' => $pdfFooter]
            ],
        ],
        'options' => [
            'title' => $title,
            'subject' => Yii::t('app', 'PDF export generated by kartik-v/yii2-grid extension'),
            'keywords' => Yii::t('app', 'krajee, grid, export, yii2-grid, pdf')
        ],
        'contentBefore' => '',
        'contentAfter' => ''
    ]
];
?>

<div class="modal fade modalSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <?= $this->render('_search', ['searchModel' => $searchModel]); ?>
</div>
<div class="order-items-index">
    <?php Pjax::begin(); ?>
    <?php
    $columns = [
        ['class' => 'yii\grid\SerialColumn', 'order' => DynaGrid::ORDER_FIX_LEFT],
        [
            'attribute' => 'id',
            'hAlign' => 'center', 'vAlign' => 'middle',
        ],
        [
            'attribute' => 'order.order_no',
            'hAlign' => 'center',
            'vAlign' => 'middle',
            'value' => function($model) {
                return 'NO PESANAN: ' . $model->order->order_no;
            },
            'group' => true,
            'groupedRow' => true, // move grouped column to a single grouped row
            'groupOddCssClass' => 'kv-grouped-row', // configure odd group cell css class
            'groupEvenCssClass' => 'kv-grouped-row',
//                'groupEvenCssClass' => 'maroon',
        ],
        [
            'label' => 'No Arahan Kerja',
            'attribute' => 'arahanKerja.no_kerja',
            'hAlign' => 'center', 'vAlign' => 'middle',
            'group' => true, 'subGroupOf' => 2,
        ],
        [
            'label' => 'Inventory Details',
            'attribute' => 'inventory_details',
            'format' => 'raw',
            'value' => function($model) {
                $card = 'Card No : ' . $model->inventory->card_no;
                $code = 'Code No : ' . $model->inventory->code_no;
                $description = 'Description : <br>' . $model->inventory->description;
                return Html::tag('span', $card . '<br>' . $code . '<br>' . $description);
            },
            'hAlign' => 'left', 'vAlign' => 'middle',
        ],
//            [
//                'attribute' => 'current_balance',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
//            [
//                'attribute' => 'transaction.check_by',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
//            [
//                'attribute' => 'transaction.check_date',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
        [
            'attribute' => 'order.order_date',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::tag('span', 'Date : ' . $model->order->order_date . '<br>' . 'Order by : ' . $model->order->ordered_by);
            },
            'hAlign' => 'center', 'vAlign' => 'middle',
            'group' => true, 'subGroupOf' => 2,
        ],
        [
            'attribute' => 'order.required_date',
            'hAlign' => 'center', 'vAlign' => 'middle',
            'group' => true, 'subGroupOf' => 2,
        ],
//            [
//                'attribute' => 'order.ordered_by',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
        [
            'label' => 'Kuantiti Stor',
            'attribute' => 'inventory.quantity',
            'hAlign' => 'center', 'vAlign' => 'middle',
        ],
//            [
//                'attribute' => 'rq_quantity',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
        [
            'label' => 'Kuantiti Permohonan',
            'attribute' => 'quantity',
            'format' => 'raw',
            'value' => function ($model) {
                if ($model->app_quantity) {
                    return Html::tag('span', 'Requested : ' . $model->rq_quantity . '<br>' . 'Approved : ' . $model->app_quantity);
                } else {
                    return Html::tag('span', 'Requested : ' . $model->rq_quantity . '<br>' . 'Approved : ' . 0);
                }
            },
            'hAlign' => 'center', 'vAlign' => 'middle',
        ],
//            [
//
//                'attribute' => 'items',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//                'format' => 'raw',
//                'value' => function($model) {
//                    $data = \yii\helpers\ArrayHelper::map($model->items, 'id', 'sku');
//                    return implode("<br>", $data);
//                },
//            ],
        [
            'label' => 'Jumlah Harga (RM)',
            'attribute' => 'unit_price',
            'hAlign' => 'center', 'vAlign' => 'middle',
        ],
        [
            'label' => 'Usage',
            'attribute' => 'usage_detail',
            'format' => 'raw',
            'value' => function($model) {
                $type = 'ID : ' . $model['vehicle']['id'];
                $regNo = 'Reg No : ' . $model['vehicle']['reg_no'];
                $description = 'Model : ' . $model['vehicle']['model'];
                return Html::tag('span', $regNo . '<br>' . $description);
            },
            'hAlign' => 'left', 'vAlign' => 'middle',
            'group' => true, 'subGroupOf' => 2,
        ],
//            [
//                'attribute' => 'order.checkout_date',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
//            [
//                'attribute' => 'order.checkout_by',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
//            [
//                'attribute' => 'created_at',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
//            [
//                'attribute' => 'updated_at',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
//            [
//                'attribute' => 'created_by',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
//            [
//                'attribute' => 'updated_by',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
        [
            'attribute' => 'order.deleted',
            'hAlign' => 'center', 'vAlign' => 'middle',
        ],
//            [
//                'attribute' => 'deleted_at',
//                'hAlign' => 'center', 'vAlign' => 'middle',
//            ],
        [
            'attribute' => 'order.approved',
            'format' => 'raw',
            'value' => function($model) {
                if ($model['order']['approved'] == 2) {
                    return Html::label('menunggu', null, ['class' => 'label label-warning']);
                } else if ($model['order']['approved'] == 1) {
                    return Html::label('lulus', null, ['class' => 'label label-success']);
                } else if ($model['order']['approved'] == 8) {
                    return Html::label('ditolak', null, ['class' => 'label label-danger']);
                } else {
                    return Html::label('TIDAK berkenaan', null, ['class' => 'label label-primary']);
                }
            },
                    'hAlign' => 'center', 'vAlign' => 'middle',
                    'group' => true, 'subGroupOf' => 2,
                ],
////            [
////                'attribute' => 'order.approved_by',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
////            [
////                'attribute' => 'order.approved_at',
////                'hAlign' => 'center', 'vAlign' => 'middle',
////            ],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'visible' => Yii::$app->user->isAdmin || Yii::$app->user->isPegawaiStor,
//                        'group' => true,
//                        'subGroupOf' => 2,
                    'template' => '{approval} {reject} {item-list} {view} {update} {delete} {recover}',
                    'viewOptions' => ['title' => 'View', 'data-toggle' => 'tooltip'],
                    'updateOptions' => ['title' => 'Update', 'data-toggle' => 'tooltip'],
//                        'group' => true, 'subGroupOf' => 2,
                    'buttons' => [
                        'kew2' => function ($url, $model) {
                            if ($model['order']['approved'] === 1) {
                                return Html::a('<span class="glyphicon glyphicon-file" style="color:green;"></span>', $url, ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
//                                    return Html::a('<span class="glyphicon glyphicon-print" style="color:green;"></span>', ['kew', 'ordersId' => $model->order->id], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
                            }
                        },
                                'view' => function ($url, $model) {
                            if ($model['order']['approved'] === 1) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model['order']['id']], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
//                                    return Html::a('<span class="glyphicon glyphicon-print" style="color:green;"></span>', ['kew', 'ordersId' => $model->order->id], ['title' => Yii::t('yii', 'KEW-PS-10'), 'data-toggle' => 'tooltip']);
                            }
                        },
                                'approval' => function ($url, $model) {
                            if ($model->deleted === 0 && $model['order']['approved'] === 2) {
                                return Html::a('<span class="glyphicon glyphicon-check" style="color:green;"></span>', ['approval', 'ordersId' => $model->order->id], ['title' => Yii::t('yii', 'Approval'), 'data-toggle' => 'tooltip']);
                            }
                        },
                                'reject' => function ($url, $model) {
                            if ($model->deleted === 0 && $model['order']['approved'] === 2) {
//                                    return Html::a('<span class="glyphicon glyphicon-remove" style="color:red;"></span>', ['reject', 'id' => $model->order->id], ['title' => Yii::t('yii', 'Reject'), 'data-toggle' => 'tooltip']);
                                return Html::a('<span class="glyphicon glyphicon-remove" style="color:red;"></span>', ['reject', 'id' => $model->id], ['title' => Yii::t('yii', 'Reject'), 'data-toggle' => 'tooltip']);
                            }
                        },
                                'item-list' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-list"></span>', $url, ['title' => Yii::t('yii', 'Item List'), 'data-toggle' => 'tooltip']);
                        },
//                                    'item-list' => function ($url, $model) {
//                                return Html::a('<span class="glyphicon glyphicon-list"></span>', ['item-list', 'ordersId' => $model->order->id], ['title' => Yii::t('yii', 'Item List'), 'data-toggle' => 'tooltip']);
//                            },
                                'update' => function ($url, $model) {
                            if ($model->deleted === 0 && $model->order->approved === 2) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Recover'), 'data-toggle' => 'tooltip']);
                            }
                        },
                                'recover' => function ($url, $model) {
                            if ($model->deleted === 1) {
                                return Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, ['title' => Yii::t('yii', 'Recover'), 'data-toggle' => 'tooltip', 'data-method' => 'post']);
                            }
                        },
                                'delete' => function ($url, $model) {
                            if ($model->deleted === 0 && $model->order->approved === 2) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'), 'data-toggle' => 'tooltip',
                                            'data-method' => 'post']);
                            }
                        },
                            ],
                        ],
                        [
                            'header' => 'Permanent Delete',
                            'class' => 'kartik\grid\ActionColumn',
                            'template' => '{delete-permanent}',
                            'buttons' => [
                                'delete-permanent' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-trash" style="color:red;"></span>', $url, [
                                                'title' => Yii::t('yii', 'Permanent Delete'), 'data-toggle' => 'tooltip', 'data-method' => 'post'
                                    ]);
                                }
                                    ],
                                    'visible' => Yii::$app->user->isAdmin,
                                ],
                            ];

                            $dynagrid = DynaGrid::begin([
                                        'columns' => $columns,
                                        'theme' => 'panel-primary',
                                        'showPersonalize' => true,
                                        'storage' => 'session',
                                        'gridOptions' => [
                                            'dataProvider' => $dataProvider,
//                                                    'filterModel' => $searchModel,
//                                                    'showPageSummary' => true,
                                            'floatHeader' => true,
//                                            'floatOverflowContainer'=>true,
                                            'floatHeaderOptions' => ['scrollingTop' => 3],
                                            'floatHeaderOptions' => [
//                                                'top' => '2',
                                                'position' => 'absolute',
                                            ],
                                            'pjax' => true,
                                            'panel' => [
                                                'heading' => $this->title,
//                                                        'before' => '<div style="padding-top: 7px;"><em>* The table header sticks to the top in this demo as you scroll</em></div>',
                                                'after' => false
                                            ],
                                            'toolbar' => [
                                                ['content' =>
                                                    Html::a('<em>Normal Grid</em>', ['index'], ['class' => 'btn btn-default', 'title' => Yii::t('app', 'Smart Grid'),]) . ' ' .
                                                    Html::button('<i class="glyphicon glyphicon-search"></i>', ['class' => 'btn btn-info', 'title' => Yii::t('app', 'Search'), 'data-toggle' => 'modal', 'data-target' => '.modalSearch']) . ' ' .
                                                    Html::a('<i class="glyphicon glyphicon-cog"></i>', ['site/maintenance'], ['class' => 'btn btn-primary', 'title' => Yii::t('app', 'Maintenance'),]) . ' ' .
                                                    Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'], ['class' => 'btn btn-success', 'title' => Yii::t('app', 'Create order-items'),]) . ' ' .
                                                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
                                                ],
                                                ['content' => '{dynagridFilter}{dynagridSort}{dynagrid}'],
                                                '{export}',
                                            ],
                                            'exportConfig' => [
                                                'pdf' => $pdf,
                                                'csv' => '{csv}',
                                                'xls' => '{xls}',
                                            ],
                                            'export' => [
                                                'fontAwesome' => true,
                                                'target' => '_self',
                                            ],
                                        ],
                                        'options' => ['id' => 'dynagrid-1'] // a unique identifier is important
                            ]);
                            if (substr($dynagrid->theme, 0, 6) == 'simple') {
                                $dynagrid->gridOptions['panel'] = false;
                            }
                            DynaGrid::end();
                            Pjax::end();
                            ?>
</div>
