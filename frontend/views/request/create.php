<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kidzen\dynamicform\DynamicFormWidget;

//die();
/* @var $this yii\web\View */
/* @var $transaction common\models\Transactions */
//die();
$this->title = Yii::t('app', 'Create Transactions');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Transactions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
//var_dump($transaction->attributes);
////var_dump($inventory->attributes);
//var_dump($order->attributes);
//var_dump($orderItems[0]->attributes);
//die();
?>
<div class="transactions-create">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <!--<div class='row'>-->
    <div class="panel panel-primary">
        <div class="panel-heading"><h4><i class="fa fa-upload"></i> <?= $this->title ?></h4></div>

        <div class="panel-body">
            <div class="row">
                <?php if (!$order->isNewRecord) { ?>
                    <div class="col-sm-4">
                        <?= $form->field($order, 'order_no')->textInput(['maxlength' => true, 'readOnly' => true, 'value' => $order->order_no]) ?>
                    </div>
                <?php } else { ?>
                    <div class="col-sm-4">
                        <?= $form->field($order, 'order_no')->textInput(['maxlength' => true, 'readOnly' => true, 'value' => date('Y/m') . '-' . $bilStock]) ?>
                    </div>
                <?php } ?>
                <div class="col-sm-4">
                    <?php
                    echo $form->field($order, 'ordered_by')->textInput(['maxlength' => true,]) ?>
                </div>
                <div class="col-sm-4">
                    <?php
                    echo $form->field($order, 'vehicle_id')->widget(kartik\select2\Select2::className(), [
                        'data' => $vehiclesArray,
//                            'theme' => 'default',
                        'options' => ['placeholder' => 'Select an inventory ...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-sm-6">
                    <?=
                    $form->field($order, 'order_date')->widget(kartik\widgets\DatePicker::className(), [
                        'value' => date('dd-M-yy'),
                        'options' => ['placeholder' => 'Select a date ...', 'value' => date('d-M-y'),],
                        'pluginOptions' => [
                            'format' => 'dd-M-yy',
//                            'startDate' => 'today',
                            'todayBtn' => 'linked',
//                            'todayHighlight' => 'true',
                            'daysOfWeekDisabled' => '06',
                            'daysOfWeekHighlighted' => '06',
//                            'defaultViewDate' => 'today',
//                            'assumeNearbyYear' => 'true',
                        ]
                    ]);
                    ?>
                </div>
                <div class="col-sm-6">
                    <?=
                    $form->field($order, 'required_date')->widget(
                            kartik\widgets\DatePicker::className(), [
//                        'value' => date('dd-M-yy'),
                        'options' => ['placeholder' => 'Select a date ...', 'value' => date('d-M-y'),],
                        'pluginOptions' => [
                            'format' => 'dd-M-yy',
//                            'startDate' => 'today',
                            'todayBtn' => 'linked',
//                            'todayHighlight' => 'true',
                            'daysOfWeekDisabled' => '06',
                            'daysOfWeekHighlighted' => '06',
//                            'defaultViewDate' => 'today',
//                            'assumeNearbyYear' => 'true',
                        ]
                    ]);
                    ?>
                </div>
                <!--                                <div class="col-sm-4">
                <?php
                $approveStat = ['0' => "New", '1' => "Pending", '2' => "Approved",
                    '8' => "Rejected", '9' => "Disposed"];
                echo $form->field($order, 'approved')
                        ->dropDownList(
                                $approveStat // Flat array ('id'=>'label')
                );
                ?>
                                                </div>-->
            </div>
            <!--////////////////////////////////estor items/////////////////////////////////-->

            <?php
            DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
//                    'limit' => 4, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $orderItems[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'id',
                    'inventory_id',
                    'rq_quantity',
                ],
            ]);

//            var_dump($transaction->approved);die();
            ?>

            <div class="container-items"><!-- widgetContainer -->
                <?php foreach ($orderItems as $i => $orderItem): ?>
                    <div class="item panel panel-default"><!-- widgetBody -->
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left">Items</h3>
                            <div class="pull-right">
                                <?php if (!$order->approved || $order->approved  == 2) { ?>
                                    <button type = "button" class = "add-item btn btn-success btn-xs"><i class = "glyphicon glyphicon-plus"></i></button>
                                <?php } ?>
                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php
                            // necessary for update action.
                            if (!$orderItem->isNewRecord) {
                                echo Html::activeHiddenInput($orderItem, "[{$i}]id");
                            }
                            ?>
                            <div class="row">
                                <div class="col-sm-5">
                                    <?=
                                    $form->field($orderItem, "[{$i}]inventory_id")->widget(kartik\select2\Select2::className(), [
                                        'data' => $inventoriesArray,
//                                        'theme' => 'default',
                                        'options' => ['placeholder' => 'Select an inventory ...'],
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                        ],
                                    ]);
//                                            ->dropDownList($inventoriesArray, ['prompt' => 'Choose...']);
                                    ?>
                                </div>
                                <div class="col-sm-5">
                                    <?= $form->field($orderItem, "[{$i}]rq_quantity")->textInput(['maxlength' => true]) ?>
                                </div>
                                <!--                                <div class="col-sm-4">
                                <?= $form->field($orderItem, "[{$i}]current_balance")->textInput(['maxlength' => true, 'value' => 0, 'readOnly' => true,]) ?>
                                                                </div>-->
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
            <div class="form-group">
                <?= Html::submitButton($order->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Kemaskini'), ['class' => $order->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

        </div>
        <!--</div>-->


    </div>
    <?php ActiveForm::end(); ?>

</div>
