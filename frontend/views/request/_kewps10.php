


<div class="box-body table-responsive">
    <div>
        <!--<p><span>Pekeliling Perbendaharaan Malaysia</span><span class="pull-right">AM 6.5 Lampiran A</span></p>-->
        <p class="pull-right form-lampiran"><strong>LAMPIRAN A</strong></p>
        <!--<div class="clearfix"></div>-->
        <p class="pull-right form-id"><strong>KEW.PS-10</strong></p>
        <!--<div class="clearfix"></div>-->
        <p class="text-center form-name"><strong>BORANG PESANAN DAN PENGELUARAN STOK</strong></p>
        <table class="kv-grid-table table table-hover table-bordered table-striped table-condensed kv-table-wrap">
        <!--<table bordered>-->
            <thead>
                <tr>
                    <th class="col-1 row-1" colspan="4"><span class="row-1-label">Daripada :</span><br><span class="row-1-data">data</span></th>
                    <th class="col-2 row-1" colspan="7"><span class="row-1-label">Kepada :</span><br><span class="row-1-data">data</span></th>
                </tr>
                <tr>
                    <th class="text-center info col-1 row-2" colspan="4">Dilengkapkan Oleh Stor Pesanan</th>
                    <th class=" right-border-bold col-2 row-2" class="text-center info" colspan="7">Dilengkapkan Oleh Stor Pengeluar</th>
                </tr>
                <tr>
                    <th class="col-1 row-3" colspan="4">No Pemesanan :</th>
                    <th class="col-2 row-3 right-border-bold" colspan="7">No Pengeluaran :</th>
                </tr>
                <tr>
                    <th class="col-1 row-4" colspan="4">Tarikh Bekalan Dikehendaki : </th>
                    <th class="col-2 row-4 text-center info right-border-bold" colspan="5">BAHAGIAN BEKALAN,KAWALAN DAN AKAUN</th>
                    <th class="col-3 row-4 text-center info" colspan="2">BAHAGIAN SIMPANAN</th>
                </tr>
                <tr>
                    <th class="col-1 row-5 text-center info" rowspan="2">No. Kod</th>
                    <th class="col-2 row-5 text-center info" colspan="2" rowspan="2">Perihal Stok</th>
                    <th class="col-3 row-5 text-center info" rowspan="2">Kuantiti</th>
                    <th class="col-4 row-5 text-center info" colspan="2">Kad Kawalan Stok</th>
                    <th class="col-5 row-5 text-center info" rowspan="2">Kuantiti Diluluskan</th>
                    <th class="col-6 row-5 text-center info" colspan="2">Harga (RM)</th>
                    <th class="col-7 row-5 text-center info" rowspan="2">Kuantiti Dikeluarkan</th>
                    <th class="col-8 row-5 text-center info" rowspan="2">Catatan</th>
                </tr>
                <tr>
                    <th class="col-1 row-6 text-center info">No. Kad</th>
                    <th class="col-2 row-6 text-center info">Baki Sedia Ada</th>
                    <th class="col-3 row-6 text-center info">Seunit</th>
                    <th class="col-4 row-6 text-center info">Jumlah</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($items as $item) { ?>
                    <tr>
                        <td class="col-1 row-1"><?= $item->inventory->code_no ?></td>
                        <td class="col-2 row-1" colspan="2"><?= $item->inventory->description ?></td>
                        <td class="col-3 row-1"><?= $item->rq_quantity ?></td>
                        <td class="col-4 row-1"><?= $item->inventory->card_no ?></td>
                        <td class="col-5 row-1"><?= $item->current_balance ?></td>
                        <td class="col-6 row-1"><?= $item->app_quantity ?></td>
                        <td class="col-7 row-1"><?= $item->unit_price / $item->app_quantity ?></td>
                        <td class="col-8 row-1"><?= $item->unit_price ?></td>
                        <td class="col-9 row-1"><?= $item->app_quantity ?></td>
                        <td class="col-10 row-1"></td>
                    </tr>
                <?php } ?>
            <tbody>
            <tfoot>
                <tr>
                    <td class="foot-head-col-1 foot-head-row-1" colspan="2">Pegawai Pemesan :</td>
                    <td class="foot-head-col-2 foot-head-row-1" colspan="2">Pegawai Penerima :</td>
                    <td class="foot-head-col-3 foot-head-row-1" colspan="5">Tarikh Diluluskan dan Direkodkan Oleh :</td>
                    <td class="foot-head-col-4 foot-head-row-1" colspan="2">Dikeluarkan dan Direkodkan Oleh :</td>
                </tr>
                <tr>
                    <td class="foot-col-1 foot-row-2"><strong>Nama :</strong></td>
                    <td class="foot-col-2 foot-row-2">data</td>
                    <td class="foot-col-3 foot-row-2"><strong>Nama :</strong></td>
                    <td class="foot-col-4 foot-row-2">data</td>
                    <td class="foot-col-5 foot-row-2"><strong>Nama :</strong></td>
                    <td class="foot-col-6 foot-row-2" colspan="4">data</td>
                    <td class="foot-col-7 foot-row-2"><strong>Nama :</strong></td>
                    <td class="foot-col-8 foot-row-2">data</td>
                </tr>
                <tr>
                    <td class="foot-col-1 foot-row-3"><strong>Jawatan :</strong></td>
                    <td class="foot-col-2 foot-row-3">data</td>
                    <td class="foot-col-3 foot-row-3"><strong>Jawatan :</strong></td>
                    <td class="foot-col-4 foot-row-3">data</td>
                    <td class="foot-col-5 foot-row-3"><strong>Jawatan :</strong></td>
                    <td class="foot-col-6 foot-row-3" colspan="4">data</td>
                    <td class="foot-col-7 foot-row-3"><strong>Jawatan :</strong></td>
                    <td class="foot-col-8 foot-row-3">data</td>
                </tr>
                <tr>
                    <td class="foot-col-1 foot-row-4"><strong>Jabatan :</strong></td>
                    <td class="foot-col-2 foot-row-4">data</td>
                    <td class="foot-col-3 foot-row-4"><strong>Jabatan :</strong></td>
                    <td class="foot-col-4 foot-row-4">data</td>
                    <td class="foot-col-5 foot-row-4"><strong>Jabatan :</strong></td>
                    <td class="foot-col-6 foot-row-4" colspan="4">data</td>
                    <td class="foot-col-7 foot-row-4"><strong>Jabatan :</strong></td>
                    <td class="foot-col-8 foot-row-4">data</td>
                </tr>
                <tr>
                    <td class="foot-col-1 foot-row-5"><strong>Tarikh :</strong></td>
                    <td class="foot-col-2 foot-row-5">data</td>
                    <td class="foot-col-3 foot-row-5"><strong>Tarikh :</strong></td>
                    <td class="foot-col-4 foot-row-5">data</td>
                    <td class="foot-col-5 foot-row-5"><strong>Tarikh :</strong></td>
                    <td class="foot-col-6 foot-row-5" colspan="4">data</td>
                    <td class="foot-col-7 foot-row-5"><strong>Tarikh :</strong></td>
                    <td class="foot-col-8 foot-row-5">data</td>
                </tr>
                <tr>
                    <td class="foot-note-col-1 foot-note-row-6" colspan="4" rowspan="8">
                        <br><br>
                        <p>Nota</p>
                        <p>Salinan 1 - Disimpan oleh pemesan</p>
                        <p>Salinan 2 - Bahagian Bekalan, Kawalan Dan Akaun</p>
                        <p>Salinan 3 - Bahagian Simpanan</p>
                        <p>Salinan 4 - Bahagian Bungkusan Dan Penghantaran</p>
                        <p>Salinan 5 – Disimpan oleh  pemesan setelah stok diterima</p>
                    </td>
                    <td class="foot-col-2 foot-row-6 foot-deliver-head text-center info" colspan="7"><strong>BAHAGIAN BUNGKUSAN DAN,PENGHANTARAN</strong></td>
                </tr>
                <tr>
                    <td class="foot-head-col-1 foot-head-row-7" colspan="2" rowspan="7"><strong>Butir-Butir Bungkusan</strong></td>
                    <td class="foot-head-col-2 foot-head-row-7" colspan="3" rowspan="7"><strong>Butir-Butir Penghantaran</strong></td>
                    <td class="foot-head-col-3 foot-head-row-7" colspan="2">Telah dibungkus dan dihantar oleh :</td>
                </tr>
                <tr>
                    <td class="foot-col-1 foot-row-8"><strong>Nama : </strong></td>
                    <td class="foot-col-2 foot-row-8">data</td>
                </tr>
                <tr>
                    <td class="foot-col-1 foot-row-9"><strong>Jawatan : </strong></td>
                    <td class="foot-col-2 foot-row-9">data</td>
                </tr>
                <tr>
                    <td class="foot-col-1 foot-row-10"><strong>Jabatan : </strong></td>
                    <td class="foot-col-2 foot-row-10">data</td>
                </tr>
                <tr>
                    <td class="foot-col-1 foot-row-11"><strong>Tarikh : </strong></td>
                    <td class="foot-col-2 foot-row-11">data</td>
                </tr>
            </tfoot>
        </table>

    </div>
</div>
