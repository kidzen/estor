
<tbody>
    <?php foreach ($items as $item) { ?>
        <tr>
            <td class="col-1 row-1" style="width:10%"><?= $item['inventory']['code_no'] ?></td>
            <td class="col-2 row-1" style="width:40%" colspan="2"><?= mb_strimwidth($item['inventory']['description'],0,35,"...",'UTF-8') ?></td>
            <td class="col-3 row-1" style="width:7%"><?= $item['rq_quantity'] ?></td>
            <td class="col-4 row-1" style="width:7%"><?= $item['inventory']['card_no'] ?></td>
            <td class="col-5 row-1" style="width:7%"><?= $item['current_balance'] ?></td>
            <td class="col-6 row-1" style="width:7%"><?= $item['app_quantity'] ?></td>
            <td class="col-7 row-1" style="width:7%"><?= $item['unit_price'] / $item['app_quantity'] ?></td>
            <td class="col-8 row-1" style="width:7%;text-align: center;"><?= $item['unit_price'] ?></td>
            <td class="col-9 row-1" style="width:8%"><?= $item['app_quantity'] ?></td>
            <td class="col-10 row-1" style="width:15%"></td>
        </tr>
    <?php } ?>
<tbody>
