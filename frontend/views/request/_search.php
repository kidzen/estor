<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TransactionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transactions-search">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Carian</h4>
            </div>
            <div class="modal-body">
                <?php
                $form = ActiveForm::begin(['action' => ['index'], 'method' => 'get',]);
                ?>

                <h3>Perincian Pesanan</h3>
                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($searchModel, 'order.order_no') ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($searchModel, 'arahanKerja.no_kerja') ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($searchModel, 'order.order_date') ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($searchModel, 'order.required_date') ?>
                    </div>
                </div>
                <h3>Perincian Inventori</h3>
                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($searchModel, 'inventory.card_no') ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($searchModel, 'inventory.code_no') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($searchModel, 'inventory.description') ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($searchModel, 'inventory.quantity') ?>
                    </div>
                </div>
                <h3> Perincian Maklumat Pesanan</h3>
                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($searchModel, 'rq_quantity') ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($searchModel, 'app_quantity') ?>
                    </div>
                    <div class="col-md-3">
                        <?php echo $form->field($searchModel, 'current_balance') ?>
                    </div>
                    <div class="col-md-3">
                        <?php echo $form->field($searchModel, 'unit_price') ?>
                    </div>
                </div>


                <?php if (Yii::$app->user->isAdmin) { ?>
                    <h3> Perincian Data</h3>
                    <div class="row">
                        <div class="col-md-4">
                            <?php echo $form->field($searchModel, 'created_by') ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo $form->field($searchModel, 'created_at') ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo $form->field($searchModel, 'updated_by') ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo $form->field($searchModel, 'updated_at') ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo $form->field($searchModel, 'deleted') ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo $form->field($searchModel, 'deleted_at') ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>



