<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\InventoryItems */

$this->title = Yii::t('app', 'Transaction Detail') . ' :' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inventory Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-info inventory-items-view">
    <div class="box-header with-border">
        <h3 class="box-title"><strong><i class="fa fa-book margin-r-5 text-blue"></i><?= Html::encode($this->title) ?></strong></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::a(Yii::t('app', 'Kembali'), Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
            <?= Html::a(Yii::t('app', 'Kemaskini'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </p>
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'transaction_id',
                'category.name',
                'inventory.card_no',
                'inventory.code_no',
                'inventory.description',
                'vendor.name',
                'items_quantity',
                [
                    'label' => 'Item Total Price (RM)',
                    'attribute' => 'items_total_price',
                    'format' => ['decimal', 2],
                    'value' => $model->items_total_price,
                ],
                'check_date',
                'check_by',
                'approved',
                'approved_by',
                'approved_at',
                'created_at',
                'updated_at',
                'created_by',
                'updated_by',
                [
                    'attribute' => 'deleted',
                    'vAlign' => 'middle',
                    'value' => $model->deleted == 1 ? 'Deleted' : 'Active',
                ],
                'deleted_at',
            ],
        ])
        ?>

    </div>
