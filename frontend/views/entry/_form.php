<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\InventoryItems */
/* @var $form yii\widgets\ActiveForm */
//var_dump($transaction->attributes);
//var_dump($checkIn->attributes);
//var_dump($inventory->attributes);
//var_dump($items->attributes);die();
?>

<div class="inventory-items-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-primary">
        <div class="panel-heading"><h4><i class="fa fa-download"> </i> <?= $this->title ?></h4></div>

        <div class="panel-body">
            <div class="row">
                <div class="col-sm-4">
                    <?=
                    $form->field($checkIn, 'inventory_id')->widget(kartik\select2\Select2::className(), [
                        'data' => $inventoriesArray,
//                            'theme' => 'default',
                        'options' => ['placeholder' => 'Select an inventory ...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ]);
                    ?>                    </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($checkIn, 'vendor_id')->widget(kartik\select2\Select2::className(), [
                        'data' => $vendorsArray,
                        'theme' => 'default',
                        'options' => ['placeholder' => 'Select a vendor ...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ]);
                    ?>

                </div>
                <div class="col-sm-4">
                    <?= $form->field($inventory, 'quantity')->textInput(['readOnly' => true]); ?>
                </div>
            </div>
            <div class="container-items"><!-- widgetContainer -->
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Child</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                        // necessary for update action.
//                            if (!$transaction->isNewRecord) {
////                                die();
////                                var_dump(Html::activeHiddenInput($transactionItem, "[{$i}]id")) ;die();
////                                echo Html::textInput($transactionItem, "[{$i}]id");
//                                echo Html::activeHiddenInput($transaction, "[{$i}]id");
//                            }
                        ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <?= $form->field($checkIn, 'items_quantity')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($items, 'unit_price')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <?= Html::a(Yii::t('app', 'Batal'), Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
            <?= Html::submitButton($checkIn->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Kemaskini'), ['class' => $checkIn->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>



    <?php ActiveForm::end(); ?>

</div>


<?php
$script = <<< JS
$("#inventoriescheckin-inventory_id").on("change", function(){
        var id = this.value;
        $.get('index.php?r=entry/get-quantity',{ id : id },function(data){
		var data = $.parseJSON(data);
		$('#inventories-quantity').attr('value',data);
	});
});
JS;

$this->registerJs($script);
?>
