<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\People;

//use common\models\MpspStaff;

/**
 * Login form
 */
class LoginForm extends Model {

    public $username;
    public $staff_no;
    public $password;
    public $rememberMe = true;
    private $_user;

    const SCENARIO_LOGIN_WITH_USERNAME = 'login-with-username';
    const SCENARIO_LOGIN_WITH_STAFF_NO = 'login-with-staff_no';

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            // username and password are both required
            [['password'], 'required'],
//            [['username', 'password'], 'required'],
            // username and password are both required
            [['username', 'password'], 'required', 'on' => self::SCENARIO_LOGIN_WITH_USERNAME],
            // staff no and password are both required
            [['staff_no', 'password'], 'required', 'on' => self::SCENARIO_LOGIN_WITH_STAFF_NO],
            // rememberMe must be a boolean value
            [['rememberMe'], 'boolean'],
//            [['rememberMe', 'login_with_username'], 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params) {
//        var_dump($this->getErrors());die();
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login() {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

//
//    /**
//     * Finds user by [[username]]
//     *
//     * @return User|null
//     */
    protected function getUser() {
        if ($this->_user === null) {
            if ($this->scenario == self::SCENARIO_LOGIN_WITH_USERNAME) {
                $this->_user = People::findByUsername($this->username);
            } else {
                $this->_user = People::findByStaffNo($this->staff_no);
            }
        }
        return $this->_user;
    }

}
