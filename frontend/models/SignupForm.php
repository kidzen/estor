<?php

namespace frontend\models;

use common\models\People;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model {

    public $USERNAME;
    public $EMAIL;
    public $PASSWORD;
    public $ROLE_ID;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['USERNAME', 'filter', 'filter' => 'trim'],
            ['USERNAME', 'required'],
            ['USERNAME', 'unique', 'targetClass' => '\common\models\People', 'message' => 'This username has already been taken.'],
            ['USERNAME', 'string', 'min' => 2, 'max' => 255],
            ['EMAIL', 'filter', 'filter' => 'trim'],
            ['EMAIL', 'required'],
            ['EMAIL', 'email'],
            ['EMAIL', 'string', 'max' => 255],
            ['EMAIL', 'unique', 'targetClass' => '\common\models\People', 'message' => 'This email address has already been taken.'],
            ['ROLE_ID', 'filter', 'filter' => 'trim'],
            ['PASSWORD', 'required'],
            ['PASSWORD', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup() {

        if (!$this->validate()) {
            return null;
        }
        $user = new People();
        $user->USERNAME = $this->USERNAME;
        $user->EMAIL = $this->EMAIL;
        $user->ROLE_ID = 2;
        $user->setPassword($this->PASSWORD);
        $user->generateAuthKey();
//        $user->save();
//        var_dump($user);die();

//        var_dump($user);die();
//        $auth = Yii::$app->authManager;
//        $authorRole = $auth->getRole('author');
//        $auth->assign($authorRole, $user->getId());
//        var_dump($user->save());
//        var_dump($user->getErrors());
//        die();
        return $user->save() ? $user : null;
    }

}
