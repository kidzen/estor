DROP VIEW "ALL_COL_NAME";
DROP VIEW "ALL_MAINTENANCE_CHECK";
DROP VIEW "ANALYTIC";
DROP VIEW "APPROVAL";
DROP VIEW "IC_PRICE_CHECK";
DROP VIEW "IC_QUANTITY_CHECK";
DROP VIEW "I_QUANTITY_CHECK";
DROP VIEW "KEWPS_10";
DROP VIEW "LAPORAN_PENYELENGARAAN_SISKEN";
DROP VIEW "LAPORAN_PENYELENGGARAAN_VIEW";
DROP VIEW "LIST_YEAR";
DROP VIEW "MAINTENANCE_CHECK";
DROP VIEW "MPSP_STAFF";
DROP VIEW "OI_APP_QUANTITY_CHECK";
DROP VIEW "REPORT_ALL";
DROP VIEW "REPORT_IN_QUARTER";
DROP VIEW "REPORT_OUT_QUARTER";
DROP VIEW "TEMP_CHECKOUT_PRICE_QUANTITY";
DROP VIEW "TRANSACTIONS_ALL";
DROP VIEW "TRANSACTION_IN";
DROP VIEW "TRANSACTION_OUT";
DROP VIEW "TRANSACTION_OUT_SISKEN";
DROP VIEW "VEHICLE_REPORT";
DROP VIEW "VENDORS_REPORT";
--------------------------------------------------------
--  DDL for View ALL_COL_NAME
--------------------------------------------------------

  CREATE VIEW "ALL_COL_NAME" ("COLNAME") AS 
  with ii_t as (
  select distinct ',ii.'||column_name||' ii_'||column_name as colName
  from all_tab_columns
  where table_name = 'INVENTORY_ITEMS'
  and owner = 'ESTOR'
)
, oi_t as (
  select distinct ',oi.'||column_name||' oi_'||column_name as colName
  from all_tab_columns
  where table_name = 'ORDER_ITEMS'
  and owner = 'ESTOR'
)
, o_t as (
  select distinct ',o.'||column_name||' o_'||column_name as colName
  from all_tab_columns
  where table_name = 'ORDERS'
  and owner = 'ESTOR'
)
, ic_t as (
  select distinct ',ic.'||column_name||' ic_'||column_name as colName
  from all_tab_columns
  where table_name = 'INVENTORIES_CHECKIN'
  and owner = 'ESTOR'
)
, i_t as (
  select distinct ',i.'||column_name||' i_'||column_name as colName
  from all_tab_columns
  where table_name = 'INVENTORIES'
  and owner = 'ESTOR'
)
, c_t as (
  select distinct ',c.'||column_name||' c_'||column_name as colName
  from all_tab_columns
  where table_name = 'CATEGORIES'
  and owner = 'ESTOR'
)
, t_t as (
  select distinct ',t.'||column_name||' t_'||column_name as colName
  from all_tab_columns
  where table_name = 'TRANSACTIONS'
  and owner = 'ESTOR'
)
, all_t as (
  select colName from ii_t
  union ALL
  select colName from oi_t
  union ALL
  select colName from o_t
  union ALL
  select colName from ic_t
  union ALL
  select colName from i_t
  union ALL
  select colName from c_t
  union ALL
  select colName from t_t
)

select "COLNAME" from all_t;
--------------------------------------------------------
--  DDL for View ALL_MAINTENANCE_CHECK
--------------------------------------------------------

  CREATE VIEW "ALL_MAINTENANCE_CHECK" ("ID", "TYPE", "TABLE_NAME", "COL_NAME", "DATA_ID", "FAULT_VALUE", "TRUE_VALUE") AS 
  with checkin as (
  select 
  ii.UPDATED_BY ii_UPDATED_BY,ii.CHECKIN_TRANSACTION_ID ii_CHECKIN_TRANSACTION_ID,ii.INVENTORY_ID ii_INVENTORY_ID
  ,ii.DELETED ii_DELETED,ii.SKU ii_SKU,ii.ID ii_ID,ii.CHECKOUT_TRANSACTION_ID ii_CHECKOUT_TRANSACTION_ID
  ,ii.CREATED_BY ii_CREATED_BY,ii.CREATED_AT ii_CREATED_AT,ii.DELETED_AT ii_DELETED_AT,ii.UPDATED_AT ii_UPDATED_AT
  ,ii.UNIT_PRICE ii_UNIT_PRICE
  ,ic.UPDATED_AT ic_UPDATED_AT,ic.APPROVED_AT ic_APPROVED_AT,ic.CREATED_BY ic_CREATED_BY
  ,ic.CREATED_AT ic_CREATED_AT,ic.DELETED ic_DELETED,ic.APPROVED ic_APPROVED,ic.CHECK_BY ic_CHECK_BY
  ,ic.ITEMS_TOTAL_PRICE ic_ITEMS_TOTAL_PRICE,ic.INVENTORY_ID ic_INVENTORY_ID,ic.DELETED_AT ic_DELETED_AT
  ,ic.APPROVED_BY ic_APPROVED_BY,ic.CHECK_DATE ic_CHECK_DATE,ic.UPDATED_BY ic_UPDATED_BY
  ,ic.ITEMS_QUANTITY ic_ITEMS_QUANTITY,ic.VENDOR_ID ic_VENDOR_ID,ic.ID ic_ID,ic.TRANSACTION_ID ic_TRANSACTION_ID
  ,i.APPROVED i_APPROVED,i.APPROVED_BY i_APPROVED_BY,i.CATEGORY_ID i_CATEGORY_ID,i.CREATED_BY i_CREATED_BY
  ,i.CREATED_AT i_CREATED_AT,i.APPROVED_AT i_APPROVED_AT,i.LOCATION i_LOCATION,i.CARD_NO i_CARD_NO,i.DELETED i_DELETED
  ,i.DESCRIPTION i_DESCRIPTION,i.DELETED_AT i_DELETED_AT,i.UPDATED_BY i_UPDATED_BY,i.UPDATED_AT i_UPDATED_AT
  ,i.MIN_STOCK i_MIN_STOCK,i.CODE_NO i_CODE_NO,i.QUANTITY i_QUANTITY,i.ID i_ID,c.DELETED c_DELETED,c.NAME c_NAME
  ,c.UPDATED_BY c_UPDATED_BY,c.CREATED_AT c_CREATED_AT,c.CREATED_BY c_CREATED_BY,c.DELETED_AT c_DELETED_AT,c.ID c_ID
  ,c.UPDATED_AT c_UPDATED_AT,t.CREATED_BY t_CREATED_BY,t.UPDATED_AT t_UPDATED_AT,t.DELETED t_DELETED
  ,t.DELETED_AT t_DELETED_AT,t.UPDATED_BY t_UPDATED_BY,t.CHECK_BY t_CHECK_BY,t.ID t_ID,t.CREATED_AT t_CREATED_AT
  ,t.CHECK_DATE t_CHECK_DATE,t.TYPE t_TYPE
  from inventory_items ii
  left join inventories_checkin ic on ic.id = ii.checkin_transaction_id
  left join transactions t on t.id = ic.transaction_id
  left join inventories i on i.id = ic.inventory_id
  left join categories c on c.id = i.category_id
)
--------------------------------------------------------
--              ALL CHECKOUT TRANSACTIONS
--------------------------------------------------------

, checkout as (
  select 
  ii.UPDATED_BY ii_UPDATED_BY,ii.CHECKIN_TRANSACTION_ID ii_CHECKIN_TRANSACTION_ID,ii.INVENTORY_ID ii_INVENTORY_ID
  ,ii.DELETED ii_DELETED,ii.SKU ii_SKU,ii.ID ii_ID,ii.CHECKOUT_TRANSACTION_ID ii_CHECKOUT_TRANSACTION_ID
  ,ii.CREATED_BY ii_CREATED_BY,ii.CREATED_AT ii_CREATED_AT,ii.DELETED_AT ii_DELETED_AT,ii.UPDATED_AT ii_UPDATED_AT
  ,ii.UNIT_PRICE ii_UNIT_PRICE
  ,oi.UNIT_PRICE oi_UNIT_PRICE,oi.DELETED_AT oi_DELETED_AT,oi.CREATED_AT oi_CREATED_AT
  ,oi.RQ_QUANTITY oi_RQ_QUANTITY,oi.CREATED_BY oi_CREATED_BY,oi.APP_QUANTITY oi_APP_QUANTITY
  ,oi.CURRENT_BALANCE oi_CURRENT_BALANCE,oi.INVENTORY_ID oi_INVENTORY_ID,oi.UPDATED_BY oi_UPDATED_BY
  ,oi.ORDER_ID oi_ORDER_ID,oi.DELETED oi_DELETED,oi.UPDATED_AT oi_UPDATED_AT,oi.ID oi_ID,o.APPROVED o_APPROVED
  ,o.ORDERED_BY o_ORDERED_BY,o.REQUIRED_DATE o_REQUIRED_DATE,o.CREATED_AT o_CREATED_AT,o.APPROVED_BY o_APPROVED_BY
  ,o.ID o_ID,o.DELETED_AT o_DELETED_AT,o.DELETED o_DELETED,o.UPDATED_BY o_UPDATED_BY,o.CHECKOUT_DATE o_CHECKOUT_DATE
  ,o.CREATED_BY o_CREATED_BY,o.CHECKOUT_BY o_CHECKOUT_BY,o.APPROVED_AT o_APPROVED_AT,o.TRANSACTION_ID o_TRANSACTION_ID
  ,o.VEHICLE_ID o_VEHICLE_ID,o.ORDER_NO o_ORDER_NO,o.ORDER_DATE o_ORDER_DATE,o.ARAHAN_KERJA_ID o_ARAHAN_KERJA_ID
  ,o.UPDATED_AT o_UPDATED_AT
  ,i.APPROVED i_APPROVED,i.APPROVED_BY i_APPROVED_BY,i.CATEGORY_ID i_CATEGORY_ID,i.CREATED_BY i_CREATED_BY
  ,i.CREATED_AT i_CREATED_AT,i.APPROVED_AT i_APPROVED_AT,i.LOCATION i_LOCATION,i.CARD_NO i_CARD_NO,i.DELETED i_DELETED
  ,i.DESCRIPTION i_DESCRIPTION,i.DELETED_AT i_DELETED_AT,i.UPDATED_BY i_UPDATED_BY,i.UPDATED_AT i_UPDATED_AT
  ,i.MIN_STOCK i_MIN_STOCK,i.CODE_NO i_CODE_NO,i.QUANTITY i_QUANTITY,i.ID i_ID,c.DELETED c_DELETED,c.NAME c_NAME
  ,c.UPDATED_BY c_UPDATED_BY,c.CREATED_AT c_CREATED_AT,c.CREATED_BY c_CREATED_BY,c.DELETED_AT c_DELETED_AT,c.ID c_ID
  ,c.UPDATED_AT c_UPDATED_AT,t.CREATED_BY t_CREATED_BY,t.UPDATED_AT t_UPDATED_AT,t.DELETED t_DELETED
  ,t.DELETED_AT t_DELETED_AT,t.UPDATED_BY t_UPDATED_BY,t.CHECK_BY t_CHECK_BY,t.ID t_ID,t.CREATED_AT t_CREATED_AT
  ,t.CHECK_DATE t_CHECK_DATE,t.TYPE t_TYPE
  from inventory_items ii
  left join order_items oi on oi.id = ii.checkout_transaction_id
  left join orders o on o.id = oi.order_id
  left join transactions t on t.id = o.transaction_id
  left join inventories i on i.id = oi.inventory_id
  left join categories c on c.id = i.category_id
  where ii.checkout_transaction_id is not null
)
--------------------------------------------------------
-- INVENTORY QUANTITY COLUMN INTEGRIRTY CHECK
--------------------------------------------------------

, i_quantity_check as (
  select * from (
    select distinct
    'int' type,
    'inventories' table_name,
    'quantity' col_name,
    i_id data_id,
    i_QUANTITY fault_value,
    count(*) over (partition by i_id) true_value
    from checkin
    where ii_CHECKOUT_TRANSACTION_ID is null
    )
  where fault_value != true_value
)
--------------------------------------------------------
-- INVENTORIES_CHECKIN ITEMS_TOTAL_PRICE COLUMN INTEGRIRTY CHECK
--------------------------------------------------------

, ic_price_check as (
  select * from (
    select distinct 
    'int' type,
    'inventories_checkin' table_name,
    'items_total_price' col_name,
    ic_id data_id,
    IC_TOTAL_PRICE fault_value,
    sum(ii_unit_price) over (partition by ic_id) true_value  
    from (
      select 
      i_id
      ,ic_id 
      ,i_quantity iv_quantity
      ,ic_ITEMS_QUANTITY ic_quantity
      ,ii_unit_price
      ,ic_ITEMS_TOTAL_PRICE ic_total_price
      from checkin
      where ii_deleted = 0 and i_deleted = 0 and ic_deleted = 0
      )
    )
  where fault_value != true_value
)
--------------------------------------------------------
-- INVENTORIES_CHECKIN ITEMS_QUANTITY COLUMN INTEGRIRTY CHECK
--------------------------------------------------------

, ic_quantity_check as (
  select *
  from (
    select distinct 
    'int' type
    ,'inventories_checkin' table_name
    ,'items_quantity' col_name
    ,ic_id data_id
    ,ic_quantity fault_value
    ,count(*) over (partition by ic_id) true_value
    from (
      select 
      i_id i_id
      ,ic_id ic_id
      ,i_quantity i_quantity
      ,ic_ITEMS_QUANTITY ic_quantity
      ,ii_unit_price
      ,ic_ITEMS_TOTAL_PRICE ic_total_price
      from checkin 
      where ii_deleted = 0 and i_deleted = 0 and ic_deleted = 0
      )
    )
  where fault_value != true_value
)
--------------------------------------------------------
-- MAINTENANCE CHECKOUT SUMMARY (NOT USED FOR MAINTENANCE) 
--------------------------------------------------------

, maintenance_check as (
  select 
  "II_PRICE","II_CHECKOUT_ID","OI_ID","IC_INVENTORY","II_BATCH_QUANTITY_CHECKOUT","OI_QUANTITY_CHECKOUT","OI_BATCH_PRICE","II_BATCH_PRICE" 
  from (
    select 
    ii_unit_price ii_price
    , ii_CHECKOUT_TRANSACTION_ID ii_checkout_id
    , oi_id oi_id
    , oi_INVENTORY_ID ic_inventory
    , COUNT(*) over (PARTITION BY ii_CHECKOUT_TRANSACTION_ID) ii_batch_quantity_checkout
    , oi_APP_QUANTITY oi_quantity_checkout
    , oi_UNIT_PRICE oi_batch_price
    , sum(ii_unit_price) over (partition by ii_CHECKOUT_TRANSACTION_ID) ii_batch_price
    from checkout
    order by ii_updated_at
    )
  where 1=1
  and ii_checkout_id != oi_id
  or oi_quantity_checkout != ii_batch_quantity_checkout
  or oi_batch_price != ii_batch_price
  and ii_checkout_id is not null
)
--------------------------------------------------------
-- INVENTORY_ITEMS CHECKOUT_TRANSACTION_ID COLUMN INTEGRIRTY CHECK (CURRENTLY NOT USED FOR MAINTENANCE)
--------------------------------------------------------

, ii_checkout_id_check as (
  select distinct
  "II_PRICE","II_CHECKOUT_ID","OI_ID","OI_INVENTORY","II_BATCH_QUANTITY_CHECKOUT","OI_QUANTITY_CHECKOUT","OI_BATCH_PRICE","II_BATCH_PRICE" 
  from (
    select 
    ii_unit_price ii_price
    , ii_CHECKOUT_TRANSACTION_ID ii_checkout_id
    , oi_id oi_id
    , oi_INVENTORY_ID oi_inventory
    , COUNT(*) over (PARTITION BY ii_CHECKOUT_TRANSACTION_ID) ii_batch_quantity_checkout
    , oi_APP_QUANTITY oi_quantity_checkout
    , oi_UNIT_PRICE oi_batch_price
    , sum(ii_unit_price) over (partition by ii_CHECKOUT_TRANSACTION_ID) ii_batch_price
    from checkout
    order by ii_updated_at
    )
  where  ii_checkout_id is not null
  and ii_checkout_id != oi_id
  or oi_quantity_checkout != ii_batch_quantity_checkout
  or oi_batch_price != ii_batch_price
)
--------------------------------------------------------
-- ORDER_ITEMS APP_QUANTITY COLUMN INTEGRIRTY CHECK
--------------------------------------------------------

,oi_app_quantity_check as (
  select distinct
  type,table_name,col_name,data_id,fault_value,true_value 
  from (
    select 
    'int' type
    ,'order_items' table_name
    ,'app_quantity' col_name
    ,oi_id data_id
    ,"OI_INVENTORY"
    ,"OI_QUANTITY_CHECKOUT" fault_value
    ,"II_BATCH_QUANTITY_CHECKOUT" true_value
    from (
      select 
      ii_unit_price ii_price, ii_CHECKOUT_TRANSACTION_ID ii_checkout_id, oi_INVENTORY_ID oi_inventory
      ,oi_id
      , COUNT(*) over (PARTITION BY ii_CHECKOUT_TRANSACTION_ID) ii_batch_quantity_checkout
      , oi_APP_QUANTITY oi_quantity_checkout
      from checkout
      order by ii_updated_at
      )
    where ii_checkout_id is not null
    )
  where fault_value != true_value
)
--------------------------------------------------------
-- ORDER_ITEMS UNIT_PRICE COLUMN INTEGRIRTY CHECK
--------------------------------------------------------

,oi_unit_price_check as (
  select distinct
  type,table_name,col_name,data_id,fault_value,true_value 
  from (
    select distinct
    'int' type
    ,'order_items' table_name
    ,'unit_price' col_name
    ,oi_id data_id
    ,"II_PRICE","II_CHECKOUT_ID","OI_INVENTORY"
    ,"OI_BATCH_PRICE" fault_value
    ,"II_BATCH_PRICE" true_value 
    from (
      select
      oi_id,ii_unit_price ii_price, ii_CHECKOUT_TRANSACTION_ID ii_checkout_id, oi_INVENTORY_ID oi_inventory
      , oi_UNIT_PRICE oi_batch_price
      , sum(ii_unit_price) over (partition by ii_CHECKOUT_TRANSACTION_ID) ii_batch_price
      from checkout
      order by ii_updated_at
      )
    where ii_checkout_id is not null
    )
  where fault_value != true_value
)
--------------------------------------------------------
-- ORDERS ORDER_NO COLUMN INTEGRITY CHECK (CURRENTLY NOT USED FOR MAINTENANCE)
--------------------------------------------------------
, o_order_no_check1 as (
  SELECT * FROM (
    SELECT 
    'text' type
    ,'orders' table_name
    ,'order_no' col_name
    ,DATA_ID
    , FAULT_VALUE
--      , CONCAT(CONCAT(CONCAT(CONCAT(YEAR,'/'),MONTH),'-'),CNT) TRUE_VALUE
, new_order_no true_value
FROM (
  SELECT 
  ID DATA_ID
  ,EXTRACT(YEAR FROM CREATED_AT) YEAR
  ,TO_CHAR(CREATED_AT,'mm') MONTH
  ,COUNT(*) OVER (PARTITION BY EXTRACT(YEAR FROM CREATED_AT),EXTRACT(MONTH FROM CREATED_AT) ORDER BY CREATED_AT) CNT
  ,concat(to_char(created_at,'yyyy/mm-'),count(*) OVER (partition by to_char(created_at,'yyyy/mm') ORDER BY created_at)) new_order_no 
  ,ORDER_NO FAULT_VALUE
  ,SUBSTR(ORDER_NO,0,4) SUBSTR
  FROM ORDERS
  )  
) 
  WHERE SUBSTR(FAULT_VALUE,0,4) = 'MPSP'
)
--------------------------------------------------------
-- ORDERS ORDER_NO COLUMN INTEGRITY CHECK 
--------------------------------------------------------

, o_order_no_check2 as (
  SELECT * FROM (
    SELECT 
    'text' type
    ,'orders' table_name
    ,'order_no' col_name
    ,DATA_ID
    , FAULT_VALUE
    , TRUE_VALUE
    FROM (
      SELECT 
      ID DATA_ID
      ,ORDER_NO FAULT_VALUE
      ,SUBSTR(ORDER_NO,6) TRUE_VALUE
      FROM ORDERS
      )  
    ) 
  WHERE SUBSTR(FAULT_VALUE,0,4) = 'MPSP'
)
--------------------------------------------------------
-- ALL COLUMN INTEGRITY CHECK
--------------------------------------------------------

, all_col_check as (
  select 
  type,table_name,col_name,data_id
  ,CAST(fault_value AS VARCHAR2(200)) fault_value
  ,CAST(true_value AS VARCHAR2(200)) true_value 
  from ic_quantity_check 
  union all
  select 
  type,table_name,col_name,data_id
  ,CAST(fault_value AS VARCHAR2(200)) fault_value
  ,CAST(true_value AS VARCHAR2(200)) true_value 
  from ic_price_check 
  union all
  select 
  type,table_name,col_name,data_id
  ,CAST(fault_value AS VARCHAR2(200)) fault_value
  ,CAST(true_value AS VARCHAR2(200)) true_value 
  from i_quantity_check
  union all
  select 
  type,table_name,col_name,data_id
  ,CAST(fault_value AS VARCHAR2(200)) fault_value
  ,CAST(true_value AS VARCHAR2(200)) true_value 
  from oi_unit_price_check
  union all
  select 
  type,table_name,col_name,data_id
  ,CAST(fault_value AS VARCHAR2(200)) fault_value
  ,CAST(true_value AS VARCHAR2(200)) true_value 
  from oi_app_quantity_check
  union all
  select 
  type,table_name,col_name,data_id
  ,CAST(fault_value AS VARCHAR2(200)) fault_value
  ,CAST(true_value AS VARCHAR2(200)) true_value 
  from o_order_no_check2
  union all
  select 
  type,table_name,col_name,data_id
  ,CAST(fault_value AS VARCHAR2(200)) fault_value
  ,CAST(true_value AS VARCHAR2(200)) true_value 
  from o_order_no_check1
)
--------------------------------------------------------
            -- SUMMARY
--------------------------------------------------------
select ROWNUM AS ID,all_col_check."TYPE",all_col_check."TABLE_NAME",all_col_check."COL_NAME",all_col_check."DATA_ID",all_col_check."FAULT_VALUE",all_col_check."TRUE_VALUE" from all_col_check
;
--------------------------------------------------------
--  DDL for View ANALYTIC
--------------------------------------------------------

  CREATE VIEW "ANALYTIC" ("YEAR", "MONTH", "I_CODE_NO", "I_CARD_NO", "I_DESCRIPTION", "FREQ", "DENSE_RANK") AS 
  with checkin as (
select 
  ii.UPDATED_BY ii_UPDATED_BY,ii.CHECKIN_TRANSACTION_ID ii_CHECKIN_TRANSACTION_ID,ii.INVENTORY_ID ii_INVENTORY_ID
  ,ii.DELETED ii_DELETED,ii.SKU ii_SKU,ii.ID ii_ID,ii.CHECKOUT_TRANSACTION_ID ii_CHECKOUT_TRANSACTION_ID
  ,ii.CREATED_BY ii_CREATED_BY,ii.CREATED_AT ii_CREATED_AT,ii.DELETED_AT ii_DELETED_AT,ii.UPDATED_AT ii_UPDATED_AT
  ,ii.UNIT_PRICE ii_UNIT_PRICE
  ,ic.UPDATED_AT ic_UPDATED_AT,ic.APPROVED_AT ic_APPROVED_AT,ic.CREATED_BY ic_CREATED_BY
  ,ic.CREATED_AT ic_CREATED_AT,ic.DELETED ic_DELETED,ic.APPROVED ic_APPROVED,ic.CHECK_BY ic_CHECK_BY
  ,ic.ITEMS_TOTAL_PRICE ic_ITEMS_TOTAL_PRICE,ic.INVENTORY_ID ic_INVENTORY_ID,ic.DELETED_AT ic_DELETED_AT
  ,ic.APPROVED_BY ic_APPROVED_BY,ic.CHECK_DATE ic_CHECK_DATE,ic.UPDATED_BY ic_UPDATED_BY
  ,ic.ITEMS_QUANTITY ic_ITEMS_QUANTITY,ic.VENDOR_ID ic_VENDOR_ID,ic.ID ic_ID,ic.TRANSACTION_ID ic_TRANSACTION_ID
  ,i.APPROVED i_APPROVED,i.APPROVED_BY i_APPROVED_BY,i.CATEGORY_ID i_CATEGORY_ID,i.CREATED_BY i_CREATED_BY
  ,i.CREATED_AT i_CREATED_AT,i.APPROVED_AT i_APPROVED_AT,i.LOCATION i_LOCATION,i.CARD_NO i_CARD_NO,i.DELETED i_DELETED
  ,i.DESCRIPTION i_DESCRIPTION,i.DELETED_AT i_DELETED_AT,i.UPDATED_BY i_UPDATED_BY,i.UPDATED_AT i_UPDATED_AT
  ,i.MIN_STOCK i_MIN_STOCK,i.CODE_NO i_CODE_NO,i.QUANTITY i_QUANTITY,i.ID i_ID,c.DELETED c_DELETED,c.NAME c_NAME
  ,c.UPDATED_BY c_UPDATED_BY,c.CREATED_AT c_CREATED_AT,c.CREATED_BY c_CREATED_BY,c.DELETED_AT c_DELETED_AT,c.ID c_ID
  ,c.UPDATED_AT c_UPDATED_AT,t.CREATED_BY t_CREATED_BY,t.UPDATED_AT t_UPDATED_AT,t.DELETED t_DELETED
  ,t.DELETED_AT t_DELETED_AT,t.UPDATED_BY t_UPDATED_BY,t.CHECK_BY t_CHECK_BY,t.ID t_ID,t.CREATED_AT t_CREATED_AT
  ,t.CHECK_DATE t_CHECK_DATE,t.TYPE t_TYPE
from inventory_items ii
left join inventories_checkin ic on ic.id = ii.checkin_transaction_id
left join transactions t on t.id = ic.transaction_id
left join inventories i on i.id = ic.inventory_id
left join categories c on c.id = i.category_id
)
, checkout as (
select 
  ii.UPDATED_BY ii_UPDATED_BY,ii.CHECKIN_TRANSACTION_ID ii_CHECKIN_TRANSACTION_ID,ii.INVENTORY_ID ii_INVENTORY_ID
  ,ii.DELETED ii_DELETED,ii.SKU ii_SKU,ii.ID ii_ID,ii.CHECKOUT_TRANSACTION_ID ii_CHECKOUT_TRANSACTION_ID
  ,ii.CREATED_BY ii_CREATED_BY,ii.CREATED_AT ii_CREATED_AT,ii.DELETED_AT ii_DELETED_AT,ii.UPDATED_AT ii_UPDATED_AT
  ,ii.UNIT_PRICE ii_UNIT_PRICE
  ,oi.UNIT_PRICE oi_UNIT_PRICE,oi.DELETED_AT oi_DELETED_AT,oi.CREATED_AT oi_CREATED_AT
  ,oi.RQ_QUANTITY oi_RQ_QUANTITY,oi.CREATED_BY oi_CREATED_BY,oi.APP_QUANTITY oi_APP_QUANTITY
  ,oi.CURRENT_BALANCE oi_CURRENT_BALANCE,oi.INVENTORY_ID oi_INVENTORY_ID,oi.UPDATED_BY oi_UPDATED_BY
  ,oi.ORDER_ID oi_ORDER_ID,oi.DELETED oi_DELETED,oi.UPDATED_AT oi_UPDATED_AT,oi.ID oi_ID,o.APPROVED o_APPROVED
  ,o.ORDERED_BY o_ORDERED_BY,o.REQUIRED_DATE o_REQUIRED_DATE,o.CREATED_AT o_CREATED_AT,o.APPROVED_BY o_APPROVED_BY
  ,o.ID o_ID,o.DELETED_AT o_DELETED_AT,o.DELETED o_DELETED,o.UPDATED_BY o_UPDATED_BY,o.CHECKOUT_DATE o_CHECKOUT_DATE
  ,o.CREATED_BY o_CREATED_BY,o.CHECKOUT_BY o_CHECKOUT_BY,o.APPROVED_AT o_APPROVED_AT,o.TRANSACTION_ID o_TRANSACTION_ID
  ,o.VEHICLE_ID o_VEHICLE_ID,o.ORDER_NO o_ORDER_NO,o.ORDER_DATE o_ORDER_DATE,o.ARAHAN_KERJA_ID o_ARAHAN_KERJA_ID
  ,o.UPDATED_AT o_UPDATED_AT
  ,i.APPROVED i_APPROVED,i.APPROVED_BY i_APPROVED_BY,i.CATEGORY_ID i_CATEGORY_ID,i.CREATED_BY i_CREATED_BY
  ,i.CREATED_AT i_CREATED_AT,i.APPROVED_AT i_APPROVED_AT,i.LOCATION i_LOCATION,i.CARD_NO i_CARD_NO,i.DELETED i_DELETED
  ,i.DESCRIPTION i_DESCRIPTION,i.DELETED_AT i_DELETED_AT,i.UPDATED_BY i_UPDATED_BY,i.UPDATED_AT i_UPDATED_AT
  ,i.MIN_STOCK i_MIN_STOCK,i.CODE_NO i_CODE_NO,i.QUANTITY i_QUANTITY,i.ID i_ID,c.DELETED c_DELETED,c.NAME c_NAME
  ,c.UPDATED_BY c_UPDATED_BY,c.CREATED_AT c_CREATED_AT,c.CREATED_BY c_CREATED_BY,c.DELETED_AT c_DELETED_AT,c.ID c_ID
  ,c.UPDATED_AT c_UPDATED_AT,t.CREATED_BY t_CREATED_BY,t.UPDATED_AT t_UPDATED_AT,t.DELETED t_DELETED
  ,t.DELETED_AT t_DELETED_AT,t.UPDATED_BY t_UPDATED_BY,t.CHECK_BY t_CHECK_BY,t.ID t_ID,t.CREATED_AT t_CREATED_AT
  ,t.CHECK_DATE t_CHECK_DATE,t.TYPE t_TYPE
from inventory_items ii
left join order_items oi on oi.id = ii.checkout_transaction_id
left join orders o on o.id = oi.order_id
left join transactions t on t.id = o.transaction_id
left join inventories i on i.id = oi.inventory_id
left join categories c on c.id = i.category_id
where ii.checkout_transaction_id is not null
)
--, freq_checkin as (
--select COUNT(*) over (partition by inventory_id) freq from checkin
--)
--, freq_checkout as (
--select COUNT(*) over (partition by inventory_id) freq from checkout
--)
--, all_check as (
--select * from freq_checkin
----union select * from checkout_u
--)
, freq_t as (
  select distinct
    EXTRACT(year FROM t_created_at) year
    ,EXTRACT(month FROM t_created_at) month
    ,i_code_no
    ,i_card_no
    ,i_description
    ,COUNT(*) over (partition by EXTRACT(month FROM t_created_at),EXTRACT(year FROM t_created_at),i_id) freq 
  --  ,rank() OVER (partition by EXTRACT(month FROM t_created_at),EXTRACT(year FROM t_created_at) ORDER BY t_created_at) rank
  from checkout
  order by year,month,freq desc
  --select * from all_check
)
select 
  freq_t."YEAR",freq_t."MONTH",freq_t."I_CODE_NO",freq_t."I_CARD_NO",freq_t."I_DESCRIPTION",freq_t."FREQ"
  ,dense_rank() over (partition by year, month order by year,month,freq desc) dense_rank
from freq_t
order by year,month,freq desc;
--------------------------------------------------------
--  DDL for View APPROVAL
--------------------------------------------------------

  CREATE VIEW "APPROVAL" ("ID", "AVAILABLE_PER_ORDER", "ID_INVENTORY_ITEMS", "ID_INVENTORIES", "ID_CATEGORIES", "ID_INVENTORIES_CHECKIN", "ID_ORDER_ITEMS", "ID_ORDERS", "SKU", "UNIT_PRICE", "ORDER_NO", "ITEMS_CATEGORY_NAME", "ITEMS_INVENTORY_NAME", "RQ_QUANTITY", "APP_QUANTITY", "APPROVED", "CURRENT_BALANCE", "TOTAL_PRICE", "ORDER_DATE", "REQUIRED_DATE") AS 
  select
  rownum as id
  ,count(*) over(partition by oi.id) available_per_order
  ,ii.id id_inventory_items
  ,i.id id_inventories
  ,c.id id_categories
  ,ic.id id_inventories_checkin
  ,oi.id id_order_items
  ,o.id id_orders
  ,ii.sku
  ,ii.UNIT_PRICE
--  ,t.TYPE
  ,o.ORDER_NO
--  ,t.CHECK_DATE transaction_date
  ,c.NAME items_category_name
  ,i.DESCRIPTION items_inventory_name
  ,oi.RQ_QUANTITY
  ,oi.APP_QUANTITY
  ,o.APPROVED
  ,oi.CURRENT_BALANCE
  ,oi.UNIT_PRICE total_price
  ,o.ORDER_DATE
  ,o.REQUIRED_DATE
from INVENTORY_ITEMS ii
left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
left join INVENTORIES i on i.id = ic.INVENTORY_ID
left join CATEGORIES c on c.id = i.CATEGORY_ID
left join ORDER_ITEMS oi on oi.inventory_id = i.id  
left join ORDERS o on o.id = oi.order_ID
--  left join vehicle_list vl on vl.id = o.vehicle_id
--  left join TRANSACTIONS t on t.id = o.TRANSACTION_ID
where ii.CHECKOUT_TRANSACTION_ID is null and ii.deleted = 0 and NVL(oi.app_quantity,0) < oi.rq_quantity;
--------------------------------------------------------
--  DDL for View IC_PRICE_CHECK
--------------------------------------------------------

  CREATE VIEW "IC_PRICE_CHECK" ("IV_ID", "IC_ID", "IC_TOTAL_PRICE", "ACTUAL_TOTAL_PRICE") AS 
  select distinct "IV_ID","IC_ID","IC_TOTAL_PRICE","ACTUAL_TOTAL_PRICE" from (
  select  
    iv_id,ic_id
    ,ic_total_price
    ,sum(unit_price) over (partition by ic_id) actual_total_price
  from (
    select 
      i.id iv_id
      ,ic.id ic_id
      ,i.quantity iv_quantity
      ,ic.ITEMS_QUANTITY ic_quantity
      ,ii.unit_price
      ,ic.ITEMS_TOTAL_PRICE ic_total_price
    from INVENTORY_ITEMS ii
    left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
    left join INVENTORIES i on i.id = ic.INVENTORY_ID
    where ii.deleted = 0 and i.deleted = 0 and ic.deleted = 0
  )
)
where ic_total_price != actual_total_price;
--------------------------------------------------------
--  DDL for View IC_QUANTITY_CHECK
--------------------------------------------------------

  CREATE VIEW "IC_QUANTITY_CHECK" ("IV_ID", "IC_ID", "IV_QUANTITY", "ACTUAL_QUANTITY") AS 
  select distinct "IV_ID","IC_ID","IV_QUANTITY","ACTUAL_QUANTITY" from (
  select
    iv_id,ic_id
    ,iv_quantity
    ,count(*) over (partition by iv_id) actual_quantity
--    ,ic_total_price
  --  ,sum(unit_price) over (partition by iv_id) actual_total_price
  from (
    select 
      i.id iv_id
      ,ic.id ic_id
      ,i.quantity iv_quantity
      ,ic.ITEMS_QUANTITY ic_quantity
      ,ii.unit_price
      ,ic.ITEMS_TOTAL_PRICE ic_total_price
    from INVENTORY_ITEMS ii
    left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
    left join INVENTORIES i on i.id = ic.INVENTORY_ID
    where ii.checkout_transaction_id is null 
    and ii.deleted = 0 and i.deleted = 0 and ic.deleted = 0
    --and ii.INVENTORY_ID = 149
  )
)
where iv_quantity != actual_quantity;
--------------------------------------------------------
--  DDL for View I_QUANTITY_CHECK
--------------------------------------------------------

  CREATE VIEW "I_QUANTITY_CHECK" ("ID", "INVENTORY_QUANTITY", "ACTUAL_INVENTORY_QUANTITY") AS 
  select "ID","INVENTORY_QUANTITY","ACTUAL_INVENTORY_QUANTITY" from (
select distinct
  i.id,
  i.QUANTITY inventory_quantity,
  count(*) over (partition by i.id) actual_inventory_quantity
from INVENTORY_ITEMS ii
left join INVENTORIES_CHECKIN iC on ic.id = ii.CHECKIN_TRANSACTION_ID
left join inventories i on i.id = ic.INVENTORY_ID
where ii.CHECKOUT_TRANSACTION_ID is null
)
where inventory_quantity != actual_inventory_quantity;
--------------------------------------------------------
--  DDL for View KEWPS_10
--------------------------------------------------------

  CREATE VIEW "KEWPS_10" ("ORDER_ID", "ORDER_REQUIRED_DATE", "ORDER_ITEM_ID", "ORDER_NO", "ORDERED_BY", "ORDER_DATE", "TRANSACTION_ID", "CARD_NO", "CODE_NO", "DESCRIPTION", "RQ_QUANTITY", "APP_QUANTITY", "CURRENT_BALANCE", "UNIT_PRICE", "AVERAGE_UNIT_PRICE", "BATCH_TOTAL_PRICE", "CREATED_DATE", "CREATED_BY", "APPROVED_BY", "APPROVED_DATE") AS 
  with data1 as (
select distinct
--select
--  rownum id
  o.id order_id
  ,ii.id
  ,o.required_date order_required_date
  ,oi.id order_item_id
  , o.order_no
  , o.ORDERED_BY
  , o.ORDER_DATE
  ,ta.id transaction_id
  , i.card_no
  , i.code_no
  , i.description
  , oi.rq_quantity
--  , count(*) over (partition by oi.id,ii.unit_price) app_quantity
  , oi.CURRENT_BALANCE
  , ii.UNIT_PRICE unit_price
--  , avg(ii.UNIT_PRICE) over (partition by oi.id,ii.unit_price) average_unit_price
--  , sum(ii.UNIT_PRICE) over (partition by oi.id,ii.unit_price) batch_total_price
  , trunc(oi.CREATED_AT) created_date
  , oi.CREATED_BY
  , o.approved_BY
  , trunc(o.approved_at) approved_date
from INVENTORY_ITEMS ii
left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
left join INVENTORIES i on i.id = ic.INVENTORY_ID
left join ORDER_ITEMS oi on oi.id = ii.checkout_transaction_id
left join ORDERS o on o.id = oi.order_ID
left join transactions t on t.id = o.TRANSACTION_ID
left join transactions_all ta on ta.id = t.id
where ii.checkout_transaction_id  is not null
-- and order_id = 1174
)
select distinct
  order_id
  , order_required_date
  , order_item_id
  , order_no
  , ORDERED_BY
  , ORDER_DATE
  , transaction_id
  , card_no
  , code_no
  , description
  , rq_quantity
  , count(*) over (partition by order_item_id,unit_price) app_quantity
  , CURRENT_BALANCE
  , unit_price
  , avg(UNIT_PRICE) over (partition by order_item_id,unit_price) average_unit_price
  , sum(UNIT_PRICE) over (partition by order_item_id,unit_price) batch_total_price
  , created_date
  , CREATED_BY
  , approved_BY
  , approved_date
from data1;
--------------------------------------------------------
--  DDL for View LAPORAN_PENYELENGARAAN_SISKEN
--------------------------------------------------------

  CREATE VIEW "LAPORAN_PENYELENGARAAN_SISKEN" ("TAHUN", "BULAN", "ID", "NO_KERJA", "ARAHAN_KERJA", "JK_JG", "BENGKEL_PANEL_ID", "NAMA_PANEL", "KATEGORI_PANEL", "SELENGGARA_ID", "TEMPOH_SELENGGARA", "ALASAN_ID", "ALASAN", "TARIKH_ARAHAN", "NO_SEBUTHARGA", "TARIKH_KENDERAAN_TIBA", "TARIKH_JANGKA_SIAP", "TARIKH_SEBUTHARGA", "STATUS_SEBUTHARGA", "NO_DO", "TARIKH_SIAP", "TEMPOH_JAMINAN", "TARIKH_DO", "TARIKH_CETAK", "STATUS", "CATATAN_SEBUTHARGA", "CATATAN", "NAMA_PIC", "TARIKH_PERMOHONAN", "ID_KENDERAAN", "KATEGORI_KEROSAKAN_ID", "KATEGORI_KEROSAKAN", "KATEGORI", "ISSERVICE", "ISPANCIT", "ODOMETER_TERKINI", "NO_PLAT", "MODEL", "KOD_JABATAN", "NAMA_JABATAN", "JUMLAH_KOS") AS 
  select "TAHUN","BULAN","ID","NO_KERJA","ARAHAN_KERJA","JK_JG","BENGKEL_PANEL_ID","NAMA_PANEL","KATEGORI_PANEL","SELENGGARA_ID","TEMPOH_SELENGGARA","ALASAN_ID","ALASAN","TARIKH_ARAHAN","NO_SEBUTHARGA","TARIKH_KENDERAAN_TIBA","TARIKH_JANGKA_SIAP","TARIKH_SEBUTHARGA","STATUS_SEBUTHARGA","NO_DO","TARIKH_SIAP","TEMPOH_JAMINAN","TARIKH_DO","TARIKH_CETAK","STATUS","CATATAN_SEBUTHARGA","CATATAN","NAMA_PIC","TARIKH_PERMOHONAN","ID_KENDERAAN","KATEGORI_KEROSAKAN_ID","KATEGORI_KEROSAKAN","KATEGORI","ISSERVICE","ISPANCIT","ODOMETER_TERKINI","NO_PLAT","MODEL","KOD_JABATAN","NAMA_JABATAN","JUMLAH_KOS" from usersisken.LAPORAN_PENYELENGGARAAN_ESTOR;
--------------------------------------------------------
--  DDL for View LAPORAN_PENYELENGGARAAN_VIEW
--------------------------------------------------------

  CREATE VIEW "LAPORAN_PENYELENGGARAAN_VIEW" ("TAHUN", "BULAN", "ID", "NO_KERJA", "ARAHAN_KERJA", "JK_JG", "BENGKEL_PANEL_ID", "NAMA_PANEL", "KATEGORI_PANEL", "SELENGGARA_ID", "TEMPOH_SELENGGARA", "ALASAN_ID", "ALASAN", "TARIKH_ARAHAN", "NO_SEBUTHARGA", "TARIKH_KENDERAAN_TIBA", "TARIKH_JANGKA_SIAP", "TARIKH_SEBUTHARGA", "STATUS_SEBUTHARGA", "NO_DO", "TARIKH_SIAP", "TEMPOH_JAMINAN", "TARIKH_DO", "TARIKH_CETAK", "STATUS", "CATATAN_SEBUTHARGA", "CATATAN", "NAMA_PIC", "TARIKH_PERMOHONAN", "ID_KENDERAAN", "KATEGORI_KEROSAKAN_ID", "KATEGORI_KEROSAKAN", "KATEGORI", "ISSERVICE", "ISPANCIT", "ODOMETER_TERKINI", "NO_PLAT", "MODEL", "KOD_JABATAN", "NAMA_JABATAN", "JUMLAH_KOS") AS 
  SELECT "TAHUN","BULAN","ID","NO_KERJA","ARAHAN_KERJA","JK_JG","BENGKEL_PANEL_ID","NAMA_PANEL","KATEGORI_PANEL","SELENGGARA_ID","TEMPOH_SELENGGARA","ALASAN_ID","ALASAN","TARIKH_ARAHAN","NO_SEBUTHARGA","TARIKH_KENDERAAN_TIBA","TARIKH_JANGKA_SIAP","TARIKH_SEBUTHARGA","STATUS_SEBUTHARGA","NO_DO","TARIKH_SIAP","TEMPOH_JAMINAN","TARIKH_DO","TARIKH_CETAK","STATUS","CATATAN_SEBUTHARGA","CATATAN","NAMA_PIC","TARIKH_PERMOHONAN","ID_KENDERAAN","KATEGORI_KEROSAKAN_ID","KATEGORI_KEROSAKAN","KATEGORI","ISSERVICE","ISPANCIT","ODOMETER_TERKINI","NO_PLAT","MODEL","KOD_JABATAN","NAMA_JABATAN","JUMLAH_KOS" FROM ESTOR.LAPORAN_PENYELENGGARAAN_VIEW;
--------------------------------------------------------
--  DDL for View LIST_YEAR
--------------------------------------------------------

  CREATE VIEW "LIST_YEAR" ("YEAR", "QUARTER") AS 
  select year,quarter from REPORT_IN_QUARTER union 
select year,quarter from REPORT_OUT_QUARTER;
--------------------------------------------------------
--  DDL for View MAINTENANCE_CHECK
--------------------------------------------------------

  CREATE VIEW "MAINTENANCE_CHECK" ("II_PRICE", "II_CHECKOUT_ID", "OI_ID", "IC_INVENTORY", "II_BATCH_QUANTITY_CHECKOUT", "OI_QUANTITY_CHECKOUT", "OI_BATCH_PRICE", "II_BATCH_PRICE") AS 
  select "II_PRICE","II_CHECKOUT_ID","OI_ID","IC_INVENTORY","II_BATCH_QUANTITY_CHECKOUT","OI_QUANTITY_CHECKOUT","OI_BATCH_PRICE","II_BATCH_PRICE" from (
  select -- distinct
    ii.unit_price ii_price
  --  , ii.CHECKIN_TRANSACTION_ID item_checkin
  --  check oi_id
    , ii.CHECKOUT_TRANSACTION_ID ii_checkout_id
    , oi.id oi_id
--    , ii.INVENTORY_ID ii_inventory
    , ic.INVENTORY_ID ic_inventory
--    check oi_app_quantity
    , COUNT(*) over (PARTITION BY ii.CHECKOUT_TRANSACTION_ID) ii_batch_quantity_checkout
    , oi.APP_QUANTITY oi_quantity_checkout
  --  check oi_unit_price
    , oi.UNIT_PRICE oi_batch_price
    , sum(ii.unit_price) over (partition by ii.CHECKOUT_TRANSACTION_ID) ii_batch_price
  --  check unit_price*items
--    , sum(ii.unit_price) over (partition by ii.CHECKOUT_TRANSACTION_ID,ii.unit_price) ii_batch_price_by_price
  --  check oi_current_balance
--    , oi.CURRENT_BALANCE oi_current_balance
--    , count(*) over (partition by i.id ORDER BY ii.created_at desc rows UNBOUNDED PRECEDING) ii_current_balance
    
  from inventory_items ii
  left join ORDER_ITEMS oi on oi.id = ii.checkout_transaction_id  
  left join ORDERS o on o.id = oi.order_ID
  left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
  left join inventories i on i.id = ic.inventory_id
order by ii.updated_at
)
where 1=1
--  and ic_inventory = 21
  and ii_checkout_id != oi_id
  or oi_quantity_checkout != ii_batch_quantity_checkout
  or oi_batch_price != ii_batch_price;
--------------------------------------------------------
--  DDL for View MPSP_STAFF
--------------------------------------------------------

  CREATE VIEW "MPSP_STAFF" ("NO_PEKERJA", "NAMA", "JAWATAN", "KETERANGAN") AS 
  SELECT "NO_PEKERJA","NAMA","JAWATAN","KETERANGAN" from payroll.paymas_estor_view;
--------------------------------------------------------
--  DDL for View OI_APP_QUANTITY_CHECK
--------------------------------------------------------

  CREATE VIEW "OI_APP_QUANTITY_CHECK" ("ID", "APP_QUANTITY", "ACTUAL_APP_QUANTITY") AS 
  select "ID","APP_QUANTITY","ACTUAL_APP_QUANTITY" from (
select distinct
  oi.id,app_quantity, count(ii.id) over (partition by oi.id) actual_app_quantity
from order_items oi
left join inventory_items ii on oi.id = ii.CHECKOUT_TRANSACTION_ID
)
where app_quantity != actual_app_quantity;
--------------------------------------------------------
--  DDL for View REPORT_ALL
--------------------------------------------------------

  CREATE VIEW "REPORT_ALL" ("ID", "YEAR", "QUARTER", "COUNT_IN", "PRICE_IN", "COUNT_OUT", "PRICE_OUT", "COUNT_CURRENT", "PRICE_CURRENT") AS 
  select
    rownum as id,
    list_year.YEAR,
    list_year.QUARTER,
    NVL(REPORT_IN_QUARTER.COUNT, '0') count_in,
    NVL(REPORT_IN_QUARTER.TOTAL_PRICE, '0') price_in,
    NVL(REPORT_out_QUARTER.COUNT, '0') count_out,
    NVL(REPORT_out_QUARTER.TOTAL_PRICE, '0') price_out,

      sum(NVL(REPORT_IN_QUARTER.COUNT, '0')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING) - 
      sum(NVL(REPORT_OUT_QUARTER.COUNT, '0')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING)
    count_current,
      sum(NVL(REPORT_in_QUARTER.TOTAL_PRICE, '0')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING) - 
      sum(NVL(REPORT_out_QUARTER.TOTAL_PRICE, '0')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING)
    price_current

from list_year
left join REPORT_IN_QUARTER 
on (REPORT_IN_QUARTER.YEAR = list_year.YEAR and list_year.QUARTER = REPORT_IN_QUARTER.QUARTER)
left join REPORT_OUT_QUARTER 
on (list_year.YEAR = REPORT_OUT_QUARTER.YEAR and list_year.QUARTER = REPORT_OUT_QUARTER.QUARTER)
order by list_year.YEAR, list_year.QUARTER;
--------------------------------------------------------
--  DDL for View REPORT_IN_QUARTER
--------------------------------------------------------

  CREATE VIEW "REPORT_IN_QUARTER" ("YEAR", "QUARTER", "COUNT", "TOTAL_PRICE") AS 
  WITH DATA1 AS (
select 
  ii.sku
  ,ii.unit_price
  ,ic.inventory_id
  ,t.check_date
  ,extract(year from t.check_date) year
  ,to_char(t.check_date,'Q') quarter 
from inventory_items ii
left join inventories_checkin ic on ic.id = ii.checkin_transaction_id
left join transactions t on t.id = ic.transaction_id
where ii.deleted = 0
)

SELECT 
  DISTINCT YEAR,
--  rownum as id,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM DATA1
ORDER BY YEAR,QUARTER;
--------------------------------------------------------
--  DDL for View REPORT_OUT_QUARTER
--------------------------------------------------------

  CREATE VIEW "REPORT_OUT_QUARTER" ("YEAR", "QUARTER", "COUNT", "TOTAL_PRICE") AS 
  WITH DATA1 AS (
select 
  ii.sku
  ,ii.unit_price
  ,oi.inventory_id
  ,t.check_date
  ,extract(year from t.check_date) year
  ,to_char(t.check_date,'Q') quarter 
from inventory_items ii
left join order_items oi on oi.id = ii.checkout_transaction_id
left join orders o on o.id = oi.order_id
left join transactions t on t.id = o.transaction_id
where ii.deleted = 0 and ii.checkout_transaction_id is not null
)

SELECT 
  DISTINCT YEAR,
--  rownum as id,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM DATA1
ORDER BY YEAR,QUARTER;
--------------------------------------------------------
--  DDL for View TEMP_CHECKOUT_PRICE_QUANTITY
--------------------------------------------------------

  CREATE VIEW "TEMP_CHECKOUT_PRICE_QUANTITY" ("CHECKIN_TRANSACTION_ID", "CHECKOUT_TRANSACTION_ID", "QUANTITY_BY_PART", "UNIT_PRICE", "PRICE_BY_PART", "QUANTITY_TOTAL", "TOTAL_PRICE") AS 
  select distinct
  CHECKIN_TRANSACTION_ID
  ,CHECKOUT_TRANSACTION_ID
  ,count(*) over (partition by unit_price) quantity_by_part
  ,unit_price
  ,sum(unit_price) over (partition by CHECKIN_TRANSACTION_ID) price_by_part
  ,count(*) over (partition by CHECKOUT_TRANSACTION_ID) quantity_total
  ,sum(unit_price) over (partition by CHECKOUT_TRANSACTION_ID) total_price
from INVENTORY_ITEMS where DELETED = 0;
--------------------------------------------------------
--  DDL for View TRANSACTIONS_ALL
--------------------------------------------------------

  CREATE VIEW "TRANSACTIONS_ALL" ("ID", "TYPE", "CHECK_DATE", "APPROVED_DATE", "CHECK_BY", "REFFERENCE", "INVENTORY_ID", "UNIT_PRICE", "COUNT_IN", "PRICE_IN", "COUNT_OUT", "PRICE_OUT", "COUNT_CURRENT", "PRICE_CURRENT") AS 
  select 
  t."ID",t."TYPE",t."CHECK_DATE",trunc(t."APPROVED_AT") approved_date,t."CHECK_BY",t."REFFERENCE",t."INVENTORY_ID",t."UNIT_PRICE",t."COUNT_IN",t."PRICE_IN",t."COUNT_OUT",t."PRICE_OUT",
  sum(COUNT_IN - COUNT_OUT) over(PARTITION BY INVENTORY_ID ORDER BY APPROVED_AT,ID ROWS UNBOUNDED PRECEDING) count_current,
  sum(PRICE_IN - PRICE_OUT) over(PARTITION BY INVENTORY_ID ORDER BY APPROVED_AT,ID ROWS UNBOUNDED PRECEDING) price_current
from (
    select
    --    rownum as id,
        IDS.ID,
        IDS.TYPE,
        IDS.CHECK_DATE,
        IDS.APPROVED_AT,
        IDS.CHECK_BY,
        IDS.REFFERENCE,
        IDS.INVENTORY_ID,
        IDS.UNIT_PRICE,
        case when IDS.TYPE = 1 then IDS.COUNT else 0 end count_in,
        case when IDS.TYPE = 1 then IDS.TOTAL_PRICE else 0 end price_in,
        case when IDS.TYPE = 2 then IDS.COUNT else 0 end count_out,
        case when IDS.TYPE = 2 then IDS.TOTAL_PRICE else 0 end price_out
    from (select * from TRANSACTION_OUT union 
    select * from TRANSACTION_IN
    ) IDS
    order by IDS.APPROVED_AT
) t;
--------------------------------------------------------
--  DDL for View TRANSACTION_IN
--------------------------------------------------------

  CREATE VIEW "TRANSACTION_IN" ("ID", "TYPE", "CHECK_DATE", "CHECK_BY", "REFFERENCE", "APPROVED_AT", "APPROVED_BY", "INVENTORY_ID", "UNIT_PRICE", "COUNT", "TOTAL_PRICE") AS 
  WITH DATA1 AS (
select 
  t.id
  ,t.type
  ,ii.sku
  ,ii.unit_price
  ,ic.inventory_id
  ,v.name vendor_name
  ,t.check_date
  ,t.check_by
  ,ic.approved_at
  ,ic.approved_by
  ,extract(year from t.check_date) year
  ,to_char(t.check_date,'Q') quarter 
  ,COUNT(*) OVER (PARTITION BY t.ID) COUNT
  ,SUM(ii.UNIT_PRICE) OVER (PARTITION BY t.ID) TOTAL_PRICE
from inventory_items ii
left join inventories_checkin ic on ic.id = ii.checkin_transaction_id
left join transactions t on t.id = ic.transaction_id
left join vendors v on v.id = ic.vendor_id
where ii.deleted = 0
)

SELECT 
  ID,
  TYPE,  
  CHECK_DATE,  
  CHECK_BY,  
  VENDOR_NAME REFFERENCE,
  APPROVED_AT,  
  APPROVED_BY,  
  INVENTORY_ID,
--  rownum as id,
--  QUARTER,
  UNIT_PRICE,
  DATA1.COUNT,
  TOTAL_PRICE
FROM DATA1
group by id,count,unit_price,total_price, type, check_date, check_by, approved_at, approved_by, inventory_id, vendor_name
ORDER BY id;
--------------------------------------------------------
--  DDL for View TRANSACTION_OUT
--------------------------------------------------------

  CREATE VIEW "TRANSACTION_OUT" ("ID", "TYPE", "CHECK_DATE", "CHECK_BY", "REFFERENCE", "APPROVED_AT", "APPROVED_BY", "INVENTORY_ID", "UNIT_PRICE", "COUNT", "TOTAL_PRICE") AS 
  WITH DATA1 AS (
select distinct
  t.id
  ,o.id o_id
  ,t.type
--  ,LISTAGG(ii.sku, ', ') WITHIN GROUP (ORDER BY ii.id) items
--  ,ii.sku
  ,ii.unit_price
  ,oi.inventory_id
  ,t.check_date
  ,t.check_by
  ,o.approved_at
  ,o.order_no
  ,o.approved_by
  ,extract(year from t.check_date) year
  ,to_char(t.check_date,'Q') quarter 
  ,COUNT(*) OVER (PARTITION BY t.ID,oi.id,ii.UNIT_PRICE) COUNT
  ,SUM(ii.UNIT_PRICE) OVER (PARTITION BY t.ID,oi.id,ii.UNIT_PRICE) TOTAL_PRICE
from inventory_items ii
left join order_items oi on oi.id = ii.checkout_transaction_id
left join orders o on o.id = oi.order_id
left join transactions t on t.id = o.transaction_id
--where ii.deleted = 0 and ii.checkout_transaction_id is not null
  where ii.deleted = 0 and ii.checkout_transaction_id is not null and o.APPROVED = 1
  order by o.id
)

SELECT 
--  *
  ID,
  TYPE,  
  CHECK_DATE,  
  CHECK_BY,  
  ORDER_NO REFFERENCE,  
  APPROVED_AT,  
  APPROVED_BY,  
  INVENTORY_ID,
--  rownum as id,
--  QUARTER,
  UNIT_PRICE,
  DATA1.COUNT,
  TOTAL_PRICE
FROM DATA1
group by id,count,unit_price,total_price, type, check_date, check_by, approved_at, approved_by, inventory_id,order_no
--group by 
ORDER BY id;
--------------------------------------------------------
--  DDL for View TRANSACTION_OUT_SISKEN
--------------------------------------------------------

  CREATE VIEW "TRANSACTION_OUT_SISKEN" ("ID", "ARAHAN_KERJA_ID", "ORDER_NO", "CHECK_DATE", "STATUS", "ORDER_ITEMS_ID", "CATEGORY_NAME", "CARD_NO", "CODE_NO", "INVENTORY_DESCRIPTION", "ITEMS", "UNIT_PRICE", "BATCH_QUANTITY", "BATCH_TOTAL_PRICE", "TOTAL_QUANTITY", "TOTAL_PRICE") AS 
  select 
  rownum as id
  ,t."ARAHAN_KERJA_ID",t."ORDER_NO",t."CHECK_DATE",t."STATUS",t."ORDER_ITEMS_ID",t."CATEGORY_NAME",t."CARD_NO",t."CODE_NO",t."INVENTORY_DESCRIPTION",t."ITEMS",t."UNIT_PRICE",t."BATCH_QUANTITY",t."BATCH_TOTAL_PRICE",t."TOTAL_QUANTITY",t."TOTAL_PRICE" from
(
select  
    arahan_kerja_id
    ,order_no
    ,check_date
    ,status
    ,order_items_id
    ,category_name
    ,card_no
    ,code_no
    ,inventory_description
    , LISTAGG(sku, ', ') WITHIN GROUP (ORDER BY sku) items
    ,unit_price
    ,batch_quantity
    ,batch_total_price
    ,total_quantity
    ,total_price
from
  (
  select
    o.arahan_kerja_id ARAHAN_KERJA_id
    ,o.order_no
    ,to_char(o.approved_at,'dd-MON-yy') check_date
    ,o.approved status
    ,oi.id order_items_id
    ,c.name category_name
    ,i.card_no
    ,i.code_no
    ,i.description inventory_description
    ,ii.sku
    ,ii.unit_price
    ,count(*) over (partition by oi.id,ii.unit_price) batch_quantity
    ,sum(ii.unit_price) over (partition by oi.id,ii.unit_price) batch_total_price
    ,count(*) over (partition by o.order_no) total_quantity
    ,sum(ii.unit_price) over (partition by o.order_no) total_price
  from inventory_items ii
  left join order_items oi on oi.id = ii.checkout_transaction_id
  left join orders o on o.id = oi.order_id
  left join transactions t on t.id = o.transaction_id
  left join inventories i on i.id = oi.inventory_id
  left join categories c on c.id = i.category_id
  where ii.checkout_transaction_id is not null 
  and o.arahan_kerja_id is not null
  order by o.order_no desc
  )
group by 
    arahan_kerja_id
    ,order_no
    ,check_date
    ,status
    ,order_items_id
    ,category_name
    ,card_no
    ,code_no
    ,inventory_description
--    ,sku
    , unit_price
    , batch_quantity
    , batch_total_price
    , total_quantity
    , total_price
order by order_no desc,order_items_id desc
) t;
--------------------------------------------------------
--  DDL for View VEHICLE_REPORT
--------------------------------------------------------

  CREATE VIEW "VEHICLE_REPORT" ("REG_NO", "SUM_USAGE") AS 
  select 
--  rownum ID
  vl.REG_NO
--  ,vl.REG_NO
--  ,o.ORDER_NO order_no
--  ,oi.UNIT_PRICE
  ,sum(NVL(oi.UNIT_PRICE, '0')) sum_usage
--  ,sum(NVL(oi.UNIT_PRICE, '0')) over (GROUP BY vl.REG_NO) sum
--  ,sum(NVL(oi.UNIT_PRICE, '0')) over (group by vl.REG_NO) sum1
from VEHICLE_LIST vl
left join orders o on o.VEHICLE_ID = vl.id
left join ORDER_ITEMS oi on oi.order_id = o.id
GROUP BY vl.REG_NO;
--------------------------------------------------------
--  DDL for View VENDORS_REPORT
--------------------------------------------------------

  CREATE VIEW "VENDORS_REPORT" ("ID", "NAME", "TOTAL") AS 
  select rownum as id, t."NAME",t."TOTAL" from (
select 
  v.NAME
  ,SUM (ic.ITEMS_TOTAL_PRICE) total
--  ,vl.REG_NO
--  ,o.ORDER_NO order_no
--  ,oi.UNIT_PRICE
--  ,sum(NVL(oi.UNIT_PRICE, '0')) sum_usage
--  ,sum(NVL(oi.UNIT_PRICE, '0')) over (GROUP BY vl.REG_NO) sum
--  ,sum(NVL(oi.UNIT_PRICE, '0')) over (group by vl.REG_NO) sum1
from VENDORS v
left join inventories_checkin ic on ic.VENDOR_ID = v.id
GROUP BY v.NAME
) t;
